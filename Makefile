PY?         =   python
SPHINXBUILD =   sphinx-build
DCK	        =   docker
PYCS        =   pycodestyle

BASEDIR     =   $(CURDIR)
DOCDIR      =   ${BASEDIR}/documentation
BUILDDIR    =   ${DOCDIR}/_build
MCSDIR      =   ${BASEDIR}/tusemcs
TESTDIR     =   ${MCSDIR}/tests
USERID      =   $(shell id -u)
DCKFILE_HEAD = ${BASEDIR}/docker/Dockerfile.head
DCKFILE_NODE = ${BASEDIR}/docker/Dockerfile.node
IMGNAME_HEAD = tcs-head
IMGNAME_NODE = tcs-node
IMGTAG      ?= latest

# GLORIOUS hack to autogenerate Makefile help
# This simply parses the double hashtags that follow each Makefile command
# https://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
help: ## Print this help message
	@echo "Makefile help for tusemcs"
	@echo "========================================================================="
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-20s\033[0m %s\n", $$1, $$2}'

install: ## Install the tusemcs module in development mode
	pip install -r requirements.in -e .

images: ## Build docker images
	${DCK} build --file ${DCKFILE_HEAD} --tag ${IMGNAME_HEAD}:latest --tag ${IMGNAME_HEAD}:${IMGTAG} ${BASEDIR}
	${DCK} build --file ${DCKFILE_NODE} --tag ${IMGNAME_NODE}:latest --tag ${IMGNAME_NODE}:${IMGTAG} ${BASEDIR}

documentation: ## produce documentation
	mkdir -p ${BUILDDIR} && \
	${DCK} run -it --rm \
	--mount "type=bind,source=${BUILDDIR},target=/build" \
	--user ${USERID} ${IMGNAME} \
	${SPHINXBUILD} -M dirhtml documentation /build

stylecheck: ## check whether the code conforms to good coding style
	cd ${MCSDIR} && ${PYCS} *.py

clean: ## remove temporary files
	rm -rf ${BUILDDIR}
	rm -f ${MCSDIR}/*.pyc ${TESTDIR}/*.pyc ${MCSDIR}/.coverage

.PHONY: help install images documentation stylecheck clean
