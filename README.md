# TUSE Master Control System #

## Installation ##

Clone the repository and run ``make install``. This installs ``tusemcs`` in editable mode and all its requirements with pip, with the command ``pip install -r requirements.in -e .``

## Requirements ##

The latest list of required python modules can be found in the requirements file ``requirements.in``

* [Python](https://www.python.org/) (2.x >= 2.7)
* [coloredlogs](https://coloredlogs.readthedocs.io/)
* [enum34](https://bitbucket.org/stoneleaf/enum34/)
* [jinja2](http://jinja.pocoo.org/)
* [katcp](https://github.com/ska-sa/katcp-python/)
* [katportalclient](https://github.com/ska-sa/katportalclient/)
* [katpoint](https://github.com/ska-sa/katpoint)
* [psutil](https://github.com/giampaolo/psutil/)
* [subprocess32](https://github.com/google/python-subprocess32/)
* [tornado](http://www.tornadoweb.org/)

For code development you may also require the following additional software, which is also specified in the requirements file:

* [lxml](https://lxml.de/)
* [nose](https://nose.readthedocs.io/)
* [nose2](https://github.com/nose-devs/nose2/)
* [numpy](http://www.numpy.org/)
* [pycodestyle](https://github.com/PyCQA/pycodestyle/)
* [pylint](https://www.pylint.org/)
* [recommonmark](https://github.com/rtfd/recommonmark/)
* [sphinx](http://www.sphinx-doc.org/)

All of these are included in the provided docker file or can be easily
installed via `pip`.

## Documentation ##

An API reference is available on the following website: [online
documentation](http://astronomy.swin.edu.au/~fjankowski/docs/tusemcs/).
