import os
import setuptools
import versioneer
from setuptools import setup

def get_long_description():
    with open("README.md", "r") as fh:
        long_description = fh.read()
    return long_description


setup(
    name='tusemcs',
    url='https://bitbucket.org/vmorello/tusemcs',
    author='Vincent Morello, Fabian Jankowski and the MeerTRAP team',
    author_email='vmorello@gmail.com',
    description='High-level control system for the MeerTRAP data processing cluster (a.k.a. TUSE)',
    long_description=get_long_description(),
    long_description_content_type='text/markdown',
    packages=setuptools.find_packages(),
    version=versioneer.get_version(),
    cmdclass=versioneer.get_cmdclass(),

    # NOTE: dependencies currently specified and installed via requirements.in
    install_requires=[],

    entry_points = {
        'console_scripts': [
            'master-controller=tusemcs.master_controller:main',
            'node-controller=tusemcs.node_controller:main',
            'configuration-authority=tusemcs.ca.configuration_authority:main',
            'web-monitor=tusemcs.suricat.suricat:main',

            'node-manager=apps.node_manager:main',
            'katcp-client=apps.katcp_cli:main',
            'reqsender=apps.reqsender:main',
            'bandpass-writer=apps.bandpass_writer:main'
        ],
    },

    license='MIT License',
    classifiers=[
        "Programming Language :: Python :: 2.7",
        "License :: OSI Approved :: MIT License",
        "Operating System :: Unix",
        "Topic :: Scientific/Engineering :: Astronomy"
        ],
)
