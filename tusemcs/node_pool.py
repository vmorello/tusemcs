import logging
import re
import collections
import json

from tornado import gen
from katcp.resource_client import KATCPClientResource, KATCPReply
from katcp import Message


log = logging.getLogger('tusemcs.node_pool')


class NodeNotSynchronised(Exception):
    """ 
    Custom exception raised when attempting to issue a request to a node using a KATCP client
    not yet synced with it.
    """
    pass


def format_katcp_reply(kr):
    """
    Converts a KATCPReply object to a nice human readable string with all escape characters
    removed. Somehow there is no one-liner, or proper __str__ or __repr__ method available in the 
    katcp library to do this.
    
    Parameters
    ----------
    kr : KATCPReply
        KATCPReply object returned when sending a request via a KATCPClientResource

    Returns
    -------
    formatted: str
    """
    m = kr.reply # kcr.reply is a Message object
    # m.arguments is a tuple containing the message arguments (i.e. body) with the escape 
    # characters removed.
    return "{}{} {}".format(Message.TYPE_SYMBOLS[m.mtype], m.name, ' '.join(m.arguments))


class NodeInterface(object):
    """
    Interface to a remote NodeController

    Parameters
    ----------
    number : int
    host : str
    port : int
    vlan : int
    """
    def __init__(self, number, host, port, vlan):
        self.number = int(number)
        self.host = str(host)
        self.port = int(port)
        self.vlan = int(vlan)
        self.started = False

        self.client = KATCPClientResource(dict(
            name='node-interface',
            address=(self.host, self.port),
            controlled=True, # Expose requests
            auto_reconnect=True,
            auto_reconnect_delay=5.0
        ))

    @property
    def synced(self):
        return self.client.synced

    def start_client(self):
        log.debug("Starting NodeInterface client to {}:{}".format(self.host, self.port))
        self.client.start()
        self.started = True

    def __del__(self):
        if self.started:
            try:
                self.client.stop()
            except Exception as err:
                log.exception(err)

    @gen.coroutine
    def issue_request(self, req_name, *args, **kwargs):
        """
        Issue the request with given name to the node. Returns a KATCPReply object.
        """
        if not self.synced:
            raise NodeNotSynchronised()
        func = getattr(self.client.req, req_name)
        reply = yield func(*args, **kwargs)
        raise gen.Return(reply)

    def __repr__(self):
        return "NodeInterface(number={s.number}, vlan={s.vlan}, address={s.host}:{s.port})".format(s=self)


class NodePool(object):
    """
    Tracks state and sensors of all NodeControllers and forwards CAM requests to them
    """
    def __init__(self):
        self.nodes_dict = {} # dict {node_number (int): NodeInterface}

    @property
    def nodes(self):
        return [self.nodes_dict[n] for n in self.node_numbers]

    @property
    def node_numbers(self):
        return sorted(list(self.nodes_dict.keys()))

    @property
    def node_addresses(self):
        return [(node.host, node.port) for node in self.nodes]

    @property
    def size(self):
        return len(self.nodes_dict)

    @property
    def synced_nodes(self):
        return list(filter(lambda n: n.synced, self.nodes))

    def get_node_by_number(self, n):
        return self.nodes_dict[n]

    def get_nodes_by_numbers(self, numbers):
        valid_numbers = set(numbers).intersection(set(self.node_numbers))
        valid_numbers = sorted(list(valid_numbers))
        return [self.get_node_by_number(n) for n in valid_numbers]

    def add_node(self, number, host, port, vlan):
        """ 
        Add a node to the pool. Both the node number and address (host, port) must be distinct
        from that of other nodes. 
        """
        if number in self.nodes_dict:
            log.warning("Node number {} is already assigned to a node in the pool".format(number))
        elif (host, port) in self.node_addresses:
            log.warning("A Node with adress {}:{} already belongs to the pool".format(host, port))
        else:
            node = NodeInterface(number, host, port, vlan)
            self.nodes_dict[number] = node
            log.debug("{!r} added to the pool".format(node))

    def start_clients(self):
        for node in self.nodes:
            node.start_client()

    @gen.coroutine
    def issue_request_all(self, req_name, *args, **kwargs):
        """
        Issue request to all nodes. See issue_request().
        """
        result = yield self.issue_request(req_name, self.node_numbers, *args, **kwargs)
        raise gen.Return(result)

    @gen.coroutine
    def issue_request(self, req_name, node_numbers, *args, **kwargs):
        """
        Issue a request to specific node numbers in the pool. Nodes that are not synced are 
        ignored.

        Parameters
        ----------
        req_name : str
        node_numbers : iterable
        *args : Positional arguments passed to the request
        **kwargs : Keyword arguments passed to the request

        Returns
        -------
        fail_numbers : list of int
            List of node numbers that replied 'fail'
        counter : collections.Counter
            dict-like object where keys are message strings, and values the number of nodes that
            sent that exact reply.
        """
        futures = {
            node.number : node.issue_request(req_name, *args, **kwargs) 
            for node in self.get_nodes_by_numbers(node_numbers)
        }
        log.info("Issued request {!r} to {} nodes".format(req_name, len(futures)))

        replies = {}
        for n, f in futures.items():
            try:
                r = yield f
                replies[n] = r
                msg = "node{:02d} replied: {}".format(n, format_katcp_reply(r))
                if r.reply.reply_ok():
                    log.debug(msg)
                else:
                    log.warning(msg)
            except NodeNotSynchronised:
                log.warning("node{:02d} is NOT synchronised, no request sent".format(n))

        fail_numbers = [n for n, r in replies.items() if not r.reply.reply_ok()]
        counter = collections.Counter([format_katcp_reply(r) for r in replies.values()])
        raise gen.Return((fail_numbers, counter))

    @gen.coroutine
    def issue_capture_start(self, sb_details, product_meta, beam_assignment_plan):
        """
        Issue capture-start to nodes. Capture-start must be treated differently, because all nodes 
        get different request arguments.

        Parameters
        ----------
        sb_details : dict
            Dictionary containing the schedule block details
        product_meta : dict
            Dictionary containing the product metadata
        beam_assignment_plan : dict
            A nested dictionary of the form
            {node_number (int): {mc_address (str) : node_ids (list of str)}}

        Returns
        -------
        fail_numbers : list of int
            List of node numbers that replied 'fail'
        counter : collections.Counter
            dict-like object where keys are message strings, and values the number of nodes that
            sent that exact reply.
        """
        futures = {
            n : self.get_node_by_number(n).issue_request(
                'capture_start', 
                json.dumps(sb_details), 
                json.dumps(product_meta), 
                json.dumps(beam_assignment_plan[n])
            )
            for n in sorted(beam_assignment_plan.keys())
        }
        log.info("Issued capture-start to {} nodes".format(len(futures)))

        replies = {}
        for n, f in futures.items():
            try:
                r = yield f
                replies[n] = r
                msg = "node{:02d} replied: {}".format(n, format_katcp_reply(r))
                if r.reply.reply_ok():
                    log.debug(msg)
                else:
                    log.warning(msg)
            except NodeNotSynchronised:
                log.warning("node{:02d} is NOT synchronised, no request sent".format(n))

        fail_numbers = [n for n, r in replies.items() if not r.reply.reply_ok()]
        counter = collections.Counter([format_katcp_reply(r) for r in replies.values()])
        raise gen.Return((fail_numbers, counter))

    @classmethod
    def from_node_specs(cls, specs):
        """ 
        Create a NodePool from a list of node specs. Nodes with hostname 'tpn-0-N' are always
        assigned number N, but it's possible to use other hostnames for test purposes.

        Parameters
        ----------
        specs : list of dicts
            List of dictionaries with keys 'host', 'vlan', 'port'. 

        Returns
        -------
        pool : NodePool
        """
        def parse_tpn_number(host):
            m = re.match(r'tpn-0-(\d+).*', host)
            return int(m.group(1)) if m else None

        pool = cls()
        for items in specs:
            host = items['host']
            port = items['port']
            vlan = items['vlan']

            # Always assign node number N to tpn-0-N
            tpn_number = parse_tpn_number(host)
            n = tpn_number if tpn_number is not None else pool.size
            pool.add_node(n, host, port, vlan)
        return pool
