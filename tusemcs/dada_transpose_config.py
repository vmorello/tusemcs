#
# DADA buffers and transpose module configuration calculator
# Vincent Morello - 2020
#
import logging
import math

import copy
from schema import Schema, And

from beam import validate_beams
from pipeline_utils import (
    validate_pipeline_mode, DotDict, output_dada_key
)


log = logging.getLogger('tusemcs.dada_config')


SCHEMA_INTGT0 = Schema(And(int, lambda n: n > 0), error='Strictly positive int required')
SCHEMA_FLOATGT0 = Schema(And(float, lambda n: n > 0), error='Strictly positive float required')


def multceil(n, b):
    """
    Returns the smallest integer >= n that is a multiple of b
    """
    return int(math.ceil(n / float(b)) * b)


def pow2ceil(n):
    """
    Returns the smallest power of two >= n
    """
    return 2 ** (n - 1).bit_length()


def get_tscrunch_extra(tsamp_input, tsamp_output_min, pow2=False):
    """
    Get the tscrunch factor necessary for the sampling time to exceed
    tsamp_min after extra time scrunching.
    Extra scrunching is performed by the transpose module on TUSE.

    Parameters
    ----------
    tsamp_fbfuse : float
        Sampling time of the FBFUSE stream
    tsamp_min : float
        Lowest acceptable sampling time for the scrunched stream
    pow2 : bool, optional
        If True, enforce tscrunch_extra to be a power of 2
    """
    if tsamp_input >= tsamp_output_min:
        return 1
    ts = int(math.ceil(tsamp_output_min / tsamp_input))
    if pow2:
        ts = pow2ceil(ts)
    return ts


def get_fscrunch_extra(nchans_input, nchans_output_max):
    """
    Get the fscrunch factor necessary for the number of channels to be no
    higher than nchan_max after extra frequency scrunching.
    Extra scrunching is performed by the transpose module on TUSE.

    Parameters
    ----------
    nchan_fbfuse : int
        Number of channels in the FBFUSE stream
    nchan_max : int
        Maximum acceptable number of channels for the scrunched stream
    """
    if nchans_output_max >= nchans_input:
        return 1
    fscrunch_min = int(math.ceil(nchans_input / nchans_output_max))

    # Now find a factor that is >= fscrunch_min and divides nchan_input
    for fscrunch in range(fscrunch_min, nchans_input):
        if nchans_input % fscrunch == 0:
            return fscrunch
    
    msg = "Could not find a suitable fscrunch_extra factor for nchans_input = {} and nchans_output_max = {}".format(
        nchans_input, nchans_output_max)
    raise ValueError(msg)


class DADABufferBase(object):
    """
    key : str
        DADA key
    bytesize : int
        Size in bytes of a single buffer in the ring
    nbuffers : int
        Number of buffers in the ring
    nreaders : int
        Number of reader threads
    """
    def __init__(self, key='dada', bytesize=1048576, nbuffers=8, nreaders=1):
        Schema(And(str, len)).validate(key)
        SCHEMA_INTGT0.validate(bytesize)
        SCHEMA_INTGT0.validate(nbuffers)
        SCHEMA_INTGT0.validate(nreaders)
        self.key = key
        self.bytesize = bytesize
        self.nbuffers = nbuffers
        self.nreaders = nreaders
        self.bytesize_total = self.nbuffers * self.bytesize

    def asdict(self):
        """
        Returns configuration parameters as a dictionary
        """
        # NOTE: if called from a derived class, this will automatically 
        # include derived members
        return copy.deepcopy(vars(self))


class DADABuffer(DADABufferBase):
    """
    """
    def __init__(self, key='dada', nbuffers=8, nreaders=1, nbeams=6, nchans=1024, nsamp=8192, tsamp=306.24e-6):
        SCHEMA_INTGT0.validate(nbeams)
        SCHEMA_INTGT0.validate(nchans)
        SCHEMA_INTGT0.validate(nsamp)
        SCHEMA_FLOATGT0.validate(tsamp)
        self.nbeams = nbeams
        self.nchans = nchans
        self.nsamp = nsamp
        self.tsamp = tsamp
        self.timespan = nsamp * tsamp
        # NOTE: Assumes real-valued 8-bit data with single pol, that's unlikely
        # to ever change
        bytesize = nbeams * nchans * nsamp
        super(DADABuffer, self).__init__(key=key, bytesize=bytesize, nbuffers=nbuffers, nreaders=nreaders)


class InputDADABuffer(DADABuffer):
    """
    """
    def __init__(self, key='bff0', nbuffers=8, nreaders=1, nbeams=6, nchans=1024,
        samples_per_heap=256, tsamp=306.24e-6, timespan_min=6.0, tscrunch_extra=1):
        # NOTE: the number of samples in a buffer must be a multiple of tscrunch_extra
        # We enforce this by making heapgroups per buffer a multiple of tscrunch_extra
        heap_timespan = samples_per_heap * tsamp
        hg = int(math.ceil(timespan_min / heap_timespan))
        hg = multceil(hg, tscrunch_extra)
        self.heapgroups_per_buffer = max(1, hg)
        nsamp = self.heapgroups_per_buffer * samples_per_heap
        
        super(InputDADABuffer, self).__init__(key=key, nbuffers=nbuffers, nreaders=nreaders, 
            nbeams=nbeams, nchans=nchans, nsamp=nsamp, tsamp=tsamp)


class OutputDADABuffer(DADABuffer):
    """
    Class to represent a ring buffer where the transpose module writes the
    data for a single beam. 'nbeams' such buffers are allocated.

    NOTE: Output DADA buffers may have a different tsamp, nsamp, nchan, etc.
    due to extra scrunching.

    NOTE: The number of samples in an output buffer must be chosen *after*
    the input buffer params have been calculated, because the number of samples
    in the input buffer has to respect a number of conditions.

    Parameters
    ----------
    ibeam : int
        Relative beam index, in the range [0, nbeams-1]. The DADA key will
        be chosen as a function of ibeam.
    
    Other keyword arguments match those of the parent class
    """
    def __init__(self, ibeam, nbuffers=8, nreaders=1, nchans=1024, nsamp=8192, tsamp=306.24e-6):
        self.ibeam = ibeam
        key = output_dada_key(ibeam)

        super(OutputDADABuffer, self).__init__(key=key, nbuffers=nbuffers, nreaders=nreaders, 
            nbeams=1, nchans=nchans, nsamp=nsamp, tsamp=tsamp)


class DADATransposeConfig(object):
    """
    Calculate and store configuration for DADA buffers and transpose module

    mode : str
        Pipeline mode, the number of readers on every buffer is chosen as
        a function of this

    bpmon : bool
        If True, assume the bandpass monitor will be run, and that the input
        DADA buffer needs an extra reader
    """
    def __init__(self, mode='production', nbuffers=8, nbeams=6, nchans_input=1024, samples_per_heap=256, subband_nchans=16, 
        heap_size=8192, tsamp_input=306.24e-6, timespan_min=6.0, tscrunch_extra=1, fscrunch_extra=1, bpmon=True):
        validate_pipeline_mode(mode)

        if not nchans_input % subband_nchans == 0:
            raise ValueError("subband_nchans must divide nchans_input")

        if not nchans_input % fscrunch_extra == 0:
            raise ValueError("fscrunch_extra must divide nchans_input")

        self.mode = mode
        self.nbeams = nbeams
        self.nchans_input = nchans_input
        self.samples_per_heap = samples_per_heap
        self.subband_nchans = subband_nchans
        self.heap_size = heap_size
        self.tsamp_input = tsamp_input

        # A heap contains 'samples_per_heap' samples, and 'subband_nchan' channels for a single beam
        # A heapgroup contains 'samples_per_heap' samples for all captured beams and for the whole captured band
        # (which currently is, and will probably always be the whole available band)
        # A heap and a heapgroup thus have the same timespan
        self.subband_nchans = subband_nchans
        self.samples_per_heap = samples_per_heap
        self.nsubbands = nchans_input // subband_nchans
        self.heap_timespan = samples_per_heap * tsamp_input
        self.heapgroup_size = nbeams * self.nsubbands * heap_size

        ### Input buffer config
        # Add an extra input reader if bandpass monitor is running
        # In discard mode, we run dada_dbnull and optionally bandpass monitor
        input_readers = 2 if bpmon else 1
        
        self.input_buffer = InputDADABuffer(
            key='bff0',
            nbuffers=nbuffers,
            nreaders=input_readers, 
            nbeams=nbeams,
            nchans=nchans_input,
            samples_per_heap=samples_per_heap,
            tsamp=tsamp_input,
            timespan_min=timespan_min,
            tscrunch_extra=tscrunch_extra
        )

        ### Output buffer config
        self.tscrunch_extra = tscrunch_extra
        self.fscrunch_extra = fscrunch_extra
        self.tsamp_output = self.tsamp_input * tscrunch_extra
        self.nchans_output = nchans_input // fscrunch_extra
        self.nsamp_output = self.input_buffer.nsamp // self.tscrunch_extra

        self.output_buffers = []
        if mode == 'production':
            self.output_buffers = [
                OutputDADABuffer(
                    ibeam, 
                    nbuffers=nbuffers, nreaders=1, nchans=self.nchans_output, 
                    nsamp=self.nsamp_output, tsamp=self.tsamp_output
                )
                for ibeam in range(nbeams)
            ]

    def asdict(self):
        """
        Returns configuration parameters as a JSON-serializable dictionary,
        which gets written to file
        """
        params = copy.deepcopy(vars(self))
        params['input_buffer'] = params['input_buffer'].asdict()
        params['output_buffers'] = [b.asdict() for b in params['output_buffers']]
        return params

    @classmethod
    def from_production_input(cls, product_meta, beams, mode='production', timespan_min=6.0,
        nbuffers=8, tsamp_output_min=300.0e-6, nchans_output_max=4096, bpmon=True):
        """
        Generate a DADATransposeConfig from live production parameters

        Parameters
        ----------
        product_meta : dict
            Product metadata read from CAM portal at ?capture-init
            The full list of keys is specified in the MasterPortalClient code
        beams : list
            List of Beam objects
        mode : str
            Pipeline mode, the number of readers on every buffer is chosen as
            a function of this
        nbuffers: int
            Number of buffers in all DADA rings
        timespan_min : float
            Minimum effective data length in seconds that a single DADA buffer
            must contain
        tsamp_output_min : float or None
            Minimum desired sampling time of the output data after additional
            time scrunching by the transpose module. If None, do not time
            scrunch.
        nchans_output_max : int or None
            Maximum number of channels in the output data after additional
            frequency scrunching by the transpose module. If None, do not
            freq scunch.
        bpmon : bool
            If True, assume the bandpass monitor will be run, and that the input
            DADA buffer needs an extra reader
        """
        validate_beams(beams)
        pm = DotDict(product_meta)
        coherent = beams[0].coherent
        beam_type = 'coherent' if coherent else 'incoherent'
        log.debug("Generating DADATransposeConfig for mode = {}, beam_type = {}".format(mode, beam_type))

        samples_per_heap = pm.coherent_beam_samples_per_heap if coherent \
            else pm.incoherent_beam_samples_per_heap

        heap_size = pm.coherent_beam_heap_size if coherent \
            else pm.incoherent_beam_heap_size

        # NOTE: it appears that the value of sensors *_beam_time_resolution read
        # from the CAM portal are missing some significant digits 
        # (truncaded to ~1 nanosec precision). We therefore recalculate 
        # the exact tsamp value to be safe.
        fbfuse_tscrunch = pm.coherent_beam_tscrunch if coherent \
            else pm.incoherent_beam_tscrunch
        tsamp_input = (1.0 / pm.bandwidth) * pm.nchannels * fbfuse_tscrunch

        subband_nchans = pm.coherent_beam_subband_nchans if coherent \
            else pm.incoherent_beam_subband_nchans
        
        # NOTE: pm.nchannels is the number of channels *before* FBFUSE fscrunching !
        fbfuse_fscrunch = pm.coherent_beam_fscrunch if coherent \
            else pm.incoherent_beam_fscrunch
        nchans_input = pm.nchannels // fbfuse_fscrunch

        # Now deal with extra scrunching
        tscrunch_extra = 1
        if tsamp_output_min is not None:
            tscrunch_extra = get_tscrunch_extra(tsamp_input, tsamp_output_min)

        fscrunch_extra = 1
        if nchans_output_max is not None:
            fscrunch_extra = get_fscrunch_extra(nchans_input, nchans_output_max)

        return cls(
            mode=mode,
            nbuffers=nbuffers,
            nbeams=len(beams),
            nchans_input=nchans_input, 
            samples_per_heap=samples_per_heap, 
            subband_nchans=subband_nchans, 
            heap_size=heap_size, 
            tsamp_input=tsamp_input, 
            timespan_min=timespan_min,
            tscrunch_extra=tscrunch_extra,
            fscrunch_extra=fscrunch_extra,
            bpmon=bpmon
        )


def test():
    import json
    import katpoint
    from beam import Beam

    # Example product_meta where we have removed the following superfluous and bulky keys:
    # - coherent_beam_multicast_group_mapping
    # - available_antennas
    product_meta = {
        'coherent_beam_samples_per_heap': 128,
        'coherent_beam_nbeams_per_server_set': 480,
        'coherent_beam_heap_size': 8192,
        'incoherent_beam_tscrunch': 16,
        'incoherent_beam_antennas': 'm000,m001,m002,m003,m004,m005,m006,m008,m009,m010,m011,m012,m013,m014,m015,m017,m018,m019,m020,m021,m024,m026,m027,m028,m032,m033,m034,m035,m038,m039,m040,m041,m042,m043,m044,m045,m046,m047,m048,m049,m050,m051,m052,m053,m054,m055,m056,m057,m058,m059,m060,m061,m062',
        'wide_adc_sample_rate': 1712000000.0,
        'fbfmc_address': ['fbfhn00.mpifr-be.mkat.karoo.kat.ac.za', 5000],
        'coherent_beam_multicast_groups_data_rate': 5136000000.0,
        'synchronisation_epoch': 1584420943.0,
        'fbfuse_proxy': 'fbfuse_2',
        'incoherent_beam_heap_size': 8192,
        'incoherent_beam_idx1_step': 16777216,
        'coherent_beam_antennas': 'm000,m001,m002,m003,m004,m005,m006,m008,m009,m010,m011,m012,m013,m014,m015,m017,m018,m019,m020,m021,m024,m026,m027,m028,m032,m033,m034,m035,m038,m039,m040,m041,m042,m043,m044,m045,m046,m047,m048,m049,m050,m051,m052,m053,m054,m055,m056,m057,m058,m059,m060,m061',
        'incoherent_beam_count': 1,
        'nchannels': 4096,
        'incoherent_beam_fscrunch': 1,
        'incoherent_beam_samples_per_heap': 128,
        'bandwidth': 856000000.0,
        'coherent_beam_fscrunch': 1,
        'incoherent_beam_multicast_group_data_rate': 428000000.0,
        'coherent_beam_time_resolution': 7.656070000000001e-05,
        'coherent_beam_ngroups': 40,
        'coherent_beam_count_per_group': 12,
        'coherent_beam_subband_nchans': 64,
        'coherent_beam_count': 480,
        'subarray_proxy': 'subarray_2',
        'product_id': 'array_2',
        'centre_frequency': 1284000000.0,
        'coherent_beam_idx1_step': 16777216,
        'coherent_beam_multicast_groups': 'spead://239.11.1.1+39:7147',
        'coherent_beam_tscrunch': 16,
        'incoherent_beam_multicast_group': 'spead://239.11.1.0+0:7147',
        'cbf_proxy': 'cbf_2',
        'incoherent_beam_subband_nchans': 64,
        'incoherent_beam_time_resolution': 7.656070000000001e-05
    }

    mc_address = "239.1.1.42:7147"
    target = katpoint.Target("J1151-6108,radec,11:51:56.86,-61:08:17.6")

    beams = [
        Beam(ii, mc_address, "cfbf{:05d}".format(ii + 100), target)
        for ii in range(2)
    ]

    conf = DADATransposeConfig.from_production_input(
        product_meta, beams, mode='production',
        timespan_min=6.0, tsamp_output_min=300e-6, nchans_output_max=1024, bpmon=False
    )

    print(json.dumps(conf.asdict(), indent=4))


if __name__ == '__main__':
    import coloredlogs
    logfmt = "[%(levelname)s - %(asctime)s - %(name)s - %(filename)s:%(lineno)s] %(message)s"
    coloredlogs.install(fmt=logfmt, level='DEBUG', milliseconds=True, logger=log)
    test()