# Utilities to monitor the health and the data output of a NodeController
# Vincent Morello - June 2019
# 
# Written mostly for the DWF run of June 2019, some of it will eventually be 
# made obsolete when the pipeline start/stop interface gets refactored
#
# TODO: maybe place the contents of ipmi_helpers.py in this file

import os
import re
import operator
import subprocess
import shlex
import glob
from datetime import datetime, timedelta

import scandir
import docker
import numpy as np
import tusebpm

from pipeline_utils import CONTROL_MAIN_OUTDIR, UTC_FORMAT


def available_disk_space():
    """
    Returns the available disk space (in bytes) on the partition where the candidates and other
    outputs are being written.
    """
    # https://stackoverflow.com/questions/4260116/find-size-and-free-space-of-the-filesystem-containing-a-given-file
    # NOTE: This also works from inside a docker containe on a bind-mounted directory.
    s = os.statvfs(CONTROL_MAIN_OUTDIR)
    return s.f_frsize * s.f_bavail


def gpu_stats():
    """
    Returns aggregated GPU statistics.

    Returns
    -------
    stats: dict
        Dictionary of stats with keys:
            usage_percent, memory_used_mb, power_draw, highest_temperature
        All stats are summed across all system GPUs, except temperature which 
        is the temperature of the hottest GPU.
    """
    def parse_stats(line):
        usage, mem, power, temp = list(map(float, line.split(',')))
        return {'usage_percent': usage, 'memory_used_mb': mem, 'power_draw': power, 'temperature': temp}

    def sumkey(dlist, key):
        return sum((d[key] for d in dlist))

    def maxkey(dlist, key):
        return max((d[key] for d in dlist))

    def aggregated_stats(stats):
        return {
            'usage_percent' : sumkey(stats, 'usage_percent'),
            'memory_used_mb': sumkey(stats, 'memory_used_mb'),
            'power_draw'    : sumkey(stats, 'power_draw'),
            'highest_temperature': maxkey(stats, 'temperature'),
        }

    cmd = "nvidia-smi --query-gpu=utilization.gpu,memory.used,power.draw,temperature.gpu --format=csv,noheader,nounits"
    out = subprocess.check_output(shlex.split(cmd))
    stats = list(map(parse_stats, out.splitlines()))
    return aggregated_stats(stats)


def candidate_file_date(fname):
    """
    Return the date (as a datetime object) contained in the filename of a candidate filterbank
    file. This represents the UTC timestamp of the candidate data itself.
    """
    __, fname = os.path.split(fname)
    base, __ = os.path.splitext(fname)
    # remove trailing number for candidate names such as: 2020_10_05_15:58:01_1.fil
    utc = '_'.join(base.split('_')[:4]) 
    return datetime.strptime(utc, "%Y_%m_%d_%H:%M:%S")


def candidate_stats(utc_start, utc_format=UTC_FORMAT, main_outdir=CONTROL_MAIN_OUTDIR):
    """
    Get some stats about the candidates produced in the pipeline output directory corresponding
    to given UTC start datetime. This function looks for files following the pattern:
    <CONTROL_MAIN_OUTDIR>/<utc_start>/beamXX/*.fil

    Parameters
    ----------
    utc_start : datetime
        UTC start of the pipeline directory
    utc_format : str, optional
        Datetime format specifier (strftime) for the UTC output directories
    main_outdir : str, optional
        Name of the top-level directory that contains the UTC output directories

    Returns
    -------
    items : list of dict
        List of dictionaries, one per candidate file in the UTC directory, with keys:
        'path' (str), 'dtime' (datetime), 'mtime' (datetime), 'lag' (float, in seconds)
        lag is equal to mtime - dtime
    """
    ustr = utc_start.replace(microsecond=0).strftime(utc_format)
    basedir = os.path.join(main_outdir, ustr)

    def scanfilt(dirpath, filt):
        for entry in scandir.scandir(dirpath):
            if filt(entry):
                yield entry

    def iter_beams(dirpath):
        return scanfilt(
            dirpath, 
            lambda entry: entry.is_dir() and re.match(r"beam\d{2}", entry.name)
        )

    def iter_cands(dirpath):
        return scanfilt(
            dirpath, 
            lambda entry: entry.is_file() and entry.name.endswith('.fil')
        )

    items = []
    for beam_entry in iter_beams(basedir):
        for cand in iter_cands(beam_entry.path):
            dtime = candidate_file_date(cand.path) # data timestamp
            mtime = datetime.utcfromtimestamp(cand.stat().st_mtime) # modification timestamp
            entry = dict(path=cand.path, dtime=dtime, mtime=mtime, 
                lag=(mtime-dtime).total_seconds()
            )
            items.append(entry)
    return items



def container_logs(cname, seconds=None):
    """
    Returns logs of given docker container name that were output in the last
    N seconds. Raises an Exception if the container name does not exist.
    """
    client = docker.client.APIClient()
    since = None
    if seconds is not None:
        since = datetime.now() - timedelta(seconds=seconds)
    return client.logs(cname, since=since)


def aa_hits_last_block(loglines):
    """
    Get the total number of hits in the last data block searched by AA.
    Returns None if no "Total number of candidates" message is present
    in loglines.

    Parameters
    ----------
    loglines: list or tuple
        List of log lines (i.e. logs split on newline chars)

    Returns
    -------
    hits: float or None
    """
    if not isinstance(loglines, (list, tuple)):
        raise ValueError

    def filt(line):
        return line.strip().startswith("Total number of candidates:")

    def parse(line):
        # Must wrap in a try block, because occasionally there are 
        # tangled log lines with text from multiple processes
        try:
            return int(line.split(':')[1])
        except:
            return None
    
    lines = list(filter(filt, loglines))
    return parse(lines[-1]) if lines else None


def iqrm_masked_fraction(loglines):
    """
    Get the the average fraction of channels masked by IQRM recently. Returns None if no IQRM log
    messages are present in loglines.

    Parameters
    ----------
    loglines : iterable
        List of log lines (i.e. logs split on newline chars)

    Returns
    -------
    mfrac : float or None
        The average fraction of channels masked by IQRM
    """
    def filt(line):
        return "rfim::iqrm : fraction masked:" in line

    def parse(line):
        return float( line.split(':')[-1] )

    mfracs = []
    for line in loglines:
        if filt(line):
            try:
                mfracs.append(parse(line))
            except:
                continue
    return sum(mfracs) / len(mfracs) if mfracs else None


### mkrecv STAT line parsing plan
# STAT 0 12288 75383 slot 12260 0 0 100433920 100433920 total 12260 0 0 100433920 100433920 trash 338144 0 0 2770075648 2770075648 age 0 30726 0 ts 400283 401243 401244 401435
# STAT 1 12288 105487 slot 12288 0 0 100663296 100663296 total 24548 0 0 201097216 201097216 trash 473315 0 0 3877396480 3877396480 age 0 43013 0 ts 400667 401627 401628 401819
# This is valid for the following mkrecv version:
# Commit 287df4f8c44498974605917eac2410ebfcf9ed8c
# 25 Jan 2021
MKRECV_STAT_KEYS = [
    None,
    "slot.index",
    "slot.size",
    "slot.time",
    None,
    "slot.heaps-completed",
    "slot.heaps-discarded",
    "slot.heaps-open",
    "slot.bytes-expected",
    "slot.bytes-received",
    None,
    "total.heaps-completed",
    "total.heaps-discarded",
    "total.heaps-open",
    "total.bytes-expected",
    "total.bytes-received",
    None,
    "trash.heaps-completed",
    "trash.heaps-discarded",
    "trash.heaps-open",
    "trash.bytes-expected",
    "trash.bytes-received",
    None,
    "age.too-old",
    "age.valid",
    "age.too-new",
    None,
    "timestamp.head",
    "timestamp.current",
    "timestamp.last",
    "timestamp.next"
]


def parse_mkrecv_stat(line):
    return {
        name : int(element)
        for name, element in zip(MKRECV_STAT_KEYS, line.split())
        if name is not None
    }


def missing_heap_fraction_last_buffer(loglines):
    """
    Get fraction of missing heaps reported by mkrecv in the last captured buffer. Returns None if 
    mkrecv didn't print any STAT log message or if parsing failed.

    Parameters
    ----------
    loglines: list
        List of log lines (i.e. logs split on newline chars)

    Returns
    -------
    frac: float or None
        Fraction of missing heaps.
    """
    if not isinstance(loglines, (list, tuple)):
        raise ValueError

    def filt(line):
        return line.strip().startswith("STAT ")

    lines = list(filter(filt, loglines))
    try:
        stat = parse_mkrecv_stat(lines[-1])
        return (stat['slot.size'] - stat['slot.heaps-completed']) / float(stat['slot.size'])
    except:
        return None


def get_bpstats_encoded(nbeams, nchans, bufname='fbfuse_beam_bandpass'):
    """
    Read the bandpass statistics from the shared memory buffer, and encode them
    into strings to be published as sensors.

    The process is: read shared memory, convert to float16, encode with
    numpy.tostring(). The arrays returned can be decoded using:

    >>> np.fromstring(array, dtype=np.float16).reshape(nbeams, nchans)

    Parameters
    ----------
    nbeams: int
        Number of beams being processed
    nchans: int
        Number of frequency channels in the data
    bufname: str, optional
        Name of the shared memory buffer where the bandpass stats are written
        (default: 'fbfuse_beam_bandpass')

    Returns
    -------
    bmean_encoded: str
        Bandpass mean, converted to float16 then encoded using numpy.tostring()
    bstd_encoded: str
        Bandpass standard deviation, converted to float16 then encoded using
        numpy.tostring()

    Raises
    ------
    posix_ipc.ExistentialError: if the shared memory segment does not exist
    posix_ipc.PermissionsError: if the user does not have permission to read
        the shared memory buffer
    """
    bmean, bstd = tusebpm.read_bpstats(nbeams, nchans, name=bufname)
    bmean_encoded = bmean.astype(np.float16).tostring()
    bstd_encoded = bstd.astype(np.float16).tostring()
    return bmean_encoded, bstd_encoded
