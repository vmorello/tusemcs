import os
import yaml


class NodeConfig(dict):
    """ 
    Subclass of dictionary used to parse node_controller.yml
    Has convenience methods to get keys within nested dictionaries.
    """
    def __init__(self, items):
        """ """
        super(NodeConfig, self).__init__(items)

    def dget(self, key):
        """ 
        Get nested key with dot notation. This is a convenience method.
        dict.dget('A.B.C') is equivalent to dict['A']['B']['C'] 
        """
        return reduce(lambda c, k: c[k], key.split('.'), self)
    
    def dset(self, key, val):
        """ 
        Set nested key with dot notation. This is a convenience method.
        dict.dset('A.B.C', value) is equivalent to dict['A']['B']['C'] = value

        Raises a KeyError if dot notation key does not exist.
        """
        # Helper function that does the setting on the underlying dict
        # 'items' is a dict
        def setter(items, key, val):
            sep = '.'
            if not sep in key:
                items[key] = val
            else:
                first, rest = key.split(sep, 1)
                setter(items[first], rest, val)

        # Check that key exists, this will raise a KeyError if not
        self.dget(key)
        setter(self, key, val)

    @classmethod
    def from_yaml_config(cls, fname):
        with open(fname, 'r') as fobj:
            return cls(yaml.safe_load(fobj))
