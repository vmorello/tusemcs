import logging
import signal
import threading
import uuid
import socket
import json
from collections import namedtuple

import tornado
from tornado import gen
import coloredlogs
import redis
from katportalclient import KATPortalClient


log = logging.getLogger('tusemcs.redis_beam_updater')


FBFMC_PRODUCT_SENSOR_PATTERNS = [
    'coherent_beam_cfbf*',
    'coherent_beam_data_suspect',
    'coherent_beam_shape',
    'incoherent_beam_data_suspect',
    'phase_reference', # IB position
    'ca_target_request'
]


SensorReading = namedtuple('SensorReading', 'name status value t rt')


def ujoin(*args):
    """ 
    Join all string arguments with underscores. Useful to expand
    sensor names 
    """
    return '_'.join(args)


def dict2reading(arg):
    """ 
    Convert the dictionary passed to the callback function of 
    KATPortalClient to a SensorReading namedtuple

    'arg' is a dictionary with the following structure:
        {
        'msg_pattern': str
        'msg_channel': str
        'msg_data': {
            'status': str, 
            'timestamp': float, 
            'value': <variable type>, 
            'name': str, 
            'received_timestamp': float
            }
        }
    """
    d = arg['msg_data']
    return SensorReading(
        d['name'], d['status'], d['value'], d['timestamp'], d['received_timestamp'])


class SamplingStrategiesError(Exception):
    def __init__(self):
        super(SamplingStrategiesError, self).__init__(
            "Failed to set sampling strategies on all required sensors")


class RedisBeamUpdater(object):
    """
    Class that pushes Redis messages with sensor updates (to be received by
    compute nodes), which contain beam-related sensor updates: positions, 
    data suspect, etc.

    Parameters
    ----------
    product_id : str
        Product ID such as 'array_1'
    portal_url : str
        URL of the CAM portal for the relevant product_id, e.g.
        'http://10.97.1.14/api/client/1'
    fbfuse_proxy_fallback: str or None
        Option used for manual testing only. If not None, the code will
        this string as the proxy name for a the FBFUSE component
    host : str
        Redis server host
    port : int
        Redis server port

    Notes
    -----
    This class pushes redis messages which contain a JSON-encoded dictionary
    with a single key-value pair. Here's the list:

    - 'ifbf{BEAM_NUMBER}' : katpoint Target string
    - 'cfbf{BEAM_NUMBER}' : katpoint Target string
    - 'coherent-beam-data-suspect' : bool
    - 'incoherent-beam-data-suspect' : bool
    - 'coherent_beam_shape' : JSON-encoded string
    - 'ca_target_request' :  JSON-encoded string
    """
    def __init__(self, product_id, portal_url, fbfuse_proxy_fallback=None, host="10.98.78.99", port=6379):
        self.product_id = product_id
        self.portal_url = portal_url
        self.fbfuse_proxy_fallback = fbfuse_proxy_fallback
        self.fbfuse_proxy = None  # str
        self.namespace = None # str
        self.sensor_prefix = None # str

        # redis server host and port
        self.host = host
        self.port = port 

        self.portal_client = None
        self.redis_client = None

    @gen.coroutine
    def start(self):
        self.redis_client = redis.StrictRedis(host=self.host, port=self.port)
        log.debug("Redis client connected to {}:{}".format(self.host, self.port))

        self.portal_client = KATPortalClient(self.portal_url, on_update_callback=self.update_callback)
        yield self.portal_client.connect()
        log.debug('KATPortalClient connected')

        self.namespace = str(uuid.uuid4()) # Cast to str matters
        yield self.portal_client.subscribe(self.namespace)
        log.debug('Subscribed to namespace {!r}'.format(self.namespace))

        log.debug('Retrieving FBFUSE proxy name')
        try:
            name = yield self.portal_client.sensor_subarray_lookup('fbfuse', sensor=None, return_katcp_name=False)
            log.debug("FBFUSE proxy name: {!r}".format(name))
        except:
            # NOTE: Fallback option useful when testing manually
            if self.fbfuse_proxy_fallback is not None:
                name = self.fbfuse_proxy_fallback
                log.warning('Could not retrieve FBFUSE proxy name, fallling back to {!r}'.format(name))
            else:
                raise
        self.fbfuse_proxy = name

        # NOTE: last empty argument is equivalent to adding a trailing underscore
        self.sensor_prefix = ujoin(self.fbfuse_proxy, 'fbfmc', self.product_id, '') 
        log.info("Successfully initialized")

    @gen.coroutine
    def setup_sampling(self):
        """
        Set sampling strategies on all required CAM portal sensors.
        Returns True if all sampling strategies were set correctly, False
        otherwise.
        """
        log.info('Setting sensor sampling strategies')
        patterns = [
            self.sensor_prefix + pt
            for pt in FBFMC_PRODUCT_SENSOR_PATTERNS
        ]
        log.debug("Patterns: {}".format(patterns))

        # 'result' is a dictionary {sensor_name: {'info': requested_strategy, 'success': True/False}}
        # NOTE: the strategy 'event-rate 1 4.2' means that we get updates every time the
        # sensor value changes; we also get an update if we haven't received one in the last 4.2 seconds
        result = yield self.portal_client.set_sampling_strategies(self.namespace, patterns, 'event')
        result = {key: val['success'] for key, val in result.items()}
        
        # We need to check that the sensor names we want are present as keys in 'result'
        if not len(result.keys()) >= len(patterns):
            msg = "Only {} sensors were matched when setting sampling strategies, expected >= {}".format(
                len(result.keys()),
                len(patterns)
            )
            log.warning(msg)
            raise gen.Return(False)
        else:
            log.debug("set_sampling_strategies matched {} sensors".format(len(result.keys())))

        # And then check that for each of them, 'success' is True
        success = all(list(result.values()))
        missing = [key for key, val in result.items() if not val]

        if success:
            log.info("Successfully set sampling strategies on all required sensors")
        else:
            missing = set(patterns).difference(result.keys())
            log.warning("Set sampling strategies returned False for {} sensors: {}".format(len(missing), missing))
        raise gen.Return(success)
    
    @gen.coroutine
    def setup_sampling_with_retries(self, retries=5, dt=10.0):
        """
        Set sampling strategies on all required CAM portal sensors, allowing
        multiple attempts. Raises an error if all attempts fail.

        Parameters
        ----------
        retries: int
            Total number of attempts allowed
        dt: float
            Minimum time in seconds between each attempt

        Raises
        ------
        SamplingStrategiesError: if unable to set all sampling strategies
        after the allowed number of retries
        """
        while retries > 0:
            wait_future = gen.sleep(dt)
            success = yield self.setup_sampling()
            if success:
                return
            retries -= 1
            log.warning("Failed to set sampling strategies, retries left: {}".format(retries))
            yield wait_future
        raise SamplingStrategiesError

    def publish_reading(self, short_name, value):
        items = {short_name: value}
        self.redis_client.publish('tcs', json.dumps(items))   
        # NOTE: redis does not support storage of boolean type
        if type(value) == bool:
            items = {short_name: int(value)}
        self.redis_client.mset(items)

    def update_callback(self, msg):
        reading = dict2reading(msg)

        # Remove prefix such as 'fbfuse_1_fbfmc_array_1'
        name = reading.name[len(self.sensor_prefix):]
        
        # Beam position update
        if name.startswith('coherent_beam_cfbf'):
            short_name = "cfbf" + name.split('cfbf')[-1]
            self.publish_reading(short_name, reading.value)
            return

        if name == 'phase_reference':
            self.publish_reading('ifbf00000', reading.value)
            return

        else:
            self.publish_reading(name, reading.value)
            return

    @gen.coroutine
    def stop(self):
        if not self.portal_client:
            return
        if self.portal_client.is_connected:
            if self.namespace is not None:
                yield self.portal_client.unsubscribe(self.namespace)
                log.debug("KATPortalClient instance unsubscribed")
            self.portal_client.disconnect()
        
        # NOTE: clear database for next run
        self.redis_client.flushdb()
        self.redis_client = None
        self.portal_client = None
        self.namespace = None
        log.debug("KATPortalClient instance disconnected")

    def __del__(self):
        self.stop()


### For test purposes only ###
def main():
    @gen.coroutine
    def start(updater):
        yield updater.start()
        yield updater.setup_sampling_with_retries()
        log.info("Started")

    @gen.coroutine
    def stop(ioloop, updater):
        yield updater.stop()
        ioloop.stop()
        log.info("Stopped")
    
    ioloop = tornado.ioloop.IOLoop.current()

    portal_url = 'http://10.97.1.14/api/client/1'
    product_id = 'array_1'
    updater = RedisBeamUpdater(
        product_id,
        portal_url, 
        #host="localhost", 
        fbfuse_proxy_fallback='fbfuse_1'
    )

    # Hook up to SIGINT so that ctrl-C results in a clean shutdown
    stopfunc = lambda sig, frame: ioloop.add_callback_from_signal(stop, ioloop, updater)
    signal.signal(signal.SIGINT, stopfunc)
    signal.signal(signal.SIGTERM, stopfunc)

    ioloop.add_callback(start, updater)
    ioloop.start()


if __name__ == '__main__':
    coloredlogs.install(
        fmt="[%(levelname)7s - %(asctime)s - %(name)s - %(filename)s:%(lineno)s] %(message)s", 
        level='DEBUG', 
        milliseconds=True
    )
    logging.getLogger('kat').setLevel('WARNING')
    main()