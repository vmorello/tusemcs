#
# Slack / Mattermost alerts module
# NOTE: the following environment variables are expected to be defined and to contain 
# the webhooks for posting messsages:
# TCS_SLACK_WH
# TCS_MATTERMOST_WH
#
import os
import json
import logging
import datetime

import tornado
from tornado import gen
from tornado.httpclient import AsyncHTTPClient, HTTPRequest


log = logging.getLogger('tusemcs.alerts')


REQUEST_HEADERS = {'Content-Type': 'application/json'}
CONNECT_TIMEOUT = 1.5 # NOTE: on TUSE head node this somehow takes 4x longer than this
REQUEST_TIMEOUT = 2.0


@gen.coroutine
def send_slack_alert(msg):
    """
    Send alert message to Slack asynchronously. Exceptions are caught and logged as a warning.

    Parameters
    ----------
    msg : str, or list of str
        Message, or list of lines of a message. If a list of strings is passed, they are joined 
        with a newline character.
    """
    if isinstance(msg, list):
        msg = '\n'.join(list(msg))

    url = os.environ.get('TCS_SLACK_WH')
    client = AsyncHTTPClient()

    # See: https://www.tornadoweb.org/en/stable/httpclient.html#request-objects
    req = HTTPRequest(
        url, 
        body=json.dumps(dict(text=msg)), 
        headers=REQUEST_HEADERS, 
        method='POST', 
        connect_timeout=CONNECT_TIMEOUT, 
        request_timeout=REQUEST_TIMEOUT
    )
    try:
        yield client.fetch(req)
        log.debug("Slack alert sent")
    except Exception as err:
        log.warning("Could not send Slack alert: {}".format(err))


@gen.coroutine
def send_mattermost_alert(msg, username='TUSE Alerts', channel='observing-alerts'):
    """
    Send alert message to mattermost asynchronously. Post username and target channel can be 
    adjusted. Exceptions are caught and logged as a warning.

    Parameters
    ----------
    msg : list of str, or str
        Message, or list of lines of a message. Markdown syntax is supported. If a list of strings
        is passed, they are joined with a newline character.
    """
    if isinstance(msg, list):
        msg = '\n'.join(list(msg))

    url = os.environ.get('TCS_MATTERMOST_WH')
    client = AsyncHTTPClient()

    # See: https://www.tornadoweb.org/en/stable/httpclient.html#request-objects
    req = HTTPRequest(
        url, 
        body=json.dumps(dict(text=msg, channel=channel, username=username)), 
        headers=REQUEST_HEADERS, 
        method='POST', 
        connect_timeout=CONNECT_TIMEOUT, 
        request_timeout=REQUEST_TIMEOUT
    )
    try:
        yield client.fetch(req)
        log.debug("Mattermost alert sent")
    except Exception as err:
        log.warning("Could not send Mattermost alert: {}".format(err))


def format_sb_details(sb_details):
    return [
        'ID: {}'.format(sb_details.get('id_code', 'N/A')),
        'Description: {}'.format(sb_details.get('description', 'N/A')),
    ]


def format_exception(exc):
    now = datetime.datetime.utcnow().strftime('%Y-%m-%d %H:%M')
    return [
        'Error UTC: {}'.format(now),
        '`{}: {}`'.format(type(exc).__name__, exc)
    ]


def sbfail_message(exc, sb_details={}):
    lines = ['@channel', '*Schedule Block FAILED*'] + format_sb_details(sb_details) + \
        format_exception(exc)
    return '\n'.join(lines)


def sbok_message(sb_details={}):
    lines = ['*Schedule Block Started*'] + format_sb_details(sb_details)
    return '\n'.join(lines)


def main():
    import signal
    import coloredlogs

    fmt = "[%(levelname)s - %(asctime)s - %(name)s - %(filename)s:%(lineno)s] %(message)s"
    coloredlogs.install(fmt=fmt, level='DEBUG', milliseconds=True)

    # Hook up to SIGINT and SIGTERM so that both ctrl-C and docker stop result in a clean shutdown
    @gen.coroutine
    def shutdown(ioloop):
        print('Shutting down ...')
        ioloop.stop()

    ioloop = tornado.ioloop.IOLoop.current()
    shutdown_func = lambda sig, frame: ioloop.add_callback_from_signal(shutdown, ioloop)
    signal.signal(signal.SIGINT, shutdown_func)
    signal.signal(signal.SIGTERM, shutdown_func)

    sb_details = {
        'description': 'Some schedule block description',
        'id_code': '20990224-0001',
        'owner': 'Johnny Meertrap',
        'proposal_id': 'SCI-20990101-MT-01',
        'sub_nr': 1,
    }

    @gen.coroutine
    def simulate_sbfail():
        try:
            raise ValueError("Very descriptive error message")
        except Exception as err:
            msg = sbfail_message(err, sb_details)
            yield send_slack_alert(msg)
            yield send_mattermost_alert(msg, channel='@vmorello')

    ioloop.add_callback(simulate_sbfail)
    ioloop.start()


if __name__ == '__main__':
    main()
