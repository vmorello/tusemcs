import os
import re
import scandir
from StringIO import StringIO
import base64
import time

import mtcutils
import png

from pipeline_utils import CONTROL_MAIN_OUTDIR, UTC_FORMAT


# Move this to mtcutils ?
def snapshot(fname, width=256, height=128):
    """
    Make a PNG snapshot plot of given candidate filterbank file. Returns the PNG file encoded as
    a base64 string. 'width' and 'height' are the image dimensions in pixels; the data are 
    scrunched accordingly to fit in the plot.
    """
    arr = mtcutils.snapshot(fname, width=width, height=height)
    # png.Writer wants a list of list of ints
    snap = [list(row) for row in arr]
    string_io = StringIO()
    w = png.Writer(width=width, height=height, bitdepth=8)
    w.write(string_io, snap)
    return base64.b64encode(string_io.getvalue())


def get_last_candidate_snapshot(utc_start):
    """
    Return a base64-encoded PNG snapshot of the last candidate produced across
    all beams of given UTC directory

    Parameters
    ----------
    utc_start : datetime
        UTC start of the pipeline directory

    Returns
    -------
    snapshot : str
        base64-encoded PNG plot of the last candidate written if any, otherwise empty string.
    mtime : float
        Modification timestamp of the last candidate written if any, otherwise 0.
    """
    def scanfilt(dirpath, filt):
        for entry in scandir.scandir(dirpath):
            if filt(entry):
                yield entry

    ustr = utc_start.replace(microsecond=0).strftime(UTC_FORMAT)
    utc_dir = os.path.join(CONTROL_MAIN_OUTDIR, ustr)

    utc_cands = []
    for beam in scanfilt(utc_dir, lambda entry: re.match(r"beam\d{2}", entry.name)):
        cands = scanfilt(beam.path, lambda entry: entry.name.endswith('.fil'))
        utc_cands.extend(list(cands))

    if not utc_cands:
        return "", 0.0
    
    last_cand = max(utc_cands, key=lambda entry: entry.stat().st_mtime)
    mtime = last_cand.stat().st_mtime
    return snapshot(last_cand.path), mtime
