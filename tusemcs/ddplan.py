#
# Class to represent a SPS dedispersion plan and parse it from a string
# Vincent Morello - 2019 
#

class DDPlanStage(object):
    """ 
    Simple object that represents a single stage in a dedispersion plan.
    This class has 3 members: start, stop, step. It can be converted to a
    tuple or a list directly, example:
    >>> stage = DDPlanStage.fromstring("0:100:0.42")
    >>> print(stage)
    DDPlanStage(start=0.0, end=100.0, step=0.42)
    >>> tuple(stage)
    (0.0, 100.0, 0.42)

    Can also be built directly from a string with the method 
    DDPlanStage.fromstring()

    Parameters
    ----------
    start: int or float
        Start DM of the stage
    end: int or float
        End DM of the stage
    step: int or float
        DM step of the stage

    Raises
    ------
    ValueError: if the specs are not valid, the following are not allowed:
        - end > start
        - start, end or step < 0
    """
    def __init__(self, start, end, step):
        tup = (start, end, step)
        if not all((x >= 0 for x in tup)):
            raise ValueError("DDPlanStage: all parameters must be >= 0")
        if not end >= start:
            raise ValueError("DDPlanStage: stop must be >= start")
        self.start = float(start)
        self.end = float(end)
        self.step = float(step)

    def __iter__(self):
        # NOTE: the iter method makes the class convertible to a list or tuple
        # https://stackoverflow.com/questions/37639363/how-to-convert-an-custom-class-object-to-a-tuple-in-python
        for x in (self.start, self.end, self.step):
            yield x

    def __str__(self):
        return "{}(start={}, end={}, step={})".format(type(self).__name__, self.start, self.end, self.step)

    def __repr__(self):
        return str(self)

    @classmethod
    def fromstring(cls, string):
        """
        Build a DDPlanStage from a string specifier
        "<start>:<stop>:<step>
        """
        try:
            start, stop, step = list(map(float, string.split(':')))
        except:
            raise ValueError("Failed to parse DDPlanStep from string: {!r}".format(string))
        return cls(start, stop, step)


class DDPlan(list):
    """ 
    A list of DDPlanStage objects. Can also be built directly from a string 
    with the method DDPlan.fromstring()

    Parameters
    ----------
    stages: list or iterable
        list of DDPlanStages
    assert_contiguous: bool
        Check that a stage's start is equal to the previous stage's end 
        (default: True)

    Raises
    ------
    ValueError: if type of stages is not DDPlanStage, or if stages are not
        contiguous when assert_contiguous=True
    """
    def __init__(self, stages, assert_contiguous=True):
        if not all((isinstance(s, DDPlanStage) for s in stages)):
            raise ValueError("All stages must be of type DDPlanStage")
        
        # Assert contiguity
        if assert_contiguous:
            for x, y in zip(stages[:-1], stages[1:]):
                if not x.end == y.start:
                    raise ValueError("The specified stages are NOT contiguous: {!r} and {!r}".format(x, y))

        super(DDPlan, self).__init__(stages)

    @classmethod
    def fromstring(cls, string):
        """
        Build a DDPlan from a comma-separated list of DDPlanStage string 
        specifiers. Spaces do not matter. Example:
        "0:370:0.307, 370:740:0.652, 740:1480:1.266"
        """
        try:
            stages = list(map(DDPlanStage.fromstring, string.split(',')))
        except:
            raise ValueError("Failed to parse DDPlan from string: {!r}".format(string))
        return cls(stages)


def test_ddplan():
    plan = DDPlan.fromstring("0:370:0.307, 370:740:0.652, 740:1480:1.266, 1480:2950:2.512, 2950:5000:4")
    for step in plan:
        print(step)

if __name__ == '__main__':
    test_ddplan()