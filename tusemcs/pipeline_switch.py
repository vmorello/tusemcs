#
# A very basic class that implements a delayed start for the pipeline.
#
# The pipeline start / stop is driven by the value of FBFUSE beam position sensors,
# which may flip back and forth quickly (a few times in a second) between values.
# We want to make sure that any START command issued is delayed by one to a few seconds,
# and that any STOP command received before that delay expires cancels a previously 
# scheduled start.
# 
# The implementation relies on IOLoop.call_later()
# https://www.tornadoweb.org/en/stable/ioloop.html#tornado.ioloop.IOLoop.call_later
#
import logging
import signal

from tornado.ioloop import IOLoop
from tornado import gen


log = logging.getLogger("tusemcs.pipeline_switch")


class PipelineSwitch(object):
    """ 
    Implements a delayed start and stop for the pipeline.
    
    Parameters
    ----------
    start_func : func
        Parameterless function that starts the pipeline
    stop_func : func
        Parameterless function that stops the pipeline
    """
    def __init__(self, start_func, stop_func):
        # Parameterless functions that do the actual 
        # starting and stopping of the pipeline
        self.start_func = start_func
        self.stop_func = stop_func

        # Stores handles returned by IOLoop.call_later(), when a call to
        # delayed_start() is made.
        self._handles = []

    def _remove_timeouts(self):
        """ Cancels all scheduled delayed_start commands. """
        for hd in self._handles:
            IOLoop.current().remove_timeout(hd)
        self._handles = []

    def delayed_start(self, wait=2.0):
        """ Start the pipeline in 'wait' seconds. Any call to stop() made 
        before this delay expires will cancel the start command before it gets
        executed. """
        log.debug("Queuing delayed start command to be executed in {:.1f} seconds".format(wait))
        handle = IOLoop.current().call_later(wait, self._start)
        self._handles.append(handle)

    def _start(self):
        """ Start the pipeline immediately. Also cancels any existing
        delayed_start commands. Do NOT call this directly ! """
        self._remove_timeouts()
        self.start_func()

    def stop(self):
        """ Stop the pipeline immediately. Also cancels any existing
        delayed_start commands. """
        self._remove_timeouts()
        self.stop_func()
