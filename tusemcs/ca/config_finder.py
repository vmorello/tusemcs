from enum import IntEnum
import os
import logging
from collections import namedtuple
from copy import deepcopy

import yaml
import numpy
from astropy.coordinates import SkyCoord
import astropy.units as uu
import ephem
import katpoint


log = logging.getLogger('tusemcs.ca.config_finder')


ArrayConstraints = namedtuple(
    'ArrayConstraints', 
    field_names='target_name sb_id proposal_id nchannels nants_min cfreq'
)

ArrayConfig = namedtuple(
    'ArrayConfig',
    field_names='usable_antennas cfreq nchannels proposal_id sb_id target_name'
)

ConditionalSBConfig = namedtuple(
    'ConditionalSBConfig',
    field_names='array_constraints parameters'
)

ConditionalTargetConfigTemplate = namedtuple(
    'ConditionalSBConfig',
    field_names='array_constraints parameters'
)

PrioritySourceBase = namedtuple(
    'PrioritySourceBase',
    field_names='name skycoord tiling_nbeams tiling_overlap'
)

class PrioritySource(PrioritySourceBase):
    @property
    def katpoint_string(self):
        ra = self.skycoord.ra.to_string(unit=uu.hour, sep=':', precision=3)
        dec = self.skycoord.dec.to_string(unit=uu.degree, sep=':', precision=3)
        return "{}, radec, {}, {}".format(self.name, ra, dec)


def load_configs():
    fname = os.path.join(os.path.dirname(__file__), 'config_store.yml')
    with open(fname, 'r') as f:
        contents = yaml.safe_load(f)
    
    ### SB Configs
    conditional_sbconfs = []
    for items in contents['schedule_block_configs']:
        constraints_dict = items['constraints']
        array_constraints = ArrayConstraints(**{
            key: constraints_dict.get(key, None)
            for key in ArrayConstraints._fields
        })
        obj = ConditionalSBConfig(
            array_constraints=array_constraints, parameters=items['parameters']
        )
        conditional_sbconfs.append(obj)
    
    ### Target Configs
    conditional_tgconfs = []
    for items in contents['target_config_templates']:
        constraints_dict = items['constraints']
        array_constraints = ArrayConstraints(**{
            key: constraints_dict.get(key, None)
            for key in ArrayConstraints._fields
        })
        obj = ConditionalTargetConfigTemplate(
            array_constraints=array_constraints, parameters=items['parameters']
        )
        conditional_tgconfs.append(obj)

    ### Priority Sources
    priority_sources = []
    for items in contents['priority_sources']:
        coord = SkyCoord(items['ra'], items['dec'], unit=(uu.hour, uu.deg))
        ps = PrioritySource(
            name=items['name'], 
            skycoord=coord, 
            tiling_nbeams=items['tiling_nbeams'], 
            tiling_overlap=items['tiling_overlap']
        )
        priority_sources.append(ps)

    return conditional_sbconfs, conditional_tgconfs, priority_sources
    

CONDITIONAL_SBCONFIGS, CONDITIONAL_TGCONFIGS, PRIORITY_SOURCES = load_configs()


def osan(func):
    """
    Or Second Argument is None.
    Decorator that modifies a two-argument function that returns a boolean.
    If the second argument is None, the decorated function returns True immediately.
    """
    def wrapped(a1, a2):
        if a2 == None:
            return True
        return func(a1, a2)
    return wrapped


@osan
def equal(a, b):
    return a == b

@osan
def ge(a, b):
    return a >= b


def check_compatible(aconf, constraints):
    """
    Check that given ArrayConfig is compatible with given ArrayConstraints.
    """
    condition = \
        ge(len(aconf.usable_antennas), constraints.nants_min) and \
        equal(aconf.cfreq, constraints.cfreq) and \
        equal(aconf.nchannels, constraints.nchannels) and \
        equal(aconf.proposal_id, constraints.proposal_id) and \
        equal(aconf.sb_id, constraints.sb_id) and \
        equal(aconf.target_name, constraints.target_name)
    return condition


def find_sb_config(aconf):
    """
    Find the ConditionalSBConfig compatible with given ArrayConfig. In case of multiple matches,
    the most specific match is returned.
    """
    filter_func = lambda sbconf: check_compatible(aconf, sbconf.array_constraints)
    # Sorting in reverse order places the "most specific config" in first position
    compatible_sbconfs = sorted(filter(filter_func, CONDITIONAL_SBCONFIGS), reverse=True)
    if not compatible_sbconfs:
        raise ValueError("No compatible ConditionalSBConfig for given array configuration")
    return deepcopy(compatible_sbconfs[0])


def find_target_config(aconf):
    """
    Find the ConditionalTargetConfigTemplate compatible with given ArrayConfig. In case of multiple
    matches, the most specific match is returned.
    """
    filter_func = lambda tgconf: check_compatible(aconf, tgconf.array_constraints)
    # Sorting in reverse order places the "most specific config" in first position
    compatible_tgconfs = sorted(filter(filter_func, CONDITIONAL_TGCONFIGS), reverse=True)
    if not compatible_tgconfs:
        raise ValueError(
            "No compatible ConditionalTargetConfigTemplate for given array configuration"
        )
    return deepcopy(compatible_tgconfs[0])


def find_priority_sources(boresight_skycoord, cfreq):
    """
    Find sources to followup within the estimated half-power radius of the incoherent beam

    Parameters
    ----------
    boresight_skycoord : astropy.SkyCoord
        Boresight coordinates
    cfreq : float
        Centre frequency in Hz. Used to estimate the IB half-power radius.

    Returns
    -------
    matches : list of PrioritySource objects
        Priority sources within the estimated half-power radius of the incoherent beam
    """
    # https://en.wikipedia.org/wiki/Airy_disk#Approximation_using_a_Gaussian_profile
    # half-power radius = 0.495 lambda/D
    D = 13.0 # dish effective diameter in m
    wl = 299792458.0 / cfreq # wavelength in m
    query_radius = 0.495 * wl/D * uu.rad
    log.debug("Checking for priority sources: centre = {}, radius = {}".format(
        boresight_skycoord, query_radius.to(uu.deg)))
    
    all_ras = [ps.skycoord.ra for ps in PRIORITY_SOURCES]
    all_decs = [ps.skycoord.dec for ps in PRIORITY_SOURCES]
    all_sources_skycoord = SkyCoord(all_ras, all_decs)

    angles = all_sources_skycoord.separation(boresight_skycoord)
    indices = numpy.where(angles < query_radius)[0]
    matches = [PRIORITY_SOURCES[ii] for ii in indices]
    return deepcopy(matches)
