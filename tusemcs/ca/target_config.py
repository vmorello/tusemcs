#
# Classes to build and represent target configurations
# Vincent Morello - 2019-2021
#
from copy import deepcopy
import uuid

import schema
import katpoint


class Tiling(object):
    """ 
    A dict wrapper representing a tiling

    Parameters
    ----------
    items: dict
        Dictionary with tiling parameters

    Raises
    ------
    schema.SchemaError: if items does not follow the expected schema
    """
    SCHEMA = schema.Schema({
        'target': schema.And(str, lambda s: katpoint.Target(s)),
        'nbeams': schema.And(int, lambda n: n > 0),
        schema.Optional('reference_frequency'): schema.And(schema.Or(int, float), lambda n: n > 0),
        schema.Optional('overlap'): schema.And(float, lambda x: 0 < x < 1),
        schema.Optional('epoch'): schema.And(schema.Or(int, float), lambda x: x > 0),
    })

    def __init__(self, items):
        self.validate(items) # raise exception if invalid
        self._items = items    

    def to_dict(self):
        return deepcopy(self._items)

    @classmethod
    def validate(cls, items):
        """
        Check if configuration dictionary 'items' is valid, raise an
        exception otherwise
        """
        return cls.SCHEMA.validate(items)

    def __str__(self):
        return "Tiling({!s})".format(self._items)

    def __repr__(self):
        return str(self)


class TilingTemplate(object):
    """
    Template for a FBFUSE tiling

    Parameters
    ----------
    target: str or None, optional
        katpoint target string, or None. If None, this will be replaced by the boresight position 
        once known.
    nbeams: int or None, optional
        Number of beams. None means allocate all remaining available beams.
    overlap: float, optional
        Overlap factor

    Raises
    ------
    schema.SchemaError: if one or more given parameters are invalid
    """
    SCHEMA = schema.Schema({
        'target': schema.Or(None, schema.And(str, lambda s: katpoint.Target(s))),
        'nbeams': schema.Or(None, schema.And(int, lambda n: n > 0)),
        'overlap': schema.And(float, lambda x: 0 < x < 1)
    })

    def __init__(self, target=None, nbeams=400, overlap=0.5):
        kwargs = dict(locals().copy())
        kwargs.pop('self')
        self.validate(kwargs)
        self._items = deepcopy(kwargs)

    @classmethod
    def validate(cls, items):
        """
        Check if init dictionary 'items' is valid, raise an
        exception otherwise
        """
        return cls.SCHEMA.validate(items)
    
    def render(self, boresight, maxbeams, epoch_offset=0.0):
        """
        Render into fully specified tiling dictionary, by defining the 
        boresight position and POSIX timestamp at which FBFUSE requested the
        target config.

        Parameters
        ----------
        boresight: str
            katpoint target string
        maxbeams: int
            Maximum number of beams to allocate
        epoch_offset: float, optional
            Make the tiling optimal 'epoch_offset' seconds in the future
            from the moment when FBFUSE reads the target config sensor
            (or in the past if epoch_offset < 0)

        Returns
        -------
        tiling: dict
            Fully specified tiling parameters
        """
        items = self._items
        tg = items['target']
        nbeams = items['nbeams']

        tiling_dict = {
            'target': tg if tg is not None else boresight,
            'tags': ['noapsuse'],
            'nbeams': min(nbeams, maxbeams) if nbeams is not None else maxbeams,
            'overlap': items['overlap'],
            'epoch_offset': epoch_offset
        }
        return tiling_dict

    def to_dict(self):
        return deepcopy(self._items)

    def __str__(self):
        return "TilingTemplate({!s})".format(self._items)

    def __repr__(self):
        return str(self)


class TargetConfigTemplate(object):
    """
    An object to store a *template* for an FBFUSE target configuration.
    """
    def __init__(self):
        # List of valid katpoint target strings
        self.beams = []

        # List of TilingTemplate objects
        self.templates = []

    def clear(self):
        self.beams = []
        self.templates = []

    def is_empty(self):
        return not self.beams and not self.templates

    def add_template(self, temp):
        self.templates.append(temp)

    def add_beam(self, beam):
        try:
            katpoint.Target(beam)
        except:
            raise ValueError("argument is not a valid katpoint Target string")
        self.beams.append(beam)

    def render(self, boresight, maxbeams, epoch_offset=0.0):
        """
        Render into fully specified target configuration dictionary, by 
        defining the boresight position and POSIX timestamp at which FBFUSE
        requested the target config.

        Parameters
        ----------
        boresight: str
            katpoint target string
        maxbeams: int
            Maximum number of beams to allocate. If maxbeams is smaller than the total
            number of beams in the template, individual beams are allocated
            with the highest priority, then tilings in the order that they
            were originally added.
        epoch_offset: float, optional
            Make the tiling optimal 'epoch_offset' seconds in the future
            from the moment when FBFUSE reads the target config sensor
            (or in the past if epoch_offset < 0)

        Returns
        -------
        conf: dict
            Fully specified target configuration parameters
        """
        conf = {
            # NOTE: this is necessary for FBFUSE to detect a configuration change
            # when asking for a retile
            'unique_id': str(uuid.uuid4()),
            'beams': [],
            'tilings': []
        }
        conf['beams'] = self.beams[:maxbeams]
        maxbeams -= len(conf['beams'])

        for t in self.templates:
            if not maxbeams > 0:
                break
            rendered = t.render(boresight, maxbeams, epoch_offset=epoch_offset)
            conf['tilings'].append(rendered)
            maxbeams -= rendered['nbeams']
        return conf

    def to_dict(self):
        conf = {
            'beams': self.beams,
            'tilings': [t.to_dict() for t in self.templates]
            }
        return conf

    @classmethod
    def from_dict(cls, params):
        tgconf = cls()
        beams = params.get('beams', [])
        for beam in beams:
            tgconf.add_beam(beam)

        tiling_params = params.get('tilings', [])
        for tiling_dict in tiling_params:
            tgconf.add_template(TilingTemplate(**tiling_dict))
        return tgconf

    def __str__(self):
        return "TargetConfigTemplate({!s})".format(self.to_dict())

    def __repr__(self):
        return str(self)
