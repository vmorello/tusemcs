#
# TUSE Smart Configuration Authority
# Vincent Morello - 2021
#

import logging
import argparse
import signal
import json
import re
import time
import datetime

from astropy.coordinates import SkyCoord
import astropy.units as uu
import tornado
import coloredlogs
from tornado import gen
from katcp import AsyncDeviceServer, Sensor, Message
from katcp.kattypes import Str, Int, Float, request, return_reply
from katportalclient import KATPortalClient
import katpoint

from tusemcs import __version__
from tusemcs.ca.config_finder import ArrayConfig, find_sb_config, find_target_config, find_priority_sources
from tusemcs.ca.target_config import TargetConfigTemplate, TilingTemplate
from tusemcs.alerts import send_slack_alert


log = logging.getLogger("tusemcs.ca")


class PortalClient(KATPortalClient):
    """
    Convenient KATPortalClient wrapper
    """
    def __init__(self, katportal_url):
        super(PortalClient, self).__init__(katportal_url, on_update_callback=None)

    @gen.coroutine
    def get_usable_antennas(self):
        # This sensor is a JSON-encoded list of 3-lists
        # The first element of each 3-list is a component name
        allocations_sensor = 'subarray_{}_allocations'.format(self.sub_nr)

        # SensorSample object
        allocations_sample = yield self.sensor_value(allocations_sensor, include_value_ts=False)
        allocations = json.loads(allocations_sample.value)

        antennas = set([
            component 
            for component, __, __ in allocations 
            if re.match(r'm\d{3}', component)
        ])

        # dict {sensor_name: SensorSample}
        faulty_samples = yield self.sensor_values(r"m\d{3}_marked_faulty")

        # dict {antenna_name: is_faulty}
        faulty = {
            sname.split('_')[0] : sample.value
            for sname, sample in faulty_samples.items()
        }
        usable_antennas = sorted([ant for ant in antennas if not faulty.get(ant, True)])
        raise gen.Return(usable_antennas)

    @gen.coroutine
    def get_cfreq(self):
        fbf_proxy = yield self.sensor_subarray_lookup('fbfuse', sensor=None)
        cfreq_sensor = '{}_fbfmc_array_{}_centre_frequency'.format(fbf_proxy, self.sub_nr)
        cfreq_sample = yield self.sensor_value(cfreq_sensor)
        cfreq = cfreq_sample.value
        raise gen.Return(cfreq)


@gen.coroutine
def get_subarray_params(katportal_url):
    """
    Get a few current subarray configuration parameters from KATPortal, return them as a dict:
    {
    'usable_antennas': list of antenna names in the array that are not marked faulty
    'cfreq': centre frequency in Hz
    }
    """
    client = PortalClient(katportal_url)

    usable_antennas_future = client.get_usable_antennas()
    cfreq_future = client.get_cfreq()
    usable_antennas = yield usable_antennas_future

    try:
        cfreq = yield cfreq_future
    except:
        # TODO FIXME: Remove this
        cfreq = 1284000000.0
        log.warning("Could not retrieve centre frequency, assuming cfreq = {}".format(cfreq))

    raise gen.Return({
        'usable_antennas': usable_antennas,
        'cfreq': cfreq
    })


def sanitize_antennas_csv(csv, usable_antennas):
    """
    Intersect the comma separated list of antennas from a SBConfig with the list of usable antennas
    currently in the subarray. Returns a comma separated list of antennas (str).
    """
    antenna_list = csv.split(',')
    common = set(antenna_list).intersection(usable_antennas)
    return ','.join(sorted(common))


class ConfigurationAuthority(AsyncDeviceServer):
    """ 
    TUSE Configuration Authority server

    Parameters
    ----------
    host: str
        IP or DNS address
    port: int
        Port on which to listen
    """
    def __init__(self, host, port, retiling=True, retiling_period=600.0):
        # Active array configs for each product
        # dict {product_id: ArrayConfig}
        self.array_configs = {}

        # Active SB configs for each product
        # dict {product_id: dict (SB config parameters)}
        self.sb_configs = {}

        # Active target config templates for each product
        # dict {product_id: TargetConfigTemplate (SB config parameters)}
        self.tg_config_templates = {}

        # Current boresight for each product
        # dict {product_id: katpoint target string}
        self.boresights = {}

        self.retiling = bool(retiling)
        self.retiling_period = float(retiling_period)

        # dict {product_id: PeriodicCallback}
        self.retiling_callbacks = {}

        # Do this last, so that setup_sensors() is called once all members above exist
        super(ConfigurationAuthority, self).__init__(host, port)

    def setup_sensors(self):
        retiling_enabled_sensor = Sensor.boolean(
            'retiling-enabled',
            default=self.retiling,
            description='Whether or not periodic re-tiling is enabled',
            initial_status=Sensor.NOMINAL
            )
        self.add_sensor(retiling_enabled_sensor)

        retiling_period_sensor = Sensor.float(
            'retiling-period',
            default=self.retiling_period,
            description='Re-tiling period in seconds',
            initial_status=Sensor.NOMINAL
            )
        self.add_sensor(retiling_period_sensor)

    def set_schedule_block_config(self, product_id, sbconf):
        self.sb_configs[product_id] = sbconf
        sensor_name = '{}-schedule-block-configuration'.format(product_id)
        if not self.has_sensor(sensor_name):
            sensor = Sensor.string(
                sensor_name,
                description="Current Schedule Block Configuration for {}".format(product_id),
                initial_status=Sensor.UNKNOWN)
            self.add_sensor(sensor)
        self.get_sensor(sensor_name).set_value(json.dumps(sbconf, indent=4))
        self.mass_inform(Message.inform('interface-changed'))

    def set_target_config_template(self, product_id, template):
        self.tg_config_templates[product_id] = template
        sensor_name = '{}-target-configuration-template'.format(product_id)
        if not self.has_sensor(sensor_name):
            sensor = Sensor.string(
                sensor_name,
                description="Current Target Configuration Template for {}".format(product_id),
                initial_status=Sensor.UNKNOWN)
            self.add_sensor(sensor)
        self.get_sensor(sensor_name).set_value(json.dumps(template.to_dict(), indent=4))
        self.mass_inform(Message.inform('interface-changed'))

    def set_boresight(self, product_id, target):
        """
        Set the boresight position for given product_id. Creates the sensor 
        '{PRODUCT_ID}-boresight' if it does not exist.

        Parameters
        ----------
        product_id: str
            Product ID such as 'array_1'
        target: str
            KATPOINT target string specifying the boresight position
        """
        self.boresights[product_id] = target
        sensor_name = '{}-boresight'.format(product_id)
        if not self.has_sensor(sensor_name):
            sensor = Sensor.string(
                sensor_name,
                description="Current boresight position for {}".format(product_id),
                initial_status=Sensor.UNKNOWN)
            self.add_sensor(sensor)
        self.get_sensor(sensor_name).set_value(target)
        self.mass_inform(Message.inform('interface-changed'))

    def refresh_target_config_sensor(self, product_id, source='target-start'):
        """
        Render the currently active TargetConfigTemplate for product_id, and publish the rendered
        target config to the appropriate sensor.

        'source' denotes where the function call came from, either 'target-start', 'retiling loop', 
        or 'manual retile'. The caller is responsible for setting this parameter.
        This is only for logging purposes.

        Notes
        --------
        This function will only work if the following conditions are True, otherwise an Exception
        is raised:
        * There must be an active SB Config for product_id
        * There must be an active TargetConfigTemplate for product_id
        * There must be an active boresight position for product_id
        """
        sname = '{}-beam-position-configuration'.format(product_id)
        if not self.has_sensor(sname):
            sensor = Sensor.string(
                sname,
                description="Target configuration parameters for {}".format(product_id),
                initial_status=Sensor.UNKNOWN
            )
            self.add_sensor(sensor)
            self.mass_inform(Message.inform('interface-changed'))

        sensor = self.get_sensor(sname)

        # Render configuration template
        template = self.tg_config_templates[product_id]
        maxbeams = self.sb_configs[product_id]['coherent-beams-nbeams']
        epoch_offset = self.retiling_period / 2.0 if self.retiling else 0.0
        boresight = self.boresights[product_id]
        tgconf = template.render(boresight, maxbeams, epoch_offset=epoch_offset)
        sensor.set_value(json.dumps(tgconf, indent=4))
        log.debug("Refreshed target configuration for {!r} (source={!r}): {}".format(
            product_id, source, json.dumps(tgconf))
        )

    def configure_retiling(self, enabled, period):
        self.retiling = bool(enabled)
        self.retiling_period = max(abs(float(period)), 1.0)
        self.get_sensor('retiling-enabled').set_value(self.retiling)
        self.get_sensor('retiling-period').set_value(self.retiling_period)

        if self.retiling:
            for product_id in self.retiling_callbacks:
                self.restart_retiling_loop(product_id)
            log.info("Re-tiling enabled, period = {:.1f} seconds".format(self.retiling_period))
        else:
            for product_id in self.retiling_callbacks:
                self.stop_retiling_loop(product_id)
            log.info("Re-tiling disabled")

    def restart_retiling_loop(self, product_id):
        """
        Start retiling loop for product_id. Has no effect if retiling is disabled.
        If a loop is already active for product_id, it is stopped and a new one is created.
        """
        if not self.retiling:
            return

        self.stop_retiling_loop(product_id)
        loop = tornado.ioloop.PeriodicCallback(
            lambda : self.refresh_target_config_sensor(product_id, source='retiling loop'),
            self.retiling_period * 1000.0
        )
        loop.start()
        self.retiling_callbacks[product_id] = loop
        log.debug("Re-tiling loop started for {!r}, period = {:.1f} seconds".format(
            product_id, self.retiling_period)
        )

    def stop_retiling_loop(self, product_id):
        """
        Stop retiling loop for product_id
        """
        loop = self.retiling_callbacks.get(product_id, None)
        if loop is not None:
            loop.stop()
            log.debug("Stopped retiling loop for {!r}".format(product_id))

    @request(Int(), Float(optional=True, default=None))
    @return_reply()
    @gen.coroutine
    def request_configure_retiling(self, req, enabled, period):
        """
        Change re-tiling loop parameters. Enabling re-tiling starts a re-tiling
        loop for every managed product_id, while disabling it stops all
        re-tiling loops.

        Parameters
        ----------
        enabled: int
            1 to enable, 0 to disable re-tiling
        period: float, optional
            Re-tiling period. If not specified, use the current period
        """
        log.info("Received request: {}".format(req.msg))
        try:
            period = self.retiling_period if period is None else period
            self.configure_retiling(enabled, period)
        except Exception as err:
            log.warning(err)
            raise gen.Return(('fail', err))
        raise gen.Return(('ok',))

    @request(Str())
    @return_reply()
    @gen.coroutine
    def request_retile(self, req, product_id):
        """
        Trigger a retile for given product_id

        Parameters
        ----------
        product_id : str
            Product ID such as 'array_1'
        """
        log.info("Received request: {}".format(req.msg))
        self.refresh_target_config_sensor(product_id, source='manual')
        raise gen.Return(('ok',))

    @request(Str(), Str(), Str(), Int(), Str(), Float(default=None, optional=True))
    @return_reply(Str())
    @gen.coroutine
    def request_get_schedule_block_configuration(self, req, product_id, sb_id, proposal_id, 
        nchannels, katportal_url, cfreq_override):
        """
        FBFUSE sends this request to the CA during provisioning of beams. At this time, FBFUSE 
        product has been created to handle the current subarray but is awaiting instructions on 
        how many beams to form and the configuration of the specifics of the beam types.

        Parameters
        ----------
        product_id : str
            The product ID associated with the current subarray configuration
        sb_id : str
            The ID of the currently active schedule block.
        proposal_id : str
            MeerKAT science project ID.
        nchannels : int
            Number of channels in the CBF F-engine data
        katportal_url : str
            The KAT Portal URL that corresponds to the currently active subarray
        cfreq_override: float, optional
            **For testing purposes only.** If specified, do not read the centre frequency from 
            KATPortal, but use this value instead when choosing the correct SBConfig.

        Returns
        -------
        config: str
            JSON-encoded dictionary containing the configuration. Key:value
            pairs are specified in section 3.6.7.1 of the FBFUSE to all 
            subscribers ICD.
        """
        log.info("Received request: {}".format(req.msg))

        subarray_params = yield get_subarray_params(katportal_url)
        if cfreq_override is not None:
            msg = "cfreq_override has been specified. Enforcing cfreq = {}".format(cfreq_override)
            log.warning(msg)
            subarray_params['cfreq'] = cfreq_override

        array_config = ArrayConfig(
            usable_antennas=subarray_params['usable_antennas'],
            cfreq=subarray_params['cfreq'],
            nchannels=nchannels,
            proposal_id=proposal_id,
            sb_id=sb_id,
            target_name=None
        )
        log.debug("Usable antennas: {}".format(len(array_config.usable_antennas)))
        log.debug("Array configuration: {!r}".format(array_config))
        self.array_configs[product_id] = array_config

        # NOTE: The maximum number of beams that FBFUSE can produce depends only on the total
        # number of antennas in the subarray, *not* on the number of antennas included in
        # beamforming. It is thus correct to pick the correct SB config params based on nants_total
        params = find_sb_config(array_config).parameters
        params['coherent-beams-antennas'] = sanitize_antennas_csv(
            params['coherent-beams-antennas'], subarray_params['usable_antennas']
        )
        params['incoherent-beam-antennas'] = sanitize_antennas_csv(
            params['incoherent-beam-antennas'], subarray_params['usable_antennas']
        )
        self.set_schedule_block_config(product_id, params)
        
        out = json.dumps(params)
        log.debug("Returning SB configuration: {}".format(out))
        raise gen.Return(('ok', out))

    @request(Str(), Str())
    @return_reply()
    @gen.coroutine
    def request_target_configuration_start(self, req, product_id, boresight):
        """
        FBFUSE sends this request to the CA upon receipt of the ?target-start request. At this time
        FBFUSE has undergone its schedule block configuration and the full details of the 
        configuration are available to the CA. Upon receiving this request, the CA shall set up a 
        sensor '<product_id>-beam-position-configuration' that can be subscribed to
        by FBFUSE to retrieve beam pointing positions.

        This sensor publishes a JSON-encoded dictionary containing the 
        configuration. Key:value pairs are specified in section 3.6.7.2
        of the FBFUSE to all subscribers ICD.

        Parameters
        ----------
        product_id: str
            The product ID associated with the current subarray configuration
        boresight: str
            A KATPOINT target string specifying the boresight pointing target
            for the subarray
        """
        log.info("Received request: {}".format(req.msg))

        self.set_boresight(product_id, boresight)
        array_config = self.array_configs[product_id]
        boresight_target = katpoint.Target(boresight)
        array_config = array_config._replace(target_name=boresight_target.name)
        log.debug("Array configuration: {!r}".format(array_config))

        ### Pick appropriate target configuration template
        tgconf_template = TargetConfigTemplate.from_dict(
            find_target_config(array_config).parameters
        )

        # Add priority sources to template
        boresight_ktg = katpoint.Target(boresight)
        # .radec() returns ephem.Angle objects
        # when these are cast to float, the output is in radians
        ra, dec = list(map(lambda x: float(x) * uu.rad, boresight_ktg.radec()))
        boresight_skycoord = SkyCoord(ra, dec)

        priority_sources = find_priority_sources(boresight_skycoord, array_config.cfreq)
        for ps in priority_sources:
            log.info("Adding priority source: {}".format(ps.katpoint_string))
            if ps.tiling_nbeams == 1:
                # Followup beams have *lower* priority than regular beams
                tgconf_template.beams.append(ps.katpoint_string)
            else:
                tt = TilingTemplate(
                    target=ps.katpoint_string, nbeams=ps.tiling_nbeams, overlap=ps.tiling_overlap
                )
                # Followup tilings have *higher* priority than regular tilings
                tgconf_template.templates = [tt] + tgconf_template.templates

        # Store target configuration template
        self.set_target_config_template(product_id, tgconf_template)

        # Render template (but only after setting active tgconf template and boresight)
        self.refresh_target_config_sensor(product_id)

        # start/restart retiling loop
        self.restart_retiling_loop(product_id)

        if priority_sources:
            now = datetime.datetime.utcnow().strftime('%Y-%m-%d %H:%M')
            alertmsg = [
                "*Followup of sources starting*",
                "UTC: {}".format(now)
            ]
            for ps in priority_sources:
                alertmsg.append("`{}`".format(ps.katpoint_string))

            # Send slack alert, but later so that the request can reply to FBFUSE first
            # Otherwise FBFUSE might time out and fail to place beams on sky
            ioloop = tornado.ioloop.IOLoop.current()
            ioloop.call_later(5.0, send_slack_alert, alertmsg)

        raise gen.Return(('ok',))


###############################################################################


@gen.coroutine
def shutdown(ioloop):
    log.info("Shutting down")
    ioloop.stop()


@gen.coroutine
def start(ca):
    log.info("Starting ConfigurationAuthority, tusemcs version: {}".format(__version__))
    yield ca.start()
    log.info("Listening at {}, Ctrl-C to terminate server".format(ca.bind_address))


def parse_args():
    parser = argparse.ArgumentParser(
        description="Launch the Configuration Authority",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument(
        '-H', '--host',
        help='Address on which the ConfigurationAuthority will listen.',
        default='0.0.0.0')
    parser.add_argument(
        '-p', '--port',
        help='Port that the ConfigurationAuthority should bind to.',
        type=int,
        default=5003)
    parser.add_argument(
        '-f', '--logfile',
        help='Path to the file where the logs will be stored.',
        default='ca.log')
    parser.add_argument(
        '-l', '--loglevel',
        help='Logging level',
        default='DEBUG')
    return parser.parse_args()


def main():
    logging.getLogger('katcp').setLevel('WARNING')
    logging.getLogger('kat').setLevel('WARNING')
    logging.getLogger('tornado').setLevel('INFO')
    logging.getLogger('urllib3').setLevel('INFO')
    fmt = "[%(levelname)s - %(asctime)s - %(name)s - %(filename)s:%(lineno)s] %(message)s"

    args = parse_args()

    if args.logfile:
        # Redirect ALL logging to file
        root_logger = logging.getLogger()
        root_handler = logging.handlers.TimedRotatingFileHandler(
            args.logfile, when='d', interval=1, utc=True
        )
        root_handler.setFormatter(logging.Formatter(fmt=fmt))
        root_logger.addHandler(root_handler)
    
    coloredlogs.install(fmt=fmt, level=args.loglevel, milliseconds=True)

    # Hook up to SIGINT and SIGTERM so that both ctrl-C and docker stop result in a clean shutdown
    ioloop = tornado.ioloop.IOLoop.current()
    shutdown_func = lambda sig, frame: ioloop.add_callback_from_signal(shutdown, ioloop)
    signal.signal(signal.SIGINT, shutdown_func)
    signal.signal(signal.SIGTERM, shutdown_func)

    ca = ConfigurationAuthority(args.host, args.port)
    ioloop.add_callback(start, ca)
    ioloop.start()


if __name__ == '__main__':
    main()