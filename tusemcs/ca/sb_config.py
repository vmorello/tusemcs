#
# Classes to build and represent schedule block configurations
# Vincent Morello - 2019
#
import re
from copy import deepcopy

import schema


class Antenna(object):
    """
    Simple object to represent an antenna

    Parameters
    ----------
    number: int
        Valid antenna number
    """
    # Min and max antenna numbers, both inclusive
    MIN_NUMBER = 0
    MAX_NUMBER = 63

    def __init__(self, number):
        if not isinstance(number, int):
            raise ValueError("Antenna number must be an int")
        if not self.MIN_NUMBER <= number <= self.MAX_NUMBER:
            raise ValueError("Invalid antenna number: {}".format(number))
        self._number = number

    @property
    def name(self):
        return "m{:03d}".format(self.number)

    @property
    def number(self):
        return self._number

    @classmethod
    def valid_name(cls, name):
        return bool(re.match(r"m\d{3}$", name))

    @classmethod
    def from_name(cls, name):
        if not cls.valid_name(name):
            raise ValueError("Invalid antenna name: {}".format(name))
        number = int(name[1:])
        return cls(number)
    
    def __hash__(self):
        return self.number

    def __eq__(self, other):
        return self.number == other.number

    def __str__(self):
        return "Antenna({!r})".format(self.name)

    def __repr__(self):
        return str(self)


class AntennaSet(set):
    """
    A set of Antenna objects with extra convenience methods

    Parameters
    ----------
    ants: iterable, optional
        List or iterable of Antenna objects. If not specified, an empty
        antenna set is returned
    """
    def __init__(self, ants=[]):
        super(AntennaSet, self).__init__(ants)
    
    @classmethod
    def from_csv(cls, csv):
        """ 
        Create AntennaSet from CSV string
        
        Parameters
        ----------
        csv: str
            Comma separated list of antenna names. Repetitions are allowed
            and safely ignored
        """
        return cls([Antenna.from_name(s.strip()) for s in csv.split(',')])

    @classmethod
    def valid_csv(cls, csv):
        try:
            cls.from_csv(csv)
            return True
        except:
            return False

    @classmethod
    def from_numbers(cls, numbers):
        return cls([Antenna(n) for n in numbers])

    @classmethod
    def all(cls):
        return cls.from_numbers(range(Antenna.MIN_NUMBER, Antenna.MAX_NUMBER))
    
    @classmethod
    def core(cls):
        raise NotImplementedError

    def to_csv(self):
        return ','.join(sorted([ant.name for ant in self]))


class SBConfig(object):
    """ 
    A dict wrapper representing a schedule block configuration

    Parameters
    ----------
    items: dict
        Dictionary with SB configuration

    Raises
    ------
    schema.SchemaError: if items does not follow the expected schema
    """
    SCHEMA = schema.Schema({
        'coherent-beams-nbeams': schema.And(int, lambda n: n > 0),
        'coherent-beams-tscrunch': schema.And(int, lambda n: n > 0),
        'coherent-beams-fscrunch': schema.And(int, lambda n: n > 0),
        'coherent-beams-antennas': AntennaSet.valid_csv,
        'coherent-beams-granularity': schema.And(int, lambda n: n > 0),
        'incoherent-beam-tscrunch': schema.And(int, lambda n: n > 0),
        'incoherent-beam-fscrunch': schema.And(int, lambda n: n > 0),
        'incoherent-beam-antennas': AntennaSet.valid_csv,
        'centre-frequency': schema.And(schema.Or(int, float), lambda n: n > 0),
        'bandwidth': schema.And(schema.Or(int, float), lambda n: n > 0),
    })

    def __init__(self, items):
        self.validate(items) # raise exception if invalid
        self._items = items    

    @property
    def aset_incoherent(self):
        return AntennaSet.from_csv(self._items['incoherent-beam-antennas'])

    @property
    def aset_coherent(self):
        return AntennaSet.from_csv(self._items['coherent-beams-antennas'])

    @property
    def nbeams(self):
        """ Number of coherent beams """
        return self._items['coherent-beams-nbeams']

    def to_dict(self, omit_cfreqbw=False):
        """ 
        Format as a dictionary to be returned to FBFUSE. If omit_cfreqbw is 
        True, then the keys 'centre-frequency' and 'bandwidth' are omitted from
        the output; FBFUSE will use the values read from the CAM portal.
        """
        items = deepcopy(self._items)
        if omit_cfreqbw:
            items.pop('centre-frequency')
            items.pop('bandwidth')
        return items

    def update(self, params):
        """
        Update in-place with given parameters, like dict.update() but
        will reject any updates incompatible with the schema and raise
        an exception in that case.

        Parameters
        ----------
        params: dict
            Dictionary of parameters to update, does not have to specify all
            parameters.
        """
        newitems = deepcopy(self._items)
        newitems.update(params)
        self.validate(newitems) # raise exception if update is invalid
        self._items = newitems

    def intersect_antennas(self, ants):
        """
        Returns a new SBConfig object where *both* sets of antennas
        have been intersected with 'ants'

        Parameters
        ----------
        ants: AntennaSet
            Set of working antennas

        Returns
        -------
        sbconf: SBConfig
            A new SBConfig object
        """
        newitems = deepcopy(self._items)
        newitems['incoherent-beam-antennas'] = self.aset_incoherent.intersection(ants).to_csv()
        newitems['coherent-beams-antennas'] = self.aset_coherent.intersection(ants).to_csv()
        return SBConfig(newitems)

    @classmethod
    def validate(cls, items):
        """
        Check if SB configuration dictionary 'items' is valid, raise an
        exception otherwise
        """
        return cls.SCHEMA.validate(items)

    def __str__(self):
        return "SBConfig({!s})".format(self._items)

    def __repr__(self):
        return "SBConfig({!r})".format(self._items)
