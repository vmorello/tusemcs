# -*- coding: utf-8 -*-
#
#   2018 Fabian Jankowski
#   IPMI sensor related helpers.
#

from __future__ import print_function
import subprocess32 as subprocess
import shlex
import os.path

TIMEOUT = 5.0


def get_ipmi_sensor(name):
    """
    Query status of an IPMI sensor.

    Parameters
    ----------
    name : str
        The name of the IPMI sensor to query.

    Returns
    -------
    status : str or None
        The current status of the sensor, or None on failure to read the
        sensor.
    """

    script = os.path.join(os.path.dirname(__file__),
                          "../scripts/get_ipmi_sensor.sh")
    script = os.path.abspath(script)

    command = "{0} '{1}'".format(script, name)
    args = shlex.split(command)

    try:
        status = subprocess.check_output(args, timeout=TIMEOUT)
    except subprocess.CalledProcessError as e:
        raise RuntimeError("Could not read the IPMI sensor: " +
                           "{0}, {1}".format(name, str(e)))
    except subprocess.TimeoutExpired as e:
        raise RuntimeError("Timeout exceeded reading IPMI sensor: " +
                           "{0}, {1}".format(name, str(e)))

    # strip newline and whitespaces
    status = status.strip()

    return status
