import logging
import argparse
import signal
import traceback
import json
import os
import itertools
import ast

import tornado
from tornado import gen
import coloredlogs
from enum import Enum
from katcp import AsyncDeviceServer, Sensor
from katcp.kattypes import request, return_reply, Str, Int
import katpoint
import psutil

from tusemcs import __version__
from tusemcs.general_helpers import build_info
from tusemcs.node_config import NodeConfig
from tusemcs.pipeline_interface import start_pipeline, stop_pipeline
from tusemcs.pipeline_switch import PipelineSwitch
from tusemcs.redis_beam_monitor import RedisBeamMonitor
from tusemcs.node_monitoring import (
    gpu_stats, available_disk_space, candidate_stats, container_logs, aa_hits_last_block,
    missing_heap_fraction_last_buffer, get_bpstats_encoded, iqrm_masked_fraction
)
from tusemcs.candidate_snapshots import get_last_candidate_snapshot
from tusemcs.spccl import SpcclWatcher


log = logging.getLogger("tusemcs.node_controller")


class State(Enum):
    """ Enum with possible states of the NodeController """
    idle, preparing, started, stopping, error = range(5)


class PipelineState(Enum):
    """ Enum with possible states of the pipeline """
    stopped, starting, running, stopping, error = range(5)


class WrongState(Exception):
    """ Raised when not in the right state to process a given request """
    pass


class NodeController(AsyncDeviceServer):
    """
    Controller class for compute nodes.

    Parameters
    ----------
    host: str
        The IP address on which the server should listen.
    port: int
        The port that the server should bind to.
    config: NodeConfig or dict
        The dict resulting from parsing node_controller.yml
    """
    # VERSION_INFO and BUILD_INFO are required
    # See: http://katcp-python.readthedocs.io/en/latest/tutorial.html
    # Section "writing your own server"
    VERSION_INFO = ("tuse-mcs-api", 0, 1)
    BUILD_INFO = build_info()

    # Default time to wait between issuing a pipeline start command, and the
    # actual launching of the pipeline. Guards against fast FBFUSE sensor 
    # changes that might trigger start and stops in rapid succession
    PIPELINE_START_DELAY = 2.0 # seconds

    def __init__(self, host, port, config):
        self.config = NodeConfig(config)

        # List of PeriodicCallbacks that we are running in the background, to update system usage 
        # sensors. They are created when NodeController.start() is called.
        self.periodic_callbacks = []

        # Active schedule block details, set at ?capture-start. If the MasterController failed to 
        # retrieve the SB details, this will be set to an empty dict
        self.sb_details = {}

        # Product metadata common to all nodes. Set at ?capture-start
        self.product_meta = {}

        # Dictionary {multicast_address: list of FBFUSE beam ids}. 
        # FBFUSE beam ids are strings such as 'cfbf00042'
        # Set at ?capture-start
        self.mca_mapping = {}

        # Nested dictionary with all the beam data that gets updated
        # during observing. This gets passed to the pipeline interface because
        # we want that metadata saved in every pipeline run
        self.beam_meta = {
            'positions': {},        # dict {beam_id: katpoint.Target}
            'shape': {},            # dict
            'ca_target_request': {} # dict with the CA target config
        }

        # Boolean flag that gets updated during observing, based on the values of FBFUSE's
        # data-suspect sensors (one per beam type, coherent and incoherent) 
        # This will be True if any beam processed by this node currently contains invalid data.
        self.data_suspect = False

        # Pipeline start/stop switch that implements a delayed start to guard against quick changes
        # of FBFUSE beam position sensors.
        self.pipeline_switch = PipelineSwitch(self.start_pipeline, self.stop_pipeline)

        # RedisBeamMonitor object
        self.beam_monitor = None

        # datetime object: last UTC start of the pipeline
        # last_utc_start matches the directory name created by the pipeline for output
        self.last_utc_start = None

        # Boolean flag that can be set through a request. 
        # If False, pipeline is not allowed to start, no matter what.
        self.pipeline_enabled = True

        # Object that updates the spccl candidates sensor
        # Created at ?capture-start, deleted at ?capture-stop
        self.spccl_watcher = None

        self.state = None
        self.pipeline_state = None
        self.bpmon_enabled = None  # set automatically by start_pipeline()

        # Initializing the parent AsyncDeviceServer calls setup_sensors()
        super(NodeController, self).__init__(host, port)

        # Once the associated sensors exist, we can update the states
        self.set_state(State.idle)
        self.set_pipeline_state(PipelineState.stopped)
        self.set_bpmon_enabled(True)

        # Create RedisBeamMonitor, it will be started at ?capture-start, stopped at ?capture-stop
        self.create_beam_monitor()

    def setup_sensors(self):
        """ 
        Set up monitoring sensors. AsyncDeviceServer derived classes must implement this method. 
        """
        node_state = Sensor.discrete(
            "node-state",
            description="Node state",
            params=[s.name for s in State],
            default=State.idle.name,
            initial_status=Sensor.NOMINAL)
        self.add_sensor(node_state)

        pipeline_state = Sensor.discrete(
            "pipeline-state",
            description="Pipeline state",
            params=[s.name for s in PipelineState],
            default=PipelineState.stopped.name,
            initial_status=Sensor.NOMINAL)
        self.add_sensor(pipeline_state)

        beam_ids = Sensor.string(
            "beam-ids",
            description="Comma separated list of FBFUSE beam numbers assigned to this node. Incoherent beam is denoted by IB",
            default="",
            initial_status=Sensor.NOMINAL)
        self.add_sensor(beam_ids)

        cpu_usage_percent = Sensor.float(
            "cpu-usage-percent",
            description="Current system-wide CPU utilization as a percentage",
            default=0.0,
            initial_status=Sensor.UNKNOWN
            )
        self.add_sensor(cpu_usage_percent)

        ram_usage_percent = Sensor.float(
            "ram-usage-percent",
            description="Used RAM in percentage of total, where used = total - available",
            default=0.0,
            initial_status=Sensor.UNKNOWN
            )
        self.add_sensor(ram_usage_percent)

        disk_space_available = Sensor.float(
            "disk-space-available",
            description="Available disk space on the partition where the data product output directory lies",
            unit='GB',
            default=0.0,
            initial_status=Sensor.UNKNOWN
            )
        self.add_sensor(disk_space_available)

        last_utc_start = Sensor.string(
            "last-pipeline-utc-start",
            description="Last UTC datetime at which the pipeline was started. This UTC will match the name of the pipeline output directory created.",
            default="",
            initial_status=Sensor.NOMINAL
            )
        self.add_sensor(last_utc_start)

        candidates_produced = Sensor.integer(
            "candidates-produced",
            description="Number of candidates produced since the last time the pipeline was launched",
            unit='',
            default=0,
            initial_status=Sensor.UNKNOWN
            )
        self.add_sensor(candidates_produced)

        candidate_lag = Sensor.float(
            "candidate-lag",
            description=(
                "Time difference between the file modification timestamp of the most recent"
                " candidate and its data timestamp, only considering candidate files produced in"
                " the current pipeline run."),
            unit='s',
            default=0,
            initial_status=Sensor.UNKNOWN
        )
        self.add_sensor(candidate_lag)

        aa_hits_recent = Sensor.float(
            "aa-hits-recent",
            description="Total hits reported by AA in the last search block",
            unit='',
            default=0,
            initial_status=Sensor.UNKNOWN
        )
        self.add_sensor(aa_hits_recent)

        mkrecv_missing_heap_fraction_recent = Sensor.float(
            "mkrecv-missing-heap-fraction-recent",
            description="Fraction of missing heaps reported by mkrecv in the last captured buffer",
            unit='',
            default=0.0,
            initial_status=Sensor.UNKNOWN
        )
        self.add_sensor(mkrecv_missing_heap_fraction_recent)

        pipeline_enabled = Sensor.boolean(
            "pipeline-enabled",
            description="Whether or not the pipeline is allowed to run",
            default=True,
            initial_status=Sensor.NOMINAL    
        )
        self.add_sensor(pipeline_enabled)

        bpmon_enabled = Sensor.boolean(
            "bpmon-enabled",
            description="Whether or not the bandpass monitor has been enabled in the last pipeline start",
            default=True,
            initial_status=Sensor.NOMINAL    
        )
        self.add_sensor(bpmon_enabled)

        bandpass_mean = Sensor.string(
            "bandpass-mean",
            description="Bandpass mean encoded as a string. This represents a float16 numpy array with shape (nbeams, nchans)",
            default="",
            initial_status=Sensor.UNKNOWN)
        self.add_sensor(bandpass_mean)

        bandpass_std = Sensor.string(
            "bandpass-std",
            description="Bandpass standard deviation encoded as a string. This represents a float16 numpy array with shape (nbeams, nchans)",
            default="",
            initial_status=Sensor.UNKNOWN)
        self.add_sensor(bandpass_std)

        gpu_usage_percent = Sensor.float(
            "gpu-usage-percent",
            description="Current system-wide GPU utilization",
            default=0.0,
            initial_status=Sensor.UNKNOWN
            )
        self.add_sensor(gpu_usage_percent)

        gpu_memory_used = Sensor.float(
            "gpu-memory-used",
            description="Total GPU memory used in MB. This is a sum across all GPUs.",
            default=0.0,
            initial_status=Sensor.UNKNOWN
            )
        self.add_sensor(gpu_memory_used)

        gpu_power_draw = Sensor.float(
            "gpu-power-draw",
            description="Total power drawn by GPUs.",
            default=0.0,
            initial_status=Sensor.UNKNOWN
            )
        self.add_sensor(gpu_power_draw)

        gpu_highest_temperature = Sensor.float(
            "gpu-highest-temperature",
            description="Temperature of the hottest GPU.",
            default=0.0,
            initial_status=Sensor.UNKNOWN
            )
        self.add_sensor(gpu_highest_temperature)

        beam_positions = Sensor.string(
            "beam-positions",
            description="Positions of the beams being processed, as JSON-encoded dictionary. \
                Keys are beam IDs, values are katpoint target strings (target.format_katcp()).",
            default=json.dumps({}),
            initial_status=Sensor.NOMINAL
            )
        self.add_sensor(beam_positions)

        pipeline_config = Sensor.string(
            "pipeline-config",
            description="Configuration parameters of the pipeline as a JSON-encoded dictionary."
                " Reflects the pipeline section of the NodeController configuration file",
            default=json.dumps(self.config['pipeline']),
            initial_status=Sensor.NOMINAL
        )
        self.add_sensor(pipeline_config)

        last_candidate_snapshot = Sensor.string(
            "last-candidate-snapshot",
            description="Base64-encoded PNG snapshot of the last candidate produced during the"
                " current pipeline run. The value timestamp of this sensor is the modification"
                " timestamp of the last candidate file. If no candidates have been produced yet,"
                " it will be an empty string with a value timestamp of 0.",
            default="",
            initial_status=Sensor.NOMINAL
        )
        self.add_sensor(last_candidate_snapshot)

        iqrm_masked_fraction_recent = Sensor.float(
            "iqrm-masked-fraction",
            description="Fraction of channels masked by IQRM recently",
            default=0.0,
            initial_status=Sensor.NOMINAL
        )
        self.add_sensor(iqrm_masked_fraction_recent)

        spccl_candidates = Sensor.string(
            "spccl-candidates",
            description="JSON-encoded list of spccl candidates produced in the last hour",
            default="[]",
            initial_status=Sensor.NOMINAL
        )
        self.add_sensor(spccl_candidates)

    def start(self):
        """
        Start the server. We override the parent start() method, so that we can setup periodic
        callback(s).
        """
        super(NodeController, self).start()

        system_usage_updater = tornado.ioloop.PeriodicCallback(
            self.update_system_usage_sensors, 
            3000.0
        )
        self.periodic_callbacks.append(system_usage_updater)

        disk_space_checker = tornado.ioloop.PeriodicCallback(
            self.check_disk_space, 
            20000.0
        )
        self.periodic_callbacks.append(disk_space_checker)

        candidate_stats_updater = tornado.ioloop.PeriodicCallback(
            self.update_candidate_stats_sensors,
            10000.0
        )
        self.periodic_callbacks.append(candidate_stats_updater)

        pipeline_stats_sensors_updater = tornado.ioloop.PeriodicCallback(
            self.update_pipeline_stats_sensors, 
            8000.0
        )
        self.periodic_callbacks.append(pipeline_stats_sensors_updater)

        bandpass_stats_updater = tornado.ioloop.PeriodicCallback(
            self.update_bandpass_sensors,
            5000.0
        )
        self.periodic_callbacks.append(bandpass_stats_updater)

        snapshot_updater = tornado.ioloop.PeriodicCallback(
            self.update_snapshot_sensor,
            15000.0
        )
        self.periodic_callbacks.append(snapshot_updater)

        spccl_updater = tornado.ioloop.PeriodicCallback(
            self.update_spccl_candidates,
            15000.0
        )
        self.periodic_callbacks.append(spccl_updater)

        for cb in self.periodic_callbacks:
            cb.start()

    @gen.coroutine
    def stop(self):
        """ 
        Stop the server. We override the parent stop() method, so that we can cleanly stop any
        periodic callbacks.
        """
        for cb in self.periodic_callbacks:
            cb.stop()
        self.periodic_callbacks = []
        super(NodeController, self).stop()

    def check_state(self, expected_state):
        if not self.state == expected_state:
            msg = "Wrong state: currently {!r}, needs to be {!r}".format(
                self.state.name, expected_state.name)
            raise WrongState(msg)

    def set_state(self, state):
        self.state = state
        self.get_sensor('node-state').set_value(self.state.name)
        log.debug("Set state = {}".format(self.state.name))

    def set_pipeline_state(self, state):
        self.pipeline_state = state
        self.get_sensor('pipeline-state').set_value(self.pipeline_state.name)
        log.debug("Set pipeline_state = {}".format(self.pipeline_state.name))

    def set_last_utc_start(self, utc):
        self.last_utc_start = utc
        self.get_sensor('last-pipeline-utc-start').set_value(str(utc))

    def set_sb_details(self, items):
        self.sb_details = items
        text = json.dumps(items, indent=4)
        log.debug("Set sb_details = \n{}".format(text))

    def clear_sb_details(self):
        self.sb_details = {}
        log.debug("Cleared sb_details")

    def set_product_meta(self, items):
        self.product_meta = items
        text = json.dumps(items, indent=4)
        log.debug("Set product_meta = \n{}".format(text))

    def clear_product_meta(self):
        self.product_meta = {}
        log.debug("Cleared product_meta")

    def clear_beam_meta(self):
        self.beam_meta['positions'] = {}
        self.get_sensor('beam-positions').set_value(json.dumps({}))
        self.beam_meta['shape'] = {}
        self.beam_meta['ca_target_request'] = {}
        log.debug('Cleared beam meta')

    def set_bpmon_enabled(self, val):
        self.bpmon_enabled = bool(val)
        self.get_sensor('bpmon-enabled').set_value(self.bpmon_enabled)

    def set_mca_mapping(self, mapping):
        self.mca_mapping = mapping
        
        # Update beam-ids sensor
        def short_beam_id(fbfuse_id):
            flag, num = fbfuse_id.split('fbf')
            num = int(num)
            if flag == 'i':
                return 'IB'
            elif flag == 'c':
                # Pad for readability in cluster monitor
                return "{:3d}".format(num) 
            else:
                raise ValueError("Invalid FBFUSE beam ID: {}".format(fbfuse_id))

        csv = ','.join(map(short_beam_id, self.beam_ids))
        self.get_sensor('beam-ids').set_value(csv)

        # IMPORTANT: Initialise all beam positions to 'unset'
        for beam_id in self.beam_ids:
            # NOTE: must create the key beam_id before calling update_beam_position(), which checks
            # for the presence of said key first
            self.beam_meta['positions'][beam_id] = None
            self.update_beam_position(beam_id, 'unset, radec, 0:00:00.00, 0:00:00.0')

        log.debug("Set mca_mapping = \n{}".format(mapping))

    def clear_mca_mapping(self):
        self.mca_mapping = {}
        self.get_sensor('beam-ids').set_value("")
        log.debug("Cleared mca_mapping")

    def set_pipeline_enabled(self, flag):
        """ 
        Set the pipeline_enabled value. flag must be a boolean. If after setting the flag, the 
        conditions to start (or stop) the pipeline are met, then it will be started (or stopped) 
        immediately.        
        """
        if not isinstance(flag, bool):
            raise ValueError("flag must be of type bool")

        self.pipeline_enabled = flag
        self.get_sensor("pipeline-enabled").set_value(flag)
        log.info("pipeline_enabled is now {}".format(self.pipeline_enabled))

        if flag:
            self.pipeline_switch.delayed_start(wait=0.0)
        else:
            self.pipeline_switch.stop()

    def update_system_usage_sensors(self):
        """ Update the CPU and RAM usage sensors """
        self.get_sensor('cpu-usage-percent').set_value(psutil.cpu_percent())
        self.get_sensor('ram-usage-percent').set_value(psutil.virtual_memory().percent)

        try:
            stats = gpu_stats()
            self.get_sensor('gpu-usage-percent').set_value(stats['usage_percent'])
            self.get_sensor('gpu-memory-used').set_value(stats['memory_used_mb'])
            self.get_sensor('gpu-power-draw').set_value(stats['power_draw'])
            self.get_sensor('gpu-highest-temperature').set_value(stats['highest_temperature'])
        except Exception as err:
            log.warning("Failed to update GPU monitoring sensors: {}".format(err))

    def check_disk_space(self):
        """ 
        Update the free disk space sensor, and disable the pipeline if we get below a certain 
        threshold. This will immediately stop the pipeline if it is running. 
        """
        try:
            gb_free = available_disk_space() / 1024.0 ** 3
            self.get_sensor('disk-space-available').set_value(gb_free)
        except Exception as err:
            log.warning("Failed to check disk space: {}".format(err))
            return

        minspace = self.config.dget('control.min_space') # GB
        if gb_free < minspace:
            msg = "Free disk space is now below {:.2f} GB threshold, disabling pipeline".format(
                minspace)
            log.warning(msg)
            self.set_pipeline_enabled(False)

    def update_candidate_stats_sensors(self):
        if self.last_utc_start is None:
            return
        
        try:
            entries = candidate_stats(self.last_utc_start)
            ncands = len(entries)
            lag = 0.0
            if entries:
                # candidate-lag is measured on the most recently written candidate file
                entries = sorted(entries, key=lambda e: e['mtime'], reverse=True)
                lag = entries[0]['lag']
            self.get_sensor('candidates-produced').set_value(ncands)
            self.get_sensor('candidate-lag').set_value(lag)
        except Exception as err:
            log.warning("Failed to update candidate stats sensors: {}".format(err))

    def update_pipeline_stats_sensors(self):
        if not self.pipeline_state == PipelineState.running:
            return

        container = self.config.dget('pipeline.container')
        try:
            loglines = container_logs(container, seconds=10.0).split('\n')
        except Exception as err:
            log.warning("Failed to get logs of pipeline container: {}".format(err))
            return

        # Update sensors only if a new value is available
        aahits = aa_hits_last_block(loglines)
        if aahits is not None:
            self.get_sensor('aa-hits-recent').set_value(aahits)

        mhfrac = missing_heap_fraction_last_buffer(loglines)
        if mhfrac is not None:
            self.get_sensor('mkrecv-missing-heap-fraction-recent').set_value(mhfrac)

        iqrm_avg = iqrm_masked_fraction(loglines)
        if iqrm_avg is not None:
            self.get_sensor('iqrm-masked-fraction').set_value(iqrm_avg)

    def update_bandpass_sensors(self):
        # Must check that bandpass monitoring is running. Otherwise we may end up reading a 
        # "stale" shared buffer
        if not self.pipeline_state == PipelineState.running \
            or not self.state == State.started \
            or not self.bpmon_enabled:
            return
        
        try:
            # Number of channels in the shared buffer is that of the data sent by FBFUSE.
            # 'nchannels' in product meta denotes the number of channels in the CBF data
            fbfuse_fscrunch = self.product_meta['coherent_beam_fscrunch'] if self.beams_all_coherent \
                else self.product_meta['incoherent_beam_fscrunch']
            nchans = self.product_meta['nchannels'] // fbfuse_fscrunch
            nbeams = len(self.beam_ids)
            emean, estd = get_bpstats_encoded(nbeams, nchans)
            self.get_sensor('bandpass-mean').set_value(emean)
            self.get_sensor('bandpass-std').set_value(estd)
        except Exception as err:
            log.warning("Failed to update bandpass sensors: {}".format(err))

    def update_snapshot_sensor(self):
        if self.last_utc_start is None:
            return
        
        try:
            encoded, mtime = get_last_candidate_snapshot(self.last_utc_start)
            # NOTE: if 'encoded' is an empty string, it means that there is no
            # candidate filterbank to plot in the current UTC directory. In this case,
            # we don't want to erase the last published snapshot
            if encoded:
                # Explicitly set the value timestamp of the sensor to the modification timestamp
                # of the file.
                self.get_sensor('last-candidate-snapshot').set_value(encoded, timestamp=mtime)
        except Exception as err:
            log.warning("Failed to update snapshot sensor: {}".format(err))

    def update_spccl_candidates(self):
        if self.spccl_watcher:
            try:
                self.spccl_watcher.update()
                self.get_sensor('spccl-candidates').set_value(self.spccl_watcher.sensor_json())
            except Exception as err:
                log.warning("Could not update spccl candidate sensor: {}".format(err))

    def log_request(self, req):
        """ Emit a log message that a request has been received. req must be a katcp.Message """
        log.info("Received request: {}".format(req.msg))

    @property
    def beam_ids(self):
        """ Returns the list of all FBFUSE beam IDs assigned to this node """
        return sorted(list(itertools.chain.from_iterable(self.mca_mapping.values())))

    @property
    def beams_all_coherent(self):
        """ Whether or not all assigned beam IDs are coherent beam IDs """
        return all([b.startswith('cfbf') for b in self.beam_ids])

    @property
    def beam_positions(self):
        return self.beam_meta['positions']

    @property
    def beams_valid(self):
        """ 
        Returns True if all beam positions are currently on sky. If any
        beam has an 'unset' position, return False 
        """
        if not self.beam_positions:
            return False
        return not any([target.name == 'unset' for target in self.beam_positions.values()])

    @property
    def pipeline_start_allowed(self):
        condition = \
            self.state == State.started \
            and self.pipeline_enabled \
            and self.pipeline_state == PipelineState.stopped \
            and self.beams_valid \
            and self.beam_meta['shape'] \
            and self.beam_meta['ca_target_request'] \
            and not self.data_suspect
        return condition

    @property
    def pipeline_stop_allowed(self):
        return self.pipeline_state not in (PipelineState.stopped, PipelineState.stopping)

    def create_beam_monitor(self):
        ioloop = tornado.ioloop.IOLoop.current()

        def threadsafe(func):
            def wrapped(*args, **kwargs):
                ioloop.add_callback(func, *args, **kwargs)
            return wrapped        

        # The callbacks passed to the RedisBeamMonitor constructor will be called from a separate 
        # thread, in which the actual listening to redis updates is performed. These functions 
        # should not be called directly, but rather via ioloop.add_callback()
        self.beam_monitor = RedisBeamMonitor(
            beampos_callback=threadsafe(self.update_beam_position),
            beamshape_callback=threadsafe(self.update_beam_shape),
            suspect_callback=threadsafe(self.update_data_suspect),
            tgconf_callback=threadsafe(self.update_ca_target_request)
        )

    def create_spccl_watcher(self):
        # NOTE: relative beam order must match that assigned by the pipeline interface
        # beam_ids is a list of FBFUSE ids (strings), sorting them in lexicographical order works
        def numeric_beam_id(fbfuse_id):
            # NOTE: IB is given -1
            return int(fbfuse_id[4:]) if fbfuse_id.startswith('c') else -1
        bnum_mapping = {
            n : numeric_beam_id(bid)
            for n, bid in enumerate(sorted(self.beam_ids))
        }
        watcher = SpcclWatcher(bnum_mapping=bnum_mapping)
        self.spccl_watcher = watcher

    def update_beam_position(self, beam_id, new_position):
        """ 
        Update position of given beam id, and start/stop the pipeline as necessary. This function 
        is called by the BeamMonitor.

        Parameters
        ----------
        beam_id : str
            FBFUSE beam ID for which to upate the position
        new_position : str
            katpoint Target *string* containing the new beam coordinates.
        """
        pos = katpoint.Target(new_position)
        self.beam_meta['positions'][beam_id] = pos

        sensor_value = json.dumps({
            bid: tg.format_katcp()
            for bid, tg in self.beam_positions.items()
        })
        self.get_sensor('beam-positions').set_value(sensor_value)

        if not self.beams_valid:
            self.pipeline_switch.stop()
        else:
            log.info("Beam positions are now valid.")
            self.pipeline_switch.delayed_start(wait=self.PIPELINE_START_DELAY)

    def update_data_suspect(self, value):
        """ 
        Update the data_suspect flag, based on sensor updates received from FBFUSE, and 
        tart / stop the pipeline as necessary. This function is called by the BeamMonitor.
        
        Parameters
        ----------
        value: bool
            Whether or not the data of any of the beams being processed is
            suspect
        """
        self.data_suspect = bool(value)
        if value:
            self.pipeline_switch.stop()
        else:
            self.pipeline_switch.delayed_start(wait=self.PIPELINE_START_DELAY)

    def update_beam_shape(self, shape_params):
        """
        Update beam shape parameters. This will be called on a target change or re-tile. Triggers a
        pipeline stop followed by a delayed start. This function is called by the BeamMonitor.

        Parameters
        ----------
        shape_params : dict
            Beam shape parameters read from FBFUSE sensor 'coherent_beam_shape'
        """
        self.beam_meta['shape'] = shape_params
        # Avoid triggering a pipeline restart on IB node
        if self.beams_all_coherent:
            self.pipeline_switch.stop()
            self.pipeline_switch.delayed_start(wait=self.PIPELINE_START_DELAY)

    def update_ca_target_request(self, params):
        """
        Update target configuration information, as was requested by the active FBFUSE 
        Configuration Authority (CA). Triggers a pipeline stop followed by a delayed start.
        This function is called by the BeamMonitor.

        Parameters
        ----------
        params : dict
            Target config parameters read from FBFUSE sensor 'ca_target_request'
        """
        self.beam_meta['ca_target_request'] = params
        # Avoid triggering a pipeline restart on IB node
        if self.beams_all_coherent:
            self.pipeline_switch.stop()
            self.pipeline_switch.delayed_start(wait=self.PIPELINE_START_DELAY)

    def start_pipeline(self):
        """
        Trigger a delayed pipeline start, if allowed. NOTE: Do NOT call this directly, use
        self.pipeline_switch.delayed_start() instead, which wraps this function.
        """
        if not self.pipeline_start_allowed:
            log.debug("Starting pipeline not currently allowed")
            return

        log.info("Starting pipeline")
        self.set_pipeline_state(PipelineState.starting)

        try:
            dry_run = self.config.dget('pipeline.dry-run')
            utc_start, bpe = start_pipeline(self.config, self.mca_mapping, 
                self.product_meta, self.beam_meta, self.sb_details, dry_run=dry_run)
            self.set_last_utc_start(utc_start)
            self.set_bpmon_enabled(bpe)
            self.spccl_watcher.set_pipeline_utc(utc_start)
        except Exception as err:
            self.set_pipeline_state(PipelineState.error)
            log.error(err)
            log.info(traceback.format_exc())
        else:
            self.set_pipeline_state(PipelineState.running)

    def stop_pipeline(self):
        """
        Trigger a delayed pipeline stop, if allowed. NOTE: Do NOT call this directly, use
        self.pipeline_switch.stop() instead, which wraps this function.
        """
        if not self.pipeline_stop_allowed:
            log.debug("Stopping pipeline not currently allowed")
            return

        log.info("Stopping pipeline")
        self.set_pipeline_state(PipelineState.stopping)
        
        try:
            stop_pipeline(self.config, self.last_utc_start)
        except Exception as err:
            self.set_pipeline_state(PipelineState.error)
            log.error(err)
            log.info(traceback.format_exc())
        else:
            self.set_pipeline_state(PipelineState.stopped)

    def capture_start(self, sb_details, product_meta, mca_mapping):
        """ 
        See request_capture_start()
        """
        self.check_state(State.idle)

        self.set_state(State.preparing)

        # Set all the metadata that remains constant throughout the SB
        self.set_sb_details(json.loads(sb_details))
        self.set_product_meta(json.loads(product_meta))
        self.set_mca_mapping(json.loads(mca_mapping))

        # Start listening to beam meta updates
        self.beam_monitor.set_beam_ids(self.beam_ids)
        self.beam_monitor.start()
        self.create_spccl_watcher()

        self.set_state(State.started)
        self.pipeline_switch.delayed_start(wait=0.0)

    def capture_stop(self, force=False):
        """ 
        See request_capture_stop()
        """
        if not force:
            self.check_state(State.started)

        self.set_state(State.stopping)
        
        # Stop listening to beam meta updates
        self.beam_monitor.stop()
        self.beam_monitor.set_beam_ids([])

        # Stop pipeline
        self.pipeline_switch.stop()

        # Clear all metadata
        self.clear_sb_details()
        self.clear_beam_meta()
        self.clear_product_meta()
        self.clear_mca_mapping()

        # Clear bandpass sensors
        self.get_sensor('bandpass-mean').set_value('')
        self.get_sensor('bandpass-std').set_value('')

        self.set_state(State.idle)

    @request(Str(), Str(), Str())
    @return_reply()
    @gen.coroutine
    def request_capture_start(self, req, sb_details, product_meta, mca_mapping):
        """ 
        Start ingesting and processing data.
        
        Parameters
        ----------
        sb_details: str
            Dictionary in JSON format with the active schedule block details as returned by a 
            KATPortalClient instance
        product_meta: str
            Dictionary in JSON format containing all the sensor values required to configure the 
            pipeline, such as bandwidth, number of channels, etc. 
        mca_mapping: str
            Dictionary in JSON format specifying the mapping between multicast addresses and 
            beam IDs that must be processed by the node, with the following format:

            {'239.11.1.1:7147': ['cfbf00000', 'cfbf00001', 'cfbf00002']}

            It may contain multiple keys if FBFUSE has been configured to stream a small number of
            beams per multicast group
        """
        log.info("Received request: ?capture-start")
        req_name = req.msg.name
        cleanup_required = False

        try:
            self.capture_start(sb_details, product_meta, mca_mapping)

        except WrongState as err:
            log.warning(err)
            log.warning("request ?{} fail".format(req_name))
            raise gen.Return(('fail', str(err)))

        except Exception as err:
            cleanup_required = True
            self.set_state(State.error)
            log.error(err)
            log.info(traceback.format_exc())
            log.error("request ?{} fail".format(req_name))
            raise gen.Return(('fail', str(err)))

        else:
            log.info("request ?{} OK".format(req_name))
            raise gen.Return(('ok',))

        finally:
            if cleanup_required:
                log.info("Running capture_stop actions to recover from failed capture_start")
                try:
                    self.capture_stop(force=True)
                    log.info("Successfully recovered from failed capture_start")
                except Exception as err:
                    self.set_state(State.error)
                    log.error("Failed to recover from failed capture_start")
                    log.error(err)
                    log.info(traceback.format_exc())

    @request()
    @return_reply()
    @gen.coroutine
    def request_capture_stop(self, req):
        """ Stop ingesting and processing data """
        self.log_request(req)
        req_name = req.msg.name

        try:
            self.capture_stop()
        
        except WrongState as err:
            log.warning(err)
            log.warning("request ?{} fail".format(req_name))
            raise gen.Return(('fail', str(err)))

        except Exception as err:
            self.set_state(State.error)
            log.error(err)
            log.info(traceback.format_exc())
            log.error("request ?{} fail".format(req_name))
            raise gen.Return(('fail', str(err)))

        else:
            log.info("request ?{} OK".format(req_name))
            raise gen.Return(('ok',))

    @request(Str(), Str())
    @return_reply(Str())
    @gen.coroutine
    def request_update_config_param(self, req, key, value_str):
        """ 
        Update configuration parameter (dot notation accepted) of the NodeConfig object. The 
        specified parameter must exist in the node's YAML config file. The change may not take 
        effect immediately, e.g. changing a pipeline parameter will only take effect the next time
        it is started. NOTE: Limited sanity checks are performed on the actual value passed,
        so be careful.

        Parameters
        ----------
        key: str
            Name of the parameter to update. Can be provided in dot notation
            when accessing a nested parameter, e.g. 'pipeline.mode'
        value_str: str
            New value for the parameter, passed as a string. If the new value
            cannot be cast to the type of the existing value for this 
            parameter, the update will be rejected.
        """
        self.log_request(req)

        # Check that the key already exists
        try:
            current_value = self.config.dget(key)
        except KeyError:
            msg = "Key {!r} cannot be found in current config, update rejected".format(key)
            log.warning(msg)
            raise gen.Return(('fail', msg))

        # Avoid updating an entire section of the config file at once
        # Updating only basic types (e.g. str, int, float) is allowed
        forbidden_types = (dict, list, tuple)
        current_value_type = type(current_value)
        if current_value_type in forbidden_types:
            msg = "Key {!r} has one of the non-updateable types {!s}, update rejected".format(
                key, forbidden_types)
            log.warning(msg)
            raise gen.Return(('fail', msg))

        try:
            # Get proper behaviour when casting string to bool
            if current_value_type == bool:
                value = bool(ast.literal_eval(value_str))
            else:
                value = current_value_type(value_str)
        except ValueError:
            msg = "Failed to cast new value {!r} to expected type {!s}, update rejected".format(
                value_str, current_value_type)
            log.warning(msg)
            raise gen.Return(('fail', msg))

        self.config.dset(key, value)
        self.get_sensor('pipeline-config').set_value(json.dumps(self.config['pipeline']))
        msg = "Updated parameter {!s} ({!s}) from {!r} to {!r}".format(
            key, current_value_type, current_value, value)
        log.info(msg)
        raise gen.Return(('ok', msg))

    @request(Int())
    @return_reply(Str())
    @gen.coroutine
    def request_set_pipeline_enabled(self, req, flag):
        """
        Enable or Disable the pipeline. The node will start (or stop) the pipeline immediately if 
        all other required conditions to do so are met.

        Parameters
        ----------
        flag: int
            1 if enabling pipeline, 0 if disabling it.
        """
        self.log_request(req)
        if not flag in (0, 1):
            msg = "flag should be either 0 or 1, update rejected"
            log.warning(msg)
            raise gen.Return(('fail', msg))
        
        self.set_pipeline_enabled(bool(flag))
        msg = "pipeline_enabled set to {}".format(self.pipeline_enabled)
        raise gen.Return(('ok', msg))

###############################################################################


def get_default_config_fname():
    """
    Returns the absolute path to the default node_controller.yml
    """
    fname = os.path.join(os.path.dirname(__file__), '..', 'config', 'node_controller.yml')
    return os.path.realpath(fname)


def parse_args():
    parser = argparse.ArgumentParser(
        description="Launch the TUSE NodeController",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument(
        "-H", "--host", 
        help="Host IP or DNS address.", default="0.0.0.0",
    )
    parser.add_argument(
        "-p", "--port", 
        help="Port to use.", type=int, default=5100,
    )
    parser.add_argument(
        '-c', '--config',
        help='YAML master configuration file for the NodeController.',
        default=get_default_config_fname()
    )
    parser.add_argument(
        "-l", "--loglevel", 
        help="Logging level", default="DEBUG"
    )
    parser.add_argument(
        "-f", "--logfile", 
        help="If specified, write logs to this file. A RotatingFileHandler " 
            "with a period of 1 day is used.", 
        default=None
    )
    return parser.parse_args()


@gen.coroutine
def shutdown(ioloop):
    log.info("Shutting down")
    ioloop.stop()


@gen.coroutine
def start(node_controller):
    log.info("Starting NodeController, tusemcs version: {}".format(__version__))
    log.info("Configuration:\n{}".format(json.dumps(node_controller.config, indent=4)))
    yield node_controller.start()
    log.info("Listening at {}, Ctrl-C to terminate server".format(node_controller.bind_address))


def main():
    logging.getLogger('katcp').setLevel('WARNING')
    logging.getLogger('kat').setLevel('WARNING')
    logging.getLogger('tornado').setLevel('INFO')
    logging.getLogger('docker').setLevel('INFO')
    logging.getLogger('urllib3').setLevel('INFO')

    fmt = "[%(levelname)s - %(asctime)s - %(name)s - %(filename)s:%(lineno)s] %(message)s"

    args = parse_args()

    if args.logfile:
        # Redirect ALL logging to file
        root_logger = logging.getLogger()
        root_handler = logging.handlers.TimedRotatingFileHandler(
            args.logfile, when="d", interval=1, utc=True
        )
        root_handler.setFormatter(logging.Formatter(fmt=fmt))
        root_logger.addHandler(root_handler)
    
    coloredlogs.install(fmt=fmt, level=args.loglevel, milliseconds=True)

    # Hook up to SIGINT and SIGTERM so that both ctrl-C and docker stop result in a clean shutdown
    ioloop = tornado.ioloop.IOLoop.current()
    shutdown_func = lambda sig, frame: ioloop.add_callback_from_signal(shutdown, ioloop)
    signal.signal(signal.SIGINT, shutdown_func)
    signal.signal(signal.SIGTERM, shutdown_func)

    config = NodeConfig.from_yaml_config(args.config)
    node_controller = NodeController(args.host, args.port, config)
    ioloop.add_callback(start, node_controller)
    ioloop.start()


if __name__ == "__main__":
    main()