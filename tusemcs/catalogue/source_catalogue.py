import os
import csv
import logging
import math

import coloredlogs
import numpy as np
import katpoint
import astropy.units as uu
from astropy.coordinates import SkyCoord

from tusemcs.general_helpers import configure_logging


log = logging.getLogger('tusemcs.source_catalogue')


###############################################################################

### Helper functions to parse psrcat's output into a proper CSV file
def get_psrcat_output():
    # psrcat -c "JNAME RAJD DECJD P0 DM S1400" -o short_csv
    pass


def normalise_psrcat_output(text, columns=['jname', 'rajd', 'decjd', 'p0', 'dm', 's1400']):
    """
    """
    lines = text.strip().split('\n')[2:]
    # Split lines into tuples, removing trailing semicolon and
    # also removing the line index
    lines = [tuple(line.strip(';').split(';')[1:]) for line in lines[2:]]
    lines = [tuple(columns)] + lines
    text = '\n'.join([','.join(line) for line in lines])
    return text

###############################################################################

class Source(object):
    """ 
    Class holding source attributes

    Parameters
    ----------
    name: str
        Source name
    coord: astropy.SkyCoord
        Source coordinates
    dm: float or None
    s1400: float or None
    """
    def __init__(self, name, coord, dm=None, s1400=None):
        self.name = str(name)
        self.coord = coord
        self.dm = dm
        self.s1400 = s1400
    
    @classmethod
    def from_psrcat_entry(cls, entry):
        """
        Build Source object from CSV entry in psrcat CSV file

        Parameters
        ----------
        entry: dict
            Dictionary with source params
        """
        name = entry['jname']
        coord = SkyCoord(entry['rajd'], entry['decjd'], unit=(uu.degree, uu.degree))
        return cls(name, coord, dm=entry['dm'], s1400=entry['s1400'])

    def katpoint_string(self):
        """ Convert to katpoint Target string """
        body = "{},radec,{},{}".format(
            self.name, 
            self.coord.ra.to_string(uu.hour, sep=':', precision=2), 
            self.coord.dec.to_string(uu.degree, sep=':', precision=2)
            )
        return body

    def katpoint_target(self):
        """ Convert to katpoint Target object """
        return katpoint.Target(self.katpoint_string())

    def infodict(self):
        """ Dictionary with source info """
        tg = self.katpoint_target()
        ra, dec = tg.astrometric_radec()
        return dict(
            jname=self.name, rajd=round(math.degrees(ra), 6), decjd=round(math.degrees(dec), 6), 
            dm=self.dm, s1400=self.s1400, ra=str(ra), dec=str(dec)
        )

    def __str__(self):
        return "{!r} DM={!r} S1400={!r}".format(self.katpoint_target(), self.dm, self.s1400)

    def __repr__(self):
        return str(self)


def parse_float(s):
    try:
        return float(s)
    except:
        return None


def load_catalogue():
    """
    Load catalogue from CSV file.

    Returns
    -------
    sources: list
        list of Source objects
    catalogue: astropy.SkyCoord
        SkyCoord containing an array of RA/Dec in the same order as 
        'sources'. This is useful to compute angular separations
        between a point and the whole catalogue of sources.
    """
    log.debug("Loading source catalogue")
    thisdir = os.path.dirname(__file__)
    fname = os.path.join(thisdir, 'catalogue.csv')
    sources = []

    with open(fname, 'r') as fobj:
        for items in csv.DictReader(fobj):
            items['dm'] = parse_float(items['dm'])
            items['s1400'] = parse_float(items['s1400'])
            s = Source.from_psrcat_entry(items)
            sources.append(s)

    catalogue = SkyCoord(
        ra=[s.coord.ra for s in sources],
        dec=[s.coord.dec for s in sources]
        )
    log.debug("Loaded {} sources".format(len(sources)))
    return sources, catalogue


SOURCES, CATALOGUE = load_catalogue()


def query_radius(coord, angle):
    """
    Return all sources within given angle of a point on sky 

    Parameters
    ----------
    coord: astropy.SkyCoord
        Centre point of the query
    angle: float
        Angular radius of the query in degrees

    Returns
    -------
    matches: list
        list of dictionaries with matching source parameters. Keys are:
        jname, rajd, recjd, dm, s1400
        For 'dm' or 's1400', a value of None means there is no published value.

    Example
    -------
    Get all sources within 0.5 degrees of the Galactic centre:

    query_radius(SkyCoord(0, 0, frame='galactic', unit=(uu.degree, uu.degree)), 0.5)
    """
    return [
        SOURCES[ii].infodict()
        for ii in np.where(CATALOGUE.separation(coord) < angle * uu.degree)[0]
        ]


if __name__ == '__main__':
    coloredlogs.install(
        fmt="[%(levelname)s - %(asctime)s - %(name)s - %(filename)s:%(lineno)s] %(message)s", 
        level=logging.DEBUG, 
        milliseconds=True
    )

    log.debug("Querying")
    sources = query_radius(
        SkyCoord(0, 0, frame='galactic', unit=(uu.degree, uu.degree)),
        1.0
    )
    log.debug("Done")

    for source in sources:
        print(source)