#
# Pipeline container shell script generator
# Vincent Morello - 2020
#
import os
import logging
import re

from jinja2 import Template, StrictUndefined

from pipeline_utils import (
    DADA_OUTKEYS_FNAME,
    CHEETAH_CONFIG_FNAME,
    MKRECV_HEADER_FNAME
)


log = logging.getLogger('tusemcs.pipeline_script')


def load_pipeline_script_template():
    thisdir = os.path.dirname(os.path.realpath(__file__))
    fname = os.path.join(thisdir, '..', 'templates', 'pipeline_script.jinja2')
    with open(fname, 'r') as fobj:
        return Template(
            fobj.read(),
            undefined=StrictUndefined, trim_blocks=True, lstrip_blocks=True)


def generate_pipeline_script(dtconf, confdir='/startup', outdir='/output', ipcrm=False, filterbank_size=2147483648, bpmon=True):
    """
    dtconf: DADATransposeConfig
        Configuration parameters for DADA buffers and transpose module
    confdir : str
        Full path to the base directory where config files are stored for this pipeline run
        NOTE: This is the path as viewed from INSIDE the pipeline container !
    outdir : str
        Full path to the base output directory for this pipeline run
        NOTE: This is the path as viewed from INSIDE the pipeline container !
    ipcrm : bool
        If True, remove existing DADA buffers using 'ipcrm -a'
    bpmon : bool
        If True, the C++ bandpass monitor will be launched
    """
    # unpack DADATransposeConfig members into a dict 
    # so they are made directly available to template generator
    kwargs = vars(dtconf) 
    TEMPLATE = load_pipeline_script_template()
    rendered = TEMPLATE.render(
        kwargs,
        outdir=outdir,
        mode=dtconf.mode,
        ipcrm=ipcrm,
        filterbank_size=filterbank_size,
        outkeys_fname=os.path.join(confdir, DADA_OUTKEYS_FNAME),
        cheetah_config_fname=os.path.join(confdir, CHEETAH_CONFIG_FNAME),
        mkrecv_header_fname=os.path.join(confdir, MKRECV_HEADER_FNAME),
        bpmon=bpmon
    )

    # Replace sequences of multiple blank lines by one blank line (twice '\n')
    rendered = re.sub(r'\n{2,}', '\n\n', rendered)
    return rendered


def test():
    import katpoint
    from beam import Beam
    from dada_transpose_config import DADATransposeConfig
    from pipeline_utils import VALID_PIPELINE_MODES

    # Example product_meta where we have removed the following superfluous and bulky keys:
    # - coherent_beam_multicast_group_mapping
    # - available_antennas
    product_meta = {
        'coherent_beam_samples_per_heap': 128,
        'coherent_beam_nbeams_per_server_set': 480,
        'coherent_beam_heap_size': 8192,
        'incoherent_beam_tscrunch': 16,
        'incoherent_beam_antennas': 'm000,m001,m002,m003,m004,m005,m006,m008,m009,m010,m011,m012,m013,m014,m015,m017,m018,m019,m020,m021,m024,m026,m027,m028,m032,m033,m034,m035,m038,m039,m040,m041,m042,m043,m044,m045,m046,m047,m048,m049,m050,m051,m052,m053,m054,m055,m056,m057,m058,m059,m060,m061,m062',
        'wide_adc_sample_rate': 1712000000.0,
        'fbfmc_address': ['fbfhn00.mpifr-be.mkat.karoo.kat.ac.za', 5000],
        'coherent_beam_multicast_groups_data_rate': 5136000000.0,
        'synchronisation_epoch': 1584420943.0,
        'fbfuse_proxy': 'fbfuse_2',
        'incoherent_beam_heap_size': 8192,
        'incoherent_beam_idx1_step': 16777216,
        'coherent_beam_antennas': 'm000,m001,m002,m003,m004,m005,m006,m008,m009,m010,m011,m012,m013,m014,m015,m017,m018,m019,m020,m021,m024,m026,m027,m028,m032,m033,m034,m035,m038,m039,m040,m041,m042,m043,m044,m045,m046,m047,m048,m049,m050,m051,m052,m053,m054,m055,m056,m057,m058,m059,m060,m061',
        'incoherent_beam_count': 1,
        'nchannels': 4096,
        'incoherent_beam_fscrunch': 1,
        'incoherent_beam_samples_per_heap': 128,
        'bandwidth': 856000000.0,
        'coherent_beam_fscrunch': 1,
        'incoherent_beam_multicast_group_data_rate': 428000000.0,
        'coherent_beam_time_resolution': 7.656070000000001e-05,
        'coherent_beam_ngroups': 40,
        'coherent_beam_count_per_group': 12,
        'coherent_beam_subband_nchans': 64,
        'coherent_beam_count': 480,
        'subarray_proxy': 'subarray_2',
        'product_id': 'array_2',
        'centre_frequency': 1284000000.0,
        'coherent_beam_idx1_step': 16777216,
        'coherent_beam_multicast_groups': 'spead://239.11.1.1+39:7147',
        'coherent_beam_tscrunch': 16,
        'incoherent_beam_multicast_group': 'spead://239.11.1.0+0:7147',
        'cbf_proxy': 'cbf_2',
        'incoherent_beam_subband_nchans': 64,
        'incoherent_beam_time_resolution': 7.656070000000001e-05
    }

    mc_address = "239.1.1.42:7147"
    target = katpoint.Target("J1151-6108,radec,11:51:56.86,-61:08:17.6")

    ### Generate some beams
    beams = [
        Beam(ii, mc_address, "cfbf{:05d}".format(ii + 100), target)
        for ii in range(6)
    ]


    for mode in VALID_PIPELINE_MODES:
        dtconf = DADATransposeConfig.from_production_input(
            product_meta, beams, mode=mode,
            timespan_min=6.0, tsamp_output_min=400e-6, nchans_output_max=1024
        )
        print(75 * '=')
        print("MODE: {}".format(mode))
        print(75 * '=')
        script_text = generate_pipeline_script(dtconf, ipcrm=False)
        print(script_text)
        print("")

if __name__ == '__main__':
    import coloredlogs
    logfmt = "[%(levelname)s - %(asctime)s - %(name)s - %(filename)s:%(lineno)s] %(message)s"
    coloredlogs.install(fmt=logfmt, level='DEBUG', milliseconds=True, logger=log)
    test()