import logging
import signal
import json
import urlparse
import datetime

import coloredlogs
import tornado
from tornado import gen
from katportalclient import KATPortalClient


# Sensors required on each component
SENSORS = {
    'cbf': ['wide_adc_sample_rate'],
    'subarray': ['synchronisation_epoch']
}

# Components for which we must retrieve the proxy name
COMPONENTS = set(list(SENSORS.keys()) + ['fbfuse'])

# Product-specific sensors required on FBFUSE
# These need to be expanded into their full names with FBFBUSE proxy name and product_id
# e.g. fbfuse_1_fbfmc_array_2_bandwidth
FBFMC_PRODUCT_SENSORS = [
    'bandwidth',
    'centre_frequency',
    'coherent_beam_antennas',
    'coherent_beam_fscrunch',
    'coherent_beam_heap_size',
    'coherent_beam_idx1_step',
    'coherent_beam_multicast_group_mapping',
    'coherent_beam_samples_per_heap',
    'coherent_beam_subband_nchans',
    'coherent_beam_time_resolution',
    'coherent_beam_tscrunch',
    'incoherent_beam_antennas',
    'incoherent_beam_fscrunch',
    'incoherent_beam_heap_size',
    'incoherent_beam_idx1_step',
    'incoherent_beam_multicast_group', # e.g. spead://239.11.1.1+15:7147
    'incoherent_beam_samples_per_heap',
    'incoherent_beam_subband_nchans',
    'incoherent_beam_time_resolution',
    'incoherent_beam_tscrunch',
    'nchannels',
    ]


log = logging.getLogger('tusemcs.product_meta_retrieval')


class PortalClientWrapper(object):
    def __init__(self, product_id, portal_url):
        self.product_id = product_id
        self.portal_url = portal_url
        self.client = KATPortalClient(self.portal_url, on_update_callback=None)
        self.proxy_names = {}

    @gen.coroutine
    def retrieve_proxy_names(self):
        proxies = yield gen.multi([
            self.client.sensor_subarray_lookup(comp, sensor=None)
            for comp in COMPONENTS
        ])
        self.proxy_names = {
            comp : proxy
            for comp, proxy in zip(COMPONENTS, proxies)
        }
        log.debug("Proxy names: {}".format(self.proxy_names))

    def full_sensor_name(self, component, short_name):
        return '_'.join([self.proxy_names[component], short_name])

    def fbfmc_full_sensor_name(self, short_name):
        return '_'.join([self.proxy_names['fbfuse'], 'fbfmc', self.product_id, short_name])

    @gen.coroutine
    def wait_fbfuse_ready(self, timeout=60.0, dt=6.0):
        log.debug('Waiting for FBFUSE to be ready')
        full_name = self.fbfmc_full_sensor_name('state')
        while timeout > 0:
            wait_future = gen.sleep(dt)
            sample = yield self.client.sensor_value(full_name)
            if sample.value in ('ready', 'starting', 'capturing'):
                log.debug('FBFUSE is ready')
                raise gen.Return()
            yield wait_future
            timeout -= dt
        raise gen.TimeoutError("Timed out waiting for FBFUSE to be ready")

    @gen.coroutine
    def read_product_meta(self):
        # Compile list of full sensor names
        mapping = {}

        for component, sensor_list in SENSORS.items():
            for short_name in sensor_list:
                mapping[short_name] = self.full_sensor_name(component, short_name)
        
        for short_name in FBFMC_PRODUCT_SENSORS:
            mapping[short_name] = self.fbfmc_full_sensor_name(short_name)

        regex = '|'.join(name for name in mapping.values())

        # dictionary {full_name: SensorSample}
        samples = yield self.client.sensor_values(regex, include_value_ts=False)

        meta = {}
        for short_name, full_name in mapping.items():
            meta[short_name] = samples[full_name].value
        raise gen.Return(meta)


def strip_multicast_address(url):
    """ 
    Reformat URL string such as  'spead://239.11.1.0+0:7147' to '239.11.1.0:7147'
    This removes the 'spead://' prefix as well as any +N postfix to the hostname
    """
    u = urlparse.urlparse(url)
    return '{}:{}'.format(u.hostname.split('+')[0], u.port)


@gen.coroutine
def retrieve_product_meta(product_id, portal_url):
    """
    Retrieve the so-called product metadata from KAT portal. The product metadata is the set
    of sensor values that remain constant during an entire schedule block.

    Parameters
    ----------
    product_id : str
        Product ID such as 'array_1'
    portal_url : str
        URL of the KAT portal relevant to the observing run,
        e.g. 'http://10.8.67.113/api/client/1'

    Returns
    -------
    product_meta : dict
        Dictionary with all metadata, except multicast addresses
    incoherent_mca : str
        Multicast address of the incoherent beam
    mca_mapping : dict
        Mapping {multicast_address: beam_ids} for coherent beams
    """
    pcw = PortalClientWrapper(product_id, portal_url)
    yield pcw.retrieve_proxy_names()
    yield pcw.wait_fbfuse_ready()
    product_meta = yield pcw.read_product_meta()
    mca_mapping = json.loads(product_meta.pop('coherent_beam_multicast_group_mapping'))
    mca_mapping = {
        strip_multicast_address(url) : beam_ids
        for url, beam_ids in mca_mapping.items()
    }
    incoherent_mca = product_meta.pop('incoherent_beam_multicast_group')
    incoherent_mca = strip_multicast_address(incoherent_mca)
    result = (product_meta, incoherent_mca, mca_mapping)
    raise gen.Return(result)


@gen.coroutine
def retrieve_active_sb_details(portal_url, timeout=5.0):
    """ 
    Get active schedule block details from the portal.

    Parameters
    ----------
    portal_url: str
        Portal URL for the relevant subarray / product ID, e.g.
        'http://10.8.67.113/api/client/1' for subarray 1
    timeout: float, optional
        Seconds to wait for a response in both function calls made to the
        portal. (default: 5.0)

    Returns
    -------
    details: dict
        Dictionary with schedule block parameters

    Raises
    ------
    gen.TimeoutError: if the portal takes too long to respond
    RuntimeError: if there are no schedule blocks assigned
    """
    client = KATPortalClient(portal_url, on_update_callback=None)
    dt = datetime.timedelta(seconds=timeout)

    try:
        sb_ids = yield gen.with_timeout(dt, client.schedule_blocks_assigned())
    except gen.TimeoutError:
        msg = "Timed out while retrieving the list of active SBs"
        raise gen.TimeoutError(msg)

    if not sb_ids:
        raise RuntimeError("No schedule blocks assigned")

    active_sb_id = sb_ids[0]

    try:
        details = yield gen.with_timeout(dt, client.schedule_block_detail(active_sb_id))
    except gen.TimeoutError:
        msg = "Timed out while retrieving the details of the active SB"
        raise gen.TimeoutError(msg)
    else:
        raise gen.Return(details)


###################################################################################################


@gen.coroutine
def on_shutdown(ioloop):
    log.info("STOP")
    ioloop.stop()


def test():
    ioloop = tornado.ioloop.IOLoop.current()

    URL = 'http://10.97.1.14/api/client/1'

    # Hook up to SIGINT so that ctrl-C results in a clean shutdown
    signal.signal(
        signal.SIGINT, 
        lambda sig, frame: ioloop.add_callback_from_signal(on_shutdown, ioloop))

    signal.signal(
        signal.SIGTERM, 
        lambda sig, frame: ioloop.add_callback_from_signal(on_shutdown, ioloop))

    @gen.coroutine
    def start_and_display():
        log.info("START")
        product_meta, incoherent_mca, mca_mapping = yield retrieve_product_meta('array_1', URL)
        log.debug(json.dumps(mca_mapping, indent=4))
        log.debug(incoherent_mca)
        log.debug(json.dumps(product_meta, indent=4))       

    ioloop.add_callback(start_and_display)
    ioloop.start()


if __name__ == '__main__':
    coloredlogs.install(
        fmt="[%(levelname)7s - %(asctime)s - %(name)s - %(filename)s:%(lineno)s] %(message)s", 
        level='DEBUG', 
        milliseconds=True
    )
    logging.getLogger('kat').setLevel('WARNING')
    logging.getLogger('tornado').setLevel('INFO')
    test()
