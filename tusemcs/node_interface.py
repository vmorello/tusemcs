import logging
from katcp.resource_client import KATCPClientResource

class NodeInterface(KATCPClientResource):
    """ Inspects a remote NodeController and exposes its requests and sensors as objects. """
    def __init__(self, number, vlan, capacity=6, port=5100, timeout=60.0):
        """ Create a NodeInterface to a remote NodeController.

        Parameters
        ----------
        number: int
            Node number, zero-indexed. From node number its name and its
            host name are automatically inferred.
        vlan: int
            VLAN number. Numbering convention is irrelevant, as long as nodes
            in the same VLAN bear the same vlan_number
        capacity: int
            Number of beams this node can process
        port: int
            Port on which the remote NodeController is listening
        timeout: float
            Timeout in seconds for KATCP requests sent via this interface.
        """
        assert isinstance(number, int)
        assert isinstance(vlan, int)
        assert isinstance(capacity, int)
        assert isinstance(port, int)

        # object-level logger
        self.log = logging.getLogger("tusemcs.node_interface")

        self._number = number
        self._vlan = vlan
        self._capacity = capacity

        name = "node{:02d}".format(self.number)
        host = "tpn-0-{:d}".format(self.number)

        resource_spec = dict(
            name=name,
            address=(host, port),
            controlled=True, # Expose requests
            auto_reconnect=True,
            auto_reconnect_delay=5.0, # seconds
            )
        super(NodeInterface, self).__init__(resource_spec)
        self.start()

        # Have to do this because KATCPClientResource does NOT expose
        # the request timeout parameter, and effectively sets it to 5.0 seconds.
        # NOTE: this must be called AFTER self.start(), because thats where the
        # _inspecting_client member is created.
        self._inspecting_client.katcp_client._request_timeout = timeout

    def __del__(self):
        self.stop()

    # NOTE: name and host properties are already defined by the parent
    # KATCPClientResource
    @property
    def number(self):
        return self._number

    @property
    def vlan(self):
        """ VLAN number (int) """
        return self._vlan

    @property
    def capacity(self):
        return self._capacity

    @property
    def sensor_names(self):
        return list(self.sensor.keys())

    def set_capacity(self, n):
        self._capacity = int(n)

    # Providing __hash__ and __eq__ allows to make sets of NodeInterfaces, and thus
    # guarantee that no two nodes have the same address in a NodePool
    def __hash__(self):
        return hash((self.host, self.port))

    def __eq__(self, other):
        return hash(self) == hash(other)

    def __str__(self):
        s = "{n.name:s} ({n.host:>8s}:{n.port:d}) vlan={n.vlan:d}, capacity={n.capacity:d}"
        return s.format(n=self)
    
    def __repr__(self):
        s = "NodeInterface(name={n.name:s}, address={n.host:s}:{n.port:d}, synced={n.synced!s})"
        return s.format(n=self)
