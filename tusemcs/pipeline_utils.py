#
# Central place to define global parameters and functions useful to all
# stages of pipeline config generation
# Vincent Morello - 2020
#
import os


VALID_PIPELINE_MODES = ['discard', 'filterbank', 'production']

MKRECV_HEADER_FNAME = 'mkrecv_header.cfg'

DTCONF_FNAME = 'dtconf.json'

DADA_OUTKEYS_FNAME = 'outkeys.txt'

CHEETAH_CONFIG_FNAME = 'cheetah.xml'

# Static channel mask file name as expected by AA
AA_CHANMASK_FNAME = 'channel_mask.dat'

PIPELINE_SCRIPT_FNAME = 'pipeline.sh'

PIPELINE_CONTAINER_LOGFILE = 'pipeline_container.log'

RUN_SUMMARY_FNAME = 'run_summary.json'

# strftime for pipeline UTC start and stop times
# This is the date format used when naming the UTC output directory
UTC_FORMAT = "%Y-%m-%d_%H:%M:%S"

# Main output directory on compute nodes
# Every time the pipeline starts, a UTC sub-directory is created, and any
# configuration and output files are written under it
HOST_MAIN_OUTDIR = '/state/partition1/node_controller/output'

# This is where HOST_MAIN_OUTDIR must be mounted inside the control container
CONTROL_MAIN_OUTDIR = '/output'

# This is where the output directory for a *single* UTC is mounted inside the
# pipeline container. A single UTC directory is seen as follows as a function
# of point of view:
# - host              : HOST_MAIN_OUTDIR/<UTC_START>
# - control container : CONTROL_MAIN_OUTDIR/<UTC_START>
# - pipeline container: PIPELINE_OUTDIR
# 
# That way, the pipeline container only has access a single UTC directory for safety
PIPELINE_OUTDIR = '/output'


def validate_pipeline_mode(mode):
    if not mode in VALID_PIPELINE_MODES:
        raise ValueError("pipeline mode must be one of {}".format(VALID_PIPELINE_MODES))


class DotDict(dict): 
    """ 
    dict wrapper with nested dot access to dictionary attributes,
    just to make notations lighter
    """
    def __getattr__(self, key): 
        val = self[key]
        return DotDict(val) if type(val) is dict else val


def output_dada_key(index):
    """
    Get the hexadecimal DADA key for given relative beam index
    beam 0 gets 'b000', beam 1 gets 'b010', etc.
    We reserve 'bff0' for the input DADA buffer.
    """
    if not 0 <= index < 255:
        raise ValueError("Relative beam index must be between 0 and 254 inclusive ")
    return "b{:02x}0".format(index)


def get_directory_path(pov, utc_start, purpose):
    """
    Get full directory path for specified purpose (e.g. config or output) as a
    function of point-of-view (e.g. host, control container, pipeline container)

    Parameters
    ----------
    pov : str
        Point-of-view, either 'host', 'control' or 'pipeline'
    utc_start : str
        The start UTC of the pipeline run, which will be the directory name
        under which all files for this run are written
    purpose : str
        Purpose of the directory. Either 'config' or 'output'

    Returns
    -------
    path : str
        Full path of the directory
    """
    if pov == 'host':
        outdir = os.path.join(HOST_MAIN_OUTDIR, utc_start)
    elif pov == 'control':
        outdir = os.path.join(CONTROL_MAIN_OUTDIR, utc_start)
    elif pov == 'pipeline':
        outdir = PIPELINE_OUTDIR
    else:
        raise ValueError("Invalid point of view")

    if purpose == 'output':
        path = outdir
    elif purpose == 'config':
        path = os.path.join(outdir, 'config')
    else:
        raise ValueError("Invalid directory purpose")

    return path


if __name__ == '__main__':
    for pov in ('host', 'control', 'pipeline'):
        for purpose in ('output', 'config'):
            print("{:12s} {:12s} {}".format(pov, purpose, get_directory_path(pov, 'SOME_UTC', purpose)))