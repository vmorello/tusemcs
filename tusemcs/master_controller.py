import logging
import argparse
import signal
import traceback
import json

import yaml
import tornado
from tornado import gen
import coloredlogs
from enum import Enum
from katcp import AsyncDeviceServer, Sensor
from katcp.kattypes import request, return_reply, Str, Int

from tusemcs import __version__
from tusemcs.node_pool import NodePool
from tusemcs.general_helpers import build_info, get_ntp_sync_status
from tusemcs.product_meta_retrieval import retrieve_product_meta, retrieve_active_sb_details
from tusemcs.slicelist import parse_slicelist_string, slicelist_to_indices
from tusemcs.beam_assignment import make_beam_assignment_plan, format_beam_assignment_plan
from tusemcs.redis_beam_updater import RedisBeamUpdater
from tusemcs.alerts import (
    send_mattermost_alert, send_slack_alert, send_mattermost_alert, sbfail_message
)


log = logging.getLogger("tusemcs.master_controller")


class State(Enum):
    """ Enum with possible states of the MasterController """
    idle, configured, preparing, started, stopping, error = range(6)


class DeviceStatus(Enum):
    """ 
    Enum reflecting the possible values for the device-status sensor required by the TUSE-CAM ICD
    in section 3.6.7.1
    """
    ok, degraded, fail = range(3)


class WrongProductID(Exception):
    """ Raised when a request received is meant for a product_id different from the one TUSE is 
    already configured for """
    pass


class WrongState(Exception):
    """ Raised when we are not in the right state to process a given request """
    pass


class MasterController(AsyncDeviceServer):
    """ 
    Controller class for the head node. 
    
    Parameters
    ----------
    host : str
    port : int
    node_specs : dict
    """
    ### VERSION_INFO and BUILD_INFO are required
    # See: http://katcp-python.readthedocs.io/en/latest/tutorial.html
    # Section "writing your own server"
    VERSION_INFO = ("tmc-api", 0, 1)
    BUILD_INFO = build_info()

    # Total number of compute nodes
    NUM_NODES = 66

    # Default node capacity as a function of the number of channels in the
    # FBFUSE data. This can be changed dynamically through a request
    DEFAULT_NODE_CAPACITIES = {
        1024: 12,
        2048: 6,
        4096: 6
    }

    def __init__(self, host, port, node_specs):
        # Current state
        self.state = None

        # Node capacity as a function of num channels {nchan: capacity}
        self.node_capacities = self.DEFAULT_NODE_CAPACITIES.copy()

        # If True, process only the IB
        # Can be changed via a request
        self.ib_only = False

        # The only subarray id that we will be processing. Set at ?configure
        self.product_id = None

        # KATPortal URL, set at ?configure
        self.portal_url = None

        # Redis-based beam sensor updater.
        self.beam_updater = None

        # This connects a client to every node
        self.node_pool = NodePool.from_node_specs(node_specs)

        # Details of the SB being processed (dict)
        self.sb_details = {}

        self.periodic_callbacks = []

        # Setup AsyncDeviceServer only now, to delay setup_sensors() call
        super(MasterController, self).__init__(host, port)

        # Only now can we set the initial state
        self.set_state(State.idle)

    def setup_sensors(self):
        """ 
        Set up monitoring sensors. AsyncDeviceServer derived classes must implement this method. 
        """
        device_status = Sensor.discrete(
            "device-status",
            description="Current health of TUSE",
            params=[s.name for s in DeviceStatus],
            default=DeviceStatus.ok.name,
            initial_status=Sensor.NOMINAL)
        self.add_sensor(device_status)

        state = Sensor.discrete(
            "state",
            description="Current TUSE state",
            params=[s.name for s in State],
            default=State.idle.name,
            initial_status=Sensor.NOMINAL)
        self.add_sensor(state)

        product_id = Sensor.string(
            "product-id",
            description="Product ID for which TUSE is currently configured",
            default="",
            initial_status=Sensor.NOMINAL)
        self.add_sensor(product_id)

        portal_url = Sensor.string(
            "portal-url",
            description="KAT portal URL that corresponds to the current product id",
            default="",
            initial_status=Sensor.NOMINAL)
        self.add_sensor(portal_url)

        sb_details = Sensor.string(
            "sb-details",
            description="JSON-encoded dictionary containing the currently active "
                "schedule block details",
            default="",
            initial_status=Sensor.NOMINAL)
        self.add_sensor(sb_details)

        local_time_synced = Sensor.boolean(
            "local-time-synced",
            description="Indicates whether TUSE local time is synced to master time reference.",
            default=True,
            initial_status=Sensor.NOMINAL)
        self.add_sensor(local_time_synced)

        ib_only_sensor = Sensor.boolean(
            "ib-only",
            description="Whether TUSE is in a mode where only the incoherent beam is processed",
            default=self.ib_only,
            initial_status=Sensor.NOMINAL
        )
        self.add_sensor(ib_only_sensor)

        node_capacities_sensor = Sensor.string(
            "node-capacities",
            description="JSON-encoded dictionary that gives the number of beams to be assigned"
                " per TUSE node as a function of the number of channels in the FBFUSE data"
                " (i.e. after FBFUSE frequency scrunching)",
            default=json.dumps(self.node_capacities),
            initial_status=Sensor.NOMINAL
        )
        self.add_sensor(node_capacities_sensor)

        fbfuse_nchannels_sensor = Sensor.integer(
            "fbfuse-nchannels",
            description="Number of channels in the data produced by FBFUSE",
            default=None,
            initial_status=Sensor.UNKNOWN
        )
        self.add_sensor(fbfuse_nchannels_sensor)

    def start(self):
        """
        Start the server. We override the parent start() method, so that we can setup periodic
        callback(s).
        """
        super(MasterController, self).start()
        self.update_ntp_sync_status()

        sync_updater = tornado.ioloop.PeriodicCallback(self.update_ntp_sync_status, 60000.0)
        self.periodic_callbacks.append(sync_updater)
        for cb in self.periodic_callbacks:
            cb.start()

        self.node_pool.start_clients()

    @gen.coroutine
    def stop(self):
        """ 
        Stop the server. We override the parent stop() method, so that we can cleanly stop any
        periodic callbacks.
        """
        for cb in self.periodic_callbacks:
            cb.stop()
        self.periodic_callbacks = []
        super(MasterController, self).stop()

    def update_ntp_sync_status(self):
        """
        Update the NTP sync status sensor
        """
        # We only report on the NTP sync status of the head node
        # TODO: could check NTP sync of the nodes in the future
        sync = False
        try:
            sync = get_ntp_sync_status("localhost")
        except Exception as err:
            log.warn("Could not get NTP sync status of head node: {}".format(err))                  
        self.get_sensor('local-time-synced').set_value(sync)

    def set_state(self, state):
        """
        Set the current state, and update both the 'state' and 'device-status' sensors
        """
        self.state = state
        self.get_sensor("state").set_value(state.name)
        device_status = DeviceStatus.fail if state == State.error else DeviceStatus.ok
        self.get_sensor("device-status").set_value(device_status.name)

    def set_sb_details(self, items):
        self.sb_details = items
        self.get_sensor('sb-details').set_value(json.dumps(self.sb_details))

    def log_request(self, req):
        """ Emit a log message that a request has been received. req must be a katcp.Message """
        log.info("Received request: {}".format(req.msg))

    def log_nodes_reply_summary(self, counter, req=None):
        """
        Log a summary of what nodes replied to a request. The same summary can also be sent via
        katcp informs to the original sender of the request (via the optional 'req' object)

        Parameters
        ----------
        counter : collections.Counter
            dict-like object where keys are message strings, and values the number of nodes that
            sent that exact reply.
        req : katcp.Request or None
            Original request
        """
        for reply_str, count in counter.items():
            msg = "{} nodes replied: {}".format(count, reply_str)
            log.info(msg)
            if req:
                req.inform(msg)

    def check_product_id(self, product_id):
        """ Raise an Exception if TUSE is already configured for a product_id different from the 
        one passed to this function """
        if self.product_id and not product_id == self.product_id:
            raise WrongProductID(
                "TUSE is already configured for another product_id: {}".format(self.product_id)
            )

    def check_state(self, expected_state):
        if not self.state == expected_state:
            msg = "TUSE is the wrong state: currently {!r}, needs to be {!r}".format(
                self.state.name, expected_state.name)
            raise WrongState(msg)

    def set_node_capacity(self, nchan, capacity):
        available_modes = list(self.node_capacities.keys())
        if not nchan in available_modes:
            raise ValueError("Unknown mode: nchan must be one of {}".format(available_modes))
        if not (isinstance(capacity, int) and capacity > 0):
            raise ValueError("capacity must be a strictly positive int")
        self.node_capacities[nchan] = capacity
        self.get_sensor('node-capacities').set_value(json.dumps(self.node_capacities))
        log.info("Changed node capacity for {} channel mode to {}".format(nchan, capacity))

    def set_ib_only(self, flag):
        flag = bool(flag)
        self.ib_only = flag
        self.get_sensor("ib-only").set_value(self.ib_only)
        log.info("Set ib_only = {}".format(flag))

    @gen.coroutine
    def configure(self, product_id, streams_json, proxy_name):
        self.check_product_id(product_id)
        self.check_state(State.idle)

        streams_json = json.loads(streams_json)
        self.product_id = product_id
        self.portal_url = streams_json['cam.http']['camdata']
        self.get_sensor('product-id').set_value(self.product_id)
        self.get_sensor('portal-url').set_value(self.portal_url)

        self.beam_updater = RedisBeamUpdater(self.product_id, self.portal_url)
        self.set_state(State.configured)

    @gen.coroutine
    def deconfigure(self, product_id, force=False):
        if not force:
            self.check_product_id(product_id)
            self.check_state(State.configured)

        self.product_id = None
        self.portal_url = None
        self.beam_updater = None

        self.get_sensor('product-id').set_value('')
        self.get_sensor('portal-url').set_value('')
        self.set_state(State.idle)

    @gen.coroutine
    def capture_start(self, product_id):
        self.check_product_id(product_id)
        self.check_state(State.configured)
        
        self.set_state(State.preparing)
        
        sb_details = yield retrieve_active_sb_details(self.portal_url)
        product_meta, imca, coherent_beam_mapping = yield retrieve_product_meta(
            self.product_id, self.portal_url)

        self.set_sb_details(sb_details)
        log.debug("sb_details = \n{}".format(json.dumps(sb_details)))
        log.debug("product_meta = \n{}".format(json.dumps(product_meta)))
        log.debug("coherent_beam_mapping = \n{}".format(json.dumps(coherent_beam_mapping)))
        log.debug("incoherent_mc_address = {}".format(imca))

        # If we are in "IB ONLY" mode, just clear the coherent_beam_mapping dictionary
        # so that only the IB is assigned
        if self.ib_only:
            log.warning("IB-Only mode enabled: coherent beams will NOT be assigned")
            coherent_beam_mapping = {}

        # Set node capacities to the right value for current active mode (number of channels)
        fbfuse_nchan = product_meta['nchannels'] // product_meta['coherent_beam_fscrunch']
        log.info("Number of channels in FBFUSE data = {}".format(fbfuse_nchan))
        self.get_sensor('fbfuse-nchannels').set_value(fbfuse_nchan)
        capacity = self.node_capacities[fbfuse_nchan]
        log.info("Node capacity for {}-channel mode = {}".format(fbfuse_nchan, capacity))

        ### Calculate beam assignment plan
        node_specs = [(node.number, node.vlan) for node in self.node_pool.synced_nodes]
        assignment_plan = make_beam_assignment_plan(
            node_specs,
            coherent_beam_mapping,
            imca,
            node_capacity=capacity
        )
        log.debug(
            "Beam assignment plan:\n{}".format(format_beam_assignment_plan(assignment_plan))
        )

        ### Send capture-start
        __, counter = yield self.node_pool.issue_capture_start(
            sb_details, product_meta, assignment_plan)
        self.log_nodes_reply_summary(counter)

        ### Start Beam Updater
        yield self.beam_updater.start()
        yield self.beam_updater.setup_sampling_with_retries()

        self.set_state(State.started)

    @gen.coroutine
    def capture_stop(self, product_id, force=False):
        if not force:
            self.check_product_id(product_id)
            self.check_state(State.started)

        self.set_state(State.stopping)
        self.set_sb_details({})
        self.get_sensor('fbfuse-nchannels').set_value(0, status=Sensor.UNKNOWN)

        yield self.beam_updater.stop()
        __, counter = yield self.node_pool.issue_request_all('capture_stop')
        self.log_nodes_reply_summary(counter)
        self.set_state(State.configured)

    @gen.coroutine
    def process_cam_request(self, req, action, cleanup=None):
        """
        req : katcp.Message
        action : coroutine that takes no arguments
        cleanup : coroutine that takes the exception that was raised as a single argument, or None
        """
        self.log_request(req)
        req_name = req.msg.name
        cleanup_required = False

        try:
            yield action()

        # Benign errors
        except (WrongProductID, WrongState) as err:
            log.warning(err)
            log.warning("request ?{} fail".format(req_name))
            raise gen.Return(('fail', str(err)))

        # Unhandled errors worth going into error state
        except Exception as err:
            cleanup_required = True
            self.set_state(State.error)
            log.error(err)
            log.info(traceback.format_exc())
            log.error("request ?{} fail".format(req_name))
            raise gen.Return(('fail', str(err)))

        else:
            log.info("request ?{} OK".format(req_name))
            raise gen.Return(('ok',))

        finally:
            if cleanup_required and cleanup is not None:
                log.info("Running cleanup for failed request ?{}".format(req_name))
                try:
                    yield cleanup(err)
                    log.info("Cleanup ran sucessfully")
                except Exception as err:
                    self.set_state(State.error)
                    log.error(
                        "An Exception occurred when running cleanup for failed request "
                        "{!r}".format(req_name)
                    )
                    log.error(err)
                    log.info(traceback.format_exc())

    @request(Str(), Str(), Str())
    @return_reply()
    @gen.coroutine
    def request_configure(self, req, product_id, streams_json, proxy_name):
        """
        Configure TUSE for a new program block

        Parameters
        ----------
        req: katcp.Message
            The katcp request.
        product_id: str
            Name of the data product, for example 'array_1'
        streams_json: str
            JSON-encoded dictionary with keys and values describing the streams, which looks like:
            {"cam.http":{"camdata":"http://10.8.67.113/api/client/1"}}
        proxy_name: str
            The CAM name for the instance of the TUSE proxy that is being
            configured, for example 'tuse_3'
        """
        result = yield self.process_cam_request(
            req,
            lambda : self.configure(product_id, streams_json, proxy_name),
            cleanup=lambda exc: self.deconfigure(product_id, force=True)
        )
        raise gen.Return(result)

    @request(Str())
    @return_reply()
    @gen.coroutine
    def request_deconfigure(self, req, product_id):
        """
        Deconfigure TUSE at the end of a program block

        Parameters
        ----------
        req: katcp.Message
            The katcp request.
        product_id: str
            Name of the data product, for example 'array_1'
        """
        result = yield self.process_cam_request(
            req,
            lambda : self.deconfigure(product_id),
            cleanup=None
        )
        raise gen.Return(result)

    @request(Str())
    @return_reply()
    @gen.coroutine
    def request_capture_init(self, req, product_id):
        """
        Request sent to TUSE when FBFUSE starts provisioning for a new SB. TUSE does nothing at
        this stage (FBFUSE is preparing), so we treat it as a dummy request.
        """
        @gen.coroutine
        def handler():
            raise gen.Return(('ok',))
        result = yield self.process_cam_request(req, handler, cleanup=None)
        raise gen.Return(result)

    @request(Str())
    @return_reply()
    @gen.coroutine
    def request_capture_done(self, req, product_id):
        """
        Request sent to TUSE at the very end of an SB, after capture-stop. We treat it as a
        dummy request.
        """
        @gen.coroutine
        def handler():
            raise gen.Return(('ok',))
        result = yield self.process_cam_request(req, handler, cleanup=None)
        raise gen.Return(result)

    @request(Str())
    @return_reply()
    @gen.coroutine
    def request_capture_start(self, req, product_id):
        """
        Request sent to TUSE when FBFUSE has finished provisioning. TUSE can now read FBFUSE
        sensors, configure the nodes appropriately, and start capturing data.
        """
        @gen.coroutine
        def cleanup(exc):
            alert_msg = sbfail_message(exc, sb_details=self.sb_details)
            yield send_slack_alert(alert_msg)
            yield send_mattermost_alert(alert_msg)
            yield self.capture_stop(product_id, force=True)

        result = yield self.process_cam_request(
            req, 
            lambda : self.capture_start(product_id), 
            cleanup=cleanup
        )
        raise gen.Return(result)

    @request(Str())
    @return_reply()
    @gen.coroutine
    def request_capture_stop(self, req, product_id):
        """
        Request sent to TUSE when the current SB ends. TUSE stops capturing and clears the current
        configuration parameters relevant to the finished SB.
        """
        result = yield self.process_cam_request(
            req, 
            lambda : self.capture_stop(product_id), 
            cleanup=None
        )
        raise gen.Return(result)

    @request(Str(), Str(), Str())
    @return_reply()
    @gen.coroutine
    def request_update_node_config_param(self, req, node_range, key, value_str):
        """ 
        Update configuration parameter (dot notation accepted) of the given range of node numbers. 
        The specified parameter must exist in the node's YAML config file. The change may not take 
        effect immediately, e.g. changing a pipeline parameter will only take effect the next time
        it is started. NOTE: Limited sanity checks are performed on the actual value passed,
        so be careful.

        Parameters
        ----------
        node_range: str
            String that specifies the node numbers to update. This is a comma separated list of 
            python slices.
            - ':' means all nodes
            - '1,5,31:34' means nodes 1, 5, 31, 32, 33
            - '::4' means nodes 0, 4, 8, 12, etc.
        key: str
            Name of the parameter to update. Can be provided in dot notation when accessing a 
            nested parameter, e.g. 'pipeline.mode'
        value_str: str
            New value for the parameter, passed as a string. If the new value cannot be cast to the
            type of the existing value for this parameter, the update will be rejected by the node.
        """
        self.log_request(req)
        node_numbers = slicelist_to_indices(parse_slicelist_string(node_range), self.NUM_NODES)
        fail_numbers, counter = yield self.node_pool.issue_request(
            'update_config_param', node_numbers, key, value_str)
        self.log_nodes_reply_summary(counter, req=req)

        if fail_numbers:
            msg = 'Update FAILED on {} node(s), check logs for details'.format(len(fail_numbers))
            raise gen.Return(('fail', msg))
        else:
            raise gen.Return(('ok',))

    @request(Str(), Int())
    @return_reply()
    @gen.coroutine
    def request_set_pipeline_enabled(self, req, node_range, flag):
        """
        Enable or disable the pipeline of the specified range of node numbers. This can trigger an 
        immediate start or stop of said pipelines if the other conditions for starting/stopping are
        met.

        Parameters
        ----------
        node_range: str
            String that specifies the node numbers to update. This is a comma 
            separated list of python slices.
            - ':' means all nodes
            - '1,5,31:34' means nodes 1, 5, 31, 32, 33
            - '::4' means nodes 0, 4, 8, 12, etc.
        flag: int
            1 to enable pipeline, 0 to disable it.
        """
        self.log_request(req)
        node_numbers = slicelist_to_indices(parse_slicelist_string(node_range), self.NUM_NODES)
        __, counter = yield self.node_pool.issue_request(
            'set_pipeline_enabled', node_numbers, flag)
        self.log_nodes_reply_summary(counter, req=req)
        raise gen.Return(('ok',))

    @request(Int(), Int())
    @return_reply(Str())
    @gen.coroutine
    def request_set_node_capacity(self, req, nchan, capacity):
        """
        Set the processing capacity of all nodes to 'capacity' for mode 'nchan'. Changes will only 
        be effective on the next ?capture-start

        Parameters
        ----------
        nchan : int
            Number of channels (mode) for which to edit the node capacity
        capacity : int
            New processing capacity of all nodes for this mode
        """
        self.log_request(req)
        try:
            self.set_node_capacity(nchan, capacity)
        except Exception as err:
            log.error(err)
            raise gen.Return(('fail', str(err)))
        msg = "Changed node capacity for {} channel mode to {}".format(nchan, capacity)
        raise gen.Return(('ok', msg))

    @request(Int())
    @return_reply(Str())
    @gen.coroutine
    def request_set_ib_only(self, req, flag):
        """
        Activate / deactivate IB-Only mode. Changes will only be effective on the next 
        ?capture-start

        Parameters
        ----------
        flag : int
            1 to enable IB-Only mode, 0 to disable it
        """
        self.log_request(req)
        flag = bool(flag)
        try:
            self.set_ib_only(flag)
        except Exception as err:
            log.error(err)
            raise gen.Return(('fail', str(err)))
        msg = "Set ib_only = {}".format(flag)
        raise gen.Return(('ok', msg))


###############################################################################


def parse_args():
    parser = argparse.ArgumentParser(
        description="Launch the TUSE MasterController",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument(
        "-H", "--host", 
        help="Host IP or DNS address.", default="0.0.0.0",
    )
    parser.add_argument(
        "-p", "--port", 
        help="Port to use.", type=int, default=5000,
    )
    parser.add_argument(
        '-n', '--nodes',
        help='YAML configuration file with the specs (host, port, vlan) of the nodes to use.',
        type=str,
        required=True
    )
    parser.add_argument(
        "-l", "--loglevel", 
        help="Logging level", default="DEBUG"
    )
    parser.add_argument(
        "-f", "--logfile", 
        help="If specified, write logs to this file. A RotatingFileHandler " 
            "with a period of 1 day is used.", 
        default=None
    )
    return parser.parse_args()


@gen.coroutine
def shutdown(ioloop):
    log.info("Shutting down")
    ioloop.stop()


@gen.coroutine
def start(master_controller):
    log.info("Starting MasterController, tusemcs version: {}".format(__version__))
    yield master_controller.start()
    log.info("Listening at {}, Ctrl-C to terminate server".format(master_controller.bind_address))


def main():
    logging.getLogger('katcp').setLevel('WARNING')
    logging.getLogger('kat').setLevel('WARNING')
    logging.getLogger('tornado').setLevel('INFO')
    logging.getLogger('urllib3').setLevel('INFO')
    fmt = "[%(levelname)s - %(asctime)s - %(name)s - %(filename)s:%(lineno)s] %(message)s"

    args = parse_args()

    if args.logfile:
        # Redirect ALL logging to file
        root_logger = logging.getLogger()
        root_handler = logging.handlers.TimedRotatingFileHandler(
            args.logfile, when="d", interval=1, utc=True
        )
        root_handler.setFormatter(logging.Formatter(fmt=fmt))
        root_logger.addHandler(root_handler)
    
    coloredlogs.install(fmt=fmt, level=args.loglevel, milliseconds=True)

    # Hook up to SIGINT and SIGTERM so that both ctrl-C and docker stop result in a clean shutdown
    ioloop = tornado.ioloop.IOLoop.current()
    shutdown_func = lambda sig, frame: ioloop.add_callback_from_signal(shutdown, ioloop)
    signal.signal(signal.SIGINT, shutdown_func)
    signal.signal(signal.SIGTERM, shutdown_func)

    with open(args.nodes, 'r') as fobj:
        node_specs = yaml.safe_load(fobj)

    master_controller = MasterController(args.host, args.port, node_specs)
    ioloop.add_callback(start, master_controller)
    ioloop.start()



if __name__ == "__main__":
    main()
