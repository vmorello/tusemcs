#
# Convenient wrapper class for all beam parameters
# Vincent Morello - 2019-2020
#
import katpoint

from pipeline_utils import output_dada_key


class Beam(object):
    """ 
    Convenient wrapper class for beam parameters, useful for generating
    configuration files.

    Parameters
    ----------
    relnum : int
        Relative beam number, or in other words, beam index on this specific
        compute node. Should be in the range [0, nbeams-1] where nbeams is
        the number of beams captured.
    mc_address : str
        Multicast address associated to this beam, with the port specified
        e.g. '239.1.1.42:7147'
    fbfuse_id : str
        Beam ID string such as 'cfbf00042' for a coherent beam, or 'ifbf00000' 
        for the incoherent beam. The abosulte beam number is parsed from this.
    target: katpoint.Target
        Target the beam is pointing at
    """
    def __init__(self, relnum, mc_address, fbfuse_id, target):
        assert isinstance(relnum, int)
        assert isinstance(mc_address, basestring)
        assert isinstance(fbfuse_id, basestring)
        assert isinstance(target, katpoint.Target)
        self.relnum = relnum
        self.target = target
        self.mc_address = mc_address
        self.fbfuse_id = fbfuse_id
        (self.ra_hms, self.dec_dms) = tuple(map(str, self.target.radec()))

    @property
    def source(self):
        """ Name of the katpoint.Target member, with spaces stripped """
        return self.target.name.strip().replace(' ', '')

    @property
    def absnum(self):
        """ Absolute beam number (int) """
        return int(self.fbfuse_id.split('fbf')[-1])
    
    @property
    def coherent(self):
        return not self.fbfuse_id.lower().startswith('i')

    @property
    def mc_ip(self):
        return self.mc_address.split(':')[0]

    @property
    def mc_port(self):
        return int(self.mc_address.split(':')[1])

    @property
    def dada_key(self):
        """
        DADA key of the output buffer associated to the relative beam number.
        Keys are chosen so as to avoid name collisions. beam 0 gets 'b000', 
        beam 1 gets 'b010', etc. We reserve 'bff0' for the input DADA buffer.
        """
        return output_dada_key(self.relnum)

    @property
    def outdir_name(self):
        """
        Name of the processing output directory for this beam, e.g. 'beam00'
        """
        return "beam{:02d}".format(self.relnum)

    def asdict(self):
        """
        Returns a dictionary of useful attributes to be saved under a JSON
        file summarizing the pipeline config for a specific run
        """
        attr_names = [
            'absnum', 'relnum', 'coherent', 'source', 'ra_hms', 'dec_dms', 
            'mc_ip', 'mc_port'
            ]
        return {
            name: getattr(self, name)
            for name in attr_names
        }

    def __str__(self):
        fmt = "Beam(mc_address={s.mc_ip}:{s.mc_port}, fbfuse_id={s.fbfuse_id}, relnum={s.relnum}, absnum={s.absnum}, coherent={s.coherent}, source={s.source}, ra={s.ra_hms}, dec={s.dec_dms})"
        return fmt.format(s=self)

    def __repr__(self):
        return str(self)


def validate_beams(beams):
    """
    Validate a list / iterable of Beam objects. Checks that:
    - beams is not empty
    - beams is sorted in relative beam number order
        This enforces consistent order for mkrecv and transpose module
    - all beams are of the same type (coherent / incoherent)
    - all beams have the same multicast port
    - all relative numbers are distinct
    - all absolute numbers are distinct

    Parameters
    ----------
    beams : list or iterable
        Beam objects to be validated

    Raises
    ------
    ValueError : if the list of beams is not valid
    """
    if not beams:
        raise ValueError("'beams' is empty")

    if not list(beams) == sorted(beams, key=lambda b: b.relnum):
        raise ValueError("beams must be sorted by increasing relative number")

    if not len(set(b.coherent for b in beams)) == 1:
        raise ValueError("All beams must be of the same type (coherent or incoherent)")

    if not len(set(b.mc_port for b in beams)) == 1:
        raise ValueError("All multicast group addresses must have the same port")

    if not len(set(b.relnum for b in beams)) == len(beams):
        raise ValueError("All beams must have distinct relative numbers")

    if not len(set(b.absnum for b in beams)) == len(beams):
        raise ValueError("All beams must have distinct absolute numbers")



if __name__ == "__main__":
    import katpoint
    import json
    target = katpoint.Target("J1151-6108,radec,11:51:56.86,-61:08:17.6")

    beam = Beam(0, "239.1.1.1:7147", "cfbf00042", target)
    print(beam.asdict())
    print(beam)

    beam = Beam(1, "239.1.1.1:7147", "ifbf00000", target)
    print(beam.asdict())
    print(beam)

    beams = [
        Beam(ii, "239.1.1.1:7147", "cfbf{:05d}".format(ii + 42), target)
        for ii in range(6)
    ]
    print(json.dumps(beam.asdict(), indent=4))
    validate_beams(beams)