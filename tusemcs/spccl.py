import os
import re
import time
import logging
import json
import socket
from collections import namedtuple

import scandir
from astropy.time import Time

from tusemcs.pipeline_utils import UTC_FORMAT, get_directory_path


log = logging.getLogger('tusemcs.spccl')


def get_node_number():
    """ Return the node number, or -1 in case of failure """
    try:
        return int(re.match(r'tpn-0-(\d+).meertrap', socket.gethostname()).group(1))
    except:
        return -1

NODE_NUMBER = get_node_number()
BEAM_REGEX = re.compile(r'beam\d{2}')

# NOTE: width is in milliseconds
# t is a UNIX timestamp
SpcclEntry = namedtuple('SpcclEntry', 'node beam t mjd dm width snr')

class SpcclEntrySet(set):
    def sorted_list(self):
        """ List of entries sorted by MJD"""
        key = lambda e: e.mjd
        return sorted(self, key=key)


def spccl_parse(fname, beam=None):
    """
    Parse spccl file into a SpcclEntrySet object.
    """
    with open(fname, 'r') as f:
        rows = f.read().strip().split('\n')

    values = []
    for row in rows[1:]: # Skip header row
        cols = row.strip().split('\t') # NOTE: columns are tab separated
        values.append(tuple(map(float, cols)))

    # Calculate unix timestamps
    timestamps = Time([v[0] for v in values], format='mjd').unix
    entries = SpcclEntrySet()
    for v, t in zip(values, timestamps):
        entry = SpcclEntry(node=NODE_NUMBER, beam=beam, t=t, mjd=v[0], dm=v[1], width=v[2], snr=v[3])
        entries.add(entry)
    return entries


def load_utc_spccl_entries(dirpath, bnum_mapping=None):
    """
    Load all spccl entries within given UTC output directory.

    Parameters
    ----------
    dirpath : str
        Path to UTC directory to load
    bnum_mapping : dict
        Dictionary where keys are relative beam numbers (int), and the values are absolute beam
        numbers (int). If not specified, spccl candidates are given a relative beam number instead
        of an absolute one.

    Returns
    -------
    entries : SpcclEntrySet
    """
    entryset = SpcclEntrySet()
    for be in scandir.scandir(dirpath):
        if not (be.is_dir() and BEAM_REGEX.match(be.name)):
            continue
        relnum = int(be.name[-2:])
        absnum = bnum_mapping[relnum] if bnum_mapping else relnum
        for fe in scandir.scandir(be.path):
            if fe.name.endswith('.spccl'):
                entryset.update(spccl_parse(fe.path, beam=absnum))
    return entryset


class SpcclWatcher(object):
    """
    Object that keeps an up to date list of recent spccl entries.

    Parameters
    ----------
    bnum_mapping : dict or None, optional
        Dictionary where keys are relative beam numbers (int), and the values are absolute beam
        numbers (int). If not specified, spccl candidates are given a relative beam number instead
        of an absolute one.
    age_max : float, optional
        Delete stored spccl entries when they exceed this age in seconds.
    entries_max : int, optional
        Maximum number of spccl entries that can be stored at any time. The oldest entries get
        deleted as necessary when that number is exceeded.
    basedir_override : str or None, optional
        For test purposes outside of the control docker container. If specified, use this
        path as the base output directory of the pipeline.
    """
    def __init__(self, bnum_mapping=None, age_max=21600.0, entries_max=1000, basedir_override=None):
        self.utc = None
        self.age_max = age_max
        self.entries_max = entries_max
        self.bnum_mapping = bnum_mapping
        self.basedir_override = basedir_override
        self.entryset = SpcclEntrySet()

    def set_pipeline_utc(self, utc):
        """ Set current pipeline UTC start as a datetime object """
        self.utc = utc

    def _ingest(self):
        """ Load spccl files from currently active UTC output dir """
        if not self.utc:
            return
        dirpath = get_directory_path('control', self.utc.strftime(UTC_FORMAT), 'output')
        if self.basedir_override is not None:
            dirpath = os.path.join(self.basedir_override, self.utc.strftime(UTC_FORMAT))
        loaded = load_utc_spccl_entries(dirpath, self.bnum_mapping)
        self.entryset.update(loaded)

    def _prune(self):
        """ 
        Remove spccl entries that exceed age_max; also, remove the required number of oldest 
        entries if entries_max is exceeded
        """
        tref = time.time() - self.age_max
        old = SpcclEntrySet([
            e for e in self.entryset
            if e.t < tref
        ])
        self.entryset = self.entryset.difference(old)

        if len(self.entryset) > self.entries_max:
            self.entryset = SpcclEntrySet(
                self.entryset.sorted_list()[-self.entries_max:]
            )

    def update(self):
        """ Load new entries from most recent spccl file, prune old entries """
        self._ingest()
        self._prune()

    def sensor_json(self):
        """
        JSON-encoded list of dicts to be published as the sensor value. Each dict contains the
        parameters of a single spccl entry.
        """
        return json.dumps([e._asdict() for e in self.entryset.sorted_list()])
