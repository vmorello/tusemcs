import os
import json
import logging
import argparse
import itertools
import math

import numpy as np
import yaml
import coloredlogs
import tornado
import katpoint
import astropy.units as uu
from astropy.coordinates import SkyCoord
from tornado import gen
from tornado.httpserver import HTTPServer
from tornado.options import define, options
from tornado.web import Application, RequestHandler, StaticFileHandler
from tornado.template import Template, Loader

from device_monitor import DeviceMonitor, utc_string
from nodes_monitor import NodesMonitor
from snapshots_manager import SnapshotsManager
from tusemcs.catalogue import query_radius


log = logging.getLogger('tusemcs.suricat')


class DevmonDataHandler(RequestHandler):
    """ 
    Wraps a DeviceMonitor connected to a single KATCP server, and serves its JSON data
    """
    def initialize(self, devmon=None):
        """
        """
        if not isinstance(devmon, DeviceMonitor):
            raise ValueError("devmon must be a DeviceMonitor")
        self.devmon = devmon

    def get(self):
        data = self.devmon.sensor_data()
        self.write(json.dumps(data))


class NodesMonitorDataHandler(RequestHandler):
    """
    Wraps the NodesMonitor and serves the nodes table JSON data
    """
    # Set of sensor names to exclude from the JSON feed
    EXCLUDE_SENSORS = set([
        'bandpass_mean',
        'bandpass_std',
        'last_candidate_snapshot',
        'spccl_candidates'
    ])

    def initialize(self, monitor=None):
        if not isinstance(monitor, NodesMonitor):
            raise ValueError("monitor must be a NodesMonitor")
        self.monitor = monitor

    def get(self):
        data = self.monitor.sensor_values(exclude_sensors=self.EXCLUDE_SENSORS)
        self.write(json.dumps(data))


class BandpassDataHandler(RequestHandler):
    """
    Serves the bandpass JSON feed for the requested node. It expects an URL of the form:
    /bandpass.json?node={NODE_NUMBER}

    If 'node' is not specified, the data for node 0 is served by default.
    """
    # Default reply if no data is available for requested node, or if request is malformed
    DEFAULT_DATA = {
        'beam': 'N/A',
        'utc': 'N/A',
        'std': [],
        'mean': [],
    }

    def initialize(self, monitor=None):
        if not isinstance(monitor, NodesMonitor):
            raise ValueError("monitor must be a NodesMonitor")
        self.monitor = monitor

    def get(self):
        # Get node number request argument
        node = self.get_argument('node', default=0)
        log.debug("Fetching bandpass data for node = {}".format(node))

        try:
            node = int(node)
        except:
            log.warning("request argument 'node' should be an int")
            self.write(json.dumps(self.DEFAULT_DATA))
            return

        if node not in self.monitor.device_monitors:
            log.warning("No such node number: {}".format(node))
            self.write(json.dumps(self.DEFAULT_DATA))
            return

        devmon = self.monitor.device_monitors[node]
        if not 'beam_ids' in devmon.readings:
            log.warning("'beam_ids' sensor not available for node = {}".format(node))
            self.write(json.dumps(self.DEFAULT_DATA))
            return

        beams_csv = devmon.readings['beam_ids'].value
        # NOTE: don't cast beam_ids to int, one of them could be the string 'IB'
        beam_ids = beams_csv.split(',') if beams_csv else [] 
        nbeams = len(beam_ids)

        if not beam_ids:
            self.write(json.dumps(self.DEFAULT_DATA))
            return

        std_reading = devmon.readings['bandpass_std']
        mean_reading = devmon.readings['bandpass_mean']
        utc = utc_string(std_reading.timestamp)

        # Sensor readings contain bandpass data for all beams
        stds = np.fromstring(
            std_reading.value, dtype=np.float16).reshape(nbeams, -1).astype(float)
        means = np.fromstring(
            mean_reading.value, dtype=np.float16).reshape(nbeams, -1).astype(float)
        
        # Keep only data for first beam
        stds = list(stds[0])
        means = list(means[0])
        
        data = {
            'beam': beam_ids[0],
            'utc': utc,
            'std': stds,
            'mean': means,
        }
        self.write(json.dumps(data))


class SnapshotsDataHandler(RequestHandler):
    """ Serves the candidate snapshots JSON feed """
    def initialize(self, manager=None):
        if not isinstance(manager, SnapshotsManager):
            raise ValueError("manager must be a SnapshotsManager")
        self.manager = manager

    def get(self):
        data = self.manager.get_data()
        self.write(json.dumps(data))


class HeaderDataHandler(RequestHandler):
    """ Serves the JSON feed with just the info necessary to fill the dymanic page header """
    def initialize(self, fbfuse, mc):
        if not isinstance(fbfuse, DeviceMonitor):
            raise ValueError("fbfuse must be a DeviceMonitor")
        if not isinstance(mc, DeviceMonitor):
            raise ValueError("mc must be a DeviceMonitor")
        self.fbfuse = fbfuse
        self.mc = mc

    def get(self):
        def get_reading_value(devmon, sname):
            if sname in devmon.readings:
                return devmon.readings[sname].value
            return None
        
        sb_details = get_reading_value(self.mc, 'sb_details')
        if not sb_details: # Ensure we have a dict
            sb_details = {}

        # Get the phase_reference sensor on FBFUSE for the currently active product ID
        product_id = get_reading_value(self.mc, 'product_id')
        phase_reference_sensor = '{}_phase_reference'.format(product_id) if product_id else \
            'array_1_phase_reference'

        data = {
            # TODO: get the right product_id, not just array_1
            'target': get_reading_value(self.fbfuse, phase_reference_sensor),
            'state': get_reading_value(self.mc, 'state'),
            'sb_id': sb_details.get('id_code', None),
            'sb_description': sb_details.get('description', None)
        }
        self.write(json.dumps(data))


class BeamPositionsHandler(RequestHandler):
    """ Serves the beams and nearby known pulsar positions JSON feed """
    def initialize(self, monitor=None):
        if not isinstance(monitor, NodesMonitor):
            raise ValueError("monitor must be a NodesMonitor")
        self.monitor = monitor

    def get(self):
        beams = []
        ib = None

        for inode, devmon in self.monitor.device_monitors.items():
            reading = devmon.readings.get('beam_positions', None)
            reading_value = reading.value if reading else {}
            for fbfuse_id, target_str in reading_value.items():
                target = katpoint.Target(target_str)
                ra, dec = target.astrometric_radec() # NOTE: ra and dec are ephem.Angle objects
                beam = dict(
                    id=fbfuse_id,
                    node=inode,
                    source=target.name,
                    rajd=round(math.degrees(ra), 6),
                    decjd=round(math.degrees(dec), 6),
                    ra=str(ra),
                    dec=str(dec)
                )
                beams.append(beam)
                if fbfuse_id.startswith('ifbf'):
                    ib = beam

        if not ib:
            pulsars = []
        else:
            centre = SkyCoord(ib['rajd'], ib['decjd'], unit=(uu.degree, uu.degree))
            # NOTE: radius should be dependent on observing frequency
            # the Airy disk half width (radius) at half maximum is 0.514 x lambda/D
            # That's 0.51 deg at 1284 MHz (L-Band), 0.80 deg at 816 MHz (UHF)
            radius = 0.66
            log.debug("Query: centre = {!r}, radius = {} deg".format(centre, radius))
            pulsars = query_radius(centre, radius)
        
        data = dict(beams=beams, pulsars=pulsars)
        self.write(json.dumps(data))


class SpcclDataHandler(RequestHandler):
    """ Serves the JSON feed containing the list of recent spccl candidates across the cluster """
    def initialize(self, monitor=None):
        if not isinstance(monitor, NodesMonitor):
            raise ValueError("monitor must be a NodesMonitor")
        self.monitor = monitor

    def get(self):
        data = []
        for inode, devmon in self.monitor.device_monitors.items():
            reading = devmon.readings.get('spccl_candidates', None)
            value = reading.value if reading else []
            data.extend(value)
        self.write(json.dumps(data))


class MainPageHandler(RequestHandler):
    """ Serves the main page """
    def initialize(self):
        pass

    def get(self):
        self.render('nodes.html')


@gen.coroutine
def initialize_device_monitors(config):
    # TODO: add parsers for FBFUSE and MC
    # Maybe move this to config file
    ca_parsers = {
        r'.+_beam_position_configuration': json.loads,
        r'.+_schedule_block_configuration': json.loads,
        r'.+_target_configuration_template': json.loads,
    }

    fbfuse_parsers = {
        r'.+_available_antennas': json.loads,
        r'.+_ca_sb_config_request': json.loads,
        r'.+_ca_target_request': json.loads,
        r'.+_coherent_beam_shape': json.loads,
        r'.+_coherent_beam_multicast_group_mapping': json.loads
    }

    mc_parsers = {
        r'node_capacities': json.loads,
        r'sb_details' : json.loads,
    }

    device_monitors = {
        'mc': DeviceMonitor(config['mc']['host'], config['mc']['port'], parsers=mc_parsers),
        'ca': DeviceMonitor(config['ca']['host'], config['ca']['port'], parsers=ca_parsers),
        'fbfuse': DeviceMonitor(config['fbfuse']['host'], config['fbfuse']['port'], parsers=fbfuse_parsers),
    }

    futures = []
    for devmon in device_monitors.values():
        futures.append(devmon.start())

    for f in futures:
        yield f
    raise gen.Return(device_monitors)


@gen.coroutine
def initialize_nodes_monitor(config):
    nodes_monitor = NodesMonitor(config['tuse_nodes'])
    yield nodes_monitor.start()
    raise gen.Return(nodes_monitor)


@gen.coroutine
def resync_loop(device_monitors, nodes_monitor, period=20.0):
    """
    Periodically reapply sampling strategies, in order to discover new sensors that appear on
    FBFUSE or the CA, or simply to reconnect to a device that previously went offline.
    """
    log.info("Start resync loop, period = {:.1f} seconds".format(period))
    while True:
        yield gen.sleep(period)
        log.debug("Resync clients")
        futures = []
        iterable = itertools.chain(
            device_monitors.values(), 
            nodes_monitor.device_monitors.values()
        )
        for devmon in iterable:
            futures.append(devmon.setup_sampling())
        for f in futures:
            yield f


@gen.coroutine
def refresh_snapshots_loop(snapshots_manager, period=10.0):
    log.info("Start snapshots refresh loop, period = {:.1f} seconds".format(period))
    while True:
        log.debug("Refresh snapshots")
        snapshots_manager.refresh()
        yield gen.sleep(period)


@gen.coroutine
def run_application(config_file):
    with open(config_file, 'r') as fobj:
        config = yaml.safe_load(fobj)

    log.info("Starting KATCP clients ...")
    device_monitors = yield initialize_device_monitors(config)
    nodes_monitor = yield initialize_nodes_monitor(config)
    log.info("KATCP clients running")

    snapshots_manager = SnapshotsManager(
        getter=lambda : nodes_monitor.get_readings('last_candidate_snapshot')
    )

    ### Configure WebServer ###
    define('port', default=42000)

    # Map http://<SERVER_ADDRESS>/static to the directory 'web'
    static_path = os.path.join(os.path.dirname(__file__), 'web')
    static_path = os.path.realpath(static_path)

    settings = dict(
        autoreload=False, 
        static_path=static_path, 
        template_path=static_path,
        # Save bandwidth when browsers periodically pull JSON data
        compress_response=True
    )

    handlers = [
        (r'/', MainPageHandler),
        (r'/(.*\.(html|css|js|png))', StaticFileHandler, dict(path=static_path)),
        (r'/mc.json', DevmonDataHandler, dict(devmon=device_monitors['mc'])),
        (r'/ca.json', DevmonDataHandler, dict(devmon=device_monitors['ca'])),
        (r'/fbfuse.json', DevmonDataHandler, dict(devmon=device_monitors['fbfuse'])),
        (r'/nodes.json', NodesMonitorDataHandler, dict(monitor=nodes_monitor)),
        (r'/bandpass.json', BandpassDataHandler, dict(monitor=nodes_monitor)),
        (r'/snapshots.json', SnapshotsDataHandler, dict(manager=snapshots_manager)),
        (r'/header.json', HeaderDataHandler, dict(fbfuse=device_monitors['fbfuse'], mc=device_monitors['mc'])),
        (r'/positions.json', BeamPositionsHandler, dict(monitor=nodes_monitor)),
        (r'/spccl.json', SpcclDataHandler, dict(monitor=nodes_monitor))
    ]

    app = Application(handlers, **settings)
    log.info("Settings: {}".format(json.dumps(app.settings)))
    http_server = HTTPServer(app)
    http_server.listen(options.port)
    log.info('Listening on http://localhost:{}'.format(options.port))
    
    ioloop = tornado.ioloop.IOLoop.current()
    ioloop.add_callback(
        resync_loop, device_monitors, nodes_monitor, period=config['resync_period']
    )
    ioloop.add_callback(
        refresh_snapshots_loop, snapshots_manager, period=config['snapshots_refresh_period']
    )


def parse_args():
    parser = argparse.ArgumentParser(
        description="Launch the TUSE Web Monitor",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument(
        "-c", "--config", 
        help="Configuration file path", 
        default=os.path.join(os.path.dirname(__file__), 'suricat.yaml'),
    )
    parser.add_argument(
        "-l", "--loglevel", 
        help="Logging level", default="INFO",
        choices=['DEBUG', 'INFO', 'WARNING']
    )
    return parser.parse_args()


def main():
    coloredlogs.install(
        fmt="[%(levelname)s - %(asctime)s - %(name)s - %(filename)s:%(lineno)s] %(message)s",
        milliseconds=True
    )
    logging.getLogger("katcp").setLevel(logging.WARNING)
    logging.getLogger("kat").setLevel(logging.WARNING)
    logging.getLogger("tornado").setLevel(logging.WARNING)

    args = parse_args()
    log.setLevel(args.loglevel)
    log.info("Using configuration file: {}".format(args.config))
    ioloop = tornado.ioloop.IOLoop.current()
    ioloop.add_callback(run_application, args.config)
    ioloop.start()


if __name__ == '__main__':
    main()