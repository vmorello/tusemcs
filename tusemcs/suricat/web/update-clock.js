window.setInterval(updateClock, 1000);


function getUTCnow() {
    // https://stackoverflow.com/a/6777470
    var now = new Date(); 
    var utc_timestamp =  Date.UTC(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate(),
        now.getUTCHours(), now.getUTCMinutes(), now.getUTCSeconds());
    return new Date(utc_timestamp);
}


function updateClock() {
    var utc_string = getUTCnow().toISOString().replace('T', ' ')
    utc_string = utc_string.substring(11, utc_string.length - 5);
    $('#header #utc').html('UTC: ' + utc_string);
}
