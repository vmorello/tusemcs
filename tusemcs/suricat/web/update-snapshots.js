window.setInterval(updateSnapshots, 10000);


function updateSnapshots() {
    var json_url = '/snapshots.json';
    console.log(`Getting JSON data from ${json_url}`);
    $.getJSON(json_url).done(updateSnapshotContainers);
}


function createSnapshotContainers() {
    var nrows = 11;
    var ncols = 6;
    var default_url = "/images/nodata.png";

    for (var irow = 0; irow < nrows; ++irow) {
        var row = $('<tr>')

        for (var icol = 0; icol < ncols; ++icol) {
            var inode = irow * ncols + icol;
            
            var imgbox = $('<div>').addClass('imgbox');
            var inode_text = $('<div>').addClass('node').text(inode);
            var utc_text = $('<div>').addClass('utc');

            var header = $('<div>').addClass('header').append(inode_text, utc_text);
            var container = $('<div>').attr('id', inode).addClass('container').append(header, imgbox);

            var cell = $('<td>').append(container);
            row.append(cell);
        }
        $('#snapshots-table').append(row);
    }
}


function snapshotOpacity(age) {
    // age is in seconds
    if (age > 600.0)
        return 0.0;
    else if (age < 120.0)
        return 1.0;
    else
        return 1.0 - (age - 120.0) / 480.0;
}


function updateSnapshotContainers(data) {
    var now = getUTCnow();

    $.each(data, function(index, entry) {
        $(`#snapshots-table #${entry.node} img`).attr("src", entry.url);

        // NOTE: Need to add a suffix 'Z' for it to be parsed a UTC date
        var utc = new Date(entry.utc + 'Z');
        var age = (now - utc) / 1000.0; // seconds
        var opacity = snapshotOpacity(age); // 0 = transparent, 1 = fully visible

        if (entry.url != null) {
            $(`#snapshots-table #${entry.node} .utc`).text(entry.utc.slice(-8));
            var imgbox = $(`#snapshots-table #${entry.node} .imgbox`);
            imgbox.empty();
            imgbox.append( $('<img>').attr('src', entry.url).css('opacity', opacity) );

            $(`#snapshots-table #${entry.node}`).hover(
                function() { $(this).find('img').css('opacity', 1.0); },
                function() { $(this).find('img').css('opacity', opacity); }
            )
        }

        else {
            $(`#snapshots-table #${entry.node} .utc`).text('');
        }
    });
}
