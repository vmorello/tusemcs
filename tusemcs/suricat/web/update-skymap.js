window.setInterval(updateSkymap, 5000);


function updateSkymap() {
    var json_url = '/positions.json';
    console.log(`Getting JSON data from ${json_url}`);
    $.getJSON(json_url).done(fillSkymap);
}


function fillSkymap(data) {
    // Base layout to be updated in each plot
    // See: https://plot.ly/javascript/layout-template/
    var layout = {
        showSendToCloud: false,
        title: {
            font: {
                family: 'sans-serif',
                size: 18,
                }
            },

        xaxis: {
            showgrid: true,
            zeroline: false,
            title: {
                text: "Right Ascension (deg)"
            }
        },

        yaxis: {
            showgrid: true,
            zeroline: false,
            title: {
                text: "Declination (deg)"
            }
        },
        hovermode: 'closest',

        // When this attribute is changed, the graph is reset
        // If kept constant, then for example when the user zooms in, calling Plotly.react()
        // won't zoom out unless uirevision is changed
        // https://plotly.com/javascript/uirevision/
        uirevision: 0,

        // Autosize but also specify plot dimensions so that we get a nice
        // high resolution when we download it
        autosize: true,
        width: 1600,
        height: 1200,
        margin: {l: 60, r: 10, b: 60, t: 10, pad: 10},
    };

    // BEAMS
    var beamsTrace = {
        x: [], 
        y: [],

        text: [],
        hovertext: [],
        textposition: 'top',
        textfont: {
            size: 10
        },
        type: 'scatter',
        mode: 'markers+text',
        marker: {
            size: 10,
            color: [],
            colorscale: 'Portland',
            opacity: 0.66
        },
        showlegend: false,
    };

    $.each(data.beams, function(i, beam) {
        if (beam.source == 'unset') // Don't display unset beams
            return;
        beamsTrace.x.push(beam.rajd);
        beamsTrace.y.push(beam.decjd);

        beamsTrace.text.push(parseInt(beam.id.slice(4)));
        beamsTrace.hovertext.push(
            `<b>${beam.id} (Node ${beam.node})</b><br>${beam.source}<br>${beam.ra} ${beam.dec}`
        );
        beamsTrace.marker.color.push(beam.node % 11);
    });

    // KNOWN PULSARS
    var pulsarsTrace = {
        x: [], 
        y: [],

        text: [],
        hovertext: [],
        textposition: 'top',
        textfont: {
            size: 13,
        },
        type: 'scatter',
        mode: 'markers+text',
        marker: {
            size: 16,
            symbol: 'diamond-open-dot',
            color: '#303030',
            line: {
                width: 3
            }
        },
        showlegend: false
    };

    $.each(data.pulsars, function(i, pulsar) {
        pulsarsTrace.x.push(pulsar.rajd);
        pulsarsTrace.y.push(pulsar.decjd);
        pulsarsTrace.text.push(pulsar.jname);

        if (pulsar.dm == null)
            pulsar.dm = 'N/A';
        if (pulsar.s1400 == null)
            pulsar.s1400 = 'N/A';

        pulsarsTrace.hovertext.push(
            `<b>PSR ${pulsar.jname}</b><br>${pulsar.ra} ${pulsar.dec}<br>DM = ${pulsar.dm} pc/cm\u00B3<br>S1400 = ${pulsar.s1400} mJy`
        );
    });

    // This basically ensures that the zoom level is reset only when the data changes
    var hash = 0;
    if (beamsTrace.x.length > 0)
        hash = beamsTrace.x[0] + beamsTrace.y[0];
    
    if (layout.uirevision != hash) {
        layout.xaxis.autorange = true;
        layout.yaxis.autorange = true;
        layout.uirevision = hash;
    }

    Plotly.react("skymap-plot", [beamsTrace, pulsarsTrace], layout);
}