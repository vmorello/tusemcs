window.setInterval(updateBandpass, 5000);


function updateBandpass() {
    var node = $('#node-select').val();
    var json_url = `/bandpass.json?node=${node}`;
    console.log(`Getting JSON data from ${json_url}`);
    $.getJSON(json_url).done(updateBandpassPlots);
}


// Base layout to be updated in each plot
// See: https://plot.ly/javascript/layout-template/
var baseLayout = {
    showSendToCloud: false,
    title: {
        font: {
            family: 'sans-serif',
            size: 18,
            }
        },
    // Autosize but also specify plot dimensions so that we get a nice
    // high resolution when we download it
    autosize: true,
    width: 1400,
    height: 400,
    margin: {l: 40, r: 25, b: 40, t: 30, pad: 10},
    uirevision: 0
};


var templateDefault = Plotly.makeTemplate({
    data: [{}], 
    layout: baseLayout
});


function updateBandpassPlots(JSONData) {
    updateStdPlot(JSONData);
    updateMeanPlot(JSONData);
}


function updateStdPlot(JSONData) {
    var traces = [
        {y: JSONData.std, name: 'std', line: {color: 'steelblue'}},
    ];
    var layout = {
        template: templateDefault,
        title: {
            text: `Standard Deviation: Beam ${JSONData.beam} (UTC ${JSONData.utc})`
        },
        yaxis: {
            autorange: true,
        },
    };
    Plotly.react("bandpass-std", traces, layout);
}


function updateMeanPlot(JSONData) {
    var traces = [
        {y: JSONData.mean, name: 'mean', line: {color: 'orange'}},
    ];
    var layout = {
        template: templateDefault,
        title: {
            text: `Mean: Beam ${JSONData.beam} (UTC ${JSONData.utc})`
        },
        yaxis: {
            range: [-128.0, 128.0],
        },
    };
    Plotly.react("bandpass-mean", traces, layout);
}