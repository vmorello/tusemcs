window.setInterval(updateHeader, 3000);


function updateHeader() {
    var json_url = '/header.json';
    console.log(`Getting JSON data from ${json_url}`);
    $.getJSON(json_url).done(fillHeader);
}


function emdashRep(val) {
    return val != null ? val : '\u2014';
}


function fillHeader(data) {
    $('#header #state').html(emdashRep(data.state));
    $('#header #sb-id').html(emdashRep(data.sb_id));
    $('#header #sb-description').html(emdashRep(data.sb_description));
    $('#header #target').html(emdashRep(data.target));
}
