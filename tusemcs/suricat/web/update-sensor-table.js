window.setInterval(updateSensorTable, 2000);

// devmon.json is a dictionary {sensor_name : sensor_value}
// TODO: implement .fail() to handle the case where fetching the JSON data fails
function updateSensorTable() {
    var device_name = $('table.sensor-table').attr('id');
    var json_url = `/${device_name}.json`;
    console.log(`Getting JSON data from ${json_url}`);
    $.getJSON(json_url).done(fillSensorTable);
}

function fillSensorTable(data) {
    // https://github.com/nodeca/js-yaml#safedump-object---options-
    var yaml_options = {
        flowLevel: 1,
        sortKeys: true
    };

    var headRow = $('<thead>').append(
        $('<th>').text('Name'),
        $('<th>').text('Status'),
        $('<th>').text('Time'),
        $('<th>').text('Value')
    );

    var content = $('<tbody>');

    $.each(data, function(i, entry) {
        var value_cell = $('<td>').addClass('value').text(entry.value);

        if (typeof(entry.value) == 'object') {
            value_cell = $('<td>').addClass('value').append(
                $('<pre>').text(jsyaml.dump(entry.value, yaml_options))
            );
        }

        // This checks if a string is a base64 encoded PNG image
        else if (typeof(entry.value) == 'string' && entry.value.slice(0, 10) == 'iVBORw0KGg' ) {
            value_cell = $('<td>').addClass('value').append(
                $('<img>').attr('src', `data:image/png;base64,${entry.value}`)
            );
        }

        var row = $('<tr>').append(
            $('<td>').text(entry.name).addClass('name'),
            $('<td>').text(entry.status).addClass('status'),
            $('<td>').text(entry.time).addClass('time'),
            value_cell,
        );
        row.appendTo(content);
    });

    $('.sensor-table').html(headRow);
    $('.sensor-table').append(content);
}
