window.setInterval(updateNodesTable, 2000);

// devmon.json is a dictionary {sensor_name : sensor_value}
// TODO: implement .fail() to handle the case where fetching the JSON data fails
function updateNodesTable() {
    var json_url = '/nodes.json';
    console.log(`Getting JSON data from ${json_url}`);
    $.getJSON(json_url).done(fillNodesTable);
}


function formatNumber(x, digits, unit, hidezero=true) {
    if (x == null || (hidezero && x == 0))
        return '';
    return parseFloat(x).toFixed(digits) + unit;
}


function formatUptime(utc_start_str, utc_now_date) {
    if (utc_start_str == '')
        return '';

    // NOTE: utc_start_str comes from the nodes.json feed, and is in the following format:
    // 2021-03-28 14:22:07.94028
    // Need to add a suffix 'Z' for it to be parsed a UTC date
    var utc_start_date = new Date(utc_start_str + 'Z');
    var delta = (utc_now_date - utc_start_date) / 1000.0; // seconds
    var minutes = `${Math.floor(delta / 60.0)}`;
    var seconds = `${Math.floor(delta - 60.0 * minutes)}`;
    if (minutes.length < 2)
        minutes = '0' + minutes;
    if (seconds.length < 2)
        seconds = '0' + seconds;
    return `${minutes}:${seconds}`;
}


function randomQuote() {
    var quotes = [
        "These are not the droids you're looking for.",
        "The Dark Side of the Force is a pathway to many abilities some consider to be unnatural.",
        "Dodge this.",
        "You're faster than this. Don't think you are, know you are.",
        "Do you think that's air you're breathing now ?",
        "Asteroids do not concern me, Admiral. I want that ship, not excuses.",
        "I'm trying to tell you that when you're ready, you won't have to.",
        "You have a problem with authority, Mr. Anderson. You believe you are special, that somehow the rules do not apply to you. Obviously, you are mistaken."
    ];
    var index = Math.floor(Math.random() * quotes.length);
    return quotes[index];
}


function createNodesContent() {

    var store = $('#modal-store');
    var thead = $('<thead>').append(
        $('<th>').text(''),
        $('<th>').text('State'),
        $('<th>').text('Pipeline'),
        $('<th>').text('Uptime'),
        $('<th>').text('CPU%'),
        $('<th>').text('RAM%'),
        $('<th>').text('Space'),
        $('<th>').text('HMiss%'),
        $('<th>').text('IQRM%'),
        $('<th>').text('Hits'),
        $('<th>').text('Cands'),
        $('<th>').text('CLag'),
        $('<th>').text('BeamIDs'),
    );

    var tbody = $('<tbody>');

    $.each(new Array(66), function(n) {
        console.log(`Creating Table Row #${n}`);
        var row = $('<tr>').attr('id', `row-${n}`).append(
            $('<td>').addClass('number').text(n),
            $('<td>').addClass('state').text('offline'),
            $('<td>').addClass('pipeline-state'),
            $('<td>').addClass('uptime'),
            $('<td>').addClass('cpu'),
            $('<td>').addClass('ram'),
            $('<td>').addClass('space'),
            $('<td>').addClass('hmiss'),
            $('<td>').addClass('iqrm'),
            $('<td>').addClass('hits'),
            $('<td>').addClass('cands'),
            $('<td>').addClass('clag'),
            $('<td>').addClass('beam-ids'),
        );
        row.addClass('offline');
        row.appendTo(tbody);

        console.log(`Creating Modal #${n}`);
        var modal = $('<div>').attr('id', `modal-${n}`).addClass('modal');
        modal.click(function(event) {
            if (event.target.id == modal.attr('id'))
                modal.css('display', 'none');
        });
        var closebtn = $('<div>').addClass('closebtn').html('&times;').click(function() {
            modal.css('display', 'none');
        });
        var title = $('<div>').addClass('title').append(
            $('<h2>').text(`Node ${n} Configuration`)
        );

        var content = $('<div>').addClass('content').append(
            title,
            closebtn,
            $('<pre>').append($('<code>').text(randomQuote())) // Prism wants a <code> wrapped in a <pre>
        );
        content.appendTo(modal);
        modal.appendTo(store);

        row.click(function() {
            modal.css('display', 'block');
        });
    });

    $('#nodes-table').append(thead, tbody);
}


function fillNodesTable(data) {
    var utcnow = getUTCnow(); // this is part of update-clock.js

    $.each(data, function(i, node) {
        var n = node.number;
        var row = $(`#nodes-table #row-${n}`);

        // FILL DATA
        // ======================
        if (node.node_state == null) {
            // Case where node is offline
            row.children('td').not('.number').text('').removeClass('attention warning critical'); // Clear text from all cells, except number cell
            row.children('td.number').attr('class', 'number'); // remove status classes from number cell
            row.children('td.state').text('offline');
            row.addClass('offline');
            var modal = $(`#modal-${n} code`);
            // This resets the random quote, but just once after the node goes offline
            if (modal.hasClass('language-yaml'))
                modal.removeClass().text(randomQuote());
            return; // move to next node
        }
        else {
            row.removeClass('offline');
            row.children('.state').text(node.node_state);
        }

        var ptext = node.pipeline_state;
        if (node.pipeline_config.mode == 'filterbank')
            ptext += ' \u2707'; // UTF tape drive icon
        if (node.bpmon_enabled == false)
            ptext += ' \u29B5';
        if (node.pipeline_enabled == false)
            ptext += ' \uD83D\uDD11'; // UTF-16 key icon
        row.children('.pipeline-state').text(ptext);

        // Display pipeline uptime only if it is running
        var uptime = '';
        if (node.pipeline_state == 'running')
            uptime = formatUptime(node.last_pipeline_utc_start, utcnow);
        row.children('.uptime').text(uptime);

        var cpubar = $('<div>').addClass('cpu-bar').css('width', `${node.cpu_usage_percent}%`);
        row.children('.cpu').text(formatNumber(node.cpu_usage_percent, 1, '')).append(cpubar);

        row.children('.ram').text(node.ram_usage_percent);
        row.children('.space').text(formatNumber(node.disk_space_available, 2, ' GB'));
        row.children('.hmiss').text(
            formatNumber(node.mkrecv_missing_heap_fraction_recent * 100.0, 3, '')
        );
        row.children('.iqrm').text(
            formatNumber(node.iqrm_masked_fraction * 100.0, 1, '', false)
        );
        row.children('.hits').text(
            formatNumber(node.aa_hits_recent, 0, '')
        );
        row.children('.cands').text(
            formatNumber(node.candidates_produced, 0, '')
        );
        row.children('.clag').text(
            formatNumber(node.candidate_lag, 0, 's')
        );
        row.children('.beam-ids').text(node.beam_ids);

        $(`#modal-${n} code`).addClass('language-yaml').text(
            jsyaml.safeDump(node.pipeline_config, {indent: 4, sortKeys: true})
        );
        // Syntax highlighting has to be run explicitly on dynamic content, see:
        // https://schier.co/blog/how-to-re-run-prismjs-on-ajax-content
        Prism.highlightElement($(`#modal-${n} code`)[0]);

        // ADD CONDITIONAL STYLES
        // ======================
        // TODO: maybe using IDs for cells would be better ?
        row.children('.number').removeClass().addClass('number'); 
        row.children('td').removeClass('attention warning critical');

        // Node state style
        row.children('.number').addClass(node.node_state);
        // Add additional 'running' or 'error' class if the pipeline is in any of these states
        // Since 'error' is declared last in the CSS, it will override any other style
        if (node.pipeline_state == 'running' || node.pipeline_state == 'error')
            row.children('.number').addClass(node.pipeline_state);

        // Disk space warnings
        if (node.disk_space_available < 10.0)
            row.children('.space').addClass('critical');
        else if (node.disk_space_available < 20.0)
            row.children('.space').addClass('warning');
        else if (node.disk_space_available < 30.0)
            row.children('.space').addClass('attention');

        // Heap miss warnings
        if (node.mkrecv_missing_heap_fraction_recent > 0.03125) // > two bands
            row.children('.hmiss').addClass('critical');
        else if (node.mkrecv_missing_heap_fraction_recent > 0.015625) // > one band
            row.children('.hmiss').addClass('warning');
        else if (node.mkrecv_missing_heap_fraction_recent > 0.0)
            row.children('.hmiss').addClass('attention');            

        // Hits warnings
        if (node.aa_hits_recent > 1000000)
            row.children('.hits').addClass('critical');
        else if (node.aa_hits_recent > 100000)
            row.children('.hits').addClass('warning');
        else if (node.aa_hits_recent > 10000)
            row.children('.hits').addClass('attention');

        // Cands warnings
        if (node.candidates_produced >= 30)
            row.children('.cands').addClass('critical');
        else if (node.candidates_produced >= 10)
            row.children('.cands').addClass('warning');
        else if (node.candidates_produced >= 3)
            row.children('.cands').addClass('attention');

        // Lag warnings
        if (node.candidate_lag >= 90)
            row.children('.clag').addClass('critical');
        else if (node.candidate_lag >= 70)
            row.children('.clag').addClass('warning');
        else if (node.candidate_lag >= 50)
            row.children('.clag').addClass('attention');
        
    }); // end $.each()
}
