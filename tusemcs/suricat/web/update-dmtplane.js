window.setInterval(updateDMTPlane, 5000);


function updateDMTPlane() {
    var json_url = `/spccl.json`;
    console.log(`Getting JSON data from ${json_url}`);
    $.getJSON(json_url).done(updateDMTPlots);
}


var DMTPlaneLayout = {
    showSendToCloud: false,
    title: {
        font: {
            family: 'sans-serif',
            size: 18,
            }
        },

    xaxis: {
        showgrid: true,
        zeroline: false,
        autorange: true,
        title: {
            text: "Time",
            font: {
                family: 'sans-serif',
                size: 18,
            }
        }
    },

    yaxis: {
        showgrid: true,
        zeroline: false,
        range: [1.3, 3.80],
        type: 'log',
        
        title: {
            text: "Dispersion Measure",
            font: {
                family: 'sans-serif',
                size: 18,
            }
        }
    },
    hovermode: 'closest',

    // When this attribute is changed, the graph is reset
    // If kept constant, then for example when the user zooms in, calling Plotly.react()
    // won't zoom out unless uirevision is changed
    // https://plotly.com/javascript/uirevision/
    uirevision: 0,

    // Autosize but also specify plot dimensions so that we get a nice
    // high resolution when we download it
    autosize: true,
    width: 1800,
    height: 900,
    margin: {l: 75, r: 10, b: 55, t: 10, pad: 10},
};


function updateDMTPlots(JSONData) {
    var candsTrace = {
        x: [], 
        y: [],

        text: [],
        hovertext: [],
        textposition: 'top',
        textfont: {
            size: 10
        },
        type: 'scattergl',
        mode: 'markers+text',
        marker: {
            size: [],
            color: [],
            symbol: [],
            colorscale: 'Portland',
            opacity: [],
            line: {
                color: '#000000',
                width: []
            }
        },
        showlegend: false,
    };

    $.each(JSONData, function(i, cand) {
        var date = new Date(1000.0 * cand.t);
        var beam = cand.beam;
        var node = cand.node;

        candsTrace.x.push(date);
        candsTrace.y.push(cand.dm);

        var mkcolor = cand.beam % 10;
        if (beam == -1)
            mkcolor = '#FFFFFF';
        if (beam == 0)
            mkcolor = '#000000';

        candsTrace.marker.color.push(mkcolor);

        // Plot credible pulsar / FRB detections with a different symbol and size
        // Width is in milliseconds
        // Width threshold has been adjusted from a small sample of pulsar detections
        var credible = (cand.snr > 9.0) && (cand.width < 30.0);

        // Default style
        var mksize = Math.min(4 * Math.pow(cand.snr - 6, 0.75), 60);
        var mksymb = 'circle';
        var mkop = 0.25; // opacity
        var mklw = 1; // line (edge) width

        // "Credible" style
        if (credible) {
            mksize = Math.min(6 * Math.pow(cand.snr - 6, 0.75), 100);
            mksymb = 'star';
            mklw = 2;
            mkop = 0.75;
        }
        candsTrace.marker.size.push(mksize);
        candsTrace.marker.symbol.push(mksymb);
        candsTrace.marker.line.width.push(mklw);
        candsTrace.marker.opacity.push(mkop);

        candsTrace.hovertext.push(
            `<b>Beam ${beam} (Node ${node})</b><br>${date.toUTCString()}<br>MJD = ${cand.mjd.toFixed(9)}<br>DM = ${cand.dm.toFixed(1)}<br>W = ${cand.width.toFixed(1)} ms<br>S/N = ${cand.snr.toFixed(1)}`
        );
    });

    Plotly.react("dmtplane-scatter", [candsTrace], DMTPlaneLayout);
}