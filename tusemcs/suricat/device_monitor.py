import signal
import logging
import json
import itertools
import re
from datetime import datetime

import coloredlogs
import tornado
from tornado import gen

from katcp.resource_client import KATCPClientResource
from katcp.resource import KATCPSensorReading, KATCPSensor, escape_name


log = logging.getLogger('tusemcs.suricat.device_monitor')


def utc_string(timestamp):
    """
    Converts POSIX timestamp into a UTC string with format 'YYYY-MM-DD HH:MM:SS'
    """
    return datetime.utcfromtimestamp(timestamp).strftime("%Y-%m-%d %H:%M:%S")


class Listener(object):
    """
    Basic sensor listener.

    Parameters
    ----------
    name : str
        Name of the sensor
    store : dict
        Dictionary {sensor_name : KATCP reading} in which to store incoming readings
    parser : callable or None, optional
        Function applied to the reading value on reception. Useful to decode JSON, for example.
    """
    def __init__(self, name, store, parser=None):
        self.name = name
        self.store = store
        self.parser = parser

    def __call__(self, sensor, reading):
        if self.parser:
            try:
                val = self.parser(reading.value)
            except:
                log.debug("Failed to parse value for {!r}: {}".format(self.name, reading.value))
                val = None
            
            # NOTE: this is a namedtuple so we can't change its values
            # Need to create a modified instance
            reading = KATCPSensorReading(
                reading.received_timestamp, 
                reading.timestamp, 
                reading.istatus, 
                val
            )
        self.store[self.name] = reading


class DeviceMonitor(KATCPClientResource):
    """ 
    KATCP client wrapper with additional methods to retrieve the list of sensor information.

    Parameters
    ----------
    host : str
    port : int
    name : str, optional
        Name given to the parent KATCPClientResource
    parsers : dict
        A dictionary {regex (str) : callable}. If a sensor matches a given pattern, the 
        corresponding function is applied to the sensor value when a reading is received.
    """
    def __init__(self, host, port, name='device-monitor', parsers={}):
        resource_spec = dict(
            name=name,
            address=(host, port),
            controlled=False,
            auto_reconnect=True,
            auto_reconnect_delay=10.0, # seconds
        )
        super(DeviceMonitor, self).__init__(resource_spec)
        self._parsers = parsers

        # Dictionary {sensor_name: KATCP reading}
        # Stores the latest known readings
        self._readings = {}

        # Dictionary {sensor_name: callable}
        # This is to avoid re-creating listener functions when re-synchonising clients
        self._listeners = {}

    @property
    def readings(self):
        """
        Return the readings dictionary if the client is synced, or an empty dict if not. 
        The idea is that we don't want to push stale sensor values to the monitor.
        """
        if self.synced:
            return self._readings
        else:
            return {}

    @property
    def sensor_names(self):
        """ List of all known sensor names """
        return self.sensor.keys()

    @gen.coroutine
    def _remove_sensors(self, sensor_keys):
        """
        We override this parent method so that we get notified when a sensor disappears on the 
        host. We can then delete the associated stale readings.
        """
        yield super(DeviceMonitor, self)._remove_sensors(sensor_keys)
        log.debug("Removing sensors: {}".format(sensor_keys))
        for s in sensor_keys:
            # NOTE: escape_name() is a katcp library method, which converts dashes and
            # dots to underscores. Somehow the names in 'sensor_keys' are NOT normalized, but
            # the names we receive in sensor update callbacks (and thus populate the readings 
            # dictionary) are normalized.
            self._readings.pop(escape_name(s), None)

    def get_parser(self, sname):
        """
        Returns the sensor value parsing function that should be applied to sensor 'sname' when
        a new reading is received. 
        
        If 'sname' does not match any pattern, returns None
        If 'sname' matches multiple patterns, returns the first one found. This situation should be
        avoided.
        """
        for pattern, func in self._parsers.items():
            if re.match(pattern, sname):
                return func
        return None

    @gen.coroutine
    def setup_sampling(self):
        """ 
        Create listeners for all known sensors and set sampling strategies to 'event' on all.
        This won't create listeners for sensors that already have one.
        """
        if not self.synced:
            return

        # Create and set sensor listeners for any sensors who don't have one yet
        listener_futures = []
        for s in set(self.sensor_names).difference(self._listeners.keys()):
            parser = self.get_parser(s)
            func = Listener(s, self._readings, parser=parser)
            self._listeners[s] = func
            msg = "Created listener for sensor {}:{}:{}, parser = {}".format(
                self.host, self.port, s, parser)
            log.debug(msg)
            listener_futures.append(self.set_sensor_listener(s, func))

        # Apply sampling strategies to sensors that require it
        strategy_futures = []
        for s in self.sensor_names:
            # See class KATCPSensor in katcp.resource module
            # KATCPSensor has a property 'sampling_strategy' which is a 1-tuple: (STRAT_NAME,)
            strat_name, = self.sensor[s].sampling_strategy
            if strat_name != 'event':
                msg = "Applying strategy to sensor {}:{}:{}".format(self.host, self.port, s)
                log.debug(msg)
                strategy_futures.append(self.set_sampling_strategy(s, 'event'))

        for f in itertools.chain(listener_futures, strategy_futures):
            yield f

    def sensor_data(self):
        """ 
        Returns a list of dictionaries, each with keys: 'name', 'value', 'status', 'time'
        The list is returned sorted on key 'name'
        """
        def iterate_sensor_items():
            for k in sorted(self.readings.keys()):
                yield k, self.readings[k]

        result = [
            dict(
                name=name,
                value=reading.value,
                status=reading.status,
                time=utc_string(reading.timestamp),
            )
            for name, reading in iterate_sensor_items()
        ]
        return result

    def sensor_values(self, exclude_sensors=set()):
        """
        Returns the latest known sensor values as a dictionary

        Parameters
        ----------
        exclude_sensors : iterable, optional
            List of sensor names to exclude

        Returns
        -------
        values_dict : dict
            A dictionary {sensor_name: sensor_value}
        """
        keys = set(self.readings.keys()).difference(set(exclude_sensors))
        return {
            name : self.readings[name].value
            for name in keys
        }

    @gen.coroutine
    def start(self):
        """
        Start client and set sampling strategies
        """
        super(DeviceMonitor, self).start()
        try:
            yield self.until_synced(timeout=5.0)
            yield self.setup_sampling()
        except gen.TimeoutError:
            pass
        log.info("DeviceMonitor {}:{} started".format(self.host, self.port))

    def stop(self):
        super(DeviceMonitor, self).stop()

    def __del__(self):
        self.stop()


###############################################################################

@gen.coroutine
def start():
    log.info("Starting")
    devmon = DeviceMonitor(host="10.98.78.99", port=5000)
    yield devmon.start()

    for x in devmon.sensor_data():
        log.debug(x)

    tornado.ioloop.IOLoop.current().add_callback(resync_loop, devmon)


@gen.coroutine
def resync_loop(devmon, period=5.0):
    while True:
        yield gen.sleep(period)
        log.info("Resync")
        yield devmon.setup_sampling()


@gen.coroutine
def shutdown(ioloop):
    log.info("Shutting down")
    ioloop.stop()


def main():
    ioloop = tornado.ioloop.IOLoop.current()
    shutdown_wrapper = lambda sig, frame: ioloop.add_callback_from_signal(shutdown, ioloop)
    signal.signal(signal.SIGINT, shutdown_wrapper)
    signal.signal(signal.SIGTERM, shutdown_wrapper)

    ioloop.add_callback(start)
    ioloop.start()


if __name__ == '__main__':
    coloredlogs.install(
        fmt="[%(levelname)s - %(asctime)s - %(name)s - %(filename)s:%(lineno)s] %(message)s", 
        level=logging.DEBUG, 
        milliseconds=True
    )
    logging.getLogger('katcp').setLevel('WARNING')
    logging.getLogger('kat').setLevel('WARNING')
    main()
