import os
import base64
import logging

from device_monitor import utc_string


log = logging.getLogger('tusemcs.suricat.snapshots_manager')


class SnapshotsManager(object):
    """
    Class in charge of fetching snapshot sensor readings and writing them to PNG files.

    Parameters
    ----------
    getter : callable
        A function that takes no arguments and returns the latest readings of the snapshot sensor
        for all nodes, as a dictionary {node_number : KATCP reading object}
    """
    IMGDIR = os.path.join(os.path.dirname(__file__), 'web', 'images')
    IMGDIR_URL = '/images'

    def __init__(self, getter):
        self._getter = getter
        # A dictionary {node number : most recent known reading} returned by the getter function
        self.readings = {} 

    def image_path(self, n):
        """ PNG image path for node number n """
        return os.path.join(self.IMGDIR, 'node{:02d}.png'.format(n))

    def image_url(self, n, timestamp=0.0):
        """ PNG image URL for node number n """
        return os.path.join(self.IMGDIR_URL, 'node{:02d}.png?t={:.1f}'.format(n, timestamp))

    def refresh(self):
        """ Re-generate all snapshot PNGs """
        new_readings = self._getter()

        for n, new in new_readings.items():
            old = self.readings.get(n, None)
            if not old or new.timestamp > old.timestamp:
                fname = self.image_path(n)
                data = base64.b64decode(new.value)
                if data:
                    with open(fname, 'wb') as f:
                        f.write(data)
                    log.debug("Updated {!r} ({} bytes)".format(fname, len(data)))
            self.readings[n] = new

    def image_info(self, n):
        """
        Returns a dictionary with the information pertaining to the PNG snapshot for node number n.
        Output keys are:
            'node': node number
            'utc': UTC of the node's snapshot sensor most recent reading
            'url': image URL for the snapshots page to use
        """
        reading = self.readings.get(n, None)
        value = None
        timestamp = None
        utc = None
        url = None

        if reading:
            value = reading.value
            timestamp = reading.timestamp
            utc = utc_string(timestamp)

        # NOTE: if value is an empty string, we don't want to display anything on the page
        if timestamp and value:
            url = self.image_url(n, timestamp)
        
        return {'node': n, 'utc': utc, 'url': url}

    def get_data(self):
        """
        List of dictionaries with PNG snapshot information, to be published in the JSON feed that
        goes to the snapshots webpage.
        """
        return [self.image_info(n) for n in self.readings.keys()]