import logging
import signal
import json

import tornado
import coloredlogs
from tornado import gen
from device_monitor import DeviceMonitor


log = logging.getLogger('tusemcs.suricat.device_monitor')


class NodesMonitor(object):
    """
    Wraps a set of DeviceMonitors, each connected to a TUSE node.

    Parameters
    ----------
    node_specs : list of dict
        List of dictionaries specifying TUSE node number, host and port
    """
    PARSERS = {
        r'pipeline_config': json.loads,
        r'beam_positions': json.loads,
        r'spccl_candidates': json.loads
    }

    def __init__(self, node_specs):
        self.device_monitors = {
            items['number']: DeviceMonitor(items['host'], items['port'], parsers=self.PARSERS)
            for items in node_specs
        }

    @gen.coroutine
    def start(self):
        futures = [devmon.start() for devmon in self.device_monitors.values()]
        for f in futures:
            yield f

    def sensor_values(self, exclude_sensors=set()):
        """
        Returns the latest known sensor values for every node, as a list of dictionaries.

        Parameters
        ----------
        exclude_sensors : iterable, optional
            List of sensor names to exclude

        Returns
        -------
        nodes_data : list of dict
            A list of dictionaries, each containing the sensor values for a node plus an 
            additional key 'number' (the node number). The list is sorted on key 'number'. 
            
        Notes
        -----
        If a node is unreachable, the dict associated to it contains ONLY the 'number' key.
        """
        result = []
        for node_number in sorted(self.device_monitors.keys()):
            items = self.device_monitors[node_number].sensor_values(
                exclude_sensors=exclude_sensors
            )
            items.setdefault('number', node_number)
            result.append(items)
        return result

    def get_readings(self, sensor_name):
        """
        Returns the latest reading of given sensor name for all nodes, as a dictionary.

        Parameters
        ----------
        sensor_name : str
            KATCP Sensor name (with underscores)

        Returns
        -------
        readings : dict
            A dictionary {node number : KATCP sensor reading object}
        """
        items = {}
        for n, devmon in self.device_monitors.items():
            reading = devmon.readings.get(sensor_name, None)
            if reading:
                items[n] = reading
        return items


###############################################################################

@gen.coroutine
def start():
    log.info("Starting")
    monitor = NodesMonitor([
        {'number': 0 , 'host': 'localhost', 'port': 45100},
        {'number': 57, 'host': 'localhost', 'port': 45157}
    ])
    yield monitor.start()
    log.debug(monitor.sensor_values())


@gen.coroutine
def shutdown(ioloop):
    log.info("Shutting down")
    ioloop.stop()


def main():
    ioloop = tornado.ioloop.IOLoop.current()
    shutdown_wrapper = lambda sig, frame: ioloop.add_callback_from_signal(shutdown, ioloop)
    signal.signal(signal.SIGINT, shutdown_wrapper)
    signal.signal(signal.SIGTERM, shutdown_wrapper)

    ioloop.add_callback(start)
    ioloop.start()


if __name__ == '__main__':
    coloredlogs.install(
        fmt="[%(levelname)s - %(asctime)s - %(name)s - %(filename)s:%(lineno)s] %(message)s", 
        level=logging.DEBUG, 
        milliseconds=True
    )
    logging.getLogger('katcp').setLevel('WARNING')
    logging.getLogger('kat').setLevel('WARNING')
    main()
