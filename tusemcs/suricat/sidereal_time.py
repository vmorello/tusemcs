from datetime import datetime
import ephem


def mkt_observer():
    mkt = ephem.Observer()
    # NOTE: floats are intepreted in radians, strings as degrees
    mkt.lon = ephem.degrees('21.4430')
    mkt.lat = ephem.degrees('-30.7130')
    return mkt


MEERKAT = mkt_observer()


def local_sidereal_time():
    """
    Return local MeerKAT sidereal time as a string HH:MM:SS
    """
    MEERKAT.date = ephem.date(datetime.utcnow())
    lst = str(MEERKAT.sidereal_time())[:-3]
    # Handle case where pyephem formats the LST as H:MM:SS
    if len(lst) < 8:
        lst = '0' + lst
    return lst


if __name__ == '__main__':
    lst = local_sidereal_time()
    print(lst)