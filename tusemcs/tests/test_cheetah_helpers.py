#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#   2018 Fabian Jankowski
#

from numpy.testing import assert_, assert_raises
from jinja2 import UndefinedError
from lxml import etree
# local
from tusemcs.cheetah_helpers import get_cheetah_config

GOOD = {
        "sps_file"      : "",
        "sps_out"       : ""
    }

BAD = {
        "bla" : "123"
    }

def test_cheetah_config_success():
    # test that a configuration can be created succesfully
    get_cheetah_config(GOOD)


def test_cheetah_config_keywords_missing():
    # missing keywords in parameters dict

    with assert_raises(UndefinedError):
        get_cheetah_config(BAD)


def test_cheetah_config_additional_keywords():
    # additional keywords in parameters dict
    # this should not raise and exception
    test = {}
    test.update(GOOD)
    test.update(BAD)

    get_cheetah_config(test)


def test_cheetah_config_xml_conformity():
    # test that the cheetah config file is valid xml
    config = get_cheetah_config(GOOD)
        
    # this is needed to make lxml happy
    config = config.encode("utf-8")
    
    parser = etree.XMLParser()
    etree.fromstring(config, parser)


# if run directly
if __name__ == "__main__":
    import nose2
    nose2.main()
