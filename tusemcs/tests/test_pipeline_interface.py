#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#   2019 Fabian Jankowski
#

from numpy.testing import assert_, assert_raises
# local
from tusemcs.pipeline_interface import get_spead_info_from_mapping, get_starting_channel_str


def test_get_spead_info_from_mapping_success():
    # coherent beams
    good = {
        '239.11.1.1:7147': ['cfbf00000', 'cfbf00001', 'cfbf00002', 'cfbf00003', 'cfbf00004', 'cfbf00005']
    }

    ips, ports, beam_lists = get_spead_info_from_mapping(good)

    assert_ (ips == ["239.11.1.1"])
    assert_ (ports == ["7147"])
    assert_ (beam_lists == [["0", "1", "2", "3" , "4", "5"]])

    # incoherent beam
    good = {
        '239.11.1.11:7148': ['ifbf00000']
    }

    ips, ports, beam_lists = get_spead_info_from_mapping(good)

    assert_ (ips == ["239.11.1.11"])
    assert_ (ports == ["7148"])
    assert_ (beam_lists == [["0"]])


def test_get_spead_info_from_mapping_inco_and_coherent():
    # XXX: update this when we adjust the function
    bad = {
        '239.11.1.1:7147': ['cfbf00000', 'cfbf00001', 'cfbf00002', 'cfbf00003', 'cfbf00004', 'cfbf00005'], 
        '239.11.1.7:7147': ['cfbf00036', 'cfbf00037', 'cfbf00038', 'cfbf00039', 'cfbf00040', 'cfbf00041']
    }

    with assert_raises(NotImplementedError):
        get_spead_info_from_mapping(bad)


def test_get_spead_info_from_mapping_too_many_ips():
    bad = {
        '239.11.1.1:7147': ['cfbf00000', 'cfbf00001', 'cfbf00002', 'cfbf00003', 'cfbf00004', 'cfbf00005'], 
        '239.11.1.7:7147': ['cfbf00036', 'cfbf00037', 'cfbf00038', 'cfbf00039', 'cfbf00040', 'cfbf00041'], 
        '239.11.1.6:7147': ['cfbf00030', 'cfbf00031', 'cfbf00032', 'cfbf00033', 'cfbf00034', 'cfbf00035'], 
        '239.11.1.9:7147': ['cfbf00048', 'cfbf00049', 'cfbf00050', 'cfbf00051', 'cfbf00052', 'cfbf00053']
    }

    with assert_raises(RuntimeError):
        get_spead_info_from_mapping(bad)


def test_get_starting_channel_str():
    # 1) good
    expected = "0:4096:64"
    result = get_starting_channel_str(64, 4096)
    assert_ (result == expected)

    # 2) good
    expected = "0:4096:256"
    result = get_starting_channel_str(256, 4096)
    assert_ (result == expected)

    # 3) bad
    with assert_raises(RuntimeError):
        get_starting_channel_str(42, 4096)

    # 4) bad
    with assert_raises(RuntimeError):
        get_starting_channel_str(12345, 4096)


# if run directly
if __name__ == "__main__":
    import nose2
    nose2.main()
