#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#   2018 Fabian Jankowski
#

from numpy.testing import assert_, assert_raises
from tusemcs.ipmi_helpers import get_ipmi_sensor


def test_get_ipmi_sensor():
    good_sensors = ["CPU1 Temperature", "CPU2 Temperature", "CPU_FAN1",
                    "CPU_FAN2", "FRNT_FAN2", "FRNT_FAN3", "REAR_FAN1"]

    for item in good_sensors:
        assert_ (get_ipmi_sensor(item) == "ok")

    with assert_raises(RuntimeError):
        get_ipmi_sensor("Bla")


# if run directly
if __name__ == "__main__":
    import nose2
    nose2.main()
