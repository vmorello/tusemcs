#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#   2018 Fabian Jankowski
#

from numpy.testing import assert_, assert_raises

def test_state_transitions_compute_node():
    # * start without prep: fail
    # * prep over prep: success
    # * stop over stop: success
    # * kill over kill: success
    # * start over start (when already running): fail
    # * full cycle: idle -> prepared -> running -> idle: success
    # * start with unknown project_id: fail
    # * prep with invalid parameters: fail
    # * maximum four configured project ids

    pass


# if run directly
if __name__ == "__main__":
    import nose2
    nose2.main()
