#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#   2018 Fabian Jankowski
#

from numpy.testing import assert_, assert_raises
import subprocess32 as subprocess
import shlex
from katcp import BlockingClient, Message

def test_full_state_cycle_multiple_nodes():
    # spawn four virtual node_controllers and do a full state cycle
    hostnames = ["localhost", "localhost", "localhost", "localhost"]
    ports = [5005, 5006, 5007, 5008]
    containers = []

    product_id = "abcd"
    product_meta = "blablabla"
    beams_meta = "blabalbalsbal123"
    multicast = "127.0.0.1"
    timestamp = 12345

    docker = "/usr/local/bin/docker"
    imgname = "control-docker"
    test_container = "tusemcs_tests"

    # spawn four virtual compute nodes
    for nr, port in enumerate(ports, start=1):
        command = "{0} run -d --rm --name tusemcs_virtual{1} ".format(docker, nr) + \
                  "--device /dev/ipmi0 " + \
                  "--network container:{0} ".format(test_container) + \
                  "{0} ".format(imgname) + \
                  "/software/tusemcs/scripts/start_compute.sh {0}".format(port)
        args = shlex.split(command)

        # capture the container id
        cid = subprocess.check_output(args)
        containers.append(cid)

    # issue the following requests:
    # 1) configure
    # 2) capture-init
    # 3) capture-start
    # 4) capture-stop
    # 5) capture-done
    # 6) deconfigure
    configure = Message.request("configure", product_id, product_meta)
    capture_init = Message.request("capture-init", product_id, multicast)
    capture_start = Message.request("capture-start", product_id, beams_meta, timestamp)
    capture_stop = Message.request("capture-stop", product_id)
    capture_done = Message.request("capture-done", product_id)
    deconfigure = Message.request("deconfigure", product_id)
    p_status = Message.request("sensor-value", "processing-status")
    pid_configured = Message.request("sensor-value", "product-ids-configured")

    requests = [configure, capture_init, capture_start, capture_stop,
                capture_done, deconfigure]
    ps_values = ["idle", "prepared", "running", "idle", "idle", "idle"]
    pid_values = [product_id, product_id, product_id, product_id, product_id,
                  ""]
    
    for request, ps_value, pid_value in zip(requests, ps_values, pid_values):
        for host, port in zip(hostnames, ports):
            client = BlockingClient(host, port)
            client.start()
            client.wait_protocol()

            reply, informs = client.blocking_request(request)
            print(reply)
            for msg in informs:
                print(msg)

            assert_ (reply.reply_ok())

            # ensure that state transition happened
            # compare processing-status sensor
            reply, informs = client.blocking_request(p_status)
            print(reply)
            for msg in informs:
                print(msg)

            # the arguments to the inform look like this
            # ['1536247094.577300', '1', 'processing-status', 'nominal', 'idle']
            ps_status = informs[0].arguments[4]

            assert_ (reply.reply_ok())
            assert_ (ps_status == ps_value)

            # also check that the product id is correctly listed
            reply, informs = client.blocking_request(pid_configured)
            print(reply)
            for msg in informs:
                print(msg)

            pid_status = informs[0].arguments[4]

            assert_ (reply.reply_ok())
            assert_ (pid_status == pid_value)

            client.stop()
            client.join()

    # shutdown the virtual compute nodes
    command = "{0} container stop -t 1 {1}".format(docker, " ".join(containers))
    args = shlex.split(command)
    subprocess.check_call(args)


def test_configure():
    # test configure request
    host = "localhost"
    port = 5005

    product_meta = "blablabla"

    docker = "/usr/local/bin/docker"
    imgname = "control-docker"
    test_container = "tusemcs_tests"

    containers = []

    # spawn four virtual compute nodes
    command = "{0} run -d --rm --name tusemcs_virtual ".format(docker) + \
              "--device /dev/ipmi0 " + \
              "--network container:{0} ".format(test_container) + \
              "{0} ".format(imgname) + \
              "/software/tusemcs/scripts/start_compute.sh {0}".format(port)
    args = shlex.split(command)

    # capture the container id
    cid = subprocess.check_output(args)
    containers.append(cid)

    for product_id in range(4):
        request = Message.request("configure", str(product_id), product_meta)

        client = BlockingClient(host, port)
        client.start()
        client.wait_protocol()

        reply, _ = client.blocking_request(request)
        assert_ (reply.reply_ok())

    # additional product id -- this should fail
    request = Message.request("configure", "4", product_meta)
    client = BlockingClient(host, port)
    client.start()
    client.wait_protocol()

    reply, _ = client.blocking_request(request)
    assert_ (reply.reply_ok() == False)

    # shutdown
    command = "{0} container stop -t 1 {1}".format(docker, " ".join(containers))
    args = shlex.split(command)
    subprocess.check_call(args)


# if run directly
if __name__ == "__main__":
    import nose2
    nose2.main()
