#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#   2018 Fabian Jankowski
#

from numpy.testing import assert_, assert_raises
import subprocess32 as subprocess
import shlex
from katcp import BlockingClient, Message

def test_requests():
    # test whether all requests are implemented correctly
    # according to the icd

    host = "localhost"
    port = 5101

    product_id = "abcd"
    streams_json = "blablabalbla"
    proxy_name = "proxy123"

    docker = "/usr/local/bin/docker"
    imgname = "control-docker"
    test_container = "tusemcs_tests"

    # spawn a virtual head node
    command = "{0} run -d --rm --name tusemcs_virtual_head01 ".format(docker) + \
              "--network container:{0} ".format(test_container) + \
              "{0} ".format(imgname) + \
              "python /software/tusemcs/tusemcs/master_controller.py " + \
	          "-i 0.0.0.0 -p {0} -f logs.txt".format(port)
    args = shlex.split(command)

    # capture the container id
    cid = subprocess.check_output(args)

    # issue the following requests:
    # 1) configure
    # 2) capture-init
    # 3) capture-start
    # 4) capture-stop
    # 5) capture-done
    # 6) deconfigure
    configure = Message.request("configure", product_id, streams_json, proxy_name)
    capture_init = Message.request("capture-init", product_id)
    capture_start = Message.request("capture-start", product_id)
    capture_stop = Message.request("capture-stop", product_id)
    capture_done = Message.request("capture-done", product_id)
    deconfigure = Message.request("deconfigure", product_id)

    requests = [configure, capture_init, capture_start, capture_stop,
                capture_done, deconfigure]
    
    for request in requests:
        client = BlockingClient(host, port)
        client.start()
        client.wait_protocol()

        reply, informs = client.blocking_request(request)
        print(reply)
        for msg in informs:
            print(msg)

        assert_ (reply.reply_ok())

        client.stop()
        client.join()

    # shutdown the virtual head node
    command = "{0} container stop -t 1 {1}".format(docker, cid)
    args = shlex.split(command)
    subprocess.check_call(args)


def test_sensors():
    # test whether all sensors are implemented correctly
    # according to the icd

    host = "localhost"
    port = 5101

    docker = "/usr/local/bin/docker"
    imgname = "control-docker"
    test_container = "tusemcs_tests"

    # spawn a virtual head node
    command = "{0} run -d --rm --name tusemcs_virtual_head02 ".format(docker) + \
              "--network container:{0} ".format(test_container) + \
              "{0} ".format(imgname) + \
              "python /software/tusemcs/tusemcs/master_controller.py " + \
	          "-i 0.0.0.0 -p {0} -f logs.txt".format(port)
    args = shlex.split(command)

    # capture the container id
    cid = subprocess.check_output(args)
    
    expected = {"device-status": "discrete",
                "last-event-detected": "string",
                "local-time-synced": "boolean"}

    request = Message.request("sensor-list")
    
    client = BlockingClient(host, port)
    client.start()
    client.wait_protocol()

    reply, informs = client.blocking_request(request)
    print(reply)
    for msg in informs:
        print(msg)

    assert_ (reply.reply_ok())

    # check that all expected sensors are there and
    # are correctly implemented
    for key in expected:
        print("Expected: {0}, {1}".format(key, expected[key]))
        found = False

        for msg in informs:
            print("Inform: {0}".format(msg.arguments))
            
            if msg.arguments[0] == key \
            and msg.arguments[3] == expected[key]:
                found = True
                print("Match found.")
                break
        
        if not found:
            raise RuntimeError("No correct match found for: {0}, {1}".format(
                               key, expected[key]))

    # shutdown the virtual head node
    command = "{0} container stop -t 1 {1}".format(docker, cid)
    args = shlex.split(command)
    subprocess.check_call(args)


# if run directly
if __name__ == "__main__":
    import nose2
    nose2.main()
