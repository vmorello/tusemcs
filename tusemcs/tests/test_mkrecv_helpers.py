#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#   2018-2019 Fabian Jankowski
#

from numpy.testing import assert_, assert_raises
from jinja2 import UndefinedError
import os.path
# local
from tusemcs.mkrecv_helpers import get_mkrecv_config
from tusemcs.pipeline_interface import parse_yaml_config


def _get_good_config():
    """
    Load good configuration from file.
    """

    good_file = os.path.join(os.path.dirname(__file__), "..", "..",
                             "config", "mkrecv_defaults.yml")
    good_file = os.path.abspath(good_file)
    good = parse_yaml_config(good_file)

    return good


def _get_bad_config():
    """
    Get bad configuration.
    """

    bad = {
        "bla" : "123"
    }

    return bad


def test_mkrecv_config_success():
    # test that a configuration can be created succesfully
    good = _get_good_config()
    get_mkrecv_config(good)


def test_mkrecv_config_keywords_missing():
    # missing keywords in parameters dict
    bad = _get_bad_config()

    with assert_raises(UndefinedError):
        get_mkrecv_config(bad)


def test_mkrecv_config_additional_keywords():
    # additional keywords in parameters dict
    # this should not raise an exception
    test = {}
    good = _get_good_config()
    bad = _get_bad_config()
    test.update(good)
    test.update(bad)

    get_mkrecv_config(test)


def test_mkrecv_output():
    good = _get_good_config()
    config_str = get_mkrecv_config(good)

    print(config_str)


# if run directly
if __name__ == "__main__":
    import nose2
    nose2.main()
