#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#   2018 Fabian Jankowski
#

from numpy.testing import assert_, assert_raises
import os.path
import subprocess32 as subprocess
import shlex
# local
from tusemcs.general_helpers import (parse_ntpq_output, _determine_ntp_synced)


def test_ntpq_output_parsing():
    # test whether ntpq output is parsed correctly
    # 1) ok
    testdir = os.path.dirname(__file__)
    goodfiles = ["ntpq_parser_1.ok", "ntpq_parser_2.ok"]

    expected = [
        [['*10.97.64.1', '.GPS.', '1', 'u', '722', '1024', '377', '10.726', '-0.268', '0.126']],
        [['*192.168.44.11', '136.186.1.110', '3', 'u', '996', '1024', '377', '0.136', '-0.272', '0.199'],
        ['192.168.44.12', '.INIT.', '16', 'u', '-', '1024', '0', '0.000', '0.000', '0.000'],
        ['127.127.1.0', '.LOCL.', '10', 'l', '70h', '64', '0', '0.000', '0.000', '0.000']]
    ]

    for testfile, expect in zip(goodfiles, expected):
        absfile = os.path.join(testdir, testfile)
        with open(absfile) as f:
            raw = f.read()
        
        parsed = parse_ntpq_output(raw)
        assert_ (parsed == expect)
    
    # 2) fail
    failfiles = ["ntpq_parser_1.fail", "ntpq_parser_2.fail"]

    for testfile in failfiles:
        absfile = os.path.join(testdir, testfile)
        with open(absfile) as f:
            raw = f.read()
        
        with assert_raises(RuntimeError):
            parse_ntpq_output(raw)


def test_ntp_synced_logic():
    # test whether the ntp synced logic works as expected
    
    # 1) ok
    testdir = os.path.dirname(__file__)
    goodfiles = ["ntpq_parser_1.ok", "ntpq_parser_2.ok",
                 "ntpd_synced_1.ok", "ntpd_synced_2.ok",
                 "ntpd_synced_3.ok", "ntpd_synced_4.ok"]

    for testfile in goodfiles:
        absfile = os.path.join(testdir, testfile)
        with open(absfile) as f:
            raw = f.read()
        
        info = parse_ntpq_output(raw)
        assert_ (_determine_ntp_synced(info) == True)

    # 2) fail
    failfiles = ["ntpd_synced_1.fail", "ntpd_synced_2.fail",
                 "ntpd_synced_3.fail", "ntpd_synced_4.fail"]

    for testfile in failfiles:
        absfile = os.path.join(testdir, testfile)
        with open(absfile) as f:
            raw = f.read()
        
        info = parse_ntpq_output(raw)
        assert_ (_determine_ntp_synced(info) == False)


# if run directly
if __name__ == "__main__":
    import nose2
    nose2.main()
