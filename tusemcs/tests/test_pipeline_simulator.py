#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#   2018 Fabian Jankowski
#

from numpy.testing import assert_, assert_raises
import os.path
from subprocess import call, check_call, check_output, CalledProcessError
import shlex

def test_operations_daemon_started():
    script = os.path.join(os.path.dirname(__file__),
                          "../pipeline_simulator.py")
    script = "python {0}".format(os.path.abspath(script))

    start_daemon = shlex.split("{0} start_daemon".format(script))
    stop_daemon = shlex.split("{0} stop_daemon".format(script))
    status = shlex.split("{0} status".format(script))

    product_id = "abcd"
    product_meta = "blablabla"
    multicast = "127.0.0.1"
    beams_meta = "beam1beam2"
    timestamp = 123456

    # prep
    call(stop_daemon)
    check_call(start_daemon)

    prep = shlex.split("{0} prep {1} {2} {3}".format(script,
                       product_id, product_meta, multicast))
    assert_ (check_output(status).strip() == "idle")
    check_call(prep)
    assert_ (check_output(status).strip() == "prepared")

    # start
    start = shlex.split("{0} start {1} {2} {3}".format(script,
                        product_id, beams_meta, timestamp))
    check_call(start)
    assert_ (check_output(status).strip() == "running")

    # stop
    stop = shlex.split("{0} stop {1}".format(script,
                       product_id))
    check_call(stop)
    assert_ (check_output(status).strip() == "idle")

    # kill
    kill = shlex.split("{0} kill".format(script))
    check_call(kill)
    assert_ (check_output(status).strip() == "idle")

    # status
    check_call(status)
    assert_ (check_output(status).strip() == "idle")

    check_call(stop_daemon)


def test_operations_daemon_not_running():
    script = os.path.join(os.path.dirname(__file__),
                          "../pipeline_simulator.py")
    script = "python {0}".format(os.path.abspath(script))

    start_daemon = shlex.split("{0} start_daemon".format(script))
    stop_daemon = shlex.split("{0} stop_daemon".format(script))
    status = shlex.split("{0} status".format(script))

    product_id = "abcd"
    product_meta = "blablabla"
    multicast = "127.0.0.1"
    beams_meta = "beam1beam2"
    timestamp = 123456

    # prep
    call(stop_daemon)
    prep = shlex.split("{0} prep {1} {2} {3}".format(script,
                       product_id, product_meta, multicast))
    check_call(prep)
    assert_ (check_output(status).strip() == "prepared")

    # start
    check_call(stop_daemon)
    start = shlex.split("{0} start {1} {2} {3}".format(script,
                        product_id, beams_meta, timestamp))
    with assert_raises(CalledProcessError):
        check_call(start)

    assert_ (check_output(status).strip() == "idle")

    # stop
    check_call(stop_daemon)
    stop = shlex.split("{0} stop {1}".format(script,
                       product_id))
    check_call(stop)
    assert_ (check_output(status).strip() == "idle")

    # kill
    check_call(stop_daemon)
    kill = shlex.split("{0} kill".format(script))
    check_call(kill)
    assert_ (check_output(status).strip() == "idle")

    # status
    check_call(status)
    assert_ (check_output(status).strip() == "idle")

    check_call(stop_daemon)


# if run directly
if __name__ == "__main__":
    import nose2
    nose2.main()
