#
# mkrecv configuration generator
# Vincent Morello - 2020
#
import os
import logging

import yaml
import netifaces
import katpoint
import astropy.time
from jinja2 import Template, StrictUndefined

from pipeline_utils import DotDict
from beam import validate_beams


log = logging.getLogger('tusemcs.mkrecv_config')


def load_mkrecv_header_template():
    thisdir = os.path.dirname(os.path.realpath(__file__))
    fname = os.path.join(thisdir, '..', 'templates', 'mkrecv_header.jinja2')
    with open(fname, 'r') as fobj:
        return Template(
            fobj.read(),
            undefined=StrictUndefined, trim_blocks=True, lstrip_blocks=True)


def load_mkrecv_default_params():
    thisdir = os.path.dirname(os.path.realpath(__file__))
    fname = os.path.join(thisdir, '..', 'config', 'mkrecv_defaults.yml')
    with open(fname, 'r') as fobj:
        params = yaml.safe_load(fobj)
    return params


def get_default_if_address(if_name='ens4d1'):
    """
    Determine the IPv4 address of the data network interface on the TUSE nodes
    from the interface name. It's currently the same on all nodes: 'ens4d1'

    Parameters
    ----------
    if_name : str, optional
        Name of the data network interface (default: ens4d1)
    
    Returns
    -------
    ip : str
        The IPv4 address of the data network interface, without the port
        e.g. '10.100.24.107'
    """
    items = netifaces.ifaddresses(if_name)
    return str(items[netifaces.AF_INET][0]['addr'])


class MkrecvConfig(object):
    """
    Create and store an mkrecv configuration. The __init__ method expects
    a single parameter (see below). Use from_production_input() to generate
    a configuration from the dynamic sensor values / TUSE config parameters

    Parameters
    ----------
    params: dict
        A dictionary containing at least some of the parameters expected by
        the jinja2 mkrecv header template. Some parameters have default values
        specified in the `mkrecv_defaults.yml` configuration file, but any
        other parameter must be specified in 'params', otherwise an error is
        raised
    """
    HEADER_TEMPLATE = load_mkrecv_header_template()
    DEFAULT_PARAMS = load_mkrecv_default_params()

    def __init__(self, params):
        self.params = self.DEFAULT_PARAMS.copy()

        # TODO: Would be nice to specify a schema for
        # the full params dictionary and then validate its contents
        # Currently, the only validation of self.params is done when rendering
        # the template, errors are raised if keys are missing for example
        self.params.update(params) 

    def generate_header(self):
        text = self.HEADER_TEMPLATE.render(self.params)
        hdr_bytesize_max = self.params['hdr_size']
        if len(text) > hdr_bytesize_max:
            raise ValueError("Generated mkrecv header exceeds the limit of {} bytes".format(hdr_bytesize_max))
        return text

    @classmethod
    def from_production_input(cls, product_meta, beams, if_address=None, network_protocol='udp'):
        """
        Create a MkrecvConfig from live production parameters

        Parameters
        ----------
        product_meta : dict
            Product metadata read from CAM portal at ?capture-init
            The full list of keys is specified in the MasterPortalClient code
        beams : list
            List of Beam objects which specify beam numbers, multicast 
            addresses, and positions on sky in particular
        if_address : str or None, optional
            IPv4 address of the network interface used for data capture.
            If None, the address will be automatically inferred from the
            list of available network interfaces.
            (default: None)
        network_protocol : str
            'udp' or 'ibv' (for infiniband)
        """
        validate_beams(beams) # raises ValueError if beams are not valid

        if if_address is None:
            if_address = get_default_if_address()

        coherent = beams[0].coherent
        beam_type = 'coherent' if coherent else 'incoherent'
        log.debug("Generating MkrecvConfig for beam_type = {}".format(beam_type))

        pm = DotDict(product_meta)

        # NOTE: it appears that the value of sensors *_beam_time_resolution read
        # from the CAM portal are missing some significant digits 
        # (truncaded to ~1 nanosec precision). We therefore recalculate 
        # the exact tsamp value to be safe.
        tscrunch = pm.coherent_beam_tscrunch if coherent \
            else pm.incoherent_beam_tscrunch
        tsamp = (1.0 / pm.bandwidth) * pm.nchannels * tscrunch

        # multicast ip addresses and port
        mcast_ips = set([b.mc_ip for b in beams])
        port = beams[0].mc_port

        ### Spead heap parameters
        heap_size = pm.coherent_beam_heap_size if coherent \
            else pm.incoherent_beam_heap_size

        idx1_step = pm.coherent_beam_idx1_step if coherent \
            else pm.incoherent_beam_idx1_step

        # List of absolute beam numbers
        # This is formatted as a comma-separated list in the header template
        idx2_list = [b.absnum for b in beams]

        subband_nchans = pm.coherent_beam_subband_nchans if coherent \
            else pm.incoherent_beam_subband_nchans
        
        # NOTE: pm.nchannels is the number of channels *before* FBFUSE fscrunching !
        fbfuse_fscrunch = pm.coherent_beam_fscrunch if coherent \
            else pm.incoherent_beam_fscrunch
        fbfuse_nchannels = pm.nchannels // fbfuse_fscrunch

        # Comma separated frequency IDs without spaces
        # This is formatted as a comma-separated list in the header template
        # E.g. [0,64,128,192] would get the first 256 frequency channels assuming
        # subband_nchans = 64
        idx3_list = list(range(0, fbfuse_nchannels, subband_nchans))

        # TODO: IDX1_MODULO
        params = {
            'bw': pm.bandwidth,          # NOTE: template generator expects Hz
            'freq': pm.centre_frequency, # NOTE: template generator expects Hz
            'nchan': fbfuse_nchannels,
            'tsamp': tsamp,
            'sample_clock': pm.wide_adc_sample_rate,
            'sync_time': pm.synchronisation_epoch,
            # Sync epoch MJD is required by the transpose module, and then passed along to cheetah
            'sync_time_mjd': astropy.time.Time(pm.synchronisation_epoch, format='unix').mjd,
            'heap_size': heap_size,
            'beams': beams,
            'mcast_ips': mcast_ips,
            'port': port,
            'network_protocol': network_protocol,
            'if_address': if_address,
            # NTHREADS should be number of MC groups + 1; mkrecv has one extra management thread
            'nthreads': len(mcast_ips) + 1,
            'idx1_step': idx1_step,
            'idx2_list': idx2_list,
            'idx3_list': idx3_list
        }        
        return cls(params)


def test():
    from beam import Beam
    # Example product_meta where we have removed the following superfluous and bulky keys:
    # - coherent_beam_multicast_group_mapping
    # - available_antennas
    product_meta = {
        'coherent_beam_samples_per_heap': 128,
        'coherent_beam_nbeams_per_server_set': 480,
        'coherent_beam_heap_size': 8192,
        'incoherent_beam_tscrunch': 16,
        'incoherent_beam_antennas': 'm000,m001,m002,m003,m004,m005,m006,m008,m009,m010,m011,m012,m013,m014,m015,m017,m018,m019,m020,m021,m024,m026,m027,m028,m032,m033,m034,m035,m038,m039,m040,m041,m042,m043,m044,m045,m046,m047,m048,m049,m050,m051,m052,m053,m054,m055,m056,m057,m058,m059,m060,m061,m062',
        'wide_adc_sample_rate': 1712000000.0,
        'fbfmc_address': ['fbfhn00.mpifr-be.mkat.karoo.kat.ac.za', 5000],
        'coherent_beam_multicast_groups_data_rate': 5136000000.0,
        'synchronisation_epoch': 1584420943.0,
        'fbfuse_proxy': 'fbfuse_2',
        'incoherent_beam_heap_size': 8192,
        'incoherent_beam_idx1_step': 16777216,
        'coherent_beam_antennas': 'm000,m001,m002,m003,m004,m005,m006,m008,m009,m010,m011,m012,m013,m014,m015,m017,m018,m019,m020,m021,m024,m026,m027,m028,m032,m033,m034,m035,m038,m039,m040,m041,m042,m043,m044,m045,m046,m047,m048,m049,m050,m051,m052,m053,m054,m055,m056,m057,m058,m059,m060,m061',
        'incoherent_beam_count': 1,
        'nchannels': 4096,
        'incoherent_beam_fscrunch': 1,
        'incoherent_beam_samples_per_heap': 128,
        'bandwidth': 856000000.0,
        'coherent_beam_fscrunch': 1,
        'incoherent_beam_multicast_group_data_rate': 428000000.0,
        'coherent_beam_time_resolution': 7.656070000000001e-05,
        'coherent_beam_ngroups': 40,
        'coherent_beam_count_per_group': 12,
        'coherent_beam_subband_nchans': 64,
        'coherent_beam_count': 480,
        'subarray_proxy': 'subarray_2',
        'product_id': 'array_2',
        'centre_frequency': 1284000000.0,
        'coherent_beam_idx1_step': 16777216,
        'coherent_beam_multicast_groups': 'spead://239.11.1.1+39:7147',
        'coherent_beam_tscrunch': 16,
        'incoherent_beam_multicast_group': 'spead://239.11.1.0+0:7147',
        'cbf_proxy': 'cbf_2',
        'incoherent_beam_subband_nchans': 64,
        'incoherent_beam_time_resolution': 7.656070000000001e-05
    }

    mc_address = "239.1.1.42:7147"
    if_address = "100.24.1.77"
    target = katpoint.Target("J1151-6108,radec,11:51:56.86,-61:08:17.6")

    beams = [
        Beam(ii, mc_address, "cfbf{:05d}".format(ii + 100), target)
        for ii in range(6)
    ]

    mkconf = MkrecvConfig.from_production_input(product_meta, beams, if_address=if_address)
    header = mkconf.generate_header()
    log.debug(mkconf.params)
    log.info('\n' + header)
    log.debug("Header bytesize: {}".format(len(header)))


if __name__ == '__main__':
    import coloredlogs
    logfmt = "[%(levelname)s - %(asctime)s - %(name)s - %(filename)s:%(lineno)s] %(message)s"
    coloredlogs.install(fmt=logfmt, level='DEBUG', milliseconds=True, logger=log)
    test()
