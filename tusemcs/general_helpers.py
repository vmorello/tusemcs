# -*- coding: utf-8 -*-
#
#   2018 - 2019 Fabian Jankowski
#   General helper functions.
#

from __future__ import print_function
import logging
import subprocess32 as subprocess
import signal
import shlex
import sys
import coloredlogs
import datetime

from katportalclient import KATPortalClient
from tornado import gen
import yaml
from tusemcs import __version__


def build_info():
    """
    Convert __version__ string into a 4-tuple to publish as a katcp server's 
    BUILD_INFO. Example: 
    'v0.5.4+2.g0867733' becomes ('tmc-build', 0, 5, '4+2.g0867733')
    """
    v = __version__.strip('v')
    try:
        major, minor, tail = v.split('.', 2)
        tail = '.' + tail
        return ('tmc-build', int(major), int(minor), tail)
    except:
        return ('tmc-build', 0, 0, 'UNKNOWN')


def configure_logging(
        level=logging.DEBUG,
        logfile=None,
        fmt="[%(levelname)s - %(asctime)s - %(name)s - %(filename)s:%(lineno)s] %(message)s"
    ):
    """ 
    Install coloredlogs on the root logger and setup logging to a file if specified

    Parameters
    ----------
    level: str, optional
        Desired logging level (default: DEBUG)
    logfile: str or None, optional
        Write messages of the 'tusemcs' logger to the specified file, leave to
        None otherwise (default: None)
    fmt: str, optional
        Logging format string
    """
    # NOTE: here we install coloredlogs on the root logger, and tone down logging on kat libraries.
    # If installing only on the tusemcs logger, then for some mysterious reason the root logger
    # gets an extra StreamHandler down the line around the time when the ioloop is started.
    # And in that case, we get all logs printed twice. WTF indeed.

    # Alternative ideas:
    # 1. Define a handler for either the root logger or the tornado logger before starting the IOLoop
    # https://stackoverflow.com/questions/30373620/why-does-ioloop-in-tornado-seem-to-add-root-logger-handler

    coloredlogs.install(fmt=fmt, level=level, milliseconds=True)
    logging.getLogger("katcp").setLevel(logging.WARNING)
    logging.getLogger("kat").setLevel(logging.WARNING)

    # Prevent cluttering debug messages when creating a docker client
    logging.getLogger("docker").setLevel(logging.INFO)
    logging.getLogger("urllib3").setLevel(logging.INFO)

    # tusemcs is the parent logger of which all components are child loggers
    # i.e. tusemcs.general_helpers, tusemcs.master_controller and so on.
    log = logging.getLogger("tusemcs")

    if logfile:
        fh = logging.FileHandler(logfile)
        formatter = logging.Formatter(fmt=fmt)
        fh.setFormatter(formatter)
        log.addHandler(fh)
    
    # Special logger to track delays in receiving sensor updates
    # This logger only prints to a CSV file when requested via the --sensorlogs
    # command line argument in master_controller.py
    # However, we still need to turn logging to stderr off explicitly even
    # when no delay logging to file happens.
    sdlog = logging.getLogger("tusemcs.sensor_delays")
    sdlog.propagate = False # No stdout/stderr output


def parse_ntpq_output(infostr):
    """
    Parse the output from ntpq.

    Parameters
    ----------
    infostr: str
        The output from ``ntpq -pn <host>``.
    
    Returns
    -------
    info : list
        A nested list with NTP peers and their properties.

    Raises
    ------
    RuntimeError:
        When ntpq output could not be parsed.
    """

    # skip the header
    infostr = "\n".join(infostr.split("\n")[2:])

    info = []

    for line in infostr.splitlines():
        entry = []
        for field in line.split(" "):
            if field != "":
                entry.append(field.strip())
        info.append(entry)
    
    if not len(info) > 0:
        raise RuntimeError("No NTP peers found.")
    
    for peer in info:
        if not len(peer) == 10:
            raise RuntimeError("Invalid number of fields in " +
                               "ntpq output: {0}".format(len(peer)))

    return info


def _determine_ntp_synced(info):
    """
    Determine NTP sync status from ``ntpq -pn`` output.

    Parameters
    ----------
    info : list
        A nested list as output by ``parse_ntpq_output``.

    Returns
    -------
    synced : bool
        Indicates whether the NTP server is synced.
    """

    # sync thresholds in milliseconds
    # offset is the offset between local and remote clock
    # jitter is the dispersion among multiple remote clock
    # measurements
    th_offset = 1.0
    th_jitter = 2.0
    
    synced = False

    for peer in info:
        if peer[0].startswith("*") \
        and abs(float(peer[8])) < th_offset \
        and abs(float(peer[9])) < th_jitter:
            synced = True

    return synced


def get_ntp_sync_status(hostname, timeout=5.0):
    """
    Checks whether the NTP server is synced to peers.

    Parameters
    ----------
    hostname : str
        Name of host to query for NTP status.
    timeout: float, optonal
        Timeout in seconds (default: 5.0)

    Returns
    -------
    synced : bool
        Indicates whether the NTP server is synced.

    Raises
    ------
    RuntimeError:
        When NTP sync status could not be determined.
    """

    command = "ntpq -pn {0}".format(hostname)
    args = shlex.split(command)

    try:
        result = subprocess.check_output(args, timeout=timeout)
    except subprocess.CalledProcessError as e:
        raise RuntimeError("Could not read NTP status: " +
                           "{0}, {1}".format(hostname, str(e)))
    except subprocess.TimeoutExpired as e:
        raise RuntimeError("Timeout exceeded reading NTP status: " +
                           "{0}, {1}".format(hostname, str(e)))

    try:
        info = parse_ntpq_output(result)
    except RuntimeError as e:
        raise RuntimeError("Could not parse ntpq output: {0}".format(str(e)))

    synced = _determine_ntp_synced(info)
    
    return synced
