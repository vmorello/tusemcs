import logging
import signal
import threading
import uuid
import socket
import json

import tornado
from tornado import gen
import coloredlogs
import redis


log = logging.getLogger('tusemcs.redis_beam_monitor')


class RedisBeamMonitor(object):
    """
    Class that monitors Redis messages with sensor updates pushed by the head
    node, which contain beam-related sensor updates: positions, data suspect.

    !!! IMPORTANT !!! 
    the actual listening to redis updates is done in a separate thread, from
    which can be called some NodeController methods with the power to 
    start/stop the pipeline. The NodeController design assumes these methods
    are blocking, but they are not if called from outside the main thread.

    Therefore, all the callback functions passed to the constructor from the
    NodeController should be of the form:
    lambda arg: ioloop.add_callback(node_controller_method(arg))

    Parameters
    ----------
    beampos_callback : callable or None
        Callable with two arguments: beam name (str), and katpoint Target string (str)
        This is to notify the parent NodeController of a beam position change
    beamshape_callback : callable or None
        Callable with one argument: shape_params (dict)
        This is to notify the parent NodeController of a beam shape change
    suspect_callback : callable or None
        Callable with one argument: data_suspect (bool)
        This is to notify the parent NodeController of a data-suspect change
    tgconf_callback : callable or None
        Callable with one argument: ca_target_request (dict)
        This is to notify the parent NodeController of a ca-target-request change
    host : str
        Redis server host
    port : int
        Redis server port
    
    Notes
    -----
    The redis messages caught by this monitor class are expected to be JSON
    encoded dictionaries with a single key-value pair. Pairs caught are:

    - 'ifbf{BEAM_NUMBER}' : katpoint Target string
    - 'cfbf{BEAM_NUMBER}' : katpoint Target string
    - 'coherent_beam_data_suspect' : bool
    - 'incoherent_beam_data_suspect' : bool
    - 'coherent_beam_shape' : str (JSON-encoded dict)
    - 'ca_target_request' : str (JSON-encoded dict)
    """
    def __init__(self, beampos_callback=None, beamshape_callback=None, 
        suspect_callback=None, tgconf_callback=None, host="10.98.78.99", port=6379):
        self.beam_ids = [] # list of str
        self.redis_client = redis.StrictRedis(host=host, port=port)
        self.pubsub = self.redis_client.pubsub()
        self.thread = None
        self.KILLMSG = "UNSUB:{}".format(socket.gethostname())

        # Whether the data for each type of beam is suspect
        self.cb_suspect = True
        self.ib_suspect = True

        # Callbacks to notify parent NodeController
        self.beampos_callback = beampos_callback
        self.beamshape_callback = beamshape_callback
        self.suspect_callback = suspect_callback
        self.tgconf_callback = tgconf_callback

    @property
    def data_suspect(self):
        """
        Returns if the data for *any* beam being monitored has suspect data.
        Returns False if no beams are monitored.
        """
        has_ib = any(bid.startswith('ifbf') for bid in self.beam_ids)
        has_cb = any(bid.startswith('cfbf') for bid in self.beam_ids)
        return (has_ib and self.ib_suspect) or (has_cb and self.cb_suspect)

    def set_beam_ids(self, beam_ids):
        """
        Set the list of FBFUSE beam IDs to track, such as 'cfbf00042' or 'ifbf00000'
        Messages related to other beam position updates are ignored.
        """
        self.beam_ids = beam_ids
        log.debug("Now tracking redis updates for beam_ids = {}".format(self.beam_ids))

    def start(self):
        self.pubsub.psubscribe('*')
        if not self.thread:
            log.debug("Starting redis listener thread")
            self.thread = threading.Thread(target=self.listen)
            self.thread.daemon = True
            self.thread.start()
        log.debug("Redis listener thread started")

    def listen(self):
        # NOTE (IMPORTANT): We do not want any uncaught exceptions in that loop
        # otherwise the thread dies and we stop receiving updates
        # In particular, the try block catches an exception raised when 
        # the value of some JSON sensors is an empty string
        for items in self.pubsub.listen():
            data = items['data']
            if data == self.KILLMSG:
                log.debug("KILLMSG received: exiting")
                break
            try:
                self.handle_msgitems(items)
            except Exception as err:
                log.warning("Failed to handle msgitems {}, reason: {}".format(items, err))

    def stop(self):
        # NOTE: this code behaves well if the redis server goes down while listening
        # Send kill message to our own listener thread via the redis server
        try:
            self.redis_client.publish('*', self.KILLMSG)
        except redis.ConnectionError:
            pass

        # TODO: avoid calling thread.join() before we know for sure the kill
        # message was received
        if self.thread:
            self.thread.join()
            log.debug("Thread joined")

        try:
            self.pubsub.punsubscribe()
        except redis.ConnectionError:
            pass
        self.thread = None

    def handle_msgitems(self, items):
        text = items['data']

        # 'text' is expected to be a JSON dict with a single key-value pair
        try:
            data = json.loads(text)
            (key, val), = data.items()
        except:
            log.debug("Failed to unpack message {!r} into one key-value pair, ignoring".format(items))
            return

        # Beam position update
        if key in self.beam_ids:
            log.debug("New position for beam {!r} = {!r}".format(key, val))
            if callable(self.beampos_callback):
                self.beampos_callback(key, val)
            return

        # data-suspect updates
        if key == 'coherent_beam_data_suspect':
            self.cb_suspect = bool(val)
            log.debug('{!r} = {}'.format(key, self.cb_suspect))
            log.debug('data_suspect = {}'.format(self.data_suspect))
            if callable(self.suspect_callback):
                self.suspect_callback(self.data_suspect)
            return

        if key == 'incoherent_beam_data_suspect':
            self.ib_suspect = bool(val)
            log.debug('{!r} = {}'.format(key, self.ib_suspect))
            log.debug('data_suspect = {}'.format(self.data_suspect))
            if callable(self.suspect_callback):
                self.suspect_callback(self.data_suspect)
            return

        # coherent_beam_shape update
        if key == 'coherent_beam_shape':
            # NOTE: here 'val' is a string containing a JSON-encoded dict
            # This will raise an exception if the sensor has its initial value
            # which is the empty string (caught in the listener loop)
            val = json.loads(val)
            log.debug('{} = {}'.format(key, val))
            if callable(self.beamshape_callback):
                self.beamshape_callback(val)
            return

        # ca_target_request update
        if key == 'ca_target_request':
            # 'val' is a string containing a JSON-encoded dict, same as above
            val = json.loads(val)
            log.debug('{} = {}'.format(key, val))
            if callable(self.tgconf_callback):
                self.tgconf_callback(val)
            return


### For test purposes only ###
def main():
    @gen.coroutine
    def start(monitor):
        log.info("Starting")
        monitor.start()
        log.info("Started")

    @gen.coroutine
    def stop(ioloop, monitor):
        log.info("Stopping")
        monitor.stop()
        ioloop.stop()
        log.info("Stopped")

    def beampos_callback(name, pos):
        log.info("Caught beam position update {} = {}".format(name, pos))

    def beamshape_callback(params):
        log.info("Caught beam shape update = {} (type = {})".format(params, type(params)))

    def suspect_callback(flag):
        log.info("Caught data-suspect update = {}".format(flag))
    
    ioloop = tornado.ioloop.IOLoop.current()

    monitor = RedisBeamMonitor(
        #host="localhost",
        beampos_callback=beampos_callback,
        beamshape_callback=beamshape_callback,
        suspect_callback=suspect_callback
    )
    monitor.set_beam_ids(['cfbf00000', 'cfbf00001'])

    # Hook up to SIGINT so that ctrl-C results in a clean shutdown
    stopfunc = lambda sig, frame: ioloop.add_callback_from_signal(stop, ioloop, monitor)
    signal.signal(signal.SIGINT, stopfunc)
    signal.signal(signal.SIGTERM, stopfunc)

    ioloop.add_callback(start, monitor)
    ioloop.start()


if __name__ == '__main__':
    coloredlogs.install(
        fmt="[%(levelname)7s - %(asctime)s - %(name)s - %(filename)s:%(lineno)s] %(message)s", 
        level='DEBUG', 
        milliseconds=True
    )
    logging.getLogger('kat').setLevel('WARNING')
    main()