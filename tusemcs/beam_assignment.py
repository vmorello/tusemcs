from collections import defaultdict


class Cluster(object):
    """
    Helper class that represents the TUSE cluster. Contains a list of VLANs.

    Parameters
    ----------
    vlan_mapping : dict
        Dictionary {vlan_id: [node_ids]}
    node_capacity : int
        Capacity for all nodes
    """
    def __init__(self, vlan_mapping, node_capacity):
        self.vlans = []
        for iv, node_ids in vlan_mapping.items():
            nodes = [Node(nid, node_capacity) for nid in sorted(node_ids)]
            self.vlans.append(VLAN(iv, nodes))
        self.vlans = sorted(self.vlans, key=lambda v: v.id)
    
    def assign_mapping(self, mapping, ib_address, ib_id):
        """
        Assign multicast group mapping to the cluster, respecting the constraint that no multicast
        group should be processed in two different VLANs. The algorithm first attempts to balance 
        groups between VLANs, and then within each VLAN balances beams between nodes. Whenever
        possible, lower beam ids are assigned to lower node ids.

        Parameters
        ----------
        mapping : dict
            Dictionary {address (str): beam_ids (list of str)}
        ib_address : str
        ib_id : str
        """
        if not mapping:
            return

        ### Assign IB immediately to the VLAN with largest capacity
        # The node that gets the IB is considered full and will not get CBs
        ib_group = Group(ib_address, [ib_id], coherent=False)
        vlan = max(self.vlans, key=lambda v: v.spare_capacity)
        if vlan.spare_capacity < 1:
            return

        vlan.collect_promises(ib_group.size)
        vlan.stage_group(ib_group)
        vlan.assign_ib()

        # Build coherent group list
        groups = [
            Group(address, beam_ids, coherent=True)
            for address, beam_ids in mapping.items()
        ]

        # Find out MC group size (they should all be the same)
        sizes = set([g.size for g in groups])
        if len(sizes) > 1:
            raise ValueError("Multicast group sizes are not all equal")
        groupsize = max(sizes)

        # Issue promises
        for __ in mapping:
            vlan = max(
                self.vlans, 
                key=lambda v: (v.fractional_spare_capacity, v.spare_capacity)
            )
            if vlan.spare_capacity >= groupsize:
                vlan.collect_promises(groupsize)
            else:
                break

        ### VLANs exchange promises for full groups
        # NOTE: we *must* do round robin assignment of groups to VLANs
        # Due to the way FBFUSE outputs beams, multicast addresses are written to sequentially. 
        # If we then subscribe to sequential multicast addresses on TUSE the problem is that the 
        # traffic for the outgoing port on the switch is bursty. This causes packets to get dropped
        # due to the lack of buffering.

        # x = packets arriving in time interval
        # - = no traffic

        # Sequential (BAD):
        # VLAN 0: xx------
        # VLAN 1: --xx----
        # VLAN 2: ----xx--
        # VLAN 3: ------xx

        # Round robin (GOOD):
        # VLAN 0: x---x---
        # VLAN 1: -x---x--
        # VLAN 2: --x---x-
        # VLAN 3: ---x---x
        groups = sorted(groups, key=lambda g: min(g.beam_ids()))
        index = 0
        for group in groups:
            vlan = max(self.vlans, key=lambda v: (v.promises, -v.id))
            if vlan.stage_group(group):
                index += 1
            else:
                break
        groups = groups[index:] # Remove already assigned groups from list

        # Fill any spare capacity with partial groups
        # VLANs with largest absolute free capacity go first
        for group in groups:
            vlan = max(self.vlans, key=lambda v: v.spare_capacity)
            cap = vlan.spare_capacity
            if cap > 0:
                vlan.staging_area.append(
                    Group(group.address, group.beam_ids()[:cap])
                )
            else:
                break

        ### Assign beams to nodes
        for vlan in self.vlans:
            vlan.assign_beams()

    def verify(self):
        """
        Check that the current assignment of groups and beams is valid, that means:
        - No multicast group assigned to multiple VLANs
        - No beam assigned to multiple nodes
        raise ValueError if any of these conditions are broken
        """
        # No MC group in multiple VLANs
        # No beam on multiple nodes
        mca_vlans = defaultdict(set) # dict {mc_address: vlans to which it is assigned}
        beam_nodes = defaultdict(set)
        for vlan in self.vlans:
            for node in vlan.nodes:
                addresses = set(b.parent.address for b in node.beams)
                for address in addresses:
                    mca_vlans[address].add(vlan)
                
                for beam in node.beams:
                    beam_nodes[beam].add(node)
        
        if any(len(vlans) > 1 for vlans in mca_vlans.values()):
            raise ValueError("Some multicast addresses are assigned to more than one VLAN")
        
        if any(len(nodes) > 1 for nodes in beam_nodes.values()):
            raise ValueError("Some beams are assigned to more than one node")

    def export_plan(self):
        """
        Export assignment plan as a nested dictionary {node_number : {mc_address: [beam_ids]}}
        """
        plan = {}
        for vlan in self.vlans:
            for node in vlan.nodes:
                subplan = defaultdict(list)
                for beam in node.beams:
                    subplan[beam.parent.address].append(beam.id)
                plan[node.id] = dict(subplan)
        return plan


class VLAN(object):
    """
    Helper class that represents a TUSE VLAN, and contains a list of nodes. It can hold beam
    promises that are subsequently exchanged for multicast groups. Until the final stage of the
    assignment algorithm, groups are stored in a 'staging area'; in the end, the beams they contain
    are assigned to nodes in a balanced fashion.

    Parameters
    ----------
    id : int
    nodes : list
        List of Node objects
    """
    def __init__(self, id, nodes):
        self.id = id
        self.nodes = nodes
        self.promises = 0 # in number of beams
        self.staging_area = [] # List of Group objects

    @property
    def capacity(self):
        return sum(node.capacity for node in self.nodes)

    @property
    def spare_capacity(self):
        return sum(node.spare_capacity for node in self.nodes) - self.promises - \
            sum(group.size for group in self.staging_area)

    @property
    def fractional_spare_capacity(self):
        return self.spare_capacity / float(self.capacity) if self.capacity else 0

    def collect_promises(self, n):
        self.promises += n

    def stage_group(self, group):
        """ Stage group in exchange for the equivalent amount of promises. Returns True if the
        group could be staged, False otherwise. """
        if group.size <= self.promises:
            self.staging_area.append(group)
            self.promises -= group.size
            return True
        else:
            return False

    def assign_ib(self):
        try:
            ib_group = list(filter(lambda g: not g.coherent, self.staging_area))[0]
        except IndexError:
            return

        empty_nodes = list(filter(lambda node: not node.beams, self.nodes))
        if not empty_nodes:
            raise ValueError("Cannot assign IB group: no empty nodes in VLAN")

        node = min(empty_nodes, key=lambda node: (node.capacity, node.id))
        for beam in ib_group.beams:
            node.assign_beam(beam)
        
        self.staging_area.remove(ib_group)
    
    def assign_beams(self):
        """ Assign all coherent beams currently in the staging area """
        ### Deal with CBs last
        # NOTE: at this stage, there might be one node with the IB already assigned, 
        # but all other nodes are empty
        allbeams = [beam for group in self.staging_area for beam in group.beams]
        allbeams = sorted(allbeams, key=lambda beam: beam.id)

        for beam in allbeams:
            node = max(self.nodes, key=lambda node: node.spare_capacity)
            if node.spare_capacity:
                node.promises += 1
            else:
                break

        index = 0
        for node in sorted(self.nodes, key=lambda node: node.id):
            k = node.promises
            beams = allbeams[index:index+k]
            node.assign_beams(beams)
            node.promises -= len(beams)
            index += len(beams)
        self.staging_area = []

    def __str__(self):
        return "VLAN {} ({} Nodes): promises={}, spare_capacity={}".format(
            self.id, len(self.nodes), self.promises, self.spare_capacity
        )

    def __repr__(self):
        return str(self)


class Node(object):
    """
    Helper class that represents a TUSE node. Nodes have a capacity, can hold beams and beam
    promises.

    Parameters
    ----------
    id : int
        Unique node identifier
    capacity : int
        Maximum number of beams the node can process
    """
    def __init__(self, id, capacity):
        self.id = id
        self.capacity = capacity
        self.promises = 0
        self.beams = []

    @property
    def has_ib(self):
        return self.beams and not all(beam.coherent for beam in self.beams)

    @property
    def spare_capacity(self):
        return 0 if self.has_ib else self.capacity - len(self.beams) - self.promises

    def assign_beam(self, beam):
        self.beams.append(beam)

    def assign_beams(self, beams):
        for beam in beams:
            self.assign_beam(beam)

    def __str__(self):
        return "Node {}: {} ({} beams, {} promises)".format(
            self.id, self.beams, len(self.beams), self.promises)

    def __repr__(self):
        return str(self)


class Group(object):
    """
    Helper class that represents a multicast group.

    Parameters
    ----------
    address : str
    beam_ids : list
        List of unique beam ids
    coherent : bool
    """
    def __init__(self, address, beam_ids, coherent=True):
        self.address = address
        self.coherent = coherent
        self.beams = [Beam(bid, self, coherent=coherent) for bid in beam_ids]

    @property
    def size(self):
        return len(self.beams)

    def beam_ids(self):
        return [b.id for b in self.beams]

    def __str__(self):
        return "Group({}, {})".format(self.address, self.beams)

    def __repr__(self):
        return str(self)


class Beam(object):
    """
    Helper class that represents a beam.

    Parameters
    ----------
    id : str or int
        Unique beam id
    parent : Group
        Parent multicast group
    coherent : bool
    """
    def __init__(self, id, parent, coherent=True):
        self.id = id
        self.coherent = coherent
        self.parent = parent

    def __str__(self):
        return str(self.id)

    def __repr__(self):
        return str(self)


###############################################################################


def make_beam_assignment_plan(node_specs, mca_mapping, ib_address, ib_id='ifbf00000', 
    node_capacity=6):
    """
    Build the beam assignment plan from the relevant FBFUSE sensor values and parameters.

    Parameters
    ----------
    node_specs : list
        List of tuples (node_number, vlan_number)
    mca_mapping : dict
        Dictionary where the keys are multicast group addresses and the values are beam ids
    ib_address : str
        Multicast address of the incoherent beam group
    ib_id : str, optional
        ID given to the incoherent beam, should be left to 'ifbf00000'
    node_capacity : int
        Maximum number of beams that any node can process

    Returns
    -------
    plan : dict
        Nested dictionary of the form {node_number : {mc_address: [beam_ids]}}
    """
    vlan_mapping = defaultdict(set)
    for inode, iv in node_specs:
        vlan_mapping[iv].add(inode)

    cluster = Cluster(vlan_mapping, node_capacity)
    cluster.assign_mapping(mca_mapping, ib_address, ib_id)
    cluster.verify() # can raise Exceptions
    return cluster.export_plan()


def format_beam_assignment_plan(plan):
    lines = []
    for node, subplan in plan.items():
        header = "Node {:02d}".format(node)
        tab = len(header) * ' '
        empty = True

        items = sorted(subplan.items(), key=lambda (a, b): b[0])
        for address, beams in items:
            text = '{:>20s}    {}'.format(address, ', '.join(beams))
            if empty:
                lines.append(header + text)
                empty = False
            else:
                lines.append(tab + text)
    return '\n'.join(lines)


###############################################################################


def generate_mca_mapping(groups, beams_per_group):
    mapping = defaultdict(list)
    for ii in range(groups):
        address = '239.1.1.{}:7147'.format(ii+1)
        offset = ii * beams_per_group
        for jj in range(beams_per_group):
            beam_id = 'cfbf{:05d}'.format(offset + jj)
            mapping[address].append(beam_id)
    return dict(mapping)


def extensive_test():
    """ Test many combinations (number of groups, number of beams per group, node capacity) """
    import os
    import yaml
    import sys

    # NOTE: here are some known configs
    # 1K full array: 24 beams/group, 768 beams, 32 groups
    # 1K half array: 12 beams/group, 384 beams, 32 groups
    # 4K full array TRAPUM: 12 beams/group, 480 beams

    EXCLUDE_NODES = {49}
    MAX_BEAMS = 2048
    
    # Load TUSE node specs
    node_specs_file = os.path.join(os.path.dirname(__file__), '..', 'config', 'nodes.yml')
    with open(node_specs_file, 'r') as f:
        node_specs = yaml.safe_load(f)

    node_specs = [
        (items['number'], items['vlan'])
        for items in node_specs
        if not items['number'] in EXCLUDE_NODES
    ]

    for groups in (0, 1, 2, 4, 8, 12, 16, 20, 24, 28, 32, 36, 40, 48, 56, 64):
        for bpg in (0, 1, 2, 3, 4, 6, 9, 12, 18, 24):
            total_beams = groups * bpg
            if total_beams > MAX_BEAMS:
                continue

            mca_mapping = generate_mca_mapping(groups, bpg)
            for capacity in (0, 1, 2, 3, 4, 6, 9, 12, 15, 18, 24):
                sys.stdout.write(
                    "groups = {:3d}, beams/group = {:3d}, node capacity = {:3d} ... ".format(
                    groups, bpg, capacity)
                )
                plan = make_beam_assignment_plan(
                    node_specs, mca_mapping, 
                    ib_address='239.1.1.0:7147', ib_id='ifbf00000', node_capacity=capacity
                )

                num_assigned = sum(
                    len(beams) for subplan in plan.values() for beams in subplan.values()
                )

                print('OK: {} / {} beams assigned'.format(num_assigned, total_beams + 1))
                # print(format_beam_assignment_plan(plan))
                # print(75 * '=')


if __name__ == '__main__':
    extensive_test()
