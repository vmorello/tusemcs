#
# Functions to convert slice strings, or a list of slice strings to
# lists of ints / bitmasks
# These functions are/will be used for at least two purposes:
# * Creating channel masks from a list of channel index ranges
# * Selecting a given list of Node IDs in some katcp requests sent to the MasterController
#   Example: update a configuration parameter only on a subset of nodes
#
# Vincent Morello - 2019
#
import numpy as np


def parse_slice_string(s):
    """
    Parse a string that specifies a python slice, in the usual notation
    start:stop:step. Returns a slice object.
    """
    def str2int(x):
        if not x:
            return None
        else:
            return int(x)

    def bad_slice_err():
        raise ValueError("Invalid slice string {!r}".format(s))

    s = s.replace(' ', '')

    # Case of a single number
    try:
        n = int(s)
        return slice(n , n+1)
    except ValueError:
        pass

    try:
        elements = list(map(str2int, s.split(':')))
    except:
        bad_slice_err()

    if not len(elements) in (2, 3):
        bad_slice_err()

    return slice(*elements)


def parse_slicelist_string(s):
    """
    Parse a comma-separated list of strings, that each specify a python slice
    in the usual notation start:stop:step. Returns a list of slice objects.
    """
    try:
        return [parse_slice_string(x) for x in s.split(',')]
    except:
        raise ValueError("Invalid multislice string {!r}".format(s))


def slicelist_to_mask(sl, size):
    """
    Convert a list of slices into a boolean numpy array of given size.
    The mask is created with all elements set to False, and any element that
    gets selected by any slice in 'sl' is set to True.
    """
    mask = np.zeros(size, dtype=bool)
    for s in sl:
        mask[s] = True
    return mask


def slicelist_to_indices(sl, size):
    """
    A convenience function that returns
    numpy.where(slicelist_to_mask(sl, size))[0]

    That is, an array of indices in the range 0:size selected by 
    any slice in sl.
    """
    return np.where(slicelist_to_mask(sl, size))[0]
