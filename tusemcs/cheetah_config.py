#
# Cheetah XML configuration file generation
# Vincent Morello - 2020
#

import logging
import os
import math
from copy import deepcopy

from lxml import etree
from jinja2 import Template, DictLoader, Environment, select_autoescape, StrictUndefined

from pipeline_utils import DotDict
from beam import validate_beams
from ddplan import DDPlan


log = logging.getLogger('tusemcs.cheetah_config')


def load_cheetah_template():
    thisdir = os.path.dirname(os.path.realpath(__file__))
    fname = os.path.join(thisdir, '..', 'templates', 'cheetah.jinja2')
    with open(fname, 'r') as fobj:
        return Template(fobj.read(), undefined=StrictUndefined)


def cleanup_xml(text):
    """
    Sanitise the generated Cheetah configuration XML text.
    This means ensuring that the XML is valid, removing comments, blank lines 
    and re-indenting properly.

    Parameters
    ----------
    text : str
        Input XML

    Returns
    -------
    cleaned: str
        Validated and cleaned XML
      
    Raises
    ------
    Any exception that lxml.etree.fromstring might raise if the input XML text
    is not valid
    """
    # Useful reads:
    # https://lxml.de/FAQ.html#why-can-t-lxml-parse-my-xml-from-unicode-strings
    # https://stackoverflow.com/questions/18313818/how-to-not-load-the-comments-while-parsing-xml-in-lxml

    # Check that raw is valid XML, this can raise exceptions that we deliberately do not catch
    # We remove both comments and empty lines
    xml = etree.fromstring(
        text.encode('utf-8'),
        parser=etree.XMLParser(remove_comments=True, remove_blank_text=True)
    )
    # Return cleaned xml with the original declaration and encoding
    cleaned = etree.tostring(xml, pretty_print=True, xml_declaration=True, encoding='utf-8')
    return cleaned


class CheetahConfig(object):
    """
    Create and store cheetah configuration.

    Parameters
    ----------
    params : dict
        The dictionary of parameters of the 'pipeline.cheetah' section
        of the node_controller.yml config file
    beams : list
        List of Beam objects
    outdir : str
        Full path to the base output directory for the pipeline run.
        If running the pipeline in another container, this must be
        *** the base output directory path as viewed from INSIDE the pipeline container ***
    bw : float
        Bandwidth in Hz. This is used to choose the appropriate DDPlan for AA.
    tsamp : float
        Sampling time of the processed data, that is AFTER extra scrunching
    """
    CHEETAH_TEMPLATE = load_cheetah_template()
    
    def __init__(self, params, beams, outdir='/output', bw=856.0e6, tsamp=306.24e-6):
        validate_beams(beams)
        self.params = deepcopy(params)
        self.beams = beams
        # NOTE: rstrip trailing slashes just in case.
        # Because the joining between outdir and each beam directory name 
        # is done in the template itself
        self.outdir = outdir.rstrip(os.path.sep)

        # Select correct DDPlan for the band
        band_name = 'c{:.0f}'.format(bw / 1.0e6)
        ddplan_str = params['ddplan_str'][band_name]
        ddplan = DDPlan.fromstring(ddplan_str)
        log.debug("Selected DDPlan = {}".format(ddplan_str))

        # Update params that need dynamic values
        aggbuffer_timespan = self.params['aggbuffer_timespan']
        dedispersion_samples = int(math.ceil(aggbuffer_timespan / tsamp))
        aggbuffer_timespan = dedispersion_samples * tsamp

        self.params['aggbuffer_timespan'] = aggbuffer_timespan
        self.params['sps']['ddplan'] = ddplan
        self.params['sps']['dedispersion_samples'] = dedispersion_samples

        log.debug("Selected SPS dedispersion_samples = {}".format(dedispersion_samples))
        log.debug("Effective aggbuffer timespan = {:.3f} seconds".format(aggbuffer_timespan))
    
    def generate_xml(self):
        """
        Returns the generated XML text
        """
        rendered = self.CHEETAH_TEMPLATE.render(self.params, beams=self.beams, outdir=self.outdir)
        return cleanup_xml(rendered)


def test():
    import yaml
    import pprint
    import katpoint

    from beam import Beam

    mc_address = "239.1.1.42:7147"
    target = katpoint.Target("J1151-6108,radec,11:51:56.86,-61:08:17.6")

    beams = [
        Beam(ii, mc_address, "cfbf{:05d}".format(ii + 100), target)
        for ii in range(1)
    ]

    with open("/home/vince/repositories/tusemcs/config/node_controller.yml", 'r') as fobj:
        node_config = yaml.safe_load(fobj)
    params = node_config['pipeline']['cheetah']

    conf = CheetahConfig(params, beams, outdir='/some_output_directory/subdir', tsamp=306.24e-6)

    pprint.pprint(conf.params)
    print("")
    print(conf.generate_xml())


if __name__ == '__main__':
    import coloredlogs
    logfmt = "[%(levelname)s - %(asctime)s - %(name)s - %(filename)s:%(lineno)s] %(message)s"
    coloredlogs.install(fmt=logfmt, level='DEBUG', milliseconds=True, logger=log)
    test()