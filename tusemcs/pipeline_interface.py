import copy
import logging
import os
import textwrap
import json
from datetime import datetime

import docker
from docker.types import Ulimit
import jinja2
import katpoint
import yaml

from tusemcs import __version__
from beam import Beam
from mkrecv_config import MkrecvConfig
from pipeline_utils import DotDict
from dada_transpose_config import DADATransposeConfig
from cheetah_config import CheetahConfig
from pipeline_script import generate_pipeline_script

from pipeline_utils import (
    MKRECV_HEADER_FNAME, CHEETAH_CONFIG_FNAME, AA_CHANMASK_FNAME,
    PIPELINE_SCRIPT_FNAME, UTC_FORMAT, PIPELINE_CONTAINER_LOGFILE,
    RUN_SUMMARY_FNAME, DTCONF_FNAME, get_directory_path
)

log = logging.getLogger('tusemcs.pipeline_interface')


def ingest_data_rate(product_meta, beams):
    """
    Returns the total ingest rate for the node in bytes/s.

    Parameters
    ----------
    product_meta : dict
        Product metadata passed from the MasterController
    beams : list
        List of Beam objects

    Returns
    -------
    rate : float
        Total ingest rate for the node in bytes/s.
    """
    nchannels = product_meta['nchannels'] # CBF number of channels
    coherent = beams[0].coherent

    if coherent:
        tau = product_meta['coherent_beam_time_resolution']
        fscrunch = product_meta['coherent_beam_fscrunch']
    else:
        tau = product_meta['incoherent_beam_time_resolution']
        fscrunch = product_meta['incoherent_beam_fscrunch']

    fbfuse_nchannels = nchannels // fscrunch
    return len(beams) * fbfuse_nchannels / tau


def write_file(fname, text, description='config', dry_run=False):
    """
    Convenience function to write text to file, with a dry run option
    which just prints a log message without actually writing.

    Parameters
    ----------
    fname : str
        File path as seen from the control container
    text : str
        Contents of the file
    description : str, optional
        Description of the file that appears in the log message,
        e.g. 'pipeline script'
    dry_run : bool, optional
        If True, only push a log message without actually writing the file
    """
    log.debug("Writing {} to file: {}".format(description, fname))
    if not dry_run:
        with open(fname, 'w') as fobj:
            fobj.write(text)


def makedirs(path, description='some directory', dry_run=False):
    """
    Same as os.makedirs(), but with a log message and a dry run option.
    Does nothing if 'path' is already an existing directory.
    """
    log.debug("Creating {}: {}".format(description, path))
    if not dry_run and not os.path.isdir(path):
        os.makedirs(path)


def assemble_beams(mca_mapping, beam_positions):
    """
    Generate a list of Beam objects from what the NodeController has in store

    Parameters
    ----------
    mca_mapping : dict
        The multicast address mapping, i.e. the beam assignment.
    beam_positions : dict
        A dictionary where keys are beam IDs such as 'cfbf00001' and values
        are katpoint.Target objects.

    Returns
    -------
    beams : list
        List of Beam objects sorted in both increasing 
        relative and absolute number
    """
    beams = [
        # NOTE: relative numbers are correctly assigned below
        Beam(0, mc_address, beam_id_str, beam_positions[beam_id_str])
        for mc_address, beam_ids in mca_mapping.items()
        for beam_id_str in beam_ids
    ]

    # Sort by absolute number, and then assign increasing relative numbers
    beams = sorted(beams, key=lambda b: b.absnum)
    for relnum, beam in enumerate(beams, start=0):
        beam.relnum = relnum

    return beams


def summary_dict(node_config, product_meta, beam_meta, beams, sb_details, mkconf, dtconf, utc_start):
    """
    Generate the JSON-serializable dictionary with a summary of all parameters
    for this pipeline run.
    NOTE: this is written to a file, that then gets edited on pipeline stop,
    where the "utc_stop" key is added.

    Parameters
    ----------
    node_config : NodeConfig
        dict-like object with the NodeController configuration
    product_meta : dict
        Product metadata passed from the MasterController
    beam_meta: dict
        Nested dictionary, with the following structure
        {
            'positions' : dict
            'shape' : dict
            'ca_target_request' : dict
        }
    beams : list
        List of beam objects that are being processed
    sb_details : dict
        Dictionary of schedule block parameters read from CAM
    mkconf : MkrecvConfig
    dtconf : DADATransposeConfig
    utc_start : str
        Pipeline UTC start as a *string*

    Returns
    -------
    params : dict
        Dictionary summarizing parameters of the run, to be read by the
        database ingest script and the plotting script
    """
    # Specification for file format is here:
    # https://docs.google.com/document/d/12XrMOUnoZWenwrcAc-VpayThCf1jJbuJbDDEzWb2N_8

    # NOTE: the output parameters 'tsamp' and 'nchan' depend on the scrunching factor
    # If transpose is not running, there are no output buffers in 'dtconf',
    # and no scrunching is taking place
    tsamp = mkconf.params['tsamp'] # input tsamp
    nchan = mkconf.params['nchan'] # input nchan
    if dtconf.output_buffers:
        outbuf = dtconf.output_buffers[0]
        tsamp = outbuf.tsamp
        nchan = outbuf.nchans

    data_params = {
        'bw': mkconf.params['bw'], # Hz
        'cfreq': mkconf.params['freq'], # Hz, NOTE: we name this cfreq for clarity (CENTRE freq)
        'nbeam': len(beams),
        'nbit': mkconf.params['nbit'],
        'nchan': nchan, # number of channels in *scrunched* data
        'npol': mkconf.params['npol'],
        'sample_clock': mkconf.params['sample_clock'], # Hz
        'sync_time': mkconf.params['sync_time'], # POSIX timestamp
        'tsamp': tsamp, # sampling time of the *scrunched* data
    }

    beam_params = {
        # NOTE: convert from CSV string to list of strings
        # These are lists of antenna names
        'cb_antennas': product_meta['coherent_beam_antennas'].split(','),
        'ib_antennas': product_meta['incoherent_beam_antennas'].split(','),

        'coherent_beam_shape': beam_meta['shape'],
        'ca_target_request': beam_meta['ca_target_request'],
        
        # Beam attributes (position, beam number, etc.)
        'list': [b.asdict() for b in beams],
    }

    pipeline_params = dict(node_config.dget('pipeline'))

    version_info = {
        'control': __version__
    }

    params = {
        'utc_start': utc_start,
        'version_info': version_info,
        'data': data_params,
        'beams': beam_params,
        'pipeline': pipeline_params,
        'sb_details': sb_details
    }
    return params  


def update_summary_file(fname, items):
    """
    Update the contents of the run summary JSON file with additional items.
    If the summary file cannot be found at the expected path, 
    this does nothing.

    Parameters
    ----------
    fname : str
        Path to the run summary JSON file, which is expected to contain a
        JSON-encoded dict
    items : dict
        Dictionary with which to update the run summary params dict
    """
    log.debug("Updating summary file {!r}".format(fname))
    if not os.path.exists(fname):
        log.warning("Summary file not found: {!r}, unable to update it".format(fname))
        return

    with open(fname, 'r') as f:
        summary = json.load(f)
    
    summary.update(items)

    # TODO: replace this with an atomic file write for extra safety
    with open(fname, 'w') as f:
        json.dump(summary, f, indent=4, sort_keys=True)


def start_pipeline(node_config, mca_mapping, product_meta, beam_meta, sb_details, 
    dry_run=False, if_address=None):
    """
    Generate script and configuration files for all pipeline executables, write
    them in the shared directories, and start the pipeline docker containers.

    NOTE: re-raise all exceptions caught in this function so that the
    NodeController can catch said exception, stop containers, and go back to a
    non-error state if possible.

    Parameters
    ----------
    node_config: NodeConfig
        NodeConfig object parsed from node_controller.yml configuration file.
    mca_mapping: dict
        The multicast address mapping, i.e. the beam assignment.
    product_meta : dict
        Product metadata read from CAM portal at ?capture-init
        The full list of keys is specified in the MasterPortalClient code
    beam_meta: dict
        Nested dictionary, with the following structure
        {
            'positions' : dict
            'shape' : dict
            'ca_target_request' : dict
        }
        'positions' is a dict where keys are beam IDs such as 'cfbf00001' and
        values are katpoint.Target objects. The rest is metadata saved in the 
        run summary file.
    sb_details: dict
        Dictionary with the active schedule block details as returned by a 
        KATPortalClient instance
    dry_run : bool, optional
        If True, do not create directories, do not write any files, and do
        not start the pipeline container. Log messages are still printed
        *exactly* as normal. Useful for testing.
    if_address : str, optional
        IPv4 address of the data capture interface. Useful for testing only.
        Leave this to None in normal operation, this means that the address
        is inferred from the list of available interfaces
    
    Returns
    -------
    utc_start: datetime
        The UTC when this function was called, as a datetime object. 
        The name of the output directory for the pipeline run started by this
        function call is simply utc_start converted to a string.
    bpmon_enabled : bool
        Whether the C++ bandpass monitor has been launched, which happens only
        if the ingest data rate is below a user-defined threshold.
    """
    if dry_run:
        log.warning("start_pipeline() has been called with dry_run=True")

    # pipeline mode
    mode = node_config.dget('pipeline.mode')

    # determine utc_start
    utc_start_datetime = datetime.utcnow()
    utc_start = utc_start_datetime.strftime(UTC_FORMAT)
    log.info("UTC start: {}".format(utc_start))

    # 1) assemble beam information
    beam_positions = beam_meta['positions']
    beams = assemble_beams(mca_mapping, beam_positions)

    # 2) Create directory structure for pipeline run
    # Create directory for config files and scripts
    config_dir_control = get_directory_path('control', utc_start, 'config')
    makedirs(config_dir_control, description='config directory', dry_run=dry_run)

    # NOTE; beam directories are only required in production mode
    # In filterbank mode, the filterbank files are written directly in the
    # base output directory
    if mode == 'production':
        for beam in beams:
            # NOTE: path as viewed from inside the CONTROL container
            beam_dir_control = os.path.join(
                get_directory_path('control', utc_start, 'output'),
                beam.outdir_name
            )
            makedirs(beam_dir_control, description='output beam directory', dry_run=dry_run)

    # 3) Generate and write mkrecv config
    mkrecv_config = MkrecvConfig.from_production_input(
        product_meta, 
        beams,
        network_protocol=node_config.dget('pipeline.mkrecv.network_protocol'),
        if_address=if_address
    )

    mkrecv_header = mkrecv_config.generate_header()
    mkrecv_header_path = os.path.join(config_dir_control, MKRECV_HEADER_FNAME)
    write_file(mkrecv_header_path, mkrecv_header, description='mkrecv header', dry_run=dry_run)

    # 4) Generate config for DADA buffers and transpose
    ingest_rate = ingest_data_rate(product_meta, beams)
    bpmon_enabled = (ingest_rate <= node_config.dget('pipeline.bpmon.maxrate'))
    log.debug("Ingest data rate: {:.2f} MBytes/s".format(ingest_rate / 1e6))
    log.info("C++ bandpass monitor enabled: {}".format(bpmon_enabled))

    dtconf = DADATransposeConfig.from_production_input(
        product_meta, 
        beams,
        mode=mode,
        timespan_min=node_config.dget('pipeline.dada.timespan'),
        nbuffers=node_config.dget('pipeline.dada.nbuffers'), 
        tsamp_output_min=node_config.dget('pipeline.scrunching.tsamp_min'), 
        nchans_output_max=node_config.dget('pipeline.scrunching.nchans_max'),
        bpmon=bpmon_enabled
    )
    dtconf_text = json.dumps(dtconf.asdict(), indent=4)
    dtconf_path = os.path.join(config_dir_control, DTCONF_FNAME)
    write_file(dtconf_path, dtconf_text, description='DADATransposeConfig', dry_run=dry_run)

    # 5) Generate and write cheetah config
    if mode == 'production':
        cheetah_config = CheetahConfig(
            node_config.dget('pipeline.cheetah'),
            beams,
            # This must be the path to the output UTC directory as seen from 
            # inside the PIPELINE container.
            outdir=get_directory_path('pipeline', utc_start, 'output'),
            bw=product_meta['bandwidth'],
            tsamp=dtconf.tsamp_output
        )
        cheetah_xml = cheetah_config.generate_xml()
        cheetah_config_path = os.path.join(config_dir_control, CHEETAH_CONFIG_FNAME)
        write_file(cheetah_config_path, cheetah_xml, description='cheetah XML config', dry_run=dry_run)

    # 6) Write channel mask for AA
    # In the future, this will be specified via cheetah, or we will use dynamic masking
    # NOTE: 'nchan' = number of channels AFTER all forms of scrunching have been applied
    # '1' denotes an open channel, '0' a masked one
    if mode == 'production':
        # AA expects the chanmask file at a hardcoded path:
        # /output/channel_mask.dat
        # That's why we write in in the base output directory here, and NOT the config dir
        chanmask_path_control = os.path.join(
            get_directory_path('control', utc_start, 'output'),
            AA_CHANMASK_FNAME
        )
        # leave all channels open, IQRM has us covered now (Feb. 2021)
        chanmask = dtconf.nchans_output * ['1'] 
        chanmask_text = '\n'.join(chanmask) + '\n' # AA expects trailing newline char
        write_file(chanmask_path_control, chanmask_text, description='channel mask', dry_run=dry_run)

    # 7) write pipeline script
    script = generate_pipeline_script(
        dtconf,
        # configuration files directory as seen from inside the pipeline container
        confdir=get_directory_path('pipeline', utc_start, 'config'),
        # output dir as seen from inside the pipeline container
        outdir=get_directory_path('pipeline', utc_start, 'output'),
        ipcrm=node_config.dget('pipeline.dada.ipcrm'),
        filterbank_size=node_config.dget('pipeline.filterbank_size'),
        bpmon=bpmon_enabled
    )
    script_path_control = os.path.join(config_dir_control, PIPELINE_SCRIPT_FNAME)
    write_file(script_path_control, script, description='pipeline script', dry_run=dry_run)

    # 8) start pipeline container
    client = docker.DockerClient()

    # name of the pipeline container
    image_name = node_config.dget("pipeline.image")
    c_name = node_config.dget("pipeline.container")

    # assemble mounts
    mounts = []

    # Output UTC directory mount
    # We use the host's docker executable and socket, and thus we need to specify
    # the UTC directory path on the host
    data_mount = docker.types.Mount(
        source=get_directory_path('host', utc_start, 'output'),
        target=get_directory_path('pipeline', utc_start, 'output'),
        type="bind"
    )
    mounts.append(data_mount)
    log.debug("Pipeline container mounts: {}".format(mounts))

    # enable core dumps if requested
    ulimits = [Ulimit(name='core', soft=0, hard=0)]
    if node_config.dget('pipeline.coredumps'):
        ulimits = []
        log.warning("Core dumps are enabled")

    # path to the pipeline script as seen from the pipeline container
    script_path_pipeline = os.path.join(
        get_directory_path('pipeline', utc_start, 'config'),
        PIPELINE_SCRIPT_FNAME
    )
    log.debug("Path to the pipeline script as seen from the pipeline container: {}".format(script_path_pipeline))

    log.info("Starting container: {}".format(c_name))
    # NOTE: allocate a pseudo-TTY so that stdout flushes every time a program
    # prints a newline char. An enlightening read on the subject:
    # https://eklitzke.org/stdout-buffering
    if not dry_run:
        try:
            client.containers.run(image_name,
                # NOTE: this must be the path to the pipeline script as seen from
                # inside the pipeline container
                command="bash {}".format(script_path_pipeline),
                auto_remove=False,  # keep the container around in case of a crash, to get logs
                remove=False,
                detach=True,
                cap_add=["IPC_LOCK"],   # so that dada_db can lock memory
                mem_limit="160g", # maximum allowed RAM usage
                memswap_limit="160g", # disallow swap usage by setting mem+swap equal to mem_limit
                mounts=mounts,
                name=c_name,
                network_mode="host",
                ipc_mode="host",
                runtime="nvidia",
                ulimits=ulimits,
                tty=True 
                )
        except docker.errors.ContainerError:
            raise RuntimeError("The docker container exited with an error: {0}".format(c_name))
        except docker.errors.ImageNotFound:
            raise RuntimeError("The docker image does not exist: {0}".format(image_name))
        except docker.errors.APIError:
            raise RuntimeError("Could not start the docker container: {0}".format(c_name))

    log.info("Successfully started container: {0}".format(c_name))

    # Write run summary params in a JSON file
    summary_params = summary_dict(node_config, product_meta, beam_meta, beams, sb_details, mkrecv_config, dtconf, utc_start)
    summary_text = json.dumps(summary_params, indent=4, sort_keys=True)

    summary_path = os.path.join(
        get_directory_path('control', utc_start, 'output'),
        RUN_SUMMARY_FNAME
    )
    write_file(summary_path, summary_text, 'start params', dry_run=dry_run)
    return utc_start_datetime, bpmon_enabled


def stop_pipeline(node_config, utc_start):
    """ 
    Stop all pipeline containers.

    Note: Raise exceptions on errors so that the node controller can catch them.

    Parameters
    ----------
    node_config: NodeConfig
        NodeConfig object parsed from node_controller.yml configuration file.
    utc_start: datetime
        The UTC when the pipeline container was started, as a datetime object.
        This is currently used to determine in which directory to save the 
        pipeline container logs. The NodeController is currently responsible
        for keeping track of the last utc_start.

    Returns
    -------
    utc_stop: datetime or None
        The UTC when the pipeline container was stopped, as a datetime object.
        If the container does not exist, return None.
    
    Raises
    ------
    RuntimeError
        If the containers could not be stopped, or if errors occured.
    """
    utc_start_str = utc_start.strftime(UTC_FORMAT)
    client = docker.DockerClient()

    # Stop container, we do not wrap this in a try block so that exceptions
    # will be caught by the NodeController, and send it into ERROR state if
    # necessary
    c_name = node_config.dget('pipeline.container')
    log.info("Stopping container {!r}".format(c_name))

    try:
        cont = client.containers.get(c_name)
        cont.stop(timeout=1)
        log.info("Successfully stopped container: {}".format(c_name))
    except docker.errors.NotFound:
        log.info("Pipeline container {!r} not found: nothing to do".format(c_name))
        return None
    except Exception as err:
        raise # will be caught by NodeController

    # determine utc_stop
    utc_stop_datetime = datetime.utcnow()
    utc_stop = utc_stop_datetime.strftime(UTC_FORMAT)
    log.info("UTC stop: {}".format(utc_stop))

    # Save logs in the base UTC directory
    try:
        pipeline_logs = cont.logs(timestamps=False)
        # NOTE: utc_start is a datetime object here
        pipeline_logfile = os.path.join(
            get_directory_path('control', utc_start_str, 'output'),
            PIPELINE_CONTAINER_LOGFILE
            )
        with open(pipeline_logfile, 'w') as fobj:
            fobj.write(pipeline_logs)
        log.info("Saved pipeline container logs to: {}".format(pipeline_logfile))

    except Exception as err:
        log.warning("Failed to save pipeline container logs: {}".format(err))

    # Update run summary JSON file with stop params
    params = {'utc_stop': utc_stop}
    summary_path = os.path.join(
        get_directory_path('control', utc_start_str, 'output'),
        RUN_SUMMARY_FNAME
    )
    update_summary_file(summary_path, params)

    # Last step: remove container
    # Here we also want to send the NodeController into error state if this fails
    cont.remove()
    return utc_stop_datetime


def test():
    import katpoint
    from node_config import NodeConfig

    # Generate some test input
    product_meta = {
        "wide_adc_sample_rate": 1712000000.0, 
        "coherent_beam_antennas": "m000,m001,m002,m003,m004,m005,m006,m007,m008,m009,m010,m011,m012,m013,m014,m015,m016,m017,m018,m019,m020,m021,m022,m023,m024,m025,m026,m027,m028,m029,m030,m031,m032,m034,m035,m036,m037,m038,m039,m040", 
        "centre_frequency": 1284000000.0, 
        "coherent_beam_samples_per_heap": 512, 
        "incoherent_beam_fscrunch": 4, 
        "coherent_beam_idx1_step": 268435456, 
        "nchannels": 4096, 
        "synchronisation_epoch": 1613400491.0, 
        "bandwidth": 856000000.0, 
        "coherent_beam_tscrunch": 64, 
        "coherent_beam_time_resolution": 0.000306242990654206, 
        "incoherent_beam_tscrunch": 64, 
        "coherent_beam_fscrunch": 1, 
        "coherent_beam_heap_size": 8192, 
        "incoherent_beam_heap_size": 8192, 
        "incoherent_beam_antennas": "m000,m001,m002,m003,m004,m005,m006,m007,m008,m009,m010,m011,m012,m013,m014,m015,m016,m017,m018,m019,m020,m021,m022,m023,m024,m025,m026,m027,m028,m029,m030,m031,m032,m033,m034,m035,m036,m037,m038,m039,m040,m042,m043,m044,m045,m046,m047,m048,m049,m050,m051,m052,m053,m054,m055,m056,m057,m058,m059,m060,m061,m062,m063", 
        "incoherent_beam_samples_per_heap": 512, 
        "incoherent_beam_idx1_step": 268435456, 
        "coherent_beam_subband_nchans": 16, 
        "incoherent_beam_subband_nchans": 16, 
        "incoherent_beam_time_resolution": 0.000306242990654206
    }

    target = katpoint.Target("J1151-6108,radec,11:51:56.86,-61:08:17.6")

    ### COHERENT beams
    # mca_mapping = {
    #     "239.1.1.1:7147": ['cfbf{:05d}'.format(ii) for ii in range(100, 106)],
    #     "239.1.1.2:7147": ['cfbf{:05d}'.format(ii) for ii in range(106, 112)]
    # }

    # beam_meta = {
    #     'positions' : {"cfbf{:05d}".format(ii) : target for ii in range(100, 112)},
    #     'shape' : {'x': 0.42, 'y': 1.42, 'angle': 2.42},
    #     'ca_target_request' : {'beams': [], 'tilings': ['LULZ', 'MDR']}
    # }

    ### INCOHERENT beam
    mca_mapping = {
        "239.1.1.1:7147": ['ifbf00000'],
    }

    beam_meta = {
        'positions' : {'ifbf00000' : target},
        'shape' : {'x': 0.42, 'y': 1.42, 'angle': 2.42},
        'ca_target_request' : {'beams': [], 'tilings': ['LULZ', 'MDR']}
    }

    sb_details = {}

    node_config = NodeConfig.from_yaml_config("/home/vince/repositories/tusemcs/config/node_controller.yml")

    utc_start, bpmon_enabled = start_pipeline(
        node_config, mca_mapping, product_meta, beam_meta, sb_details, 
        dry_run=True, if_address='192.168.0.42'
    )
    print(utc_start.isoformat(), bpmon_enabled)

    stop_pipeline(node_config, utc_start)


if __name__ == '__main__':
    import coloredlogs
    logfmt = "[%(levelname)s - %(asctime)s - %(name)s - %(filename)s:%(lineno)s] %(message)s"
    coloredlogs.install(fmt=logfmt, level='DEBUG', milliseconds=True, logger=logging.getLogger('tusemcs'))
    test()