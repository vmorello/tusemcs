.. TUSE MCS documentation master file, created by
   sphinx-quickstart on Wed May 16 18:17:16 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to TUSE MCS's documentation!
====================================

.. toctree::
    :maxdepth: 2
    :caption: Contents:

    api
    changes
    todo

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
