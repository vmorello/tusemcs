###############################################################################
# Read files saved by the bandpass monitor in a specified date range
# and save a bandpass history heatmap plot
# NOTE: requires python3 and matplotlib
#
# Vincent Morello - 2019
###############################################################################
import os
import argparse
import logging
from typing import NamedTuple
from datetime import datetime
import numpy as np

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt


log = logging.getLogger('jackson')


def date2str(date):
    return date.strftime("%Y-%m-%d_%H:%M:%S")

def str2date(s):
    return datetime.strptime(s, "%Y-%m-%d_%H:%M:%S")

def configure_logging():
    handler = logging.StreamHandler()
    formatter = logging.Formatter(
        "%(asctime)s %(filename)s:%(lineno)-4s %(levelname)-8s %(message)s")
    handler.setFormatter(formatter)
    log.addHandler(handler)
    log.setLevel(logging.DEBUG)


class BandpassFile(NamedTuple):
    fname: str
    node: str
    timestamp: float
    stat: str
    date: datetime
    
    @classmethod
    def from_fname(cls, fname):
        """ Parse a bandpass sample file without loading it """
        fname = os.path.realpath(fname)
        __, basename = os.path.split(fname)
        basename, __ = os.path.splitext(basename)
        node, timestamp, stat = basename.split('_')
        timestamp = float(timestamp)
        date = datetime.utcfromtimestamp(timestamp)
        return cls(fname, node, timestamp, stat, date)


class BandpassSample(NamedTuple):
    fname: str
    node: str
    timestamp: float
    stat: str
    date: datetime
    nbeams: int
    nchans: int
    data: np.ndarray

    @classmethod
    def from_fname(cls, fname):
        """ Parse a bandpass sample file and load the data it contains """
        bpf = BandpassFile.from_fname(fname)
        data = np.load(bpf.fname).astype(np.float32)
        nbeams, nchans = data.shape
        return cls(bpf.fname, bpf.node, bpf.timestamp, bpf.stat, bpf.date, nbeams, nchans, data)


def equal_to(x, ref):
    """ Special equality test that returns True if the second argument
    'ref' is None """
    return True if ref is None else x == ref


def load_bandpass_files(dir, node=None, utc_start=None, utc_end=None):
    def earlier_than(date, ref):
        return True if ref is None else date <= ref

    def later_than(date, ref):
        return True if ref is None else date >= ref

    valid = lambda bpf: (
        earlier_than(bpf.date, utc_end) and
        later_than(bpf.date, utc_start) and
        equal_to(bpf.node, node)
    )

    dir = os.path.realpath(dir)
    files = []
    for fname in os.listdir(dir):
        try:
            fname = os.path.join(dir, fname)
            bpf = BandpassFile.from_fname(fname)
            if valid(bpf):
                files.append(bpf)
        except:
            pass
    return files


def load_bandpass_samples(dir, node=None, utc_start=None, utc_end=None, nchans=None):    
    files = load_bandpass_files(dir, node=node, utc_start=utc_start, utc_end=utc_end)
    files = list(filter(lambda f: f.stat == 'std', files))

    log.debug("Sample files after filtering for node and date: {}".format(len(files)))
    samples = [
        BandpassSample.from_fname(bpf.fname) 
        for bpf in files
        ]
    log.debug("Loaded sample data")

    samples = list(filter(lambda s: equal_to(s.nchans, nchans), samples))
    log.debug("Sample files after filtering for nchans: {}".format(len(samples)))

    samples = sorted(samples, key=lambda s: s.date)
    date_first = samples[0].date.strftime("%Y-%m-%d_%H:%M:%S")
    date_last = samples[-1].date.strftime("%Y-%m-%d_%H:%M:%S")
    log.debug("Date of first sample: {}".format(date_first))
    log.debug("Date of last sample : {}".format(date_last))
    return samples


def plot_bandpass_history(data, date_start, date_end, outdir=os.getcwd()):
    """
    data: ndarray
        Bandpass standard deviation history of one beam, 
        shape = (nbeams, nchans)
    date_start: datetime
        Date of first bandpass sample
    date_end: datetime
        Date of last bandpass sample
    """
    ### Determing image size and dpi
    # side_max = 2000
    # nsamp, nchans = data.shape
    # dpi = max(data.shape) / side_max * 100
    # figsize = (nchans / dpi, nsamp / dpi) # width = channels, height = samples

    nsamp, nchans = data.shape
    dpi = 100.0
    figsize = (
        max(nchans / dpi, 600.0 / dpi), 
        max(nsamp / dpi, 600.0 / dpi)
        )

    # Scale all fonts
    matplotlib.rcParams.update({'font.size': 5 * max(data.shape) / 1000.0})

    log.debug("Generating bandpass history plot with figsize={}, dpi={}".format(figsize, dpi))

    plt.figure(figsize=figsize, dpi=dpi)
    plt.imshow(data, aspect='auto')
    plt.imshow(np.log10(data), aspect='auto')
    plt.xlabel("Channel Index")
    plt.ylabel("Sample Index")
    plt.xticks()
    title = "Bandpass Standard Deviation\n{} to {}".format(
        date2str(date_start),
        date2str(date_end)
    )
    plt.title(title)
    plt.tight_layout()

    fname = os.path.join(outdir, 'bandpass_history.png')
    plt.savefig(fname)
    log.debug("Saved bandpass standard deviation history plot to: {}".format(fname))


###############################################################################

def dirpath(s):
    if not os.path.isdir(s):
        raise argparse.ArgumentTypeError("{!r} is not a directory".format(s))
    return s


def parse_args():
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        description="Reads a subset of bandpass samples in a directory "
        "and saves them to two numpy arrays (sample_dates and data). "
        "Only the data from the first beam is saved. "
        )

    parser.add_argument(
        '--node',
        type=str,
        default=None,
        help="Collect samples only from this node name"
        )

    parser.add_argument(
        '--start',
        type=str2date,
        default=None,
        help="Collect samples only after specified start date, in YYYY-MM-DD_HH:MM:SS format"
        )

    parser.add_argument(
        '--end',
        type=str2date,
        default=None,
        help="Collect samples only before specified start date, in YYYY-MM-DD_HH:MM:SS format"
        )

    parser.add_argument(
        '--nchans',
        type=int,
        default=None,
        help="Collect samples only with specified number of channels"
        )

    parser.add_argument(
        '--ibeam',
        type=int,
        default=0,
        help="Save data only from this beam number. This is a relative beam index within the "
            "bandpass sample files, NOT an absolute fbfuse beam number."
        )

    parser.add_argument(
        '--outdir',
        type=dirpath,
        default=os.getcwd(),
        help="Save output(s) in this directory"
        )

    parser.add_argument(
        'dir',
        type=dirpath,
        help='Directory in which the bandpass samples are'
        )
    return parser.parse_args()


def main():
    configure_logging()
    args = parse_args()

    samples = load_bandpass_samples(
        args.dir,
        utc_start=args.start,
        utc_end=args.end,
        nchans=args.nchans
    )
    dates = np.asarray([s.date for s in samples], dtype=np.datetime64)

    log.debug("Keeping only data from relative beam index {}".format(args.ibeam))
    data = np.asarray([s.data[args.ibeam] for s in samples])

    fname_dates = os.path.join(args.outdir, 'sample_dates.npy')
    np.save(fname_dates, dates)
    log.debug("Saved sample dates to: {}".format(fname_dates))

    fname_data = os.path.join(args.outdir, 'data.npy')
    np.save(fname_data, data)
    log.debug("Saved bandpass standard deviation data of first beam to: {}".format(fname_data))

    plot_bandpass_history(data, samples[0].date, samples[-1].date, outdir=args.outdir)


if __name__ == '__main__':
    main()
