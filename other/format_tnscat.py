#
# Select interesting sources from TNS catalogue for followup
# Convert them to our source list YAML format
#
# NOTE: this is a python3 utility that requires a 'fixed' version of the frbcat python module
# For now, it's here: https://github.com/fjankowsk/frbcat
# At least until it gets merged into the main repo
#
import numpy as np
import yaml
import textwrap

import pandas
import frbcat
import astropy.units as uu
from astropy.coordinates import SkyCoord


def fix_name(s):
    """ 
    Some FRB names, especially in the repeater_of_objid column, have two spaces
    instead of one betwen the 'FRB' prefix and their name.
    
    WE REMOVE ALL SPACES, all names are like 'FRB20110523A'
    """
    if isinstance(s, str):
        return ''.join(s.split())
    elif pandas.isnull(s):
        return s
    else:
        raise ValueError(f"Cannot fix name {s}")

def parse_uncertainty_arcmin(s):
    """ Convert uncertainty string to a float value in arcsec """
    val, unit = s.split()
    unit = getattr(uu, unit)
    val = float(val) * unit
    return val.to(uu.arcmin).to_value()

# At 50% overlap: each ring of beams expands the coverage radius by 2 x beam radius (~2.4 arcmin)
# Tiaan said that the average half-power semi-major axis of a beam is 1.2 arcmin.
#  1 beam  = 1.2 arcmin radius
#  7 beams = 3.6 arcmin radius
# 19 beams = 6.0 arcmin radius
TILING_STRATEGY = [
    # min_uncertainty_arcmin, nbeams, overlap
    (0.000, 1, 0.5),   # 1.2' smaxis
    (0.800, 7, 0.5),   # 3.6' smaxis
    (2.400, 19, 0.5),  # 6.0' smaxis
]

TILING_THRESHOLDS = [t[0] for t in TILING_STRATEGY]

def get_tiling_params(u):
    """ u (float) is an uncertainty in arcmin """   
    index = np.digitize(u, TILING_THRESHOLDS) - 1
    index = min(index, len(TILING_THRESHOLDS))
    index = max(index, 0)
    return TILING_STRATEGY[index][1:]  
    
    
def entry_to_dict(e):
    nbeams, overlap = get_tiling_params(e['pos_err'])
    return {
        'name': e['name'],
        'ra': e['ra'],
        'dec': e['decl'],
        'tiling_nbeams': nbeams,
        'tiling_overlap': overlap,
    }


df = frbcat.TNS().df

# BUGFIX: repeater_of_objid column has two spaces between 'FRB' and the actual ID
df['repeater_of_objid'] = df['repeater_of_objid'].apply(fix_name)
df['name'] = df['name'].apply(fix_name)

repeater_names = set(df.repeater_of_objid).difference({np.nan})
print(f"repeater_names = {repeater_names}")

# Add a 'repeater' boolean column
df['repeater'] = np.asarray([s in repeater_names for s in df.name])

# Remove repeats, but keep initial detections !
keep = df.repeater_of_objid.isnull() | df['repeater']
assert set(df[df.repeater].name) == repeater_names
df = df[keep]

# Sort by name
df = df.sort_values('name')

# Parse uncertainty columns, add pos_err column (unit = arcmin)
df['ra_err'] = df['ra_err'].apply(parse_uncertainty_arcmin)
df['decl_err'] = df['decl_err'].apply(parse_uncertainty_arcmin)
df['pos_err'] = (df['ra_err']**2 + df['decl_err']**2) ** 0.5

# Filter out things above declination of +40
coord = SkyCoord(df.ra, df.decl, unit=(uu.hour, uu.deg))
visible = coord.dec < 40*uu.deg
df = df[visible]

# Remove sources with too high positional uncertainty
keep = df.repeater | (df.pos_err < 15.0)
df = df[keep]

print(f"Final number of sources to follwup: {len(df)}")

# Convert to YAML-formatted list
class MyDumper(yaml.SafeDumper):
    # HACK: insert blank lines between top-level objects
    # inspired by https://stackoverflow.com/a/44284819/3786245
    def write_line_break(self, data=None):
        super().write_line_break(data)

        if len(self.indents) == 1:
            super().write_line_break()

catalogue = [
    entry_to_dict(entry)
    for __, entry in df.iterrows()
]

text = yaml.dump(catalogue, Dumper=MyDumper, sort_keys=False)

print(5 * '\n')
print(textwrap.indent(text, 2 * ' '))