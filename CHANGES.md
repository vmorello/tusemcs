# Version history #


## 0.13.3 (2021-11-11) ##

This version features a modification of the python dependencies. Control system docker images must be rebuilt from scratch.

* Improved candidate snapshot generation procedude in python dependency `mtcutils`
* New followup sources
* New default pipeline image: `full-pipeline:0.2.0b3`. **This image fixes the candidate extraction offset for good.**

## 0.13.2 (2021-11-02) ##

* New default pipeline image: `full-pipeline:0.2.0b2`. It contains a further fix for the candidate extraction offset, to be fully tested yet.
* Updated followup source list
* Fix: `NodeController` does not erase its snapshot sensor on pipeline restart, this allows the web monitor to retain snapshot plots for much longer
* Increase "lookback time" of `NodeController` spccl candidate sensor from 2 hours to 6 hours. This means that the web monitor can now show candidates up to 6 hours old in the DM-Time plane plot.

Changes to the web monitor's DM time plane plot:
* Display pulse width in hovertext
* Display node number in hovertext
* Credible candidates (sufficiently high S/N and low pulse width) are now plotted with a different style

## 0.13.1 (2021-10-29) ##

This version follows an intense two-week period of fixing and upgrading Cheetah.

The spccl candidate time offset has been fixed, but an identical issue was found to happen with the extraction of candidate filterbanks, which can be offset from the true time of the pulse. The candidate extraction issue has been **partly** fixed, but is still present in some edge cases where multiple spccl candidates are attached to the same candidate filterbank. The candidate extraction failure rate has gone from 37% (`full-pipeline:0.1.7-clustering-fix`) to less than 2% (`0.2.0b1`). The final fix is expected within days now.

* New default pipeline image: `full-pipeline:0.2.0b1`. This image contains a fix for the spccl time offset, a partial fix for the candidate extraction offset, **and an extremely significant improvement to RFI mitigation.** The RFI mitigation was tested on 28/10/2021. It reduces the false positive rate by a factor 5 to 10 in most conditions, boosts the SNR of genuined pulse detections by 30 to 40% on average and detects events undetectable before. The new RFI mitigation does this, in this order: IQRM, mask channels with high ACC1, spectral whitening, Z-dot filter, independently rescale and clip every channel to 3-sigma. In practice this corresponds to an output mean = 127 and stddev = 42.5.
* Increase SNR threshold to 8. Maintaining a SNR threshold of 7 until now was to work around the fact that AA often underestimated the SNR or real events, due to the residual RFI biasing the estimation of the background noise standard deviation. The data it receives are now clean enough that the SNR as evaluated by AA can be trusted. It is also clear from tests that candidates between AA SNR 7 and 8 are now very often random noise detections (i.e. we are digging into the noise floor). This measure will further reduce the false positive rate by a factor 5+.
* Reduce IQRM `edge_channels` from 48 to 32. It should probably reduced to 0 eventually after some testing, as this parameter was also a stopgap measure.


## 0.13.0 (2021-10-18) ##

This new version is tied to upgrades and fixes to cheetah, namely:
* In the previous `clustering-fix` cheetah image, there was a critical issue where candidates after clustering could be offset in time by a few different possible values. This has been fixed, although the fix remains to be rigorously tested.
* In the meantime, the IQRM implementation in cheetah has been upgraded, which has required minor changes in the control system.

Changes:  
* New default pipeline image: `full-pipeline:0.1.7-clustering-fix`
* IQRM parameters have been renamed to match the published paper: `max_lag` is now called `radius`, and `nsigma` is now called `threshold`
* Adjusted the function that parses the cheetah log message with the fraction of channels masked, which has changed

## 0.12.1 (2021-10-05) ##

* Fix: Add `unique_id` key to target config dictionaries with a value generated with `uuid.uuid4()`, so that FBFUSE now detects a config change on retile. The removal of the `epoch` key in the last version had the unintended side effect of FBFUSE ignoring retiles.


## 0.12.0 (2021-10-04) ##

* Changed CA interface: the dictionaries specifying tilings now contain an additional entry `tags: ['noapsuse']`. To specify the time at which the tiling is optimal, there is now also a new `epoch_offset` key that we set to half the retiling period, rather than manually specify an `epoch` in the future.
* New default pipeline image: `full-pipeline:clustering-fix`, which fixes an incorrect distance calculation in the raw candidate clustering algorithm. See the [associated cheetah merge request](https://gitlab.com/SKA-TDT/cheetah/-/merge_requests/90).
* When running `make images`, tag the resulting new docker images with both the image tag specified on the command line **and** the `latest` tag as well.


## 0.11.16 (2021-09-10) ##

* Change clustering parameter `linking_length` default value from 1.0 to sqrt(3) = 1.732
* Update followup sources

## 0.11.15 (2021-09-02) ##

* Added new followup sources

## 0.11.14 (2021-08-23) ##

* Fix: Make sure that changes of the sensors `coherent_beam_shape` and `ca_target_request` do not trigger a pipeline restart on the IB node; it can keep going unless the pointing position changes.
* Updated followup sources

## 0.11.13 (2021-07-28) ##

* Add CA config for 4K half-array mode

## 0.11.12 (2021-07-15) ##

This version features a modification of the python dependencies. **Docker images must be rebuilt from scratch.**

* Use `mtcutils.snapshot()` function to generate snapshots in `NodeController`. The latest `mtcutils` depends on the new `iqrm` python module.
* Added `pytest-cov<=2.9` to install_requires to avoid a conflict. Indeed `astropy` needs `pytest<=3.7`, but `iqrm` will pull the latest pytest-cov by default, which itself pulls pytest 4+.
* Updated source list with latest TNS catalogue.


## 0.11.11 (2021-06-04) ##

* Fix: FBFUSE needs *strictly* more than 32 antennas in the array to form the full set of 768 beams. `nants_min` for all full-array configurations has been increased to 33 (from 32). Important note: the maximum number of beams that FBFUSE can produce depends only on the total number of antennas in the subarray, regardless of the number of antennas included in beamforming (which can be smaller).

## 0.11.10 (2021-06-03) ##

* Add dedispersion plan for upcoming S-Band receiver
* Updated source list

## 0.11.9 (2021-05-10) ##

* Keep no more than the most recent 1000 spccl candidates in store on every node, to avoid overflowing the live DM-Time plane plot.
* Updated source list


## 0.11.8 (2021-04-23) ##

* Changed beam assignment algorithm: consecutive MC groups are assigned to VLANs on a round robin basis. This is necessary to avoid heap losses otherwise caused by traffic bursts on the output ports of the CBF switches.
* Tone down spccl parser logging on NodeController
* Added new followup sources


## 0.11.7 (2021-04-15) ##

* Added preliminary DM-Time plane plot on the web monitor, to be finished.
* Updated source list


## 0.11.6 (2021-04-14) ##

Cleaner and better beam assignment plan to be tested. New features:
* Load balancing between nodes inside a VLAN. When there are less beams in total than the cluster can accommodate, the nodes will get an approximately equal number, rather than the old situation where some nodes could have 12 beams and others remained empty. The node capacity that is set via `?set-node-capacity` now represents an upper limit, the plan may assign less.
* Lower coherent beam IDs are assigned to lower node IDs. The only exception is when the beam capacity VLANs is not a multiple of the number of beams per multicast group; in this case, any unassigned multicast groups that remain after a first pass of assignment will be used as "backfill".


## 0.11.5 (2021-04-08) ##

* Request 384 beams in half array mode, 768 is too much for FBFUSE
* Add specific CA configuration for FRB followup open time proposal (use all antennas for beamforming)
* Fix: config_finder module must return only *copies* of configurations. The code in `?get-schedule-block-configuration` was modifying the configurations from the global store when intersecting their antenna lists with the currently available antennas.
* Fix: spccl-candidates sensor default value should be an empty list


## 0.11.4 (2021-04-06) ##

This is a preliminary version to be tested.

* Send Slack/Mattermost alerts using the tornado async http client
* New default `NodeController` parameters: pipeline version `0.1.7`, new UHF DDPlan up to DM 2500, `iqrm.edge_channels` increased to 48
* Add parsing of spccl files and a `spccl-candidates` sensor: the list of candidate parameters from the last hour.
* Add `spccl.json` feed on the web monitor, which will be eventually used to make a dynamic plot.


## 0.11.3 (2021-04-02) ##

* *Really* ensure followup slack alert does not block on target-start when fibre link is down. Somehow the actual timeout on the alert POST request is 4x larger in practice than the one specified; it was 3 seconds, but now 0.75 seconds. Now also calling the (blocking) slack alert function with `ioloop.call_later()`, so that the call goes through after the `?target-configuration-start` request has returned.
* Fixed web monitor issues: pipeline start times and snapshot times are now correctly parsed as UTC, instead of local time. Web monitor should now display correct pipeline uptime regardless of timezone and/or daylight savings time.
* Followup tiling overlap has now 84% overlap (Nyquist sampled) rather than 95.7% (which corresponds to half the spacing for 84% overlap).


## 0.11.2 (2021-03-19) ##

* Set a short timeout to the function that sends messaging alerts. The function would block for several minutes when the fibre link was down, which indirectly caused the CA to not update the target configuration sensor on `?target-configuration-start`. The issue is now fixed.
* Added TNS catalogue sources for followup
* Added a `NodeController` option to enable core dumps for debugging purposes. Changed the base working directory of all pipeline programs to the output UTC directory on the file system. That's where the core dumps will be saved as a result.


## 0.11.1 (2021-03-14) ##

Added automatic follow-up of priority sources in the CA. The list of sources is currently stored in `config_store.yml`, along with the SB and Target configs. An interface to dynamically edit priority sources will be implemented later. 

On reception of `?target-configuration-start`, the CA checks whether there are any sources to follow up in the half-power radius of the incoherent beam. If so, it will place a single beam or tiling on every source with high priority; the size and overlap of the tiling is specified in the config file separately for each source. When this happens, the CA will notify us with a Slack message.

Currently all sources except the perfectly localised ones will ask for a 19-beam tiling with 0.957 overlap. This has been chosen such that, assuming that the source lies at the centre of the tiling, the expected SNR in the outermost beams would be 50% of that on boresight.


## 0.11.0 (2021-03-12) ##

This version contains a brand new Configuration Authority, that will choose the SB and Target configs intelligently based on the current telescope configuration. SB and Target configs are stored with so-called constraints, that is the range of array parameters for which they should be picked. Array constraints currently can be, in order of decreasing specificity:  

- Target name (ignored when choosing SB configs)
- SB ID
- Proposal ID
- Number of CBF channels
- *Minimum* number of usable antennas (ie. not marked faulty).
- Centre frequency

Any of those constraints parameters can be left empty, which means that the attached SB/Target config is valid regardless of that constraint parameter value. When the CA is asked for a SB/Target config, it builds a list of configs compatible with the current constraints, and picks the one that is the *most specific match*. Most specific means the configuration that is most likely to have been tailored for the current telescope configuration. For example, faced with a choice between a config attached to a specific SB ID and another with a specific Proposal ID (but where any SB ID is allowed), the one with the specific SB ID will be chosen, following the array constraints order above.

The list of available SB and Target configs is currently stored in a file and cannot be dynamically edited. An interface to do so will be added later, which will require significant additional work. There are enough configs stored to cover all the usual modes: 1K, 4K, 32K at any centre frequency, 1K half-array at any centre frequency.

Other changes:  

* Full array SB configs now request 768 beams (down from 864)
* Half-array SB configs now request 768 beams (up from 480), this was Ewan's recommendation and still needs to be tested.
* Lowered the pipeline S/N threshold to 7.0 (down from 8.0)
* Increased sifter's `max_candidates` from 50,000 up to 200,000


## 0.10.3 (2021-03-07) ##

* Added Slack/Mattermost alert messages when `?capture-start` fails


## 0.10.2 (2021-02-16) ##

Made the nodes configuration smarter and less error prone.  

* Removed the static channel masks. AA still gets passed a maskfile full of `1`s (all channels open).
* The node automatically chooses the DDPlan appropriate for the band. DDPlans for each band can be edited separately in the node config file.
* The bandpass monitor is automatically disabled when the node's ingest rate exceeds some therehsold, controlled by the configuration parameter `bpmon.maxrate`


## 0.10.1 (2021-01-26) ##

Version that corresponds to a new pipeline image with more recent versions of both `cheetah` and `mkrecv`. We are now using `mkrecv_v4`. Using a new image `tuse.meertrap:7000/full-pipeline:0.1.6` with the following changes:

* Fixed tscrunch issues, which carries over from the previous default pipeline image used in `v0.10.0`.
* Merged with dev of cheetah that includes changes to the SinglePulse pipeline interface. The PipelineHandler `SinglePulseHandler` changed to `SinglePulse`.
* Using latest version `mkrecv_v4` (commit `287df4f8c44498974605917eac2410ebfcf9ed8c` from 25 Jan 2021)

Changes on the control system:  

* Get all sensor values in product_meta using a single `sensor_values()` call with a regex. This is faster and prevents a recent problem where fetching the product metadata with lots of `sensor_value()` calls returned a `HTTP 599` error.
* Add `fbfuse-nchannels` sensor to MC.
* Add `node-capacities` sensor to MC. The only allowed modes are now 1024, 2048, and 4096 channels.
* cheetah `SinglePulseHandler` has been renamed back to `SinglePulse`
* `mkrecv_v4` is now the only allowed mkrecv executable. The mrkecv header template has been updated accordingly.
* Reduced default DADA buffer length to 3 seconds (down from 6), but increased default number of buffers from 10 to 20. `mkrecv_v4` now skips the first buffer, so that the pipeline avoids processing partially filled buffers. The number of buffers to skip is specified via the new `SLOTS_SKIP` key.


## 0.10.0 (2021-01-13) ##

This is a preliminary version created as part of the migration of the control system to the `observer` account.  

* Renamed head node bandpass history monitor to `bandpass_writer`
* Fixed a hidden issue where the bandpass writer would not work if launched at a time when the node it monitors was offline. It should now be robust to disconnects/reconnects. This has caused us to lose all bandpass history for a month between Dec 15 and Jan 13.
* Pipeline image with tscrunch fix is now the new default: `tuse.meertrap:7000/pipeline-tscrunch:0.1`
* Moved all scripts to a single directory: `apps`. They used to be split between `scripts` and `other` directories.
* Added a `console_script` entry point to all applications. docker-compose setup has been updated accordingly.
* Use a lighter `nvidia/cuda` image as base for the NodeController image
* Remove `meertrapdb` from requirements, as it was unused and increased the docker image sizes significantly
* Web monitor: Enable transparent compression of responses. Browsers now pull a compressed version of the JSON data that is 5 to 10 times smaller.
* Web monitor: Close node config popup when clicking outside of it


## 0.9.1 (2020-12-14) ##

Control system updates:  

* Added `iqrm_masked_fraction` sensor to `NodeController`
* Added `product_id`, `portal_url` and `sb_details` to `MasterController`
* Value timestamp of snapshot sensor is now the mtime of the last candidate file, and is therefore useful to display on web monitor.

Web monitor upgrades:  

* Added SB ID and description to page header
* `DeviceMonitor` now deletes readings when the associated sensors disappear on host, this avoids pushing stale values to the monitor.
* Display snapshot UTCs
* Snapshots now have dynamic opacity as a function of age
* Added favicon: the "M" of the meertrap logo in 32x32 format.
* Refactored the nodes table webpage: content is created once on load and then updated, instead of being re-created on data change.
* Added IQRM masked percentage to nodes table.
* Highlight table rows on hover
* Clicking on a node table rows brings up a pop up with its pipeline configuration in `YAML` with syntax highlighting
* Skymap page has returned in improved form. Nearby known pulsars are now displayed, mousing over them shows their DM and flux at 1.4 GHz (if known in PSRCAT).


## 0.9.0 (2020-12-07) ##

This version features a full rewrite of the web monitor, which was much needed. Additional sensors will be added to both `MasterController` and `NodeController`.

* Web monitor is now a proper navigable website with a menu bar, rather than a collection of pages. It also looks substantially better, and that's an understatement.
* Web monitor backend rewritten from scratch. It is mainly based on a `DeviceMonitor` class that extends the `KATCPResourceClient`; it can apply arbitrary parsers to the incoming sensor value updates; in particular, JSON-encoded string sensors are now correctly converted to objects and then fed as such to the web pages JavaScript. It is now very easy to add a monitoring page for additional `KATCP` servers.
* Removed almost all static HTML code. All dynamic content (e.g. tables) is periodically generated using jQuery only.
* Styling is now properly done via CSS classes only, and as such is easier to tweak than before.
* Page header now display the current UTC time, MasterController state and current pointing target. More information will be added.
* Added pages that display as a neat table the sensors of: FBFUSE, TUSE CA, TUSE MasterController. String sensors that represent JSON objects are displayed using `jsyaml` for readability. String sensors that represent a base64 encoded image are rendered as such.
* Revamped, more readable nodes table page. Here are the new features:
    - Use sans serif instead of monospace font for readability
    - A number of table cells now have a dynamically coloured background that serves as a warning level. There are three warning levels (attention, warning, critical) shown as shades of yellow/orange. The following sensors are concerned: disk space, missing heaps, AA hits, candidates, candidate lag. It is now obvious when a sensor value is abnormal. The warning thresholds for each sensor are currently hardcoded to something reasonable in `update-nodes-table.js`.
    - CPU load is now represented as a horizontal load bar. Prematurely exited pipelines are now easy to spot.
    - Added icon that denotes whether the bandpass monitor is disabled.
* On the bandpass page, the user can now choose the node for which to display the data. Both the standard deviation and the mean of the bandpass are now shown.
* Snapshots webpage has been re-styled


## 0.8.5 (2020-12-04) ##

* CA interface change: the `?get_schedule_block_configuration` request now expects the following list of arguments: product ID, schedule block ID, science project ID, number of channels, KAT portal URL associated with current subarray.

## 0.8.4 (2020-11-20) ##

* The cheetah implementation of IQRM was flagging both low and high outliers in the bandpass standard dev, instead of only high outliers. This was causing some overflagging and prevented any increase of `max_lag` parameter (which would cause even more overflagging). This has been fixed in image `full-pipeline:0.1.1prod`, which is the new default image.
* `iqrm.max_lag` is now 15 by default (was 5). With both these changes, we should get LESS channels flagged but still an equivalent or even higher data quality after flagging.
* Added `--ibeam` option to bandpass history plotting utility, which used to always plot the data for relative beam index 0.

## 0.8.3 (2020-11-20) ##

* Changed: `candidate-lag` sensor now denotes the lag for the most recent candidate
* Fixed a node monitoring issue where the candidate file names with a `_N.fil` suffix were not being parsed properly. When such a file appeared, the candidate number and candidate lag sensors stopped being updated until the next pipeline start.
* Fixed a bug where the `sb_details` JSON-string was not being parsed into a dictionary when the `NodeController` receives them at `capture-start`. It would then be written as a string (instead of a dict) in `run_summary.json`


## 0.8.2 (2020-11-09) ##

* Added: in the web monitor, hovering on a node's number in the table displays a tooltip with all the pipeline configuration parameters of that node.
* Added: UHF 32K mode presets to the CA
* `NodeController` default config update: `nchans_max` is now 1024, default pipeline image is `full-pipeline:0.1.0prod` following latests cheetah updates that made IQRM + thresholding real-time again.


## 0.8.1 (2020-11-03) ##

* Give RFI mitigation its own resource pool in cheetah. Previously it was sharing the same CPUs as the SPS pool, causing it to be slower than necessary, and the pipeline was thus sub real-time (NOTE: more importantly, there was also a problem with the implementation of thresholding in cheetah, which has been made much faster). SPS pool now has 8 physical CPU cores (down from 10), and RFIM gets the rest (another 8).
* Added a `candidate-lag` sensor to `NodeController`: the maximum time difference between the file modification timestamp of candidates and their data timestamp, only considering candidate files produced in the current pipeline run. That will tell us whether the pipeline is falling behind or not.


## 0.8.0 (2020-10-24) ##

This version features a major change in the control logic: we stop doing any TUSE initialisation before `capture-start`, as the only reliable way to get correct sensor readings from FBFUSE is to wait for it to reach `ready` state; this is expected to happen exactly when we receive `capture-start`. As a consequence, all the initialisation is now done at `capture-start` and not before. `MasterController`, `NodeController` and `NodePool` have been significantly refactored, the codebase is considerably cleaner, and a lot of minor things have been improved along the way.

* `capture-init` is now treated as a dummy request by the `MasterController`. We do nothing at this stage.
* `NodeController` has now only two CAM-related requests: `capture-start` and `capture-stop`. `configure` and `capture-init` have been removed.
* `NodeController` has no awareness of `product_id` anymore, since it never really needed it.
* `NodeController` now avoids re-creating a `RedisBeamMonitor` instance at every new SB. This was wasteful and generated lots of stale connections to the `redis` server.
* Retrieval of product metadata is now done with by a new function `retrieve_product_meta()`.  It only uses calls to `katportalclient.sensor_value()`, as opposed to the old method of subscribing to product meta sensors and waiting for readings. The `MasterPortalClient` class has been deleted.
* Removed re-scheduling of `capture-start` introduced in `0.7.11`, as the associated issue is now solved by the new control logic.
* Removed non-live-editable params from `node_controller.yml`
* Removed obsolete `--override` option from command line arguments in `node_controller.py`
* Command line arguments of `node_controller.py` now match that of `master_controller.py`
* Added optional logging to rotating file to `node_controller.py`, like in `master_controller.py`
* Changed default value of `iqrm.edge_channels` to 32
* In `NodeController`, clear bandpass sensors on `capture-stop`. Not doing so caused the web monitor to display bandpass data from a previous SB reshaped to sometimes incorrect dimensions, if the number of beams per node or number of channels had changed.
* Updated CA config presets: in all configurations except TRAPUM, scrunch to 1024 channels, request 864 beams and tile 768.
* Deleted obsolete code: `sensor_reading.py` and some functions in `general_helpers.py`


## 0.7.12 (2020-10-19) ##

* Merged a minor change to match the latest cheetah version, where the syntax of the `<astroaccelerate>` section is slightly different.
* From this point onwards, the pipeline image names will NOT include the prefix `tuse.meertrap:7000/` anymore.


## 0.7.11 (2020-10-14) ##

* Fixed: NodeController bandpass sensor gets updated only if bandpass monitoring is enabled, otherwise we can end up reading a stale buffer, written in a previous run by the C++ bandpass monitor.
* Fixed: reshape the bandpass buffer data with the correct number of channels. We were always assuming nchan = cbf_nchan, while it should be nchan = cbf_nchan / fbfuse_fscrunch. The web monitor was then showing a visibly incorrect graph with the wrong number of channels.
* Added: requests received by the MasterController can now be re-scheduled to be executed later if the TUSE state is not (yet) correct. Currently (and probably for the foreseeable future) we only re-schedule capture-start requests 60 seconds later. This avoids an annoying issue where FBFUSE takes too long to provision, then CAM raises a timeout and sends us capture-start before we could finish processing capture-init and reach the 'INITIALIZED' state. If this happens, we get a second chance 60 seconds later.


## 0.7.10 (2020-08-03) ##

* Candidate snapshot monitoring page updates the PNGs only when strictly necessary, which makes the page much more reactive. Client browsers also use much less precious bandwidth to download said PNGs.


## 0.7.9 (2020-06-29) ##

* Merged IQRM branch after extensive testing. There is now a `pipeline.cheetah.iqrm` section in `node_controller.yml`.
* Fixed an issue with `RedisBeamMonitor`: the listening to redis updates is performed in a separate thread, that was directly calling `NodeController` methods running in the main thread. This introduced a rare race condition where `start_pipeline` would be called before `stop_pipeline` had returned (or vice-versa), which should **never** happen. Now, when a beam sensor update is received, the listening thread calls the corresponding `NodeController` method via the thread-safe `ioloop.add_callback()` function.
* Fixed minor issue where parsing of AA hits would raise a benign exception, because the log message `Total number of candidates:` can sometimes be mixed with logs from other simultameously running pipeline programs.


## 0.7.8 (2020-06-25) ##

* Added `reqsender.py` app, to send requests in bulk to multiple katcp servers. This is quite useful to run in a cron job, to change CA and MC configurations in the middle of the night between two scheduled observing runs.
* Control containers on nodes are now being run without the `--rm` option so that the logs persist after a crash
* Added a candidate snapshot live monitoring page to the web monitor


## 0.7.7 (2020-05-19) ##

* Change: to avoid misconfiguring FBFUSE when loading a CA schedule block configuration for the wrong band (e.g. using L-band params while observing at UHF frequencies), the CA now omits the keys `centre-frequency` and `bandwidth` from the dictionary it returns to FBFUSE. FBFUSE will then set these parameters to the current parameters of the full observing band it reads from the CAM portal. **In practice this means that the centre-frequency and bandwidth specified by the TUSE CA user are systematically ignored by FBFUSE**. If there is a future need to use only a fraction of the band, only two lines need to be changed.


## 0.7.6 (2020-05-10) ##


* Fix: when setting sampling strategies on beam positions / metadata sensors in `RedisBeamUpdater`, correctly check that enough sensor names were matched by the `set_sampling_strategies()` function. It does not work on sensors that don't exist yet, and sometimes it is called before the FBFUSE beam position sensors exist. In this case the dictionary returned by the function does not contain the beam sensors names as keys, and checking that all subscriptions were successful is not sufficient. The code now checks the number of sensor names matched, and properly re-attempts `set_sampling_strategies()` if the number is lower than expected.


## 0.7.5 (2020-05-04) ##

This version fixes issues revealed in the 30/04/2020 test session where we tried FBFUSE fscrunching for the first time (from 4096 to 2048 channels).

* The `DEFAULT_NODE_CAPACITIES` dictionary of `MasterController` is now populated with default values for all realistic possible number of channels. Indeed if FBFUSE is frequency scrunching the data to some non-standard number of channels (always power of two though), `MasterController` needs to know how many beams per node to assign.
* Fix: the `nchannels` FBFUSE sensor provides the number of channels **before** FBFUSE applies frequency scrunching. It was incorrectly assumed in `MkrecvConfig` and `DADATransposeConfig` that `nchannels` denoted the number after FBFUSE fscrunching.


## 0.7.4 (2020-04-27) ##

This version fixes a beam monitor issue observed during the Meertime UHF run of 27/04/2020.

* Now ensuring that all exceptions are caught in the listener thread of `RedisBeamMonitor`. Otherwise, uncaught exceptions cause the thread to die and the node does not receive any more beam sensor updates. An exception was raised at the start of the run where the initial value of `coherent-beam-shape` was an empty string, which could not be decoded by `json.loads()`.
* Stopped printing `product_meta` to the logs, to avoid clutter


## 0.7.3 (2020-04-24) ##

* Implemented tracking of `ca_target_request` sensor, and its propagation to `run_summary.json`
* Tweaked down logging of `NodeController` by a lot. In particular, we're not logging the contents of all config files anymore, that's just overkill and it made the logs unreadable. These files are available on disk to be checked.
* When the schedule block details are not available, ensure that the `_sb_details` attribute of `NodeController` is an empty dictionary rather than `None`. This will avoid breaking the format specification of `run_summary.json`, although this case should probably never present itself.


## 0.7.2 (2020-04-24) ##

* Separate docker images for head and compute nodes, so that running containers use less space on the head node. The head image is based on `ubuntu:18.04` while the node image is based on `nvidia/cuda` (so that we can run `nvidia-smi` for monitoring purposes).
* Refactored `Makefile`, can now specify the desired image tag when building, for example: `IMGTAG=0.7.2 make images`


## 0.7.1 (2020-04-23) ##

* Added a `last-candidate-snapshot` sensor to the `NodeController`. The UTC directory for the current pipeline UTC is periodically checked for recent candidates, and the most recent is plotted into a PNG file with `mtcutils`, which then gets encoded as a base64 string and published as a sensor. Only the most recent candidate across all beams is plotted, rather than the most recent for each beam because reading candidate files can be slow (especially for high DM candidates which have a longer time span). Currently the sensor is updated every 15 seconds, only candidates more recent than 120 seconds are eligible for plotting, and the plots are 256 x 128 greyscale PNGs. To be refined and tweaked.


## 0.7.0 (2020-04-21) ##

* Node controllers now get their beam metadata and position sensor updates via a `redis` server running on the head node. At capture-init, the `MasterController` starts a single instance of `KATPortalClient` that subscribes to all beam position and metadata sensors, and then publishes all the updates it receives to the `redis` server. This means instantaneous `?capture-init` on all nodes without any risk of timeouts, unlike the previous design that connected 66 clients to FBFUSE.
* Now tracking the `coherent-beam-shape` FBFUSE sensor, whose value is propagated to the `run_summary.json` file at the start of a pipeline run. Receiving a `coherent-beam-shape` update triggers a pipeline stop followed by a delayed start.
* In the `docker-compose` configuration, all head node container names are now prefixed with `tcs_`


## 0.6.1 (2020-04-20) ##

* Updated `run_summary.json` format to final specification


## 0.6.0 (2020-04-17) ##

This is a preliminary version. Still need to finalize the `run_summary.json` file format.  

* Massive refactoring of the pipeline interface, which adds support for: new transpose module with scrunching, 
* Pipeline configuration now generated via a number of separate classes: `MkrecvConfig`, `DADATransposeConfig`, `CheetahConfig`
* Added a nice jinja2 template for the pipeline script, which is now generated by a *single* function rather than one function per pipeline mode.
* Mount paths inside control and pipeline containers have been removed from `node_controller.yaml` and are now hardcoded as constants in a **single** place (currently `pipeline_utils.py`), which is much safer. That way, `node_manager.py` knows which paths to mount inside the control containers when launching them on all nodes. In the same file are also specified: the names of every config / output file are specified as constants and the UTC format. We don't want to change any of this without prior discussion, so these parameters have no place in the `node_controller.yaml` configuration file.
* Added option to disable C++ bandpass monitor, as it is too slow on 4K mode data with 76 us sampling
* Phased out the docker shared volume that was used to store generated config files and scripts for the pipeline. These are now saved in the `config` sub-directory of every output UTC
* `pipeline_events.log` has been replaced with a single JSON file `run_summary.json` whose format is being discussed and standardized with Fabian, Mat and the rest of the team.
* `mkrecv` executable name and network protocol (UDP vs infiniband) can now be live-changed
* Length of DADA buffers is now specified as a number of seconds. Length in seconds is identical for both input and output (post-transpose) buffers. Corresponding bytesizes are automatically calculated as a function of data stream parameters.
* Length of the aggregate single pulse search buffer is now also specified in seconds. `dedispersion_samples` (the relevant parameter for cheetah) is then automatically calculated.
* Added option to remove DADA buffers with `ipcrm -a` to avoid issues when switching between pipeline images which contain DADA versions incompatible with each other
* Added time and frequency scrunching. This is controlled by two parameters: `pipeline.scrunching.tsamp_min` and `pipeline.scrunching.nchans_max`. The code then calculates the appropriate time and freq scrunching factors as a function of data stream parameters.


## 0.5.16 (2020-04-16) ##
* Added "IB Only" mode to `MasterController`, along with a `?set-ib-only` request to enable/disable it. When this mode is active, only the IB is assigned to the nodes at capture init.
* Added saving `product_meta` to `pipeline_events.log` file (although said file will be phased out soon)
* Less verbose logging on `MasterController`


## 0.5.15 (2020-03-18) ##
* Fixed a critical issue where `MasterPortalClient` took more than 60 seconds to retrieve product metadata. This was because the `phase-reference` sensor was erroneously part of the required sensor list; its reading becomes nominal only after FBFUSE starts capturing.
* Fixed an oversight in the bandpass history plotter: the timestamps of bandpass sample files are UTC, not local. The title of the bandpass plot was erroneously displaying the local time as UTC.
* Individual DADA buffers are now 6 seconds long by default, with 12 buffers in the ring
* Default DM threshold now 20.0
* Default S/N threshold now 8.0


## 0.5.14 (2020-02-27) ##
* Added `docker-compose` config to start head node containers
* Added `node_manager.py` script to start/stop/restart NodeControllers from the head node. One specifies a range of nodes and a docker image to use, and the script runs the right rocks command.
* New `MasterPortalClient` class that handles retrieving product metadata from CAM portal in cleaner fashion. Outputs detailed error messages if some required sensor values cannot be retrieved.


## 0.5.13 (2020-02-22) ##
* Added logging to file for both `MasterController` and `ConfigurationAuthority`. The file names are automatically rotated every day, so that we get one log file per UTC day, e.g. `ca.log.2020-02-22`. Logs are written to `/storage/logs`.
* `MasterController` now sets the node capacity and chooses the channel mask based on the number of channels *after* scrunching, as it should be. Note that we are *NOT* 32K-ready yet even after this fix, as our mkrecv configuration calculator does not handle `fscrunch` correctly. In a future update, we will instead use the mkrecv parameters provided by FBFUSE.
* CA: Overlap factor 0.25 is the new default in the CA
* CA: Now prints the actual epoch of the tiling on a re-tile, rather than the time at which the re-tile was requested.


## 0.5.12 (2020-02-18) ##

* Enforce a hard 160GB limit on pipeline container RAM usage to avoid any crashes due to pipeline leaking memory
* DADA buffer sizes are now specified via a desired length in seconds worth of data, rather than a number of heapgroups. This avoids situations where buffers were about one second long for L-Band 4K mode with 76us sampling time. Default DADA buffer size is now 16 seconds of data regardless of band / mode, both for the large input buffer and the individual beam buffers.
* New pipeline mode `discard`, where we only run mkrecv and the bandpass monitor to consume the incoming data. This should be useful in capture data rate tests.
* Removed plotting command from production pipeline script, it now runs separately as a daemon
* Added preset CA configurations for UHF 1K mode
* New preset target configs with both 0.50 and 0.25 overlap. Presets target configs now have names like L4K50, U1K25, i.e. {BAND}{MODE}{OVERLAP}.
* `?update-node-config-param` can now return `fail` and sends informs about why it failed on some nodes. This should avoid any confusion to a katcp client user, and removes the need to look at `MasterController` logs to see if an update was successful.


## 0.5.11 (2020-01-31) ##

* `MasterController` now has one default node capacity for each mode. Each can be dynamically set independently. Added sensors `node-capacity-N` where `N` is the number of channels for a given mode.
* `ConfigurationAuthority` now has one active SB config and target config configuration per mode.
* Added sensors to `ConfigurationAuthority` for each active product_id: `boresight` and `active-mode`
* Changed default number of beams in tiling to 768 for 1K mode and 384 for 4K mode, as it should be.


## 0.5.10 (2020-01-21) ##

* Node capacity can now be set dynamically on the `MasterController` via the request `?set-node-capacity`
* `ConfigurationAuthority` now logs the requested tiling epoch on every re-tile
* New default `NodeController` configuration with safer parameters: S/N threshold 10, DM threshold 50, updated channel mask for both 1K and 4K modes
* Added `pipeline-configuration` sensor, which publishes the currently active `pipeline` section of the node's configuration as a JSON-encoded dict

## 0.5.9 (2020-01-10) ##

* Fix: Do not send pipeline in error state if container does not exist when stopping
* Bandpass monitor now dynamically reads the number of beams in bandpass, instead of a hardcoded 6
* Added: Bandpass history plotting app

## 0.5.8 (2019-12-13) ##

* Added missing dependency `schema` (required by the CA) to `requirements.in`
* Added distinct `MasterController` configuration files with a node capacities of 6, 9, and 12
* Waiting for up to 60 seconds for FBFUSE to be ready at capture-init to avoid any regrettable TimeoutError
* New 1K mask and a translation of it for 4K mode
* Properly log all requests received by `MasterController`, we should now be able to distinguish requests sent by CAM from those sent by us via the katcp client
* CA: automatically exclude antennas marked faulty from SB config
* CA: extended core antenna radius to 650 metres (43 antennas total)
* CA: enable 10-minute periodic retiling by default, ask FBFUSE for tilings optimised 5 minutes after request time

## 0.5.7 (2019-11-21) ##

* The new Configuration Authority is ready and has been merged into `develop`. It can specify flexible target configurations with multiple tilings, and handles the periodic retiling.
* Using "persistent" sensor listening method of katcp in the `FBFUSEBeamMonitor`, that should persist through disconnections.
* Nodes now track the new `data-suspect` sensors of FBFUSE and start / stop the pipeline reactively. In particular, the pipeline will only run when all antennas are on source.
* `spsift.max_candidates` is now 50,000 by default, 500K was not manageable
* Even stricter 1K channel mask
* Improved docker build even more. Pinned requirements are now compiled during the build, which removes the need to manually generate and commit `requirements.txt` every time `requirements.in` is modified. Doing a manual installation with `make install` installs `requirements.in` directly.
* Deleted `requirements.txt` as a result. Required modules are specified only in `requirements.in` now.


## 0.5.6 (2019-11-06) ##

* New default mask for 1K mode based on recent bandpass data and the analysis of a lot of candidate files
* `spsift.max_candidates` is now 500,000 by default, to avoid overloading the clustering stage and falling behind with the search
* Updated Dockerfile so that the python dependencies now get cached, which results in a much faster build time. Logical python module requirements are now specified in `requirements.in`, which is compiled into `requirements.txt`: the file that contains requirements *pinned to a specific version*.  Every time `requirements.in` is modified, one must run `pip-compile requirements.in > requirements.txt` and commit the result. The `pip-compile` command is part of the `pip-tools` module.


## 0.5.5 (2019-11-04) ##

* Added dynamic versioning using `versioneer` module, i.e. `__version__` is based on `git describe`
* The `BUILD_INFO` member of `MasterController` and `NodeController` is now dynamically parsed from `__version__` and should display correctly in CAM portal.
* Lighter requirements for production code. Full requirements with extras are specified in `requirements_full.txt`
* `dedispersion_samples` default value cut in half from 230,000 to 115,000. That will reduce the default length of the search block from 70.4 to 35.2 seconds with the usual FBFUSE configuration (306.24 microseconds sampling time) .


## 0.5.4 (2019-11-01) ##

* Added live plots to web monitor: node health sensors, beam placement map and bandpass on first beam of node 0
* Do not reset missing heap, aa hits and bandpass sensors on pipeline stop to make monitoring easier, i.e. avoid erasing the last known valid sensor value.
* New adjustable parameters: `spsift.active` (default: True) and `spsift.max_candidates` (default: 0 = unlimited).


## 0.5.3 (2019-10-30) ##

* New GPU monitoring sensors on `NodeController`: load, memory used, power draw, temperature. As a result, the new docker image has to be based based on `nvidia/cuda:8.0-devel-ubuntu16.04` so that we can run the `nvidia-smi` utility.
* Missing heaps and hits sensors now updated every 5 seconds (by reading the last 10 seconds of pipeline logs). Both sensors are based only on the last captured DADA buffer and last search block, respectively. If no information can be read from the pipeline logs the sensor values are unchanged, rather than set to zero.
* GPU sensors now appear on the web monitor
* `NodeController` has a new `beam-positions` sensor, which is a JSON-encoded dictionary with FBFUSE beam IDs as keys and katpoint target strings as values

## 0.5.2 (2019-10-11) ##

* `NodeController` automatically diables the pipeline when disk space gets below some threshold. The threshold is specified by the new configuration parameter `control.min_space`
* Monitor now displays pipeline uptime
* Monitor now displays disabled pipelines as a lock icon
* Lightened up cheetah XML config generator
* Exposed as configurable parameters the number of CPUs and GPUs allocated to single pulse search in cheetah: `pipeline.cheetah.sps.pool.cpus`, `pipeline.cheetah.sps.pool.gpus`.
* New helper class `DDPlan` to parse, check and manipulate dedispersion plans.
* Exposed the dedispersion plan as a configurable parameter `pipeline.cheetah.sps.ddplan`, which is a string representing a comma-separated sequence of dedispersion plan stages `<dm_start>:<dm_end>:<dm_step>`.

## 0.5.1 (2019-10-04) ##

* `NodeController` configuration now has one separate channel mask per mode. Channel masks for each mode are now specified as configuration parameters such as `pipeline.chanmask.1024C` and `pipeline.chanmask.4096C`. The appropriate mask is picked dynamically on pipeline start, and the masks can be updated independently.
* Polished and committed the TUSE Web Monitor `suricat`
* Bugfix: pass `heap_size` argument to mkrecv template in all pipeline modes. This was an uncaught bug introduced during the bandpass monitoring update, which caused the pipeline to crash in filterbank mode.
* Added a manual pipeline locking mechanism to `NodeController`. It now has a `_pipeline_enabled` member, a `pipeline-enabled` sensor, and a `?set-pipeline-enabled` request. The use case is for recording very long observations of R1. We can start recording filterbanks on one node, and have others ready to start recording immediately as soon as the first node fills up. This should come in handy in other situations.
* `MasterController` has a corresponding `?set-pipeline-enabled` request to target a specific range of nodes
* The node range argument of `?update-node-config-param` and `?set-pipeline-enabled` on the MasterController has a more powerful syntax. Any comma-separated list of python slices can be specified, e.g. `0,1,5,42,11:16,::3`.
* `dedispersion_samples` in cheetah config now a free parameter in `node_controller.yml`

## 0.5.0 (2019-09-30) ##

* Bandpass monitoring has been merged into develop branch, details below
* Run `beam_bandpass_shm` in pipeline script, for all pipeline modes. It writes bandpass stats to a shared memory buffer
* Run pipeline container with `ipc=host` in all modes, so that we can read the bandpass shared memory buffer from the control container
* In pipeline scripts, now deleting DADA buffers cleanly with `dada_db -d` rather than `ipcrm -a`. This is a safety measure now that we run the pipeline container with `ipc=host`.
* Added `bandpass-mean` and `bandpass-std` sensors, which represent numpy arrays encoded as strings. They get updated every 10 seconds.
* Added `python-tk` to the list of required packages in the dockerfile, this is necessary just to be able to import `matplotlib`
* Added simple bandpass monitor client (`other/bpmon.py`), that listens to bandpass sensors of a given node and saves sensor updates to `.npy` files

* Candidate plotting script now gets passed `candidate_window_ms` as an argument
* Added automatic FBFUSE retiling client (`other/retiler.py`). This is a temporary solution until the official method (refreshing the tiling strategy sensor of the CA) gets implemented.
* Bugfix: in `FBFUSEBeamMonitor`, must catch a `KATCPSensorError` when setting a sampling strategy times out
* Bugfix: at the end of capture-init, `NodePool` is now printing the right correspondence between node names and node replies to the logs. There was a mixup due to the replies list not always being in the same order as the node list.


## 0.4.16 (2019-09-12) ##

* Added `spscl` single pulse clustering section to Cheetah XML template.
* Reorganisation of keyword hierarchy in `node_controller.yml` config file in preparation for an extension of the set of changeable cheetah parameters. All cheetah params are now found in `pipeline.cheetah` section, and they are organised into sensible subsections, e.g. `pipeline.cheetah.spsift.dm_thresh`.
* `spscl` cheetah parameters are now part of `node_controller.yml` and therefore live-adjustable

## 0.4.15 (2019-09-11) ##

* Reflecting the fact that CBF sample rate sensor will be called `wide-adc-sample-rate` from 12/09/2019 (changed from `i0-adc-sample-rate`)
* The corresponding `product_meta` dictionary key is now called simply `sample_rate` to avoid any future confusion (changed from `i0_adc_sample_rate`)
* Reflected changes on how Mat's plotting script is called (now with more arguments)
* Now installing latest releases of `katcp` and `katportalclient` from PyPI. This follows a backwards-incompatible update to CAM requiring an upgrade of both libraries to their latest versions.
* Using new channel mask derived from 10/09/2019 bandpass data by default
* Reduced candidate window half-size to 500 ms
* Went back to more conservative SNR threshold of 10 by default
* Default pipeline version now `0.3.0a`

## 0.4.14 (2019-09-04) ##

* Fixed a minor issue where the value of `fbf_tscrunch` written to the pipeline events logs was always the default value of 16 specified in `mkrecv_defaults.yml`, rather than the correct value read from the FBFUSE sensors.
* Default S/N threshold for both SPS and sifter is now 8.0
* Now using the `karoocamv23` releases of both `katcp` and `katportalclient`
* Reduced stagger delay to 0.3 seconds, because it seems that FBFUSEBeamMonitors never time out in the first 20 seconds. Raise timeout of `FBFUSEBeamMonitor.until_synced()` to 15 seconds.

## 0.4.13 (2019-08-01) ##

* Added channel mask for 1k mode in `node_controller.yml`. Right now the 4K channel mask is commented out.
* Test function that replicates the beam assignment on the full TUSE cluster

## 0.4.12 (2019-07-11) ##

* Run pipeline container with `tty=True` so that stdout flushes every time a pipeline program outputs a newline char. Otherwise, we only get to see mkrecv STAT logs every couple of minutes or so, and we might miss the final error logs on a pipeline crash.
* With the stdout buffering delay issue fixed, we now average both mkrecv stats and AA hits over the last 60 seconds
* Now running the pipeline container with `auto_remove=False` and `remove=False`. Save the logs of the pipeline container to a file when `stop_pipeline()` is called (works even if it already crashed), and manually remove the container after that.

## 0.4.11 (2019-07-04) ##

* Cluster monitor now displays the uptime of the pipeline container in seconds.
* Log node replies to requests in human readable form
* Added a custom key `SYNC_TIME_MJD` to the mkrecv header, to save Kaustubh the pain of converting `SYNC_TIME` (the CBF sync epoch as a UNIX timestamp) to MJD in C++

## 0.4.10 (2019-06-27) ##

* Added dynamic channel mask generation, which is then passed to AA directly. The channel mask is specified via the config parameter `pipeline.chanmask`
* Changed `?update-node-config-param` on the MasterController so it can now target a specific node, or range of nodes.

## 0.4.9 (2019-06-27) ##

* FBFUSEBeamMonitor may retry sensor subscription if it times out (1 retry at the moment)
* Now averaging mkrecv stats over last 5 minutes, and AA hits over last 30 seconds. This is because the STAT logs lines of mkrecv are flushed to stdout in groups every 4 minutes or so, while no such issue occurs with AA.
* Reset pipeline stats sensors on pipeline stop
* More significant digits on %HMiss column in cluster monitor

## 0.4.8 (2019-06-26) ##

* Candidate window half size for the candidate extractor is now part of `node_controller.yml` and can therefore be changed live
* New sensors: `aa-hits-recent` and `mkrecv-missing-heap-fraction-recent`, where recent currently refers to the last 60 seconds. These values are inferred from the logs of the pipeline container


## 0.4.7 (2019-06-25) ##

* Added retrieval of active SB details at capture-start by the `MasterController`, which then get passed to the nodes and then written into of the pipeline event logs of every node on a pipeline start. This is done on the MasterController since the CAM portal will probably not deal with a schedule block info request from 66 nodes at the same time.

## 0.4.6 (2019-06-25) ##

* Fixed dry-run mode bug introduced when we started encasing start and stop pipeline functions in try blocks, where the pipeline state in dry-run mode was not switching to `running` anymore.
* Stop the pipeline and gracefully stop the FBFUSE beam monitor when `NodeController.stop()` is called
* Moved old code to attic: a master controller clone used in first CAM integration test in Dec. 2018 (`master_controller_dummy.py`), and pipeline simulator from 2018 with its helper functions.
* `start_pipeline()` now returns the utc_start it picked, as a datetime object. Similar change for `stop_pipeline()`.
* Added new sensors `disk-space-available`, `last-pipeline-utc-start`, `total-candidates-produced` (in the active pipeline run only). `last-pipeline-utc-start` is the utc_start picked by the `start_pipeline()` function.


## 0.4.5 (2019-06-24) ##

* Default pipeline parameters in node configuration file are now `sps_threshold = 10`, `spsift_dm_thresh = 10`, `spsift_sigma_thresh = 10`
* Added a `pipeline_params` to the JSON dictionary written by the pipeline event logger when the pipeline starts. `pipeline_params` is a copy of the dictionary `node_config['pipeline']` at the time the pipeline was started. The structure and list of keys of that dictionary mirrors the `pipeline` section of the `NodeController` configuration file `node_controller.yml`.
* Reinstated CPU and RAM usage sensors on `NodeController`, which refresh every 2 seconds
* Cluster monitor now displays percentage of all CPU pool used, and percentage of total RAM used

## 0.4.4 (2019-06-23) ##

* Critical last minute fix: run Mat's candidate plotting script in the background in production mode

## 0.4.3 (2019-06-23) ##

* The functions that start and stop the pipeline in `NodeController` are now encased in try blocks. If the pipeline docker container cannot be started or stopped properly, the pipeline state will flip to `ERROR`. However, a premature exit of the pipeline container after a successful launch will still *not* be detected, need to implement a watchdog for that.
* Added a `beam-ids` sensor to `NodeController` for our monitoring purposes. The cluster monitor now displays its value as well.


## 0.4.2 (2019-06-23) ##

* Fixed a subtle oversight in `NodeController`: now properly clearing the beam positions dictionary at `?capture-done`. There was a situation where if a node was assigned different beams on a new `?capture-init`, the previous set of beams would remain stored in the dictionary with their last known positions, and the node would still require these to all be on sky before starting the pipeline.
* Added an updated real-time cluster monitor

## 0.4.1 (2019-06-21) ##

State of the code following the 20 June 2019 testing session, where we switched to subscribing to beam position sensors directly on FBFUSE, because the CAM portal could not handle all the TUSE nodes setting sensor sampling strategies at the same time.

* Now directly sampling the beam position sensors directly on FBFUSEMasterController, using the new `FBFUSEBeamMonitor` class
* Moved creation (resp. deletion) of the `FBFUSEBeamMonitor` instance of each `NodeController` to `?capture-init` (resp. `?capture-done`), rather than capture start and stop. This makes a lot more sense, because we can already set sensor sampling strategies at `?capture-init`, and the operation is time consuming.
* The `?capture-init` request is now staggered across the cluster to prevent nodes from attempting to set beam sensor sampling strategies all at the same time. Nodes are scheduled so that every `dt` seconds, a new node performs its `?capture-init` actions. Currrently `dt` is 0.4 seconds, which means that with all 66 nodes up, `?capture-init` should take about 30 seconds and not overload FBFUSE with sampling strategy requests.
* Fixed the start/stop pipeline logic in `NodeController`. In particular, fixed an issue where if FBFUSE set beams on sky before a node would receive `?capture-start`, the node would not start the pipeline. This was because the only pipeline start trigger was located in the `update_beam_position()` method. We now forcibly queue a pipeline start command on `?capture-start`, which gets executed only if allowed.
* The pipeline may now be stopped even if it is in `starting` state, as it should be.
* Retrieve `fbfmc_address` from CAM portal on the head node and pass it to nodes in `product_meta`. Nodes need this address for beam sensor subscriptions.
* Production mode now creates a new directory for data products on each start of the pipeline. The directory name is simply the UTC at which the pipeline is started.
* Added `mkrecv_params` to the JSON dictionary written by the pipeline event logger on a pipeline start


## 0.4 (2019-06-20) ##

State of the code at the end of the 19 June 2019 testing session: successful run of the production mode on 8 nodes at once for 30 minutes.

* Now running all pipeline executables in a single `pipeline` container, instead of two containers `receive` and `cheetah`
* Working production mode
* Implemented "transpose" mode: like production mode, without running cheetah, with DADA buffers visible from the host. That way, someone can manually attach a cheetah instance to the output buffers for test purposes.
* Brand new beam assignment plan that forbids assigning the same multicast group to two different VLANs. Traffic balancing between VLANs.
* Fixed a bug in mkrecv config where the wrong list of antennas sensor was used to generate some parameters. The sensor `available-antennas` of FBFUSE is the right one to use. `IDX1_STEP` and channels per band could previously end up wrong by a factor of 2.
* Run `mkrecv` in with `--quiet` flag to avoid outrageous volumes of logs when heaps are dropped/missing
* Run pipeline container with the right permissions and flags: `runtime=nvidia`, no core dumps.
* Added a custom request `?update-config-param` to `NodeController` allowing to live change (without needing a restart) any parameter present in `node_controller.yml` config file. The MasterController now also has `?update-node-config-param` to change a parameter on all synced nodes at once.
* Made a number of cheetah parameters adjustable in `node_controller.yml`, which means they can now be changed live.
* Added pipeline events logger, that save a JSON entry to a separate file on every pipeline start and stop. This is to keep track of useful metadata during the DWF run (beam ids, positions, start and stop times, etc.), since the pipeline does not yet propagate coordinates and timestamp to the output candidate files.
* Added a `kill_switch.sh` script to forcibly stop all pipeline and control containers from the head node
* Remove MariaDB from the control system image. We will run the database in a separate database docker container and connect to it from the control docker container via a TCP connection.



## 0.3 (2019-06-07) ##

State of the code at the end of the 06 June 2019 testing session, where capture of filterbanks on all nodes was achieved.

* Working filterbank mode for the pipeline, including generation of correct `mkrecv` header files and bash script allocating DADA buffers with the right size and launching of the transpose module.
* Output filterbanks now have correct identifiers (coordinates, source name, beam index). Passing of MJD_START not implemented yet.
* Allocate a separate node for incoherent beam processing. Forbid processing a mix of coherent and incoherent beams on a node for now.
* Added subscription to incoherent beam position sensor, which is treated in a special way (no `ifbf00000` sensor on FBFUSE at the moment)
* Working configuration authority. By default, it currently places a coherent beam on every priority source in the incoherent beam FWHM, then tiles the remaining available beams in a single tiling with overlap factor of 0.5. Priority sources currently include every pulsar in PSRCAT.
* Added interactive command line katcp client `katcp_cli`, copied over from `mpikat` repository
* Pipeline container(s) now started/stopped using python docker API ``dockerpy`` rather than launching docker commands via ``subprocess``
* Fixed beam assignment plan which did not work correctly when FBFUSE multicast groups contain 12+ beams
* Added `?reconfigure` request to MasterController
* MasterController now displays in the logs which node replied what to every request


## 0.2 (2019-04-30) ##

This version of the code reflects the state that it was in shortly after the March/April 2019 FBFUSE and TUSE busy week.

* A MariaDB database was added to the control docker image.
* Major overhaul of the node controller to be more modular and to revert state changes on errors. The node controllers subscribe directly to CAM sensors now.
* Implemented a dummy master controller for initial tests with the production system.
* Added code to monitor CAM sensors via the `KATPortalClient` library.
* Added mechanism to measure CAM sensor update delays for testing purposes.
* Added functionality to easily deploy the code across the TUSE cluster. Further deployment scripts were added later.
* The logging functionality was unified throughout the code to provide a single hook for logging to a database. Database logging is still disabled at the moment. The `meertrapdb` module was added.
* The code was packaged properly as a python package.
* Implemented initial configuration file generators for `mkrecv` and the `Cheetah` pipeline. Significant refinement has happened during data capture tests with `mkrecv`.
* A working interface between the node controller and pipeline (receive docker) was created. There are currently three different operation modes implemented.
* A translation layer between FBFUSE sensors and pipeline keywords was started, but it is not used yet.
* General code improvement and cleanup.

## 0.1 (2018-09-23) ##

* Fully functional pipeline simulator implemented. It includes multiple modes to simulate failures of the data analysis pipeline.
* Added various tools to workflow to ensure code correctness, formatting and to estimate regression test coverage (``nose2``, ``pycodestyle``, ``pylint``).
* Added initial documentation and API HTML reference generated using ``sphinx``.
* Moved production code, development, regression tests and documentation generation into docker container.
* Fully functional node controller implemented, including extensive regression tests. Various hardware and state parameters are provided as ``KATCP`` sensors.
* Master controller partially implemented. It fulfils the requirements defined in the TUSE-CAM ICD. Interaction with CAM and FBFUSE has been tested in the CAM test environment.
* Added initial functionality to interface with node controllers from the master controller, e.g. to pass requests and to subscribe to sensors.
* Initial implementation of a portal monitor that is used to subscribe to CAM sensors using ``KATPortalClient``.
* Configuration file generators for ``mkrecv`` and ``Cheetah`` implemented, including simple sanity checks.
* Cluster monitoring tool implemented that displays various sensor values from the cluster nodes on the console.
