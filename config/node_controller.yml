### Control container and NodeController parameters
control:
  # Stop and disable the pipeline when free disk space gets below 
  # this threshold in GB
  min_space: 3.0


### Pipeline parameters
pipeline:
  dry-run: False

  # Processing mode, valid choices:
  #   discard   : catch data stream, and consume it with dada_dbnull
  #               useful for ingest data rate tests
  #   filterbank: catch data stream and write out filterbank chunks
  #   production: true production mode with full pipeline running
  mode: production
  image: tuse.meertrap:7000/full-pipeline:0.2.0b3
  container: pipeline

  # Enable or disable core dumps
  coredumps: False

  # Filterbank chunk size in bytes when in 'filterbank' mode
  filterbank_size: 2147483648
  
  mkrecv:
    network_protocol: udp

  # Option to avoid running the bandpass monitor, because the C++ executable
  # can't keep up with the data in 4K + 76us mode
  bpmon:
    # Maximum node ingest data rate (bytes/s) beyond which bpmon is disabled
    # 12 beams in MeerTIME L-Band is ~40 MB/s
    maxrate: 50000000.0

  dada:
    # If True, remove existing DADA buffers on pipeline start using 'ipcrm -a'
    # If False, use dada_db to remove them
    ipcrm: True

    # Minimum number of seconds of data a DADA buffer should contain
    timespan: 3.0

    # Number of buffers in both input and output ring
    nbuffers: 20
  
  # Parameters of the extra scrunching performed by the transpose module
  scrunching:
    tsamp_min: 300.0e-6
    nchans_max: 1024
    
  # Configuration parameters for Cheetah XML generation
  cheetah:
    # Length of the aggregate buffer in seconds
    aggbuffer_timespan: 36.0

    # Comma separated string of dedispersion stages '<start>:<end>:<step>' 
    # One plan for every band, following the usual notation c{bandwidth_mhz}
    ddplan_str: 
      # UHF
      c544: "0:370:0.307, 370:740:0.652, 740:1480:1.266, 1480:2500:2.512"
      # L Band
      c856: "0:370:0.307, 370:740:0.652, 740:1480:1.266, 1480:2950:2.512, 2950:5000:4"
      # S Band
      c875: "0:370:0.307, 370:740:0.652, 740:1480:1.266, 1480:2950:2.512, 2950:5000:4"

    # All keywords below mirror the cheetah XML structure
    sps:
      # resource pool allocated to SPS specifically
      pool:
        # NOTE: cpus denotes a number of PHYSICAL cores
        cpus: 8
        gpus: 2
      
      # SPS S/N threshold
      threshold: 8.0

    spsift:
      active: True
      dm_thresh: 20.0
      sigma_thresh: 8.0
      pulse_width_threshold: 300.0
      # Threshold for maximum number of candidates. If the number of candidates 
      # exceeds the threshold then we resize the list to the threshold (0 = unlimited)
      maximum_candidates: 200000

    spscl:
      active: True
      time_tolerance: 100.0
      dm_thresh: 23.5
      pulse_width_tolerance: 100.0
      linking_length: 1.732

    iqrm:
      active: True
      radius: 100
      threshold: 3.0
      edge_channels: 32

    candidate_window_ms: 500.0
