#
# Katcp client that subscribes to a node's bandpass sensors and records
# the evolving values to numpy arrays
#
# Vincent Morello - 2019
#
import signal
import logging
import argparse

import numpy as np
import tornado
from tornado import gen

from katcp.resource_client import KATCPClientResource
from tusemcs import __version__


log = logging.getLogger('bpmon')


class StatListener(object):
    """
    Parameters
    ----------
    host: str
        Host name being monitored
    stat: str
        Name of the stat array being saved, currently either 
        'mean' or 'std'
    nbeams: int or None
        Number of beams processed by the node. None if not known yet.
    """
    def __init__(self, host, stat, nbeams=None):
        self.host = host
        self.stat = stat
        self.nbeams = nbeams

    def set_nbeams(self, n):
        """
        n: int or None
            New value for nbeams
        """
        log.debug("Set nbeams = {}".format(n))
        self.nbeams = n

    def __call__(self, sensor, reading):
        log.debug('Update received for stat = {}, time = {}'.format(self.stat, reading.timestamp))
        if not len(reading.value):
            log.debug('Value for stat {!r} is an empty string, skipping'.format(self.stat))
            return
        if not self.nbeams:
            log.debug('nbeams is {!r}, cannot infer shape of the bandpass data'.format(self.nbeams))
            return
        data = np.fromstring(reading.value, dtype=np.float16).reshape(self.nbeams, -1)
        fname = "{}_{}_{}.npy".format(self.host, reading.timestamp, self.stat)
        log.debug("Saving data to: {}".format(fname))
        np.save(fname, data)


class BeamIdsListener(object):
    """ """
    def __init__(self, host, stat_listeners):
        self.host = host
        self.stat_listeners = stat_listeners

    def __call__(self, sensor, reading):
        beam_ids_csv = reading.value
        if not ',' in beam_ids_csv:
            nbeams = None
        else:
            nbeams = len(beam_ids_csv.split(','))
        log.debug('Number of beams on host {!r} changed to {}'.format(self.host, nbeams))
        for sl in self.stat_listeners:
            sl.set_nbeams(nbeams)


class NodeMonitor(KATCPClientResource):
    """ """
    def __init__(self, host, port=5100):
        """ """
        resource_spec = dict(
            name=host,
            address=(host, port),
            controlled=False, # do not expose requests
            auto_reconnect=True,
            auto_reconnect_delay=5.0, # seconds
            )
        super(NodeMonitor, self).__init__(resource_spec)
        self.mean_listener = None
        self.std_listener = None
        self.beam_ids_listener = None

    @gen.coroutine
    def setup_sampling(self):
        """
        Subscribe to the bandpass sensors
        """
        self.mean_listener = StatListener(self.host, 'mean', nbeams=None)
        yield self.set_sensor_listener('bandpass-mean', self.mean_listener)
        yield self.set_sampling_strategy('bandpass-mean', 'event')

        self.std_listener = StatListener(self.host, 'std', nbeams=None)
        yield self.set_sensor_listener('bandpass-std', self.std_listener)
        yield self.set_sampling_strategy('bandpass-std', 'event')

        self.beam_ids_listener = BeamIdsListener(
            self.host, [self.mean_listener, self.std_listener])
        yield self.set_sensor_listener('beam-ids', self.beam_ids_listener)
        yield self.set_sampling_strategy('beam-ids', 'event')


    def __del__(self):
        self.stop()


def parse_args():
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        description="Start a bandpass monitor client, that connects to a node and saves "
        "every new bandpass mean and stddev to .npy files. The files are written to the current "
        "working directory."
        )

    parser.add_argument(
        '-H', '--host',
        help='Host address to monitor',
        default='tpn-0-0'
        )

    parser.add_argument(
        '-p', '--port',
        type=int,
        help="Host port",
        default=5100
        )

    return parser.parse_args()


def main():
    logging.basicConfig(
        level=logging.DEBUG,
        format="%(asctime)s %(filename)s:%(lineno)-4s %(levelname)-8s %(message)s"
        )
    logging.getLogger('katcp').setLevel('WARNING')

    @gen.coroutine
    def on_shutdown(ioloop):
        log.info("Shutting down")
        ioloop.stop()

    args = parse_args()

    log.info("Starting bandpass history monitor, tusemcs version: {}".format(__version__))
    ioloop = tornado.ioloop.IOLoop.current()

    # Hook up to SIGINT so that ctrl-C results in a clean shutdown
    signal.signal(
        signal.SIGINT,
        lambda sig, frame: ioloop.add_callback_from_signal(on_shutdown, ioloop)
        )

    signal.signal(
        signal.SIGTERM,
        lambda sig, frame: ioloop.add_callback_from_signal(on_shutdown, ioloop)
        )
    
    @gen.coroutine
    def start_and_display():
        monitor = NodeMonitor(args.host, port=args.port)
        yield monitor.start()
        yield monitor.setup_sampling()
        log.info("Monitor created and set up, Ctrl-C to terminate app.")

    ioloop.add_callback(start_and_display)
    ioloop.start()


if __name__ == '__main__':
    main()
