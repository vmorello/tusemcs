import signal
import logging
import argparse

import yaml
import tornado
from tornado import gen

from katcp.resource_client import KATCPClientResource
from katcp import CallbackClient, Message


log = logging.getLogger('tusemcs.reqsender')


def parse_args():
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        description="Send requests in bulk to multiple katcp servers"
        )
    parser.add_argument(
        'config',
        help='YAML configuration file, see reqsender_conf_example.yaml'
    )
    return parser.parse_args()


@gen.coroutine
def shutdown():
    log.info("Shutting down")
    ioloop = tornado.ioloop.IOLoop.current()
    ioloop.stop()


@gen.coroutine
def reply_callback(arg):
    log.debug("Reply: {}".format(arg))


@gen.coroutine
def inform_callback(arg):
    log.debug("Inform: {}".format(arg))


@gen.coroutine
def sendreq(client, req):
    log.debug("Sending request: {}".format(req))

    elements = req.split()
    name = elements[0]
    name = name.replace('?', '')
    name = name.replace('-', '_')
    args = elements[1:]

    func = client.req[name]
    output = yield func(*args)
    for inf in output.informs:
        log.debug(str(inf).replace(r'\_', ' '))
    log.debug(str(output.reply).replace(r'\_', ' '))


@gen.coroutine
def send_requests(host, port, requests):
    resource_spec = dict(
        name='sender',
        address=(host, port),
        controlled=True, # Expose requests
        auto_reconnect=True,
        auto_reconnect_delay=5.0, # seconds
    )
    client = KATCPClientResource(resource_spec)
    yield client.start()
    yield client.until_synced(timeout=5.0)

    log.debug("Client connected to {}:{}".format(host, port))
    for req in requests:
        yield sendreq(client, req)
    
    yield client.stop()
    log.debug("Client disconnected")


@gen.coroutine
def runner(config):
    for element in config:
        yield send_requests(element['host'], element['port'], element['requests'])
    yield shutdown()


def main():
    logging.basicConfig(
        level=logging.DEBUG,
        format="%(asctime)s %(filename)s:%(lineno)-4s %(levelname)-8s %(message)s"
        )
    logging.getLogger('katcp').setLevel('WARNING')

    args = parse_args()
    ioloop = tornado.ioloop.IOLoop.current()

    # Hook up to SIGINT so that ctrl-C results in a clean shutdown
    shutdown_func = lambda sig, frame: ioloop.add_callback_from_signal(shutdown)
    signal.signal(signal.SIGINT, shutdown_func)
    signal.signal(signal.SIGTERM, shutdown_func)

    with open(args.config, 'r') as f:
        config = yaml.safe_load(f)

    ioloop.add_callback(runner, config)
    ioloop.start()


if __name__ == '__main__':
    main()
