#
# Convenient script to start / stop / restart nodes
# Vincent Morello - 2020
#
import logging
import subprocess
import shlex
import argparse

import coloredlogs

from tusemcs.pipeline_utils import HOST_MAIN_OUTDIR, CONTROL_MAIN_OUTDIR

log = logging.getLogger('tusemcs.node_manager')


def parse_node_range_string(s):
    start, stop = list(map(int, s.split('-')))
    assert start >= 0
    assert stop <= 65
    assert stop >= start
    return range(start, stop+1) # NOTE: stop is inclusive


def node_name(index):
    return 'tpn-0-{:d}'.format(index)


def docker_command_start(image):
    python_cmd = 'python tusemcs/node_controller.py'

    args = [
        'docker run',

        # Mount docker socket so the control container has the power to launch
        # a sibling pipeline container
        '--mount type=bind,source=/var/run/docker.sock,target=/var/run/docker.sock',

        # Global output directory
        '--mount type=bind,source={},target={}'.format(HOST_MAIN_OUTDIR, CONTROL_MAIN_OUTDIR),

        # Allocate a pseudo-TTY so that stdout flushes every time a program
        # prints a newline char. An enlightening read on the subject:
        # https://eklitzke.org/stdout-buffering
        '-t',
        '-d',
        '--network=host',

        # So that we can periodically fetch the POSIX shared buffer where the bandpass
        # monitor C++ executable stores the bandpass statistics
        '--ipc=host',

        # So we can monitor GPU usage from inside the control container
        '--runtime=nvidia',
        '--name control',
        image,
        python_cmd
    ]
    return ' '.join(args)


def docker_command_stop():
    return 'docker stop -t 1 control'


def docker_command_remove_container():
    return 'docker rm control'


def rocks_run_host(nodes, cmd, dry_run=False):
    rocks_cmd = "rocks run host {} collate=true command='{}'".format(nodes, cmd)
    log.debug("Command:\n{}".format(cmd))
    log.debug("Full rocks command:\n{}".format(rocks_cmd))

    if not dry_run:
        output = subprocess.check_output(shlex.split(rocks_cmd))
        log.info("Rocks command returned:\n{}".format(output))


def stop_nodes(indices, dry_run=False):
    if not dry_run:
        log.warning("STOPPING NODES")
    else:
        log.info("PRETENDING to stop nodes")
    log.info("Node indices: {}".format(list(indices)))

    names = ' '.join(map(node_name, indices))
    rocks_run_host(names, docker_command_stop(), dry_run=dry_run)
    rocks_run_host(names, docker_command_remove_container(), dry_run=dry_run)


def start_nodes(indices, image, dry_run=False):
    if not dry_run:
        log.warning("STARTING NODES")
    else:
        log.info("PRETENDING to start nodes")
    log.info("Using image {!r}".format(image))
    log.info("Node indices: {}".format(list(indices)))

    docker_cmd = docker_command_start(image)
    names = ' '.join(map(node_name, indices))
    rocks_run_host(names, docker_cmd, dry_run=dry_run)


def restart_nodes(indices, image, dry_run=False):
    stop_nodes(indices, dry_run=dry_run)
    start_nodes(indices, image, dry_run=dry_run)


def parse_args():
    parser = argparse.ArgumentParser(
        description="Remotely Start/Stop/Restart a range of NodeControllers by sending an "
            "appropriately generated 'rocks run host' command",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument(
        '-a', '--action',
        help="Action to perform. 'restart' is a stop followed by a start",
        choices=('start', 'stop', 'restart'),
        type=str,
        required=True)
    parser.add_argument(
        '-i', '--image',
        help='Docker image tag to use',
        type=str)
    parser.add_argument(
        '-r', '--range',
        help="Target node range as a string start-stop, e.g. 0-65 means indices 0 to 65 INCLUSIVE.",
        default='0-65',
        type=parse_node_range_string)
    parser.add_argument(
        '--dry-run',
        help="If specified, only display the rocks command that would otherwise be executed.",
        action='store_true'
        )
    return parser.parse_args()


def main():
    logfmt = "[%(levelname)s - %(asctime)s - %(name)s - %(filename)s:%(lineno)s] %(message)s"
    coloredlogs.install(fmt=logfmt, level='DEBUG', milliseconds=True)
    args = parse_args()

    if args.action in ('start', 'restart') and not args.image:
        raise ValueError("Must specify a docker image to use when starting / restarting")

    if args.action == 'restart':
        restart_nodes(args.range, args.image, dry_run=args.dry_run)
    elif args.action == 'start':
        start_nodes(args.range, args.image, dry_run=args.dry_run)
    elif args.action == 'stop':
        stop_nodes(args.range, dry_run=args.dry_run)
    else:
        raise ValueError("Invalid action name")


if __name__ == '__main__':
    main()